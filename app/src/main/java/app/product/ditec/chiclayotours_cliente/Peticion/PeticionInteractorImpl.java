package app.product.ditec.chiclayotours_cliente.Peticion;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Model.Oferta;
import app.product.ditec.chiclayotours_cliente.Model.User;

public class PeticionInteractorImpl implements PeticionInteractor {
    private Socket socket;
    private OnPeticionListener listener;
    public PeticionInteractorImpl(OnPeticionListener _listener){
        listener = _listener;
        // Se obtiene el Socket de la App
        socket = App.getSocket();
        // Se activa los escuchadores
        // para el evento de reconectar
        // conectar, reconectando,
        // reconectar, desconectar y cuando existe error
        socket.on(Socket.EVENT_RECONNECT,connect);
        socket.on(Socket.EVENT_CONNECT, connect);
        socket.on(Socket.EVENT_RECONNECTING,reconnecting);
        socket.on(Socket.EVENT_RECONNECT,connect);
        socket.on(Socket.EVENT_DISCONNECT,disconnect);
        socket.on(Socket.EVENT_ERROR,event_error);
        // Se activa el mensaje "arrive","error" y "taxi location"
/*        socket.on("arrive",arrive);
        socket.on("error", error);
        socket.on("taxi location",taxi_location);
        socket.on("ontravel", ontravel);
        socket.on("getonboard", getonboard);*/
        socket.on("propuestataxista",propuestataxista);
        socket.on("retirarofertataxista",retirarofertataxista);
        // Se conecta el socket
        socket.connect();
    }
    // Escuchador cuando se "Conecta"
    private Emitter.Listener connect = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            // Metodo que envia el signin
            // para pasar el socket_id
            signin();
        }
    };
    // Metodo que envia el "signIn"
    @Override
    public void signin() {
        // Se crea una tarea en el hilo principal
        new Handler(Looper.getMainLooper()).post(
                new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(App.getInstance(),"Conectando..",Toast.LENGTH_SHORT).show();
                        new Handler(Looper.getMainLooper()).post(
                                new Runnable() {
                                    @Override
                                    public void run() {

//                                        Toast.makeText(App.getInstance(),"Esta conectado..",Toast.LENGTH_LONG).show();
                                    }
                                }
                        );
                        // Se verifica que el socket esta conectado
                        if(socket.connected()) {

                            // Se emite un mensaje "signin" y se envia
                            // como parametros el email del pasajero

                            socket.emit("signin", User.loadEmail(), new Ack() {
                                @Override
                                public void call(Object... args) {
                                    // Se obtiene la respuesta

                                }
                            });
                        }
                    }
                }
        );

    }
    // Escuchador cuando "Reconectando"
    private Emitter.Listener reconnecting = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            // Se ejecuta el metodo del presentador para indicar que se reconectando
                        }
                    }
            );
        }
    };
    private Emitter.Listener disconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            // Se ejecuta el metodo del presentador para indicar
                            // que esta desconectado

                        }
                    }
            );

        }
    };

    // Escuchador cuando se "Evento error"
    private Emitter.Listener event_error = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(((MapPresenter) listener).getView().getContext(), "Hubo un error al conectar..", Toast.LENGTH_LONG).show();
                        }
                    }
            );
        }
    };

    public void aumentar(final JSONObject jsonParams){
        socket.emit("aumentar", jsonParams,  new Ack() {
            @Override
            public void call(Object[] args) {

            }
        });
    }

    public void cancelar(final JSONObject jsonParams){
        socket.emit("precancel_oferta", jsonParams,  new Ack() {
            @Override
            public void call(Object[] args) {

            }
        });
    }

    public void acceptoferta(final JSONObject jsonParams){
        socket.emit("acceptoferta", jsonParams,  new Ack() {
            @Override
            public void call(Object[] args) {

            }
        });
    }

    public void rechazaroferta(final JSONObject jsonParams){
        socket.emit("rechazaroferta", jsonParams,  new Ack() {
            @Override
            public void call(Object[] args) {

            }
        });
    }

    private Emitter.Listener propuestataxista = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

        try{
            JSONObject paramsRequest = (JSONObject) args[0];
            Oferta oferta = new Oferta();
            oferta.setDistance(paramsRequest.getString("distancia"));
            oferta.setPrecio(paramsRequest.getString("precio"));
            oferta.setTiempo(paramsRequest.getString("tiempo"));
            oferta.setEmaildriver(paramsRequest.getString("email_driver"));
            oferta.setType(paramsRequest.getString("car_model"));
            oferta.setName(paramsRequest.getString("name"));
            oferta.setSocketidtaxi(paramsRequest.getString("socketidtaxi"));

            oferta.setPhone(paramsRequest.getString("phone"));
            oferta.setAutoplate(paramsRequest.getString("autoplate"));
            oferta.setDNI(paramsRequest.getString("dni"));
            oferta.setCar_brand(paramsRequest.getString("car_model"));
            oferta.setImageUrl(paramsRequest.getString("image"));
            listener.updatelist(oferta);


        } catch (JSONException e) {
            Log.e("JSONExceptionRequest", e.toString());
        }

        }
    };

    private Emitter.Listener retirarofertataxista = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            try{
                JSONObject paramsRequest = (JSONObject) args[0];

                listener.retirarofertataxista(paramsRequest.getString("email_driver"));


            } catch (JSONException e) {
                Log.e("JSONExceptionRequest", e.toString());
            }

        }
    };
}
