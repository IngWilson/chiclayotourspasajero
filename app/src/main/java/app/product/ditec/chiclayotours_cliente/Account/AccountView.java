package app.product.ditec.chiclayotours_cliente.Account;

import android.content.Context;
import android.widget.ImageView;

/**
 * Created by angel on 05/05/15.
 */
public interface AccountView {

    void setFirstName(String firstName);

    void setLastName(String lastName);

    void setPhoneNumber(String phoneNumber);

    void setEmail(String email);

    Context getContext();

    void showProgress();

    void hideProgress();

    void showDialogInternetError();

    void setEnabled(String enabled);

    ImageView getImageIsable();


}
