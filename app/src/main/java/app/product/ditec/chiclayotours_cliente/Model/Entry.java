package app.product.ditec.chiclayotours_cliente.Model;

import com.activeandroid.Model;

/**
 * Created by Julio on 01/08/2015.
 */

public class Entry extends Model {

    public String name;
    public String autoplate;
    public String address;
    public String phone;
    public String company;
    public String date;


    public String getName() {
        return name;
    }

    public String getAutoplate() {
        return autoplate;
    }
    public String getPhone(){
        return  phone;
    }
    public String getAddress(){
        return address;
    }
    public String getCompany(){
        return company;
    }
    public String getDate(){
        return date;
    }
}