package app.product.ditec.chiclayotours_cliente.SPINNER;


import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Spinner;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.http.util.EntityUtils;
import java.util.ArrayList;
import android.os.AsyncTask;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpVersion;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.CoreProtocolPNames;
import android.widget.ArrayAdapter;
import java.util.Arrays;
import android.widget.AdapterView;
import android.view.View;

import java.util.List;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.R;

public class SPINNER extends AppCompatActivity {
    JSONObject jsonobject;
    JSONArray jsonarray;

    JSONObject jsonObjectcity;
    JSONArray jsonarraycity;
    //ProgressDialog mProgressDialog;

    ArrayList<String> worldlist;
    ArrayList<WorldPopulation> world;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        //inicializamos  la funcion  en un nuevo hilo
        new DownloadJSON().execute();
    }

    //funcion que teoricamente baja archivos del json
    private class DownloadJSON extends AsyncTask<Void, Void, Void>
    {
        //"doInBackground" uno de los tres pasos  para hacer el uso de hilos  con AsyncTask
        @Override
        protected Void doInBackground(Void... params) {
            //inicializamos los objetos con los objetos  "WorldPopulation"
            world = new ArrayList<WorldPopulation>();
            worldlist = new ArrayList<String>();

            try
            {
                //declaramos una variable HttpClient
                // en esta  fragmento trasformamos los datos del json en objetos json
                HttpClient httpclient = new DefaultHttpClient();
                httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
                //la ruta del json
                HttpGet request = new HttpGet(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_world));
                HttpResponse response = httpclient.execute(request);
                HttpEntity resEntity = response.getEntity();

                String _response= EntityUtils.toString(resEntity);
                jsonobject = new JSONObject(_response);
                jsonarray = jsonobject.getJSONArray("Mundo");
                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonobject = jsonarray.getJSONObject(i);

                    WorldPopulation worldpop = new WorldPopulation();
                    worldpop.setciudad(jsonobject.optString("Ciudad"));
                    world.add(worldpop);

                    worldlist.add(jsonobject.optString("Pais"));
                }
            }catch(Exception e)
            {

            }
            return null;
       }
        protected void onPostExecute(Void args) {
            // Locate the spinner in activity_main.xml

            Spinner mySpinner = (Spinner) findViewById(R.id.my_spinner);
         // Spinner adapter
            mySpinner
                    .setAdapter(new ArrayAdapter<String>(SPINNER.this,
                            android.R.layout.simple_spinner_dropdown_item,
                            worldlist));

            // Spinner on item click listener
            mySpinner
                    .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            String cityfull = world.get(position).getciudad();
                            List<String> city = new ArrayList<String>(Arrays.asList(cityfull.split(",")));
                            //
                            final Spinner ciuda =(Spinner) findViewById(R.id.ciuda);
                            // Spinner adapter
                            ciuda
                                    .setAdapter(new ArrayAdapter<String>(SPINNER.this,
                                            android.R.layout.simple_spinner_dropdown_item,
                                            city));
               }
                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub
                        }
                    });
        }
    }
}
