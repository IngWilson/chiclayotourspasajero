package app.product.ditec.chiclayotours_cliente.Map;

import android.location.Location;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.GCM;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 20/04/15.
 */

// Clase interactor que define los metodos para comunicarse
// con el server
public interface MapInteractor {

    void sendRequest(final JSONObject jsonParams, Validation validator, OnMapFinishedListener listener, int type, final GCM gcm);

    void sendRequestOferta(final JSONObject jsonParams, Validation validator, OnMapFinishedListener listener);

    void getUserData(final String email, final Validation validator, final OnMapFinishedListener listener);

    void sendVersion(final JSONObject jsonParams, Validation validator, final OnMapFinishedListener listener);

    void checkRequest(final JSONObject jsonParams, final Validation validator, final OnMapFinishedListener listener);

    void getAddress(Location location,Validation validator);

    void desactiveSocketInstance();

    void signin();

    void checkRequestState(final JSONObject params, final Validation validator, final OnMapFinishedListener listener);

    void getDataPromotion(String email, Validation validator);

    void validateRecepcionMensaje(JSONObject params, final Validation validator);

    void sendShareLocation(JSONObject params, final Validation validator);

    void recibirShareLocation(JSONObject params, final Validation validator);
}
