package app.product.ditec.chiclayotours_cliente.Account;

import app.product.ditec.chiclayotours_cliente.Model.User;

/**
 * Created by angel on 09/05/15.
 */
public interface OnAccountFinishedListener {

    void onError();

    void onInternetError();

    void onSuccessGetUserData(User user);
}
