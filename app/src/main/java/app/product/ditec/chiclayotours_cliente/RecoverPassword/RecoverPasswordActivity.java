package app.product.ditec.chiclayotours_cliente.RecoverPassword;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import app.product.ditec.chiclayotours_cliente.R;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

//import com.actionbarsherlock.app.ActionBar;
//import com.actionbarsherlock.app.SherlockActivity;

/**
 * Created by lenovo on 19/06/2015.
 */
public class RecoverPasswordActivity extends AppCompatActivity implements RecoverPasswordView, Dialog.OnClickListener {

    EditText edtPassword;
    Button btnSendPassword;
    RecoverPasswordPresenter presenter;

    private AlertDialog.Builder dialogConfirmPassword;
    private AlertDialog.Builder alertDialog;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recoverpassword);

        //Se coloca una barra personalizada con el logo de akitaxi
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_bar);

        presenter = new RecoverPasswordPresenterImpl(this);

        edtPassword=findViewById(R.id.edtNewPassword);
        btnSendPassword=findViewById(R.id.btnSendPassword);

        dialogConfirmPassword = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        alertDialog = new AlertDialog.Builder(this);

    }

    @Override
    public Context getContext() {
        return RecoverPasswordActivity.this;
    }

    //Metodo que muestra un dialogo indicando que se envio el pedido de recuperacion correctamente
    @Override
    public void showDialogSuccessSend() {
        Toast.makeText(this, getString(R.string.Dialog_Success_Send), Toast.LENGTH_LONG).show();
    }

    //Metodo que muestra un dialogo indicando que no se pudo enviar correctamente el pedido de recuperacin
    @Override
    public void showDialogFailedSend() {
        Toast.makeText(this, "Error al enviar el email", Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToCode(String email) {
    }

    //Metodo que muestra un dialogo preguntando si el email ingresado es correcto

    public void showDialogConfirmEmail() {
        dialogConfirmPassword.setCancelable(false)
                .setTitle("Advertencia")
                .setMessage(getString(R.string.recover_pass_email_correct) +" "+ edtPassword.getText().toString() + " ?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.validateEmail(edtPassword.getText().toString());

                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void hideProgress() {

        progressDialog.hide();

    }

    //Metodo que muestra un dialogo de progreso
    @Override
    public void showProgress() {
        progressDialog.setIndeterminate(false);
        //se esta enviando y verificando el Email
        progressDialog.setMessage(getString(R.string.recover_pass_send_email));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    //Metodo que muestra un dialogo indicando que no hay conexion
    @Override
    public void showDialogInternetError() {
        alertDialog.setTitle(getString(R.string.dialog_conection_title))
                .setMessage(getString(R.string.dialog_conection_message))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }

    //Metodo que muestra un dialogo indicando un mensaje que indica que el correo no esta registrado
    @Override
    public void showDialogEmailError() {
        Toast.makeText(this, getString(R.string.Dialog_Email_Error), Toast.LENGTH_LONG).show();
    }

    //Metodo que muestra un dialogo que indica que hay un error al envia el email
    @Override
    public void showDialogError() {
        Toast.makeText(this, getString(R.string.Dialog_Error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void finished() {
        finish();
    }

    //Metodo que muestra un dialogo que indica que no hay un email en el campo
    @Override
    public void showDialogEmptyemail() {
        Toast.makeText(this, getString(R.string.recover_pass_email), Toast.LENGTH_LONG).show();
    }

    //Metodo para el evento onClick en el dialogo
    @Override
    public void onClick(DialogInterface dialog, int which) {
        startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
    }

    @Override
    public void showDialogErrorSend() {
//        Toast.makeText(this,"Er")
    }
}
