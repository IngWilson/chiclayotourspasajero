package app.product.ditec.chiclayotours_cliente.RecoverPassword;

/**
 * Created by lenovo on 19/06/2015.
 */
public interface OnRecoverPasswordFinishedListener {
    void onSuccessSend();

    void onFailedSend();

    void onInternetError();

    void onEmailError();

    void onError();

    void onErrorSend();

    void onEmptyEmailError();
}
