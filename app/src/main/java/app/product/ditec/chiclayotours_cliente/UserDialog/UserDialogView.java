package app.product.ditec.chiclayotours_cliente.UserDialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.ProgressBar;

/**
 * Created by angel on 15/04/15.
 */
public interface UserDialogView {

    void setUserNameTaxiDriver(String name);

    void setAutoPlateTaxiDriver(String autoplate);

    void setPhoneTaxiDriver(String phone);

    void setEmailTaxiDriver(String email);

    ImageView getImageTaxiDriver();

    ProgressBar getProgressBarImage();

    void setImageTaxiDriver(Bitmap bitmap);

    Context getContext();

}
