package app.product.ditec.chiclayotours_cliente;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import app.product.ditec.chiclayotours_cliente.Model.User;

/**
 * Created by Wilson on 16/04/15.
 */
public class GCM {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private GoogleCloudMessaging gcm;
    private String regId;
    private String msg;
    private Context ctx;
    private User user;

    public GCM(Context ctx) {
        this.ctx = ctx;
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

   /* public String registerGCM(Void... params) {

        gcm = GoogleCloudMessaging.getInstance(ctx);
        regId = getRegistrationId();

        if (regId.isEmpty()) {
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(ctx);
                }
                regId = gcm.register(Config.GOOGLE_PROJECT_ID);
                Log.d("RegisterActivity", "registerInBackground - regId: "
                        + regId);
                msg = "Device registered, registration ID=" + regId;

                storeRegistrationId(regId);
            } catch (IOException ex) {
                msg = " Error :" + ex.getMessage();
                Log.d("RegisterActivity", "Error: " + msg);
            }
            Log.d("RegisterActivity", "AsyncTask completed: " + msg);
            return regId;
        }
        return regId;
    }*/


    public String getRegistrationId() {
        String registrationId = User.loadGCM();
        if (registrationId.isEmpty()) {
            Log.i(Config.TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(Config.APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion();
        if (registeredVersion != currentVersion) {
            Log.i(Config.TAG, "App version changed.");
            return "";
        }
        Log.i("RegistrationId", registrationId);
        return registrationId;
    }

    public int getAppVersion() {
        try {
            PackageInfo packageInfo = ctx.getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("RegisterActivity",
                    "I never expected this! Going down, going down!" + e);
            throw new RuntimeException(e);
        }
    }


    public void storeRegistrationId(String gcm_user) {
        int appVersion = getAppVersion();
        Log.i(Config.TAG, "Saving regId on app version " + appVersion);

        user.saveGCM(gcm_user);

        editor.putInt(Config.APP_VERSION, appVersion);
        editor.commit();
    }
}
