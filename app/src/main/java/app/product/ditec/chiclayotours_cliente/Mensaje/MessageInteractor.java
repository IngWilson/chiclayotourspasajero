package app.product.ditec.chiclayotours_cliente.Mensaje;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

public interface MessageInteractor {
    void sendArrive(JSONObject params, final Validation validator);
}
