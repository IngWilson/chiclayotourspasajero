package app.product.ditec.chiclayotours_cliente.ShareLocationDialog;

import android.content.Context;

public interface ShareLocationDialogView {
    Context getContext();
    void hideProgress();
    void showDialogFailedSend();
    void showProgress();
    void showDialogConnection();
    void envioexitoso();
    void SuccessFinalizarSend();
    void showDialogFailedNotFound();
}
