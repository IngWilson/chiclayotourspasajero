package app.product.ditec.chiclayotours_cliente.Code;

import android.content.Context;

/**
 * Created by lenovo on 13/05/2015.
 */
public interface CodePresenter {

    void validateCodeData(String code);

    void validateFirebase();

   // void generateGCM();

    Context getContext();
    void resendcode();

    void resendcodefirebase();
}
