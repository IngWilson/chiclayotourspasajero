package app.product.ditec.chiclayotours_cliente.Login;


public interface OnLoginFinishedListener {

    void onUsernameError();

    void onPasswordError();

    void onSuccess();

    void onLoginError();

    void onUserPassError();

    void onConnectionError();

    void onSuccessPhone(String email);

    void onRegisterPhone();

    void onPhoneError();

}
