package app.product.ditec.chiclayotours_cliente.PreRequest;

import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.github.nkzawa.socketio.client.Socket;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Map.MapPresenter;
import app.product.ditec.chiclayotours_cliente.Map.OnMapFinishedListener;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 08/06/15.
 */
public class PreRequestInteractorImpl implements PreRequestInteractor {


    String address="";
    //
    private JSONObject params = null;

    // Se declara objeto para comunicarse con el server
    private OkHttpRequest okHttpRequest;

    // Se declara objeto OnMapFinishedListener
    private OnMapFinishedListener listener;

    // Se declara el manejador para los hiloss
    private Handler handler = new Handler();

    // Se declaran banderas para saber si se envio
    // o se cancelo
    private boolean flag_send=false;
    private boolean flag_cancel=false;

    // Se declaran banderas para saber si se logeo
    boolean flag_signin=false;

    private Runnable runnable;

    // Se declara el socket
    private Socket socket;


    public void getAddress(Location location, Validation validator){

        // Se adjunta la longitude y latitude en la ruta
        String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                + location.getLatitude()+ "," + location.getLongitude();

        okHttpRequest = new OkHttpRequest(URL);

        Log.i("URL_MAPS", URL);

        // Se coloca en el layout que se esta buscando direccion
        ((MapPresenter)listener).setAddressDirection("Buscando direccion...");

        // Se valida que esta conectado a internet
        if(validator.isOnline()) {

            try {
                // Metodo que realiza una peticion GET
                okHttpRequest.getNotParams(new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                    }
                    @Override
                    public void onResponse(Response response) throws IOException {

                        String res = response.body().string();

                        Log.i("Response Body", res);
                        try {
                            // Se convierte el cuerpo del mensaje
                            // y se convierte en un objeto JSON
                            JSONObject result = new JSONObject(res);
                            // Se verifica que tiene el campo "results"
                            if (result.has("results")) {
                                try {
                                    // Se obtiene el JSONArray a partir
                                    // del campo "results"
                                    JSONArray array = result.getJSONArray("results");
                                    // Se verifica que el array no este vacio
                                    if (array.length() > 0) {
                                        // Se obtiene el primer valor del array
                                        JSONObject place = array.getJSONObject(0);
                                        // Se obtiene el JSONArray a partir del campo "address_components"
                                        JSONArray components = place.getJSONArray("address_components");
                                        // Se recorre el Array
                                        for (int i = 0; i < components.length(); i++) {
                                            // Se obtiene el JSON a partir del JSONArray
                                            JSONObject component = components.getJSONObject(i);
                                            // Se obtiene el campo del JSONArray "types" y se guarda en otro
                                            // JSONArray
                                            JSONArray types = component.getJSONArray("types");
                                            // Se coge solo el primer elemento del JSONArray
                                            String type = types.getString(0);
                                            //Se verifica que el valor del type sea igual a "route"
                                            if (type.equals("route")) {
                                                try {
                                                    // Se formatea el valor del address para UTF-8 para reconocer tildes
                                                    // y se guarda en un String
                                                    address = new String(component.getString("short_name").getBytes(), "UTF-8");
                                                } catch (UnsupportedEncodingException e) {
                                                    e.toString();
                                                }
                                            }
                                        }

                                        // Se verifica que el campo no sea vacio
                                        if (!address.isEmpty()) {

                                            // Se crea una tarea y se coloca en el hilo principal
                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    // Se pasa el valor obtenido y se pasa
                                                    // al presentador
                                                    ((MapPresenter) listener).setAddressDirection(address);
                                                }
                                            });

                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (JSONException e) {
                            Log.e("JSONArrayException", e.toString());
                        }

                    }
                });
            } catch (IOException e){}
        }else
            ((MapPresenter) listener).setAddressDirection("Hay problemas con tu conexion");
    }


    @Override
    public void calculatePrice(final JSONObject params, Validation validator, final OnPreRequestListener listener) {
        String URL = App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_COSTXDISTANCE);
        okHttpRequest = new OkHttpRequest(URL);

        Log.i("URL_MAPS", URL);

        // Se coloca en el layout que se esta buscando direccion
//        ((MapPresenter)listener).setAddressDirection("Buscando direccion...");

        // Se valida que esta conectado a internet
        if(validator.isOnline()) {

            try {
                // Metodo que realiza una peticion GET
                okHttpRequest.post(params.toString(),new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                    }
                    @Override
                    public void onResponse(Response response) throws IOException {

                        String res = response.body().string();

                        Log.i("Response Body", res);
                        try {
                            // Se convierte el cuerpo del mensaje
                            // y se convierte en un objeto JSON
                            final JSONArray result = new JSONArray(res);
                            final JSONObject r = result.getJSONObject(0);
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    try{

                                        listener.setPrice(String.valueOf(4+r.getDouble("cost")*(int)Math.floor(params.getDouble("distanceR"))));
                                    }catch (JSONException e){}
                                }
                            });


                        } catch (JSONException e) {
                            Log.e("JSONArrayException", e.toString());
                        }

                    }
                });
            } catch (IOException e){}
        }else
            ((MapPresenter) listener).setAddressDirection("Hay problemas con tu conexion");
    }
}
