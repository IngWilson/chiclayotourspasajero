package app.product.ditec.chiclayotours_cliente.Map;

import android.content.Context;
import android.location.Location;

import org.json.JSONObject;


// Interface que define los metodos
// que se usaran en el presentador
public interface MapPresenter {

   // void validateSendRequest(String note,String address,String number);
    void validateSendRequest(String note,String address,String number,String to,String price, String exactprice,String tipomovil, String grupo);

    void validatesendOferta(String note, String address,String number,String to,String price, String exactprice,String tipomovil);

    void verifyVersion();

    void onAddMyLocationMarker();

    void onDeleteMarker();

    void onDisconnect();

    void onConnect();

    void validateGPS();

    void validateINTERNET();

    void validateCancelRequest();

    void resetParams();

    MapView getView();

    void validateGetUserData();

    boolean checkData();

    void setMyCurrentLocation(Double latitude, Double longitude);

    Location getMyCurrentLocation();

    boolean checkIsAble();

    boolean checkOnCall();

    boolean checkOnRequest();

    boolean checkOnVerified();

    boolean checkOnCancel();

    void setOnRequest(Boolean active);

    void returnUserDialog();

    void setOnCall(boolean active);

    void resetService();

    void restartMyMarker();

    boolean checkOnUserDialog();

    boolean checkOnRadar();

    boolean checkMyCancel();

    void setGPS(boolean active);

    void setAddressNro(String nro);

    boolean getGPS();

    void callTaxiDriver();
    //fiorella
    void callcentral();

    void returnDataTaxiDriver();

    void updateSlideTaxiDriver();

    void onRadar(boolean active);

    void checkUpdateRequest();

    void removeCheckUpdateRequest();

    String getAliasCode();

    Context getActivityContext();

    void setDataReceived(boolean active);

    void updateLocationTaxi(double latitude,double longitude, double bearing);

    void showStateConnected();

    void showStateReconnecting();

    void showStateDisconnected();

    void setAddressDirection(String address);

    void getAddressDirection();

    void desactiveSocket();

    void sendSignin();

    void setTravelingState(boolean state);

    void validateFinishedRequest();

    void verifyRequest();

    boolean getTravelingState();

    void stopVerifyRequestState();
 void validateGetData();
 void checkaddress();
 void prepareaceptaroferta();

 void validateRecepcionMensaje(String message);

 void onErrorConnection();

 void onSuccessSend();

 void onFailedSend();

 void onErrorConnection2();

 void sendShareLocation(Double latitude, Double longitude);

 void recibirShareLocation(String sharenumber);

 void updateShareLocation(JSONObject jsonParams);

 void retirarenviarubicacion(JSONObject jsonParams);
}
