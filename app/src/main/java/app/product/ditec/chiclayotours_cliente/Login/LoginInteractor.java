package app.product.ditec.chiclayotours_cliente.Login;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;


public interface LoginInteractor {

    void login(JSONObject jsonParams, OnLoginFinishedListener onLoginFinishedListener, Validation validator);

    void loginPhone(JSONObject jsonParams, OnLoginFinishedListener onLoginFinishedListener, Validation validator);
}
