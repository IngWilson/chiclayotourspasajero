package app.product.ditec.chiclayotours_cliente.Code;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by lenovo on 16/05/2015.
 */

public class CodeInteractorImpl implements CodeInteractor {

    OkHttpRequest okHttpRequest;

    // Se envia el codigo de verificacion
    @Override
    public void sendCode(JSONObject params, Validation validator, final OnCodeDialogFinishedListener listener) {

        //Se valida que esta conectado a internet
        if (validator.isOnline()) {

            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_VERIFY));

            Boolean error = false;

            try {
                //Se verifica que los parametros del JSON no esten vacios
                if (TextUtils.isEmpty(params.getString("code"))) {
                    //Se muestra un mensaje de error en el edittext
                    listener.onCodeError();
                    error = true;
                }
            } catch (JSONException e) {
                Log.e("JSONException", e.toString());
            }

            //En caso de que no halla error
            if (!error) {

                //listener.onSuccessSend();

                try {
                    okHttpRequest.post(params.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    //Metodo que se ejecuta 
                                    // cuando hay un error al enviar
                                    listener.onErrorSend();
                                }
                            });
                        }

                        @Override
                        public void onResponse(final Response response) throws IOException {

                            Log.i("SC_Code:", "" + response.code());

                            //Se obtiene el hilo principal y se ejecuta una tarea
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {

                                    //Se verifica el codigo de error
                                    if (response.code() == 200)
                                        listener.onSuccessSend();
                                    else if (response.code() == 503)
                                        listener.onFailSend();
                                    else
                                        listener.onFailSend();
                                }
                            });
                        }
                    });
                } catch (IOException e) {
                    Log.i("SC_IOException", e.toString());
                }
            }
        } else
            listener.onInternetError();
    }

    @Override
    public void resendCode(JSONObject params, Validation validator, final OnCodeDialogFinishedListener listener) {

        //Se valida que esta conectado a internet
        if (validator.isOnline()) {

            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_ReSendCode));

            Boolean error = false;

                try {
                    okHttpRequest.post(params.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    //Metodo que se ejecuta
                                    // cuando hay un error al enviar
                                    listener.onErrorSend();
                                }
                            });
                        }

                        @Override
                        public void onResponse(final Response response) throws IOException {
                            //Se obtiene el hilo principal y se ejecuta una tarea
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {

                                    //Se verifica el codigo de error
                                    if (response.code() == 200)
                                        listener.onSuccessreSend();
                                    else if (response.code() == 503)
                                        listener.onFailreSend();
                                    else
                                        listener.onFailreSend();
                                }
                            });
                        }
                    });
                } catch (IOException e) {
                    Log.i("SC_IOException", e.toString());
                }

        } else
            listener.onInternetError();
    }

    @Override
    public void Verifyfirebase(JSONObject params, Validation validator, final OnCodeDialogFinishedListener listener) {

        //Se valida que esta conectado a internet
        if (validator.isOnline()) {

            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_VERIFY_FIREBASE));

            Boolean error = false;

            //En caso de que no halla error
            if (!error) {

                //listener.onSuccessSend();

                try {
                    okHttpRequest.post(params.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    //Metodo que se ejecuta
                                    // cuando hay un error al enviar
                                    listener.onErrorSend();
                                }
                            });
                        }

                        @Override
                        public void onResponse(final Response response) throws IOException {

                            Log.i("SC_Code:", "" + response.code());

                            //Se obtiene el hilo principal y se ejecuta una tarea
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {

                                    //Se verifica el codigo de error
                                    if (response.code() == 200)
                                        listener.onSuccessSend();
                                    else if (response.code() == 503)
                                        listener.onFailSend();
                                    else
                                        listener.onFailSend();
                                }
                            });
                        }
                    });
                } catch (IOException e) {
                    Log.i("SC_IOException", e.toString());
                }
            }
        } else
            listener.onInternetError();
    }


    @Override
    public void login(final JSONObject jsonParams, final OnCodeDialogFinishedListener listener, final Validation validator) {


        if (validator.isOnline()) {

            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_LOGIN));

            boolean error = false;
            try {

                if (TextUtils.isEmpty(jsonParams.getString("email"))) {
                    //listener.onUsernameError();
                    error = true;
                }

                if (TextUtils.isEmpty(jsonParams.getString("password"))) {
                    //listener.onPasswordError();
                    error = true;
                }

                if (TextUtils.isEmpty(jsonParams.getString("gcm"))) {
                    //listener.onLoginError();
                    error = true;
                }

            } catch (JSONException e) {
                Log.e("JSONException", e.toString());
            }

            if (!error) {


                try {
                    okHttpRequest.post(jsonParams.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    //listener.onLoginError();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {

                            int code = response.code();
                            Log.i("CodeLogin", "" + code);

                            if (code == 200) {
                            }
                            //listener.onSuccess();
                            else if (code == 503) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        //    listener.onUserPassError();
                                    }
                                });

                            } else {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        //  listener.onLoginError();
                                    }
                                });
                            }
                        }
                    });
                } catch (IOException e) {
                    Log.e("IOException", e.toString());
                }
            }


/*
            new AsyncTask<Void, Void, HashMap>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    boolean error = false;
                    try {

                        if (TextUtils.isEmpty(jsonParams.getString("user"))) {
                            listener.onUsernameError();
                            error = true;
                        }

                        if (TextUtils.isEmpty(jsonParams.getString("pass"))) {
                            listener.onPasswordError();
                            error = true;
                        }

                        if (TextUtils.isEmpty(jsonParams.getString("gcm"))) {
                            listener.onLoginError();
                            error = true;
                        }

                    } catch (JSONException e) {
                        Log.e("JSONException", e.toString());
                    }

                    if (error) {
                        Log.i("AsyncTaskInteractor", "AsyncTask was cancelled");
                        cancel(true);
                    }
                }

                @Override
                protected HashMap doInBackground(Void... params) {

                    Log.i("AsyncTaskInterc", "AsyncTask executing");
                    while (!isCancelled())
                        return appUtil.makeServiceCall(Config.LOGIN, appUtil.POST, jsonParams);
                    return null;
                }

                @Override
                protected void onPostExecute(HashMap result) {
                    super.onPostExecute(result);

                    if (result != null) {
                        String response = result.get(1).toString();
                        String codeResponse = result.get(2).toString();

                        if (codeResponse.equals("200"))
                            listener.onSuccess();
                        else if (codeResponse.equals("503"))
                            listener.onUserPassError();
                        else
                            listener.onLoginError();
                    } else
                        listener.onLoginError();
                }
            }.execute(null, null, null);
*/
        } else {
        }
        //listener.onConnectionError();
    }
}
