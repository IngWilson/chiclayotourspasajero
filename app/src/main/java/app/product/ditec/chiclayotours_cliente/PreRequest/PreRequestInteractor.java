package app.product.ditec.chiclayotours_cliente.PreRequest;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 08/06/15.
 */
public interface PreRequestInteractor {
    void calculatePrice(JSONObject params, Validation validator,OnPreRequestListener listener);
}
