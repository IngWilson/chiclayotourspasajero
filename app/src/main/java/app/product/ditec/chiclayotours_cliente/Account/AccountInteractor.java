package app.product.ditec.chiclayotours_cliente.Account;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 05/05/15.
 */
public interface AccountInteractor {
    JSONObject getDataUser(String email, Validation validator, OnAccountFinishedListener listener);
}

