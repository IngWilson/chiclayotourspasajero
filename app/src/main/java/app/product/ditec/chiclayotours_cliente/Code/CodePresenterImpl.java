package app.product.ditec.chiclayotours_cliente.Code;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by lenovo on 16/05/2015.
 */
public class CodePresenterImpl implements CodePresenter, OnCodeDialogFinishedListener {

    private CodeView codeView;
    private CodeInteractor interactor;
    private Validation validator;
    private Context ctx;
    private Bundle extras;

    private User user;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    public CodePresenterImpl(CodeView codeView) {
        this.codeView = codeView;
        ctx = codeView.getContext();
        interactor = new CodeInteractorImpl();
        validator = new Validation(ctx);
        extras = ((Activity) ctx).getIntent().getExtras();
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    //
    @Override
    public void validateCodeData(String code) {

        //Se muestra un dialogo de progreso
        codeView.showProgress();
        // Se crea un JSONObject
        JSONObject params = new JSONObject();
        String phone;
// se comenta dado que va a recuperar desde el preferences
      /*  if (extras != null)
            phone = "+51 " + extras.getString("phone");
        else*/
            phone = User.loadPhone();

        Log.i("VCD_phone", phone);
        Log.i("VCD_code", code);

        //Se coloca los parametros del telefono y codigo en el JSON
        try {
            params.put("phone", phone);
            params.put("code", code);
        } catch (JSONException e) {
            Log.e("VCD_JSONException", e.toString());
        }
        //Se envia el codigo para verificar
        interactor.sendCode(params, validator, this);
    }

    @Override
    public void validateFirebase() {
        //Se muestra un dialogo de progreso
        codeView.showProgress();
        // Se crea un JSONObject
        JSONObject params = new JSONObject();
        String phone;
        phone = User.loadPhone();
        //Se coloca los parametros del telefono y codigo en el JSON
        try {
            params.put("phone", phone);
        } catch (JSONException e) {
            Log.e("VCD_JSONException", e.toString());
        }
        //Se envia el codigo para verificar
        interactor.Verifyfirebase(params, validator, this);
    }

    @Override
    public void resendcode() {

        //Se muestra un dialogo de progreso
        codeView.showProgress();
        // Se crea un JSONObject
        JSONObject params = new JSONObject();
        String phone;
        phone = User.loadPhone().substring(4);


        //Se coloca los parametros del telefono y codigo en el JSON
        try {
            params.put("phone", phone);
            params.put("codpais", "+51");
        } catch (JSONException e) {
            Log.e("VCD_JSONException", e.toString());
        }
        //Se envia el codigo para verificar
        interactor.resendCode(params, validator, this);
    }

    @Override
    public void resendcodefirebase() {
        String phone;
        phone = User.loadPhone();
        Log.i("minumero",phone);
        codeView.sendVerificationCode(phone);
    }
    @Override
    public void onSuccessSend() {

        editor.putBoolean(Config.ONVERIFY, false);
        //añadido para ingreso directo
        editor.putBoolean(Config.ONLOGIN, true);
        //fin del añadido
        editor.commit();

        //Se oculta el dialogo de progreso
        codeView.hideProgress();
        // Se muestra el dialogo que indica que el codigo se ha enviado correctamente
        codeView.showDialogSuccessSend();
        // Se dirige
        codeView.navigateToLogin();
        codeView.finished();
    }

    @Override
    public void onFailSend() {
        codeView.hideProgress();
        codeView.showDialogFailSend();
    }

    @Override
    public void onFailreSend() {
        codeView.hideProgress();
        codeView.showDialogFailreSend();
    }

    @Override
    public void onSuccessreSend() {
        codeView.hideProgress();
        codeView.successresend();
    }

    @Override
    public void onInternetError() {
        codeView.showDialogInternetError();
    }

    @Override
    public void onErrorSend() {
        codeView.hideProgress();
        codeView.showDialogErrorSend();
    }


    public void generateGCM() {

      /*  final GCM gcm = new GCM(ctx);

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                return gcm.registerGCM();
            }

            @Override
            protected void onPostExecute(String regID) {
                super.onPostExecute(regID);
            }
        }.execute(null, null, null);*/
    }

    @Override
    public void onCodeError() {
        codeView.hideProgress();
        codeView.showCodeError();
    }

    @Override
    public Context getContext() {
        return codeView.getContext();
    }
}
