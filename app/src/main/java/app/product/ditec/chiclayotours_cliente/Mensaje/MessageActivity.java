package app.product.ditec.chiclayotours_cliente.Mensaje;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;
import app.product.ditec.chiclayotours_cliente.R;

public class MessageActivity extends AppCompatActivity implements MessageView {
    MessagePresenter presenter;
    private ProgressDialog progressDialog;
    private EditText EdtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensajes);



        /*android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_account);*/
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(androidx.appcompat.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_bar);

        presenter = new MessagePresenterImpl(this);


        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Enviando respuesta al conductor...");
        progressDialog.setCancelable(false);

        EdtMessage=(EditText)findViewById(R.id.edtmessageaki);
/*        request = getIntent().getExtras().getParcelable("Request");
        costumer = request.getCostumer();*/
    }

    public void SendMessage(View view) {
        presenter.validateSendArrive((String)view.getTag());
    }

    public void SendMessage2(View view){
        String validar=EdtMessage.getText().toString();
        if(!validar.equals(""))
            presenter.validateSendArrive(EdtMessage.getText().toString());
    }
    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void showDialogFailedSend() {
        Toast.makeText(this,"No se pudo enviar correctamente, intentelo nuevamente",Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    @Override
    public void showDialogConnection(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage("Activa los datos o WI-FI en la configuracion");
        builder.setTitle("Los datos estan desactivados");
        builder.setPositiveButton("Configuración", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                return;
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                return;
            }
        });

        builder.show();
    }

    public void callUser(View view){
        //Se crea un intent y se coloca el telefono ademas se indica el tipo de intent y se ejecuta
        String phone = TaxiDriver.loadPhone();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phone));
        //intent.setData(Uri.parse("tel:" + costumer.getPhone().toString()));
        // intent.setData(Uri.parse("tel:" + request.getCostumer().getPhone()));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try{
            //Si es que el mapa es igual a null
            startActivity(intent);
        }
        catch (Exception e){
            Toast.makeText(this, "No se pudo realizar la llamada", Toast.LENGTH_SHORT).show();
        }

    }
    public void sendSMS(View v)
    {
        String phone = TaxiDriver.loadPhone();
        Uri uri = Uri.parse("smsto:"+phone);
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", "Por favor llamame");
        try{
            startActivity(it);
        }
        catch (Exception e){
            Toast.makeText(this, "No se pudo enviar sms", Toast.LENGTH_SHORT).show();
        }
    }
}
