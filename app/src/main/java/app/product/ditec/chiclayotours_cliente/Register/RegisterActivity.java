package app.product.ditec.chiclayotours_cliente.Register;

import android.os.AsyncTask;
import androidx.appcompat.app.ActionBar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Code.CodeActivity;
import app.product.ditec.chiclayotours_cliente.Login.LoginActivity;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.R;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

//import com.actionbarsherlock.app.ActionBar;
//import com.actionbarsherlock.app.SherlockActivity;


public class RegisterActivity extends AppCompatActivity implements RegisterView, Dialog.OnClickListener {

    EditText edtName;
    EditText edtLastname;
    EditText edtPhone;
    EditText edtEmail;

    EditText edtCode;
    EditText edtPassword;
    TextView btnRegister;

    Spinner mySpinner;
    Spinner ciudad;

//       = (Spinner) findViewById();
//     final Spinner ciudad = (Spinner) findViewById(R.id.MyCitySpinner);

    ProgressDialog progressDialog;
    RegisterPresenter presenter;
    AlertDialog.Builder alertDialog;
// Agregando spinner ciudad
    JSONObject jsonobject;
    JSONArray jsonarray;

    JSONObject jsonObjectcity;
    JSONArray jsonarraycity;
    //ProgressDialog mProgressDialog;

    ArrayList<String> worldlist;
    ArrayList<NewCity> world;
    String textPais;
    String textCiudad;
    String textcodpais;
// fragmento final
    boolean X=false;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        // Metodo para colocar el actionbar personalisado en el proyecto akitaxi
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_autoregistro);

        progressDialog = new ProgressDialog(this);
        presenter = new RegisterPresenterImpl(this);
        alertDialog = new AlertDialog.Builder(this);

        edtName=findViewById(R.id.edtName);
        edtLastname=findViewById(R.id.edtLastName);
        edtPhone=findViewById(R.id.edtPhone);
        edtEmail=findViewById(R.id.edtEmail);
        edtCode=findViewById(R.id.edtcode);
        edtPassword=findViewById(R.id.edtPassword);
        btnRegister=findViewById(R.id.btnRegister);
        mySpinner=findViewById(R.id.MyCountrySpinner);
        ciudad=findViewById(R.id.MyCitySpinner);

//      Spinner mySpinner = (Spinner) findViewById(R.id.MyCountrySpinner);
//     final Spinner ciudad = (Spinner) findViewById(R.id.MyCitySpinner);

        //se obtiene las fuentes que utilizaremos en la aplicacion
        Typeface typeFaceRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        edtName.setTypeface(typeFaceRoboto);
        edtLastname.setTypeface(typeFaceRoboto);
        edtPhone.setTypeface(typeFaceRoboto);
        edtEmail.setTypeface(typeFaceRoboto);
        edtPassword.setTypeface(typeFaceRoboto);
        typeFaceRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Black.ttf");
        btnRegister.setTypeface(typeFaceRoboto);
// Spinner
        //inicializamos  la funcion  en un nuevo hilo
        new DownloadJSON().execute();
        //

        edtPhone.setText(User.loadPhone().substring(4));
        edtName.requestFocus();
    }
    //metodo para mostrar un showprogress que dig registrando usuario
    @Override
    public void showProgress() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage(getString(R.string.progress_register_title));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    ///metodo para enviarte a la pagina web de terminos y condiciones
    public void website(View view) {
        Uri webpage = Uri.parse(getString(R.string.URL_TERMINOS));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(webIntent);
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    //metodo para mostrar mensaje completar el campo si el edittext no se a llenado
    @Override
    public void setNameError() {
        //Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        //edtName.setBackground(mBackground);
        Toast.makeText(this, "Complete el campo del nombre",
                Toast.LENGTH_LONG).show();
    }
    //metodo para mostrar mensaje completar el campo si el edittext no se a llenado
    @Override
    public void setLastNameError() {
        //Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        //edtLastname.setBackground(mBackground);
        Toast.makeText(this, "Complete el campo de apellido",
                Toast.LENGTH_LONG).show();
    }
    //metodo para mostrar mensaje completar el campo si el edittext no se a llenado
    @Override
    public void setPasswordError() {
        //Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        //edtPassword.setBackground(mBackground);
        Toast.makeText(this, "Complete el campo de contraseña",
                Toast.LENGTH_LONG).show();
    }
    //metodo para mostrar mensaje completar el campo si el edittext no se a llenado
    @Override
    public void setEmailError() {
        //Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        //edtEmail.setBackground(mBackground);
        Toast.makeText(this, getString(R.string.register_campo_email),
                Toast.LENGTH_LONG).show();
    }
    //metodo para cambiar cambiar el background (fondo) de el edit text si hubo un error a ingresar
    //o no ingresar nada

    @Override
    public void setSpaceError() {
        //Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        //edtEmail.setBackground(mBackground);
        Toast.makeText(this, getString(R.string.register_errorspaceuser),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPhoneNumberError() {

        Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        //edtPhoneNumber.setBackground(mBackground);
    }
    //metodo para obtener el context
    @Override
    public Context getContext() {
        return RegisterActivity.this;
    }

    //metodo para validar los datos que se ingresan en el registro,(si hubo algun error al ingresarlos)
    public void registerinServer(View view) {
        X=false;
        //Se tendría que validar la existencia del codigo antes de enviarlo en otra ubicación de la interaccion
        if(TextUtils.isEmpty(edtCode.getText().toString()))
        {registrar();}
        else {
            presenter.validatecodepromo(edtCode.getText().toString());
            //
          }
    }
    @Override
    public void codepromovalido(){
        X=true;
        registrar();
        /*Toast.makeText(this, "Codigo Correcto", Toast.LENGTH_LONG).show();*/
    }

    @Override
    public void codepromoerror(){
        Toast.makeText(this, "Codigo Invalido Ingrese uno valido o dejelo Vacio", Toast.LENGTH_LONG).show();
    }

    @Override
    public void codeexit(){
        Toast.makeText(this, "Codigo Promocional guardado", Toast.LENGTH_LONG).show();
    }

    private void registrar()
    {
        textCiudad = (String) ciudad.getSelectedItem();
        textPais = (String) mySpinner.getSelectedItem();

        textcodpais = world.get(mySpinner.getSelectedItemPosition()).getcodpais();
//        presenter.validateParamsUser(edtEmail.getText().toString(), edtPassword.getText().toString(), edtName.getText().toString(), edtLastname.getText().toString(), edtPhoneNumber.getText().toString());
        presenter.validateParamsUser(edtPhone.getText().toString(), edtPhone.getText().toString(), edtName.getText().toString(), edtLastname.getText().toString(), textCiudad, textPais, textcodpais, edtPhone.getText().toString());
//

        Log.i("SPINNER", "" + textPais);
        Log.i("SPINNER", "" + textCiudad);
        Log.i("SPINNER", "" + textcodpais);
       // Toast.makeText(this, "Podemos meter otro metodo", Toast.LENGTH_LONG).show();
    }
    //metodo para mostrar mensaje de, se registro correctamente
    @Override
    public void showDialogRegister() {
        Toast.makeText(this, getString(R.string.dialog_register_message), Toast.LENGTH_LONG).show();
    }

    @Override
    public void finishRegister() {

        if(X)
        {
            //metodo para Enviar codigo promociional
            presenter.SendCodePromo(edtEmail.getText().toString(),edtCode.getText().toString());
        }

        finish();
    }
    //metodo para mostrar mensaje de, el telefono ya se encuentra registrado
    @Override
    public void showDialogPhoneError() {
        Toast.makeText(this, getString(R.string.dialog_phone_error), Toast.LENGTH_LONG).show();
    }

    //metodo para mostrar mensaje de, el email ya se encuentra registrado
    @Override
    public void showDialogEmailError() {
        Toast.makeText(this, getString(R.string.dialog_email_error), Toast.LENGTH_LONG).show();
    }
    //metodo que muestre mensaje si hay algun error en el registro.
    @Override
    public void showDialogError() {

        Toast.makeText(this, getString(R.string.RegisterActivity_Dialog_Error), Toast.LENGTH_LONG).show();
    }
    //metodo que invoca a otra actividad en este caso verifityphoneActivity
    @Override
    public void navigateToVerifyPhone(String email) {
        startActivity((new Intent(this, CodeActivity.class)));
    }
    //metodo que te manda a ajustes por si no tienes datos y no estas conectado a intenet
    @Override
    public void showDialogInternetError() {
        alertDialog.setTitle(getString(R.string.dialog_conection_title))
                .setMessage(getString(R.string.dialog_conection_message))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }
    //llamada para abrir ajustes del celular
    @Override
    public void onClick(DialogInterface dialog, int which) {
        startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
    }
    //metodo para regresar a la actividad del login una vez registrado
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, LoginActivity.class));
        finishRegister();
    }
    //metodo para almacenar datos antes de detenerse la actividad en este caso seran edtname etc.
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(User.NAME, edtName.getText().toString());
        outState.putString(User.LAST_NAME, edtLastname.getText().toString());
        outState.putString(User.PASSWORD, edtPassword.getText().toString());
        outState.putString(User.EMAIL, edtEmail.getText().toString());
        super.onSaveInstanceState(outState);
    }
    //metodo para recuperar los dato guardados
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public class DownloadJSON extends AsyncTask<Void, Void, Void>
    {
        //"doInBackground" uno de los tres pasos  para hacer el uso de hilos  con AsyncTask
        @Override
        public Void doInBackground(Void... params) {
            //inicializamos los objetos con los objetos  "WorldPopulation"
            world = new ArrayList<NewCity>();
            worldlist = new ArrayList<String>();

            try
            {
                //declaramos una variable HttpClient
                // en esta  fragmento trasformamos los datos del json en objetos json
                HttpClient httpclient = new DefaultHttpClient();
                httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
                //la ruta del json
                HttpGet request = new HttpGet(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_world));
                HttpResponse response = httpclient.execute(request);
                HttpEntity resEntity = response.getEntity();

                String _response= EntityUtils.toString(resEntity);
                jsonobject = new JSONObject(_response);
                jsonarray = jsonobject.getJSONArray("Mundo");
                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonobject = jsonarray.getJSONObject(i);

                    NewCity worldpop = new NewCity();
                    worldpop.setciudad(jsonobject.optString("Ciudad"));
                    worldpop.setcodpais(jsonobject.optString("CodPais"));
                    world.add(worldpop);

                    worldlist.add(jsonobject.optString("Pais"));
                }
            }catch(Exception e)
            {

            }
            return null;
        }
        public void onPostExecute(Void args) {
            // Locate the spinner in activity_main.xml

//--> spiner pais
            //Spinner mySpinner = (Spinner) findViewById(R.id.MyCountrySpinner);
            // Spinner adapter
            mySpinner
                    .setAdapter(new ArrayAdapter<String>(RegisterActivity.this,
                            android.R.layout.simple_spinner_dropdown_item,
                            worldlist));

            // Spinner on item click listener
            mySpinner
                    .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            String cityfull = world.get(position).getciudad();
                            List<String> city = new ArrayList<String>(Arrays.asList(cityfull.split(",")));
                            //
            //spinner de ciudad
                         //   final Spinner ciudad = (Spinner) findViewById(R.id.MyCitySpinner);
                            // Spinner adapter
                            ciudad
                                    .setAdapter(new ArrayAdapter<String>(RegisterActivity.this,
                                            android.R.layout.simple_spinner_dropdown_item,
                                            city));


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub
                        }

                    });

        }
    }

//
}
