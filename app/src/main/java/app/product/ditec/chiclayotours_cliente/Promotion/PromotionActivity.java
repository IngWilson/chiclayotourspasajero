package app.product.ditec.chiclayotours_cliente.Promotion;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import androidx.appcompat.app.ActionBar;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.TextView;
import android.widget.EditText;
import app.product.ditec.chiclayotours_cliente.R;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

import android.graphics.Typeface;
import android.widget.Toast;

public class PromotionActivity extends AppCompatActivity implements PromotionView, Dialog.OnClickListener{

    TextView txtIdPromotion;
    TextView txtTotalCarreras;
    TextView txtSituacion;
    TextView txtEstado;
    EditText edtCodigo;
    TextView txtCIdPromotion;
    TextView txtCTotalCarreras;
    TextView txtCSituacion;
    TextView txtCEstado;
    TextView edtCCodigo;



    PromotionPresenter    presenter;
    AlertDialog.Builder alertDialog;
    ProgressDialog progressDialog;
    ProgressDialog progressDialog1;
    AlertDialog.Builder dialogConfirmCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String promo= getIntent().getExtras().getString("promo");



        if(promo.equals("present"))
        {
            super.onCreate(savedInstanceState);
          /*  setTheme(R.style.Theme_UserDialog);
            requestWindowFeature(Window.FEATURE_NO_TITLE);*/
           // setTheme(R.style.AppDialogTheme);
            setContentView(R.layout.activity_present);
        }
            else {
            super.setTheme(R.style.AppTheme);
            super.onCreate(savedInstanceState);
         //   setTheme(R.style.AppTheme2);
            setContentView(R.layout.activity_promotion);
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.layout_logo_promociones);
        }

        txtIdPromotion=findViewById(R.id.txtIdPromotion);
        txtTotalCarreras=findViewById(R.id.txtTotalCarreras);
        txtSituacion=findViewById(R.id.txtSituacion);
        txtEstado=findViewById(R.id.txtEstado);
        edtCodigo=findViewById(R.id.edtCodigo);
        txtCIdPromotion=findViewById(R.id.txtCIdPromotion);
        txtCTotalCarreras=findViewById(R.id.txtCTotalCarreras);
        txtCSituacion=findViewById(R.id.txtCSituacion);
        txtCEstado=findViewById(R.id.txtCEstado);
        edtCCodigo=findViewById(R.id.edtCCodigo);


        Typeface typeFaceRoboto =Typeface.createFromAsset(getAssets(),"Roboto-Black.ttf");
        txtCIdPromotion.setTypeface(typeFaceRoboto);
       txtCTotalCarreras.setTypeface(typeFaceRoboto);
        txtCSituacion.setTypeface(typeFaceRoboto);
        txtCEstado.setTypeface(typeFaceRoboto);
        edtCCodigo.setTypeface(typeFaceRoboto);

        typeFaceRoboto =Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");

        txtIdPromotion.setTypeface(typeFaceRoboto);
        txtTotalCarreras.setTypeface(typeFaceRoboto);
        txtSituacion.setTypeface(typeFaceRoboto);
        txtEstado.setTypeface(typeFaceRoboto);


        presenter   = new PromotionPresenterImpl(this);
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Obteniendo tus datos...");

        presenter.validateGetData();


        //
        progressDialog1=new ProgressDialog(this);
        dialogConfirmCode = new AlertDialog.Builder(this);

        //
    }
    //Coloca el nombre del taxista en el layout
    @Override
    public void setId(String idpromotion) {
        txtIdPromotion.setText(convertir(idpromotion));
    }

    @Override
    public void setTotalc(String totalc) {
        txtTotalCarreras.setText(totalc);
    }

    @Override
    public void setSituacion(String situacion) {
        txtSituacion.setText(situacion);
    }

    @Override
    public void setEstado(String estado) {
        txtEstado.setText(estado);
    }

//del envio de datos
    public void sendcode() {
     presenter.validateCode(edtCodigo.getText().toString());
    }
    //metodo para afirmacion si el numero se envio correctamente
    @Override
    public void showDialogSuccessSend() {
        Toast.makeText(this, getString(R.string.PromotionActivity_showDialogSuccessSend), Toast.LENGTH_LONG).show();
        presenter.validateGetData();
    }

    //metodo que te dice si el numero de verificacion que ingresaste ya esta registrado
    @Override
    public void showDialogCodeError() {
        Toast.makeText(this, getString(R.string.PromotionActivity_Dialog_Code_Error), Toast.LENGTH_LONG).show();
    }


    //metodo para enviar error si el telefono no es correcto
    @Override
    public void showDialogFailedSend() {
        Toast.makeText(this, getString(R.string.PromotionActivity_showDialogFailedSend), Toast.LENGTH_LONG).show();
    }

    //metodo de advertencia para ver si el numero de telefono ingresado es el correcto
    public void showDialogConfirmCode(View view) {
        dialogConfirmCode.setCancelable(false)
                .setTitle("Advertencia")
                .setMessage("¿El código ingresado es el correcto " + edtCodigo.getText().toString() + " ?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.validateCode(edtCodigo.getText().toString());

                    }
                })
                .setNegativeButton("No", null)
                .show();

    }


    @Override
    public void hideProgressE() {
        progressDialog1.hide();
    }

    //showprogress que te dice enviando telefono es como un dialogo de aviso
    @Override
    public void showProgressE() {
        progressDialog1.setIndeterminate(false);
        progressDialog1.setMessage(getString(R.string.PromotionActivity_showProgress));
        progressDialog1.setCancelable(false);
        progressDialog1.show();
    }

    //metodo que te dice si  ingresaste un numero incorrecto para verificar tu cuenta.
    public void showDialogEmptyCode() {
        Toast.makeText(this, getString(R.string.PromotionActivity_Dialog_Empty_Code), Toast.LENGTH_LONG).show();
    }
    //metodo por si nose envio el telefono por errores de coneccion o por el internet lento
    @Override
    public void showDialogError() {
        Toast.makeText(this, getString(R.string.PromotionActivity_Dialog_Error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialogRepeatCodeError() {
        Toast.makeText(this, getString(R.string.PromotionActivity_showDialogRepeatCode), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialogCodeExpireError() {
        Toast.makeText(this, getString(R.string.PromotionActivity_Dialog_CodeExpire), Toast.LENGTH_LONG).show();
    }

// fin de ello
    //Obtiene el contexto de la actividad
    @Override
    public Context getContext() {
        return PromotionActivity.this;
    }

    //Muestra un dialogo de progreso
    @Override
    public void showProgress() {
        progressDialog.show();
    }

    //Oculta el dialogo de progreso
    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void finishAccount() {
    }

    //Muestra un dialogo indicando que se active la configuracion de datos
    @Override
    public void showDialogInternetError() {
        alertDialog.setTitle(getString(R.string.dialog_conection_title))
                .setMessage(getString(R.string.dialog_conection_message))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }

    //Evento onClick para el boton "Configuracion" del dialogo para cuando no se tiene conexion a internet
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -2:
                return;
            case -1:
                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
            default:
                return;
        }
    }

    public static String convertir(String valor1) {
        long valor2=(Long.parseLong(valor1)+1357)*7-1;
        String nro1=Long.toHexString(valor2);
       // int suma=nro1+15;
        String resu=String.valueOf(nro1);
        return resu;
    }
}
