package app.product.ditec.chiclayotours_cliente.Map;

import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


import com.github.nkzawa.socketio.client.Socket;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.GCM;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;



// Clase interactor implementada que implementa
// los metodos para comunicarse con el server
public class MapInteractorImpl implements MapInteractor{

    //
    private JSONObject params = null;

    // Se declara objeto para comunicarse con el server
    private OkHttpRequest okHttpRequest;

    // Se declara objeto OnMapFinishedListener
    private OnMapFinishedListener listener;

    // Se declara el manejador para los hiloss
    private Handler handler = new Handler();

    // Se declaran banderas para saber si se envio
    // o se cancelo
    private boolean flag_send=false;
    private boolean flag_cancel=false;

    // Se declaran banderas para saber si se logeo
    boolean flag_signin=false;

    private Runnable runnable;

    // Se declara el socket
    private Socket socket;

    //
    public MapInteractorImpl(OnMapFinishedListener _listener){
        listener = _listener;
        // Se obtiene el Socket de la App
        socket = App.getSocket();
        // Se activa los escuchadores
        // para el evento de reconectar
        // conectar, reconectando,
        // reconectar, desconectar y cuando existe error
        socket.on(Socket.EVENT_RECONNECT,connect);
        socket.on(Socket.EVENT_CONNECT, connect);
        socket.on(Socket.EVENT_RECONNECTING,reconnecting);
        socket.on(Socket.EVENT_RECONNECT,connect);
        socket.on(Socket.EVENT_DISCONNECT,disconnect);
        socket.on(Socket.EVENT_ERROR,event_error);
        // Se activa el mensaje "arrive","error" y "taxi location"
        socket.on("arrive",arrive);
        socket.on("confirmarubicacioncompartida",confirmarubicacioncompartida);
        socket.on("ShareLocation",ShareLocation);
        socket.on("retirarenviarubicacion",retirarenviarubicacion);
        socket.on("error", error);
        socket.on("taxi location",taxi_location);
        socket.on("ontravel", ontravel);
        socket.on("getonboard", getonboard);
        // Se conecta el socket
        socket.connect();
    }

    // Escuchador cuando "Reconectando"
    private Emitter.Listener reconnecting = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            // Se ejecuta el metodo del presentador para indicar que se reconectando
                            ((MapPresenter)listener).showStateReconnecting();
                        }
                    }
            );
        }
    };

    // Escuchador cuando "Ya llegue"
    private Emitter.Listener arrive = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            try {

                // Se crea un objeto JSON
                JSONObject jsonObject = (JSONObject)args[0];
                // Se obtiene la longitude y latitude
                final String message= jsonObject.getString("message");
                Log.i("Message", message);
            // Se crea un manejador y que obtiene
            // el hilo principal


            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {

                            // Se ejecuta el metodo del presentador para mostrar el dialogo
                            // indicando que el taxi ya llego, pero antes se verifica si esta
                            // en pleno pedido
                           // if(((MapPresenter)listener).checkOnRequest())
                                listener.onArrive(message);
                        }
                    }
            );
            }catch (JSONException e){
                Log.e("JSONExpcetionMessage",e.toString());
            }
        }
    };
    private Emitter.Listener confirmarubicacioncompartida = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            try {

                // Se crea un objeto JSON
                JSONObject jsonObject = (JSONObject)args[0];
                // Se obtiene la longitude y latitude
                final String origen= jsonObject.getString("email");
                new Handler(Looper.getMainLooper()).post(
                        new Runnable() {
                            @Override
                            public void run() {

                                // Se ejecuta el metodo del presentador para mostrar el dialogo
                                // indicando que el taxi ya llego, pero antes se verifica si esta
                                // en pleno pedido
                                // if(((MapPresenter)listener).checkOnRequest())
                                Log.i("ubicacioncompartida","message");
                                listener.onConfirmarUbicacionCompartida(origen);
                            }
                        }
                );
            }catch (JSONException e){
                Log.e("JSONExpcetionMessage",e.toString());
            }
        }
    };

    private Emitter.Listener ontravel = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.i("ontravel","se ejecuto el socketontravel");
                 new Handler(Looper.getMainLooper()).post(
                        new Runnable() {
                            @Override
                            public void run() {

                                listener.onFinishRequest();
                                listener.goodtravel();
                            }
                        }
                );

        }
    };

    private Emitter.Listener getonboard = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.i("ontravel2","se ejecuto el socket getonboard");

                new Handler(Looper.getMainLooper()).post(
                        new Runnable() {
                            @Override
                            public void run() {

                                listener.onCancelRequest();
                            }
                        }
                );

        }
    };

    // Escuchador cuando se "Desconecta"
    private Emitter.Listener disconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            // Se ejecuta el metodo del presentador para indicar
                            // que esta desconectado
                            ((MapPresenter)listener).showStateDisconnected();
                        }
                    }
            );

        }
    };

    // Escuchador cuando se "Evento error"
    private Emitter.Listener event_error = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(((MapPresenter) listener).getView().getContext(), "Hubo un error al conectar..", Toast.LENGTH_LONG).show();
                        }
                    }
            );
        }
    };

    // Escuchador cuando se "Conecta"
    private Emitter.Listener connect = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            // Metodo que envia el signin
            // para pasar el socket_id
            signin();
        }
    };


    // Escuchador "error"
    private Emitter.Listener error = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            if(((MapPresenter) listener ).checkOnCall()) {
                ((MapPresenter)listener).setOnCall(false);
            }
        }
    };

    // Metodo para el evento de la localizacion constante
    private Emitter.Listener taxi_location = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {

                // Se crea un objeto JSON
                JSONObject jsonObject = (JSONObject)args[0];
                // Se obtiene la longitude y latitude
                final double latitude= jsonObject.getDouble("latitude");
                final double longitude= jsonObject.getDouble("longitude");
                final double bearing= jsonObject.getDouble("bearing");

                Log.i("LOCATION_SOCKET_TAXI", latitude + "," + longitude);

                // Se crea un manejador y que obtiene
                // el hilo principal
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

//                        Toast.makeText(App.getInstance(),"TaxiLocation: "+latitude+","+longitude,Toast.LENGTH_LONG).show();
                        // Se verifica que esta en request
                        if(((MapPresenter) listener).checkOnRequest()) {
                            // Se actualiza la localizacion del taxi cuando esta en pedido
                            ((MapPresenter)listener).updateLocationTaxi(latitude,longitude,bearing);
                        }

                    }
                });
            }catch (JSONException e){
                Log.e("JSONExpcetionLocation",e.toString());
            }

        }
    };

    private Emitter.Listener ShareLocation = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
                // Se crea un objeto JSON
                final JSONObject jsonObject = (JSONObject)args[0];
                Log.i("sharelocation",jsonObject.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                            ((MapPresenter)listener).updateShareLocation(jsonObject);
                    }
                });
        }
    };

    private Emitter.Listener retirarenviarubicacion = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // Se crea un objeto JSON
               final JSONObject jsonObject = (JSONObject)args[0];
             new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    ((MapPresenter)listener).retirarenviarubicacion(jsonObject);
                    leavesharelocation(jsonObject);

                }
            });

        }
    };

    public void leavesharelocation(JSONObject params){
        socket.emit("leavesharelocation", params, new Ack() {

            @Override
            public void call(Object... args) {
                String response = (String)args[0];
                Log.i("sendShareLocation", "response:"+response);
                //Si es que es "OK" la respuesta entonces se envio correctamente
                if(response.equals("OK")) {

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });


                }else
                    //Si es que es "error" entonces hubo un error en el server
                    if(response.equals("error")){
                        new Handler(Looper.getMainLooper()).post(
                                new Runnable() {
                                    @Override
                                    public void run() {

                                    }
                                }
                        );
                    }
            }
        });
    }

    int count_sent;
    int timeout;
    private int count=0;
    Runnable setTimeOut;
    private Boolean verify=false;

Runnable repeatpeticion;

    public void sendRequestOferta(final JSONObject jsonParams,
                            final Validation validator,
                            final OnMapFinishedListener listener
                            ) {
        if(socket.connected()) {
            // Se verifica que esta conectado a internet
            if(validator.isOnline()) {
               // Se emite un mensaje "new request" con los parametros
                socket.emit("newrequest_oferta", jsonParams, new Ack() {
                    @Override
                    public void call(Object... args) {

                        // Se obtiene la respuesta
                        String response = (String) args[0];
                        final String response2=(String) args[1];
                        Log.i("valorrespuesta",response);

                        //Se verifica la respuesta si es que es "OK" es decir se envio
                        // correctamente
                        if (response.equals("OK")) {
                            // Se crea una tarea en el hilo principal
                            new Handler(Looper.getMainLooper()).post(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            listener.saveId(response2);
                                            listener.onSuccessCalloferta();
                                        }
                                    }
                            );
                            // En caso contrario se verifica que
                            // la respuesta es "banned"
                        }else if (response.equals("banned")) {
                            // Se crea una tarea en el hilo principal
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onFailCall();
                                    // Se remueve las tareas del hilo principal
                                }
                            });
                        }
                        else {
                            new Handler(Looper.getMainLooper()).post(
                                    new Runnable() {
                                        @Override
                                        public void run() {

                                            Log.i("valorrespuesta", "ok2");
                                        }
                                    }
                            );
                        }

                    }
                });



            }


        }
        else
            // Metodo que se ejecuta cuando hay un error de conexion
            listener.onErrorConnection();
    }

    // Se envio el mensaje de solicitud de taxi
    public void sendRequest(final JSONObject jsonParams,
                            final Validation validator,
                            final OnMapFinishedListener listener,
                            final int type, final GCM gcm) {
        count_sent  = 0;
        // Se coloca este valor para ver cuantas veces se envia
        // el tiempo
        timeout     = 0;
        flag_send   = false;
        ///Se verifica el tipo de mensaje que quiere enviar
        if(type==0)
            // Se verifica conecta el socket
            if(socket.connected()) {
                // Se verifica que esta conectado a internet
                if(validator.isOnline()) {

                    //Se crea una tarea
                    setTimeOut = new Runnable() {
                        @Override
                        public void run() {

                            if(timeout==10) {
                                listener.onErrorPreCall();
                                handler.removeCallbacks(this);
                            }
                            else {
                                timeout++;
                                handler.postDelayed(this, 1000);
                            }
                        }
                    };

                    // Se emite un mensaje "new request" con los parametros
                    socket.emit("new request", jsonParams, new Ack() {
                        @Override
                        public void call(Object... args) {

                            // Se obtiene la respuesta
                            String response = (String) args[0];
                            final String response2=(String) args[1];
                            //Se verifica la respuesta si es que es "OK" es decir se envio
                            // correctamente
                            if (response.equals("OK")) {
                                // Se crea una tarea en el hilo principal
                                new Handler(Looper.getMainLooper()).post(
                                        new Runnable() {

                                            @Override
                                            public void run() {

                                                // Se verifica que se siga en la activida del radar
                                                if (((MapPresenter) listener).checkOnCall()) {
                                                    // Metodo que se activa cuando se realiza
                                                    // una llamada correctamente
                                                    listener.saveId(response2);
                                                    listener.onSuccessCall();
                                                    // Booleano que indica que se envio
                                                    // correctamente el mensaje
                                                    flag_send = true;
                                                    // Se remueve la tarea asincrona
                                                    handler.removeCallbacks(setTimeOut);

                                                   // Log.i("prueba", String.valueOf(prefs.getBoolean(Config.ONCALL, false)));
                                                }
                                                //probando hilo para repetirpeticion

                                                    /*repeatpeticion = new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (((MapPresenter) listener).checkOnRadar() || verify) {
                                                                verify=true;
                                                                count++;
                                                                //Se verifica que la cantidad de veces sea igual a 6
                                                                if (count % 5 == 0) {

                                                                    handler.removeCallbacks(repeatpeticion);
                                                                } else {
                                                                    boolean check = ((MapPresenter) listener).checkOnRadar();
                                                                    if (check) {

                                                                        if (count != 1)
                                                                            socket.emit("new request", jsonParams, new Ack() {
                                                                                @Override
                                                                                public void call(Object... args) {

                                                                                    // Se obtiene la respuesta

                                                                                }

                                                                            });
                                                                        handler.postDelayed(repeatpeticion, 8000);

                                                                    } else
                                                                        handler.removeCallbacks(repeatpeticion);
                                                                }
                                                            }
                                                            else
                                                                handler.postDelayed(repeatpeticion, 2000);
                                                        }
                                                    };
                                                    repeatpeticion.run();*/

                                                //fin prueba

                                            }
                                        }
                                );
                            // En caso contrario se verifica que
                            // la respuesta es "banned"
                            } else if (response.equals("banned")) {
                                // Se crea una tarea en el hilo principal
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onFailCall();
                                        // Se indica que se envio
                                        flag_send = true;
                                        // Se remueve las tareas del hilo principal
                                        handler.removeCallbacks(setTimeOut);
                                    }
                                });
                            }
                        }
                    });

                    // Se ejecuta la tarea
                    setTimeOut.run();


                    // Se verifica que se ha enviado
                    if (flag_send) {
                        //
                        flag_send = false;
                        listener.onErrorPreCall();
                    }


/*                                 count_sent++;

                    if (flag_send) {
                        flag_send = false;
                        handler.removeCallbacks(this);
                    }
                    else {
                        if(count_sent==2){
                            listener.onErrorPreCall();
                        }
                        else
                            handler.postDelayed(this, 5000);
                    }*/
/*                                }else {
                    handler.removeCallbacks(this);
                    listener.onConnectionErrorCall();*/
                }
                Log.i("Round : ",""+count_sent);

            }
            else
                // Metodo que se ejecuta cuando hay un error de conexion
                listener.onErrorConnection();
        else
            // Si el tipo es 1 entonces es del tipo cancel
            if(type==1) {

                /*if(socket.connected()) {
                    Runnable DoThings = new Runnable(){
                        @Override
                        public void run() {
                            if(validator.isOnline()) {
                                socket.emit("cancel request", jsonParams, new Ack() {

                                    @Override
                                    public void call(Object... args) {
                                        String response = (String) args[0];
                                        Log.i("ACK_CALL", response);
                                        if (response.equals("OK")) {
                                            new Handler(Looper.getMainLooper()).post(
                                                    new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (((MapPresenter) listener).checkOnCall()) {
                                                                listener.onSuccessCancel();
                                                                flag_cancel = true;
                                                            }
                                                        }
                                                    }
                                            );
                                        } else if (response.equals("taxinotfound")){

                                        }
                                        else
                                            listener.onErrorCancel();
                                    }

                                });
                                if (flag_cancel)
                                    handler.removeCallbacks(this);
                                else
                                    handler.postDelayed(this, 5000);
                            }else {
                                handler.removeCallbacks(this);
                                listener.onConnectionErrorCall();
                            }
                            i++;
                            Log.i("Round : ",""+i);
                        }
                    };
                    DoThings.run();
                }
                else
                    listener.onErrorConnection();*/

                // Se valida que esta conectado a internet
                if(validator.isOnline()) {
                    // Se crea a nueva peticion http
                    okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_CANCEL));
                    try {
                        // Se crea una nueva peticion POST y se envia los parametros
                        okHttpRequest.post(jsonParams.toString(), new Callback() {

                            @Override
                            public void onFailure(Request request, IOException e) {
                                Log.i("MapFailure", "Request Failure");
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onErrorPreCancel();
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                Log.i("SR_Code", " " + response.code());
                                Log.i("SR_Message", "" + response.body().string());
                                // Se pone un switch para verificar
                                // el codigo de respuesta
                                switch (response.code()) {
                                    // Se verifica que sea 200 entonces
                                    case 200:
                                        // Se crea una nueva tarea en el hilo principal
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                // Metodo que se ejecuta cuando se envia
                                                // satisfactoriamente el cancelar
                                                listener.onSuccessCancel();
                                            }
                                        });
                                        break;
                                    // Se verifica que sea 503 entonces
                                    case 503:
                                        // Se crea una nueva tarea en el hilo principal
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                // Metodo que se ejecuta cuando no se envia
                                                // el pedido correctamente
                                                listener.onErrorCancel();
                                            }
                                        });
                                        break;
                                    // Se verifica que sea 500 entonces
                                    case 500:
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                // Metodo que se ejecuta cuando hay un error
                                                // en el servidor
                                                listener.onFailCall();
                                            }
                                        });
                                        break;
                                    default:
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (type == 0)
                                                        listener.onErrorCall();
                                                else if (type == 1)
                                                    listener.onErrorCancel();
                                            }
                                        });
                                }

                            }
                        });
                    } catch (IOException e) {
                        Log.e("IOException", e.toString());
                    }
                }
                else
                    listener.onConnectionErrorCancel();
            }
            else if(type==3){
                if(validator.isOnline()) {
                    // Se crea a nueva peticion http
                    okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_FINISHED));
                    try {
                        // Se crea una nueva peticion POST y se envia los parametros
                        okHttpRequest.post(jsonParams.toString(), new Callback() {

                            @Override
                            public void onFailure(Request request, IOException e) {
                                Log.i("MapFailure", "Request Failure");
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onErrorPreCancel();
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                Log.i("SR_Code", " " + response.code());
                                Log.i("SR_Message", "" + response.body().string());
                                // Se pone un switch para verificar el codigo de respuesta
                                switch (response.code()) {
                                    // Se verifica que sea 200 entonces
                                    case 200:
                                        // Se crea una nueva tarea en el hilo principal
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                // Metodo que se ejecuta cuando se envia
                                                // satisfactoriamente el cancelar
                                                listener.onSuccessFinish();
                                            }
                                        });
                                        break;
                                    // Se verifica que sea 503 entonces
                                    case 503:
                                        // Se crea una nueva tarea en el hilo principal
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                // Metodo que se ejecuta cuando no se envia
                                                // el pedido correctamente
                                                listener.onErrorCancel();
                                            }
                                        });
                                        break;
                                    // Se verifica que sea 500 entonces
                                    case 500:
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                // Metodo que se ejecuta cuando hay un error
                                                // en el servidor
                                                listener.onFailCall();
                                            }
                                        });
                                        break;
                                    default:
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (type == 0)
                                                    listener.onErrorCall();
                                                else if (type == 1)
                                                    listener.onErrorCancel();
                                            }
                                        });
                                }

                            }
                        });
                    } catch (IOException e) {
                        Log.e("IOException", e.toString());
                    }
                }
                else
                    listener.onConnectionErrorCancel();
            }

    }

    // Metodo que obtiene los datos del usuario
    @Override
    public void getUserData(final String email, final Validation validator, final OnMapFinishedListener listener) {

        // Se verifica que esta conectado a internet
        if (validator.isOnline()) {

            String URL = App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_GETDATAUSER) + "?email=" + email;
            okHttpRequest = new OkHttpRequest(URL);

            // Se realiza una peticion GET
            try {
                okHttpRequest.getNotParams(new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {

                        Log.i("AccountFailure", "Failure when get user data");
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {

                        // Se obtiene la respuesta
                        Log.i("AccountAccept", "Get user data");
                        String res = response.body().string();
                        Log.i("Response Body", res);

                        try {
                            // Se obtiene los array del JSON
                            JSONArray jsonArray = new JSONArray(res);
                            // Se obtiene el primer elemento del array
                            params = jsonArray.getJSONObject(0);

                            Log.i("GDU_params", params.toString());
                            // Se obtienen los parametros
                            // y se guardan en el preferences

                            User.saveName(params.getString("first_name"));
                            User.saveLastname(params.getString("last_name"));
                            User.saveEmail(params.getString("email"));
                            User.savePhone(params.getString("phone"));
                            User.saveIsable(params.getBoolean("isable"));
                            User.saveVerified(params.getBoolean("verified"));

                            listener.onSuccessGetData();

                        } catch (JSONException e) {
                            Log.e("JSONArrayException", e.toString());
                        }
                    }
                });
            } catch (IOException e) {
                Log.e("IOException", e.toString());
            }
        }
    }


    //Metodo que envia la version de la aplicacion
    @Override
    public void sendVersion(JSONObject jsonParams, Validation validator, final OnMapFinishedListener listener) {

        // Se valida que este conectado a internet
        if(validator.isOnline()){

            // Se realiza una peticion POST
            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_VERSION1));
            try{
                okHttpRequest.post(jsonParams.toString(), new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {

                        Log.i("AccountFailure", "Failure when get user data");
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {

                        String res = response.body().string();
                        final int code = response.code();
                        Log.i("SV_R", "" + res);
                        Log.i("SV_Code", "" + code);

                        //Se obtiene el hilo principal
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                // Se verifica el codigo si es 200 es decir que la version es
                                // la misma que en la BD
                                if (code == 200)
                                    listener.onCorrectVersion();
                                else if (code == 503)
                                // En caso contrario entonces se ejecuta el codigo
                                // para el mostrar el mensaje para actualizar
                                    listener.onIncorrectVersion();
                            }
                        });
                    }
                });
            }catch (IOException e){
                Log.e("IOException",e.toString());
            }

        }
    }

    // Metodo que verifica el request
    @Override
    public void checkRequest(JSONObject jsonParams, Validation validator, final OnMapFinishedListener listener) {
        // Metodo que verifica que esta conectado
        if (validator.isOnline()) {
            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_checkRequest));

            // Metodo que envia una peticion POST y se envia los parametros
            try {
                okHttpRequest.post(jsonParams.toString(), new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {

                        Log.i("AccountFailure", "Failure when get user data");
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {


                        final int code = response.code();
                        Log.i("CR_Code", "" + code);
                        // Se crea una tarea en el hilo principal
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                // Se verifica que el codigo sea 200
                                if (code == 200){
                                    listener.onCancelRequest();
                                }
                            }
                        });
                    }
                });
            } catch (IOException e) {
                Log.e("IOException", e.toString());
            }
        }
    }

    @Override
    public void checkRequestState(final JSONObject params, final Validation validator, final OnMapFinishedListener listener) {
        if(validator.isOnline()){
            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_checkState));
            try{
                okHttpRequest.post(params.toString(), new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {

                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        final int code = response.code();
                        Log.i("CRS_Code", "" + code);
                        // Se crea una tarea en el hilo principal
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                // Se verifica que el codigo sea 200
                                if (code == 200)
                                    listener.onFinishRequest();
                                if (code == 500)
                                    listener.onSuccessFinish();
                            }
                        });
                    }
                });

            }catch (IOException e){
                Log.e("IOException",e.toString());
            }
        }
    }

    String address="";
    String nro="";
    // Metodo que obtiene la direccion
    public void getAddress(Location location,Validation validator){
        Log.i("checkeoaddress", "se uso la api");
        // Se adjunta la longitude y latitude en la ruta
        String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                + location.getLatitude()+ "," + location.getLongitude()+"&key=AIzaSyDrHeUMhfaXrkIMZkvMS1LFL69JJBt-34E";

        okHttpRequest = new OkHttpRequest(URL);

        Log.i("URL_MAPS", URL);

        // Se coloca en el layout que se esta buscando direccion
        ((MapPresenter)listener).setAddressDirection("");

        // Se valida que esta conectado a internet
        if(validator.isOnline()) {

            try {
                // Metodo que realiza una peticion GET
                okHttpRequest.getNotParams(new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                    }
                    @Override
                    public void onResponse(Response response) throws IOException {

                        String res = response.body().string();

                        Log.i("Response Body", res);
                        try {
                            // Se convierte el cuerpo del mensaje
                            // y se convierte en un objeto JSON
                            JSONObject result = new JSONObject(res);
                            // Se verifica que tiene el campo "results"
                            Log.i("Estadovalor",result.getString("status"));

                                if (result.has("results")) {
                                    try {
                                        // Se obtiene el JSONArray a partir
                                        // del campo "results"
                                        JSONArray array = result.getJSONArray("results");
                                        // Se verifica que el array no este vacio
                                        if (array.length() > 0) {
                                            // Se obtiene el primer valor del array
                                            int miroutecheck=0;
                                            for (int checkcomponent = 0; checkcomponent < array.length(); checkcomponent++) {
                                                JSONObject placecheck = array.getJSONObject(checkcomponent);
                                                // Se obtiene el JSONArray a partir del campo "address_components"
                                                JSONArray componentscheck = placecheck.getJSONArray("address_components");
                                                for (int routecheck = 0; routecheck < componentscheck.length(); routecheck++) {
                                                    JSONObject componentcheck = componentscheck.getJSONObject(routecheck);
                                                    JSONArray typescheck = componentcheck.getJSONArray("types");
                                                    String typecheck = typescheck.getString(0);
                                                    if (typecheck.equals("route")) {
                                                        miroutecheck=checkcomponent;
                                                        routecheck=componentscheck.length();
                                                        checkcomponent=array.length();
                                                    }
                                                }
                                            }
                                            JSONObject place = array.getJSONObject(miroutecheck);
                                            // Se obtiene el JSONArray a partir del campo "address_components"
                                            JSONArray components = place.getJSONArray("address_components");
                                            // Se recorre el Array
                                            Log.i("checkeoaddress",String.valueOf(components.length()));
                                            for (int i = 0; i < components.length(); i++) {
                                                // Se obtiene el JSON a partir del JSONArray
                                                JSONObject component = components.getJSONObject(i);
                                                // Se obtiene el campo del JSONArray "types" y se guarda en otro
                                                // JSONArray
                                                JSONArray types = component.getJSONArray("types");
                                                // Se coge solo el primer elemento del JSONArray
                                                String type = types.getString(0);
                                                //Se verifica que el valor del type sea igual a "route"
                                                if (type.equals("route")) {
                                                    try {
                                                        // Se formatea el valor del address para UTF-8 para reconocer tildes
                                                        // y se guarda en un String
                                                        address = new String(component.getString("short_name").getBytes(), "UTF-8");

                                                    } catch (UnsupportedEncodingException e) {
                                                        e.toString();
                                                    }

                                                }
                                                if (type.equals("street_number")) {
                                                    try {
                                                        // Se formatea el valor del address para UTF-8 para reconocer tildes
                                                        // y se guarda en un String
                                                        nro = new String(component.getString("short_name").getBytes(), "UTF-8");

                                                    } catch (UnsupportedEncodingException e) {
                                                        e.toString();
                                                    }

                                                }

                                                // Se verifica que el campo no sea vacio
                                                if (!address.isEmpty()) {
                                                    // Se crea una tarea y se coloca en el hilo principal
                                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            // Se pasa el valor obtenido y se pasa
                                                            // al presentador
                                                            Log.i("checkeoaddress",address);
                                                            Log.i("checkeoaddress",nro);
                                                            ((MapPresenter) listener).setAddressDirection(address);
                                                            ((MapPresenter) listener).setAddressNro(nro);
                                                        }
                                                    });

                                                }
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }


                        } catch (JSONException e) {
                            Log.e("JSONArrayException", e.toString());
                        }

                    }
                });
            } catch (IOException e){}
        }else
            ((MapPresenter) listener).setAddressDirection("Hay problemas con tu conexion");
    }
    // Metodo que envia el "signIn"
    @Override
    public void signin() {
        // Se crea una tarea en el hilo principal
        new Handler(Looper.getMainLooper()).post(
                new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(App.getInstance(),"Conectando..",Toast.LENGTH_SHORT).show();
                        new Handler(Looper.getMainLooper()).post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        ((MapPresenter)listener).showStateConnected();
//                                        Toast.makeText(App.getInstance(),"Esta conectado..",Toast.LENGTH_LONG).show();
                                    }
                                }
                        );

                        // Se verifica que el socket esta conectado
                        if(socket.connected()) {

                            // Se emite un mensaje "signin" y se envia
                            // como parametros el email del pasajero
                            Log.i("Email_driver",User.loadEmail());
                            socket.emit("signin", User.loadEmail(), new Ack() {
                                @Override
                                public void call(Object... args) {
                                    // Se obtiene la respuesta
                                    final String res = (String) args[0];
                                    //final String socket_id = (String) args[1];
                                    // Y se verifica
                                    // que su valor sea "OK"
                                    if (res.equals("OK")) {
                                        // Se coloca un booleano en verdadero para indicar
                                        // que se envio correctamente el mensaje
                                        flag_signin = true;
                                    }
                                    else
                                        flag_signin = false;
                                }
                            });
                        }
                    }
                }
        );

    }

    //Se desactiva el socket ID
    @Override
    public void desactiveSocketInstance() {
        // Se desconecta el socket
        socket.disconnect();
        // Se desactiva los escuchadores
        socket.off(Socket.EVENT_CONNECT, connect);
        socket.off(Socket.EVENT_RECONNECTING, reconnecting);
        socket.off(Socket.EVENT_DISCONNECT, disconnect);
        socket.off(Socket.EVENT_ERROR, event_error);
        socket.off("error", error);
        socket.off("taxi location", taxi_location);
        socket.off("arrive", arrive);

    }

    @Override
    public void getDataPromotion(final String email, final Validation validator) {

        //Clase que implementa metodos para peticiones http
        OkHttpRequest okHttpRequest = new OkHttpRequest
                (App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_GETDATAUSER)+"?email="+email);

        //Se valida si es que hay conexion a internet
        if(validator.isOnline())
        {
            try {
                //Se hace un peticion GET sin parametros explicitos
                okHttpRequest.getNotParams(new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {
                        Log.i("AccountFailure", "Failure when get user data");
                    }
                    @Override
                    public void onResponse(Response response) throws IOException {
                        Log.i("AccountAccept","Get user data");
                        String res = response.body().string();
                        Log.i("Response Body", res);

                        Log.i("GTDX_Code",""+response.code());
                        // final User promotion = new user();

                        // Se obtiene los parametros del taxidriver desde
                        // el servidor
                        try {
                            JSONArray jsonArray = new JSONArray(res);
                            params = jsonArray.getJSONObject(0);


                            final String idpromotion         = params.getString("codigo");
                            final String totalcar          = params.getString("services_count");
                            final String situacion              = params.getString("situacion");
                            final String estado             = params.getString("bono");
                            Log.i("Response Body", idpromotion);
                            Log.i("Response Body", totalcar);
                            Log.i("Response Body", situacion);
                            Log.i("Response Body", estado);

                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onSuccessGetPromotionData(idpromotion, totalcar, situacion, estado);
                                }
                            });

                            Log.i("GDU_Account",params.toString());

                        }catch (JSONException e){
                            Log.e("JSONArrayException",e.toString());
                        }
                    }

                });
            }catch (IOException e)
            {
                Log.e("IOException",e.toString());
            }
        }

    }

    @Override
    public void validateRecepcionMensaje(JSONObject params, final Validation validator) {
        Log.i("mierrormensaje", " " + params);
        Log.i("mierrormensaje", " " + "inicio interactor");
        //Se valida si hay internet
        if(validator.isOnline()){

            //Se emite el mensaje de "arrive" al pasajero
            socket.emit("msg_usertotaxi", params, new Ack() {

                @Override
                public void call(Object... args) {
                    String response = (String)args[0];
                    Log.i("mierrormensaje", "response:"+response);
                    //Si es que es "OK" la respuesta entonces se envio correctamente
                    if(response.equals("OK")) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                ((MapPresenter) listener).onSuccessSend();
                            }
                        });


                    }else
                        //Si es que es "error" entonces hubo un error en el server
                        if(response.equals("error")){
                            new Handler(Looper.getMainLooper()).post(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            ((MapPresenter) listener).onFailedSend();
                                        }
                                    }
                            );
                        }
                }
            });

        }
        else
            ((MapPresenter) listener).onErrorConnection2();

    }

    @Override
    public void sendShareLocation(JSONObject params, final Validation validator) {
        //Se valida si hay internet
        if(validator.isOnline()){
            socket.emit("sendShareLocation", params, new Ack() {

                @Override
                public void call(Object... args) {
                    String response = (String)args[0];
                    Log.i("sendShareLocation", "response:"+response);
                    //Si es que es "OK" la respuesta entonces se envio correctamente
                    if(response.equals("OK")) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });


                    }else
                        //Si es que es "error" entonces hubo un error en el server
                        if(response.equals("error")){
                            new Handler(Looper.getMainLooper()).post(
                                    new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }
                            );
                        }
                }
            });

        }
        else
            ((MapPresenter) listener).onErrorConnection2();

    }

    @Override
    public void recibirShareLocation(JSONObject params, final Validation validator) {
        //Se valida si hay internet
        if(validator.isOnline()){
            socket.emit("recibirShareLocation", params, new Ack() {

                @Override
                public void call(Object... args) {
                    String response = (String)args[0];
                    Log.i("sendShareLocation", "response:"+response);
                    //Si es que es "OK" la respuesta entonces se envio correctamente
                    if(response.equals("OK")) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });


                    }else
                        //Si es que es "error" entonces hubo un error en el server
                        if(response.equals("error")){
                            new Handler(Looper.getMainLooper()).post(
                                    new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }
                            );
                        }
                }
            });

        }
        else
            ((MapPresenter) listener).onErrorConnection2();

    }
}
