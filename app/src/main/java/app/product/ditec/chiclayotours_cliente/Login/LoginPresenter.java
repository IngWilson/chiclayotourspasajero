package app.product.ditec.chiclayotours_cliente.Login;


public interface LoginPresenter {


    void validateCredentials(String username, String password);

    void validatePhone(String phone);

    boolean validateSession();

    boolean validateVerifyPhone();

    boolean validateRegister();

   boolean validatetoRegister();

    void validateUserTest();

}
