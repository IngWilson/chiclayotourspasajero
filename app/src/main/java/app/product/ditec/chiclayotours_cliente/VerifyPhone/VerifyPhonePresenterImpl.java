package app.product.ditec.chiclayotours_cliente.VerifyPhone;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 24/05/15.
 */
public class VerifyPhonePresenterImpl implements VerifyPhonePresenter, OnVerifyPhoneFinishedListener {

    VerifyPhoneInteractor interactor;
    Validation validator;
    VerifyPhoneView verifyPhoneView;
    Context ctx;
    User user;
    Bundle extras;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;


    public VerifyPhonePresenterImpl(VerifyPhoneView verifyPhoneView) {
        this.verifyPhoneView = verifyPhoneView;
        interactor = new VerifyPhoneInteractorImpl();
        ctx = verifyPhoneView.getContext();
        validator = new Validation(ctx);
        extras = ((Activity) ctx).getIntent().getExtras();
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    // Metodo que valida el telefono
    @Override
    public void validatePhone(String phone) {
        String email;

        // Metodo que muestra
        // un dialogo de progreso
        verifyPhoneView.showProgress();
        // Se verifica si es que los datos que se envian
        // desde la actividad no sean nulos
        if (extras != null)
            email = extras.getString("email");
        else
            email = User.loadEmail();

        Log.i("VP_email", email.toString());
        Log.i("VP_phone", phone.toString());

        // Se crea un objeto JSON
        JSONObject param = new JSONObject();

        // Se coloca los parametros del telefono y el email
        try {
            param.put("phone", phone);
            param.put("email", email);
            param.put("codpais", User.loadcodpais() );
        } catch (JSONException e) {
            Log.e("VP_JSONException", e.toString());
        }

        // Se envia los parametros para verificar el telefono
        interactor.sendPhone(param, validator, this);
    }

    // Metodo que muestra un dialogo indicando
    // que hay un error conexion
    @Override
    public void onInternetError() {
        verifyPhoneView.showDialogInternetError();
    }

    // Metodo que se ejecuta
    // cuando se envia correctamente el telefono
    @Override
    public void onSuccessSend(String phone) {

        // Metodo en preferences que indica
        // que se paso la actividad de la verificacion
        setOnVerify();

        // Se guarda en preferences
        // el usuario
      Log.i("codpais", User.loadcodpais());
        User.savePhone(User.loadcodpais() +" "+ phone);

        // Se coloca en preferences que 
        // ya no se encuentra en el registro
        setOnRegister();

        // Muestra un dialogo indicando que se envio correctamente
        // el telefono
        verifyPhoneView.showDialogSuccessSend();
        // Se oculta el dialogo de progreso
        verifyPhoneView.hideProgress();
        // Se navega a la actividad
        // y se envia como parametro el telefono
        verifyPhoneView.navigateToCode(phone);
        // Se finaliza la actividad de la verificacion 
        // del telefono
        verifyPhoneView.finished();

    }

    // Metodo que coloca en preferences el valor para indicar que 
    // no esta en el registro
    public void setOnRegister() {
        // Se pone en preferences un booleano para
        editor.putBoolean(Config.ONREGISTER, false);
        editor.commit();
    }

    // Metodo que coloca en preferences 
    // el valor para indicar que esta en la actividad
    // de la verificacion
    public void setOnVerify() {
        // Se pone en preferences un booleano
        editor.putBoolean(Config.ONVERIFY, true);
        editor.commit();
    }

    // Metodo que se ejecuta cuando hay un 
    // error al enviar
    @Override
    public void onFailedSend() {
        verifyPhoneView.showDialogFailedSend();
        verifyPhoneView.hideProgress();
    }

    // Metodo que se ejecuta cuando hay un 
    // error en el telefono
    @Override
    public void onPhoneError() {
        verifyPhoneView.showDialogPhoneError();
        verifyPhoneView.hideProgress();
    }

    // Metodo que se ejecuta cuando hay 
    // el campo del telefono esta vacio
    public void onEmptyPhoneError() {
        verifyPhoneView.showDialogEmptyPhone();
        verifyPhoneView.hideProgress();
    }

    // Metodo que se ejecuta cuando hay 
    // un error
    @Override
    public void onError() {
        verifyPhoneView.showDialogError();
        verifyPhoneView.hideProgress();
    }
}
