package app.product.ditec.chiclayotours_cliente.Account;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 05/05/15.
 */

public class AccountPresenterImpl implements AccountPresenter, OnAccountFinishedListener {

    AccountView accountView;
    AccountInteractor accountInteractor;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Validation validator;
    Context ctx;

    public AccountPresenterImpl() {
    }

    public AccountPresenterImpl(AccountView accountView) {
        this.accountView = accountView;
        this.accountInteractor = new AccountInteractorImpl();
        prefs = accountView.getContext().getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
        ctx = accountView.getContext();
        validator = new Validation(accountView.getContext());

        //Se verifica si el usuario esta activo o no
        if (checkIsable()) {
            //Y se coloca en disponible y la luz se pone en verde
            accountView.getImageIsable().setImageResource(R.mipmap.btn_verde);
            accountView.setEnabled("Disponible");
        } else {
            //En caso contrario se coloca la imagen de la luz en rojo y se coloca como no disponible
            accountView.getImageIsable().setImageResource(R.mipmap.btn_rojo);
            accountView.setEnabled("No disponible");
        }
    }
    //
    @Override
    public void validateGetData() {

        //Se obtiene el email del usuario desde el preferences
        String email    = User.loadEmail();

        String gcm      = User.loadGCM();

        Log.i("GCM_USER", gcm);

        //Se verifica que los datos ya han sido obtenidos en caso contrario 
        // se descargan los datos desde el servidor
        if (!checkData())
            accountInteractor.getDataUser(email, validator, this);

        //Se asignan los valores obtenidos desde el preferences 
        // del pasajero
        String first_name   = User.loadName();
        String last_name    = User.loadLastname();
        String phone        = User.loadPhone();
        Boolean isable      = User.loadIsable();

        Log.i("VGD_first_name", first_name);
        Log.i("VGD_last_name", last_name);
        Log.i("VGD_email", email);
        Log.i("VGD_phone", phone);
        Log.i("VGD_isable", "" + isable);

        //Se colocan los valores obtenidos
        // y se colocan en el layout
        accountView.setFirstName(first_name + " " + last_name);
        accountView.setLastName(last_name);
        accountView.setEmail(email);
        accountView.setPhoneNumber(phone);

        //Se oculta el dialogo de progreso
        accountView.hideProgress();

    }

    // Se verifica si los datos han sido obtenidos
    @Override
    public boolean checkData() {
        return prefs.getBoolean(Config.GETDATA, false);
    }

    // Meotod que se ejecuta cuando no hay conexion a internet
    @Override
    public void onInternetError() {
        accountView.showDialogInternetError();
    }

    @Override
    public void onError() {
    }

    // 
    public boolean checkIsable() {
        return User.loadIsable();
    }

    // Metodo que se ejecuta cuando
    // se obtiene correctamente los datos
    @Override
    public void onSuccessGetUserData(User user) {

        // Se guardan los datos en el preferences
        User.saveName(user.getName());
        User.saveLastname(user.getLastname());
        User.savePhone(user.getPhone());
        User.saveEmail(user.getEmail());
        User.saveIsable(user.isable());
        User.saveVerified(user.isVerified());

        // Se coloca en el preferences que se ha obtenido los datos correctamente
        editor.putBoolean(Config.GETDATA, true);
        editor.commit();
    }
}
