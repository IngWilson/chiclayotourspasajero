package app.product.ditec.chiclayotours_cliente.Peticion;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.ImageRefactor;
import app.product.ditec.chiclayotours_cliente.Model.Oferta;
import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

public class PeticionPresenterImpl implements PeticionPresenter,OnPeticionListener{
    PeticionView peticionView;
    Context ctx;
    PeticionInteractor interactor;
    Validation validation;
    private ImageLoader imageLoader;
    private ImageSize targetSize;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    public PeticionPresenterImpl(PeticionView _peticionView) {
        this.peticionView = _peticionView;
        ctx = peticionView.getContext();
        interactor = new PeticionInteractorImpl(this);
        validation = new Validation(peticionView.getContext());
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }
    public void cancelar(){
      //Se crea un objeto JSON y se pone el email del taxista y usuario
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ide", Request.loadId());
            jsonObject.put("email", User.loadEmail());
        } catch (JSONException e) {
            Log.e("JSONExceptionPresenter", e.toString());
        }

        interactor.cancelar(jsonObject);

    }
    @Override
    public void aceptaroferta(String emaildriver, String emailuser, String ide, Oferta oferta)
    {

        TaxiDriver.saveName(oferta.getName());
        TaxiDriver.savePhone(oferta.getPhone());
        TaxiDriver.saveAutoplate(oferta.getAutoplate());
        TaxiDriver.saveDNI(oferta.getDNI());
        TaxiDriver.saveCar_brand(oferta.getCar_brand());
        TaxiDriver.saveCompany("");
        TaxiDriver.saveGCM("");
        TaxiDriver.saveImageUrl(oferta.getImageUrl());
        TaxiDriver.saveEmail(oferta.getEmaildriver());
        Oferta.savePrecio(oferta.getPrecio());
        Log.i("FINALIZO", "FINALIZO EL RADAR");
        //Se finaliza la actividad del dialogo


        imageLoader = ImageLoader.getInstance();
        targetSize = new ImageSize(300, 80);
        //Se descarga la imagen en la URL del taxista
        imageLoader.loadImage(TaxiDriver.loadImageUrl(), targetSize, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                //Se oculta el imageView y se muestra un icono de progreso
               /* userDialogView.getImageTaxiDriver().setVisibility(View.GONE);
                userDialogView.getProgressBarImage().setVisibility(View.VISIBLE);*/
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage != null ) {
                    // Se guarda la imagen en el preferences codificandola a base 64
                    TaxiDriver.saveImage(ImageRefactor.encodeTobase64(loadedImage));
                    editor.putBoolean(Config.GETIMAGE, true);
                    editor.commit();
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });


        editor.putBoolean(Config.ONREQUEST, true);
        editor.commit();


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ide", ide);
            jsonObject.put("email_user", emailuser);
            jsonObject.put("email_driver", emaildriver);
            jsonObject.put("socket_driver", oferta.getSocketidtaxi());
            jsonObject.put("price", oferta.getPrecio());
            jsonObject.put("time", oferta.getTiempo());
        } catch (JSONException e) {
            Log.e("JSONExceptionPresenter", e.toString());
        }
        interactor.acceptoferta(jsonObject);

    }

    @Override
    public void rechazaroferta(String socketidtaxi){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emailuser", User.loadEmail());
            jsonObject.put("socket_driver", socketidtaxi);
        } catch (JSONException e) {
            Log.e("JSONExceptionPresenter", e.toString());
        }
        interactor.rechazaroferta(jsonObject);
    }
    @Override
    public void updatelist(Oferta oferta){
        boolean exist   = false;
        int position    = 0;

        //Se recorre toda la lista para verificar si hay un request igual al que se esta enviando
        for (int count = 0; count < peticionView.getLista().size(); count++) {

            //Se obtiene el email de cada request existente en la lista
            String email = peticionView.getLista().get(count).getEmaildriver();

            //Se verifica si es que hay un request con el mismo email que el request nuevo que se recibe
            if (email.equals(oferta.getEmaildriver())) {
                //Si es que hay un request igual entonces se coloca en true a exist y
                // se obtiene la posicion del request en la lista
                exist = true;
                position = count;
            } else
                exist = false;
        }

        //Si es que no existe entonces se añade un nuevo request
        // y si es que existe se actualiza el pedido en la lista usando su posicion
        if(!exist) {
             peticionView.addNewNotification(oferta);
        }
        else {

            peticionView.updateNotification(position, oferta);
        }
       /* peticionView.getLvNotificationAdapter()(listNotificationView.getListRequest());*/
        //Se notifica que los datos han sido modificados
        peticionView.updateList();
        peticionView.reproducirnewoferta();
    }

    public void aumentar(JSONObject aumento){
        interactor.aumentar(aumento);
    }

    @Override
    public void retirarofertataxista(String email_driver){

            //Recorre toda la lista en busca de un pedido que coincida con el email que se esta recibiendo
            for (int i = 0; i < peticionView.getLista().size(); i++) {
                // Si obtiene los email de cada pedido existentes y se verifica si coincide con el parametro "id"
                // Si es que coincide se procede a eliminarlo
                if ((peticionView.getLista().get(i)).getEmaildriver().equals(email_driver))
                    peticionView.deleteNotification(i);
            }

        //Se notifica que los datos ha sido modificados y se actualiza el listview
        peticionView.updateList();
    }
}
