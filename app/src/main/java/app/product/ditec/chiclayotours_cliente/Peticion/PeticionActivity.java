package app.product.ditec.chiclayotours_cliente.Peticion;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.Model.Oferta;
import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.R;

public class PeticionActivity  extends AppCompatActivity implements PeticionView {
    PeticionPresenter presenter;
    MediaPlayer mpnewoferta;
    ArrayList<Oferta> listoferta = new ArrayList<Oferta>();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    TextView txtValorOferta;
    private MiAdaptador adaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peticion);
        presenter = new PeticionPresenterImpl(this);
        recyclerView = findViewById(R.id.recycler_view);
        adaptador = new MiAdaptador(this,listoferta);
        recyclerView.setAdapter(adaptador);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        txtValorOferta=findViewById(R.id.txtValorOferta);
       txtValorOferta.setText((String)getIntent().getExtras().get("price"));
        Uri sonido = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sound_newoferta);
        mpnewoferta = MediaPlayer.create(this, sonido);
    }

    @Override
    public ArrayList<Oferta> getLista() {
        return listoferta;
    }

    @Override
    public void addNewNotification(Oferta oferta) {
        listoferta.add(oferta);

    }

    @Override
    public void updateNotification(int position, Oferta oferta) {
          listoferta.set(position, oferta);

    }

    @Override
    public void reproducirnewoferta(){
        mpnewoferta.start();
    }

    @Override
    public void updateList() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getLvNotificationAdapter().notifyDataSetChanged();
                recyclerView.invalidate();
                recyclerView.refreshDrawableState();
            }
        });

    }
    public MiAdaptador getLvNotificationAdapter() {
        return adaptador;
    }


    public void salirahora(View view){
        presenter.cancelar();
        finish();
    }

    public void aumentar(View view){

        JSONObject request = new JSONObject();
        try {
            request.put("price", txtValorOferta.getText().toString());
            request.put("note", (String)getIntent().getExtras().get("note"));
            request.put("email", (String)getIntent().getExtras().get("email"));
            request.put("latitude", (String)getIntent().getExtras().get("latitude").toString());
            request.put("longitude", (String)getIntent().getExtras().get("longitude").toString());
            request.put("latitudeB", Request.loadLatitudeB());
            request.put("longitudeB", Request.loadLongitudeB());
            request.put("to", (String)getIntent().getExtras().get("to"));
            request.put("exactprice", txtValorOferta.getText().toString());
            request.put("address", (String)getIntent().getExtras().get("address"));
            request.put("tipomovil", (String)getIntent().getExtras().get("tipomovil"));
            request.put("id", Request.loadId());
        } catch (JSONException e) {
            Log.e("VSR_JSONException", e.toString());
        }
        presenter.aumentar(request);

    }
    public void Decrementar(View view){
        Double precio=Double.parseDouble(txtValorOferta.getText().toString());
        precio=precio-0.5;
        txtValorOferta.setText(String.valueOf(precio));
    }
    public void Incrementar(View view){
        Double precio=Double.parseDouble(txtValorOferta.getText().toString());
        precio=precio+0.5;
        txtValorOferta.setText(String.valueOf(precio));

    }
    @Override
    public void salir()
    {
        presenter.cancelar();
        finish();
    }
    @Override
    public void aceptaroferta(String emaildriver, String emailuser, String ide, Oferta oferta)
    {
        presenter.aceptaroferta(emaildriver,emailuser,ide, oferta);
        Intent returnIntent = new Intent();
        setResult(3, returnIntent);
        finish();
    }

    @Override
    public void rechazaroferta(String socketidtaxi)
    {
        presenter.rechazaroferta(socketidtaxi);
    }
    @Override
    public void onBackPressed() {
        //Toast.makeText(context,"Se presiono a atras",Toast.LENGTH_LONG).show();
    }

    //Metodo para evitar que el dialogo se cierre cuando se presione afuera de su contorno
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);

        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public Context getContext() {
        return PeticionActivity.this;
    }

    @Override
    public void deleteNotification(int position) {
        listoferta.remove(position);
    }
}
