package app.product.ditec.chiclayotours_cliente.History;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.format.Time;
import com.activeandroid.query.Select;

import java.util.List;

import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * Created by Julio on 24/07/2015.
 */
public class HistoryActivity  extends AppCompatActivity{
    //declaras las variables de tipo ViewPager,TabLayout y Toolbar
    ViewPager viewPager;
    TabLayout tabLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        //inicializa la funcion settoolbar que es donde ponemos el actionbar
        setToolbar();

        //capturar ID de viewpager
        viewPager = (ViewPager) findViewById(R.id.Pager);

        //enlazar viewpager del layout con el adaptador del viewpager
        viewPager.setAdapter(new HistoryPageAdapter(getSupportFragmentManager(), HistoryActivity.this));
        //capturar ID de tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        //enlazar el viewpager con el tablayout
        tabLayout.setupWithViewPager(viewPager);

        Time date = new Time(Time.getCurrentTimezone());
        date.setToNow();

        viewPager.setCurrentItem(date.month, true);

        //ingresar logo perzonalizado.
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_historial);

    }

    public static List<Request> getmRequest(int month){
        return new Select()
                .from(Request.class)
                .where("month=?",month)
                .orderBy("day DESC")
                .orderBy("hour DESC")
                .orderBy("minute DESC")
                .execute();
    }
    //e captura el ID del toolbar
    public void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        //agregar icono y opciones de diseño al toolbar
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_launcher);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

}