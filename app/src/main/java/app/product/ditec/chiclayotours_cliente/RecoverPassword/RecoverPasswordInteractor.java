package app.product.ditec.chiclayotours_cliente.RecoverPassword;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by lenovo on 19/06/2015.
 */
public interface RecoverPasswordInteractor {

    void sendEmail(JSONObject jsonObject, Validation validator, OnRecoverPasswordFinishedListener listener);

}
