package app.product.ditec.chiclayotours_cliente.History;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;

import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.R;
/**
 * Created by Julio on 25/07/2015.
 */

//se creo una clase que extiende de baseadapter
public class HistoryAdapter extends BaseAdapter {
    private List<Request> history;

    /*creamos una variable de tipo viewholder para agrupar los datos
    de el elemento del listview*/
    ViewHolder holder;
    Context ctx;
    //creamos un contructor para la clase para enviar el context de la clase
    public HistoryAdapter(Context context) {
        super();
        ctx = context;
    }

    public void setData(List<Request> _history) {
        history = _history;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater mLayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = mLayoutInflater.inflate(R.layout.listview_history, null);
            holder = new ViewHolder();
            //captura los id de los items del elemento del listview para poder introducir los datos.
            holder.txtListName      = (TextView) view.findViewById(R.id.txtListName);
            holder.txtListAutoPlate = (TextView) view.findViewById(R.id.txtListAutoPlate);
            holder.txtListPhone     = (TextView) view.findViewById(R.id.txtListPhone);
            holder.txtListCompany   = (TextView) view.findViewById(R.id.txtListCompany);
            holder.txtListAddress   = (TextView) view.findViewById(R.id.txtListAddress);
            holder.txtListDate      = (TextView) view.findViewById(R.id.txtListDate);
            holder.txtprecioacordado = (TextView) view.findViewById(R.id.txtprecioacordadoh);
            //pongo el grupo de de item del elemento del listview en holder
            view.setTag(holder);

        } else
            holder = (ViewHolder) view.getTag();

        //obtengo la posicion del elemento del listview
        Request request = history.get(position);


        holder.txtListName.setText(request.getTaxiDriver().getName());
        holder.txtListPhone.setText(request.getTaxiDriver().getPhone());
        holder.txtListAutoPlate.setText(request.getTaxiDriver().getAutoplate());
        holder.txtListCompany.setText(request.getTaxiDriver().getCompany());
        holder.txtListAddress.setText(request.getAddress());
        holder.txtprecioacordado.setText(request.getPrecio());
        String fHour;
        String fMinute;

        if( request.getHour()>=0&&request.getHour() <10)
            fHour = "0"+request.getHour();
        else
            fHour=""+request.getHour();

        if( request.getMinute()>=0&&request.getMinute() <10)
            fMinute = "0"+request.getMinute();
        else
            fMinute=""+request.getMinute();

        holder.txtListDate.setText(request.getDay()+"/"+request.getMonth()+"/"+request.getYear()
                + " " + fHour +":"+fMinute);

        return view;
    }

    //creamos nuestra clase view holdler para agrupar los datos del elemento de listview
    private class ViewHolder {
        TextView txtListName;
        TextView txtListAutoPlate;
        TextView txtListPhone;
        TextView txtListAddress;
        TextView txtListCompany;
        TextView txtListDate;
        TextView txtprecioacordado;
    }

    //son metodos propios del adaptador que controlan la pocision de los elementos del listview
    @Override
    public int getCount() {
        return history.size();
    }
    @Override
    public Object getItem(int position) {
        return history.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
}