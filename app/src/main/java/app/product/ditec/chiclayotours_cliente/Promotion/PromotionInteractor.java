package app.product.ditec.chiclayotours_cliente.Promotion;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by usuario on 29/03/2016.
 */
public interface PromotionInteractor {
    void getDataPromotion(String email, Validation validator);

    void sendCode(JSONObject jsonObject, Validation validator, OnPromotionFinishedListener listener);
}
