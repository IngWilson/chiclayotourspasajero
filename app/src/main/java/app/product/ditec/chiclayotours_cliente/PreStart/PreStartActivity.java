package app.product.ditec.chiclayotours_cliente.PreStart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Publicidad.Publicidad;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * Created by Wilson on 26/05/2015.
 */
public class PreStartActivity extends Activity implements PrestartView {

    private PreStartPresenter presenter;

    private static NetworkImageView publicidadimg;
    private static RequestQueue colaPeticiones;
    private static ImageLoader lectorImagenes;

    private ImageView publiimg;
    private int c=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prestart);

/*        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setMax(maximumProgress());*/


        //Intento de enviar desde actividad a otra la imagen de publicidad

        /*colaPeticiones = Volley.newRequestQueue(this);
        lectorImagenes = new ImageLoader(colaPeticiones, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }
        });
        publicidadimg=(NetworkImageView) findViewById((R.id.publicidadimg));
        publicidadimg.setScaleType(ImageView.ScaleType.FIT_XY);
        publicidadimg.setImageUrl("http://files2.akitaxi.com/auspuser.jpg", lectorImagenes);*/

        //animacion logo akitaxi
/*        imageview= (ImageView) findViewById(R.id.imageView);
        ObjectAnimator animacion = ObjectAnimator.ofFloat(imageview, "translationX", 10, 1000);

        animacion.setDuration(1500);
        animacion.setInterpolator(new DecelerateInterpolator());
        animacion.setStartDelay(1500);
        ObjectAnimator animacion1 = ObjectAnimator.ofFloat(imageview, "alpha", 1, 0);
        animacion1.setDuration(2500);
        animacion1.setInterpolator(new DecelerateInterpolator());*/

        //  animacion.setRepeatCount(4);
       // animacion.setRepeatMode(ValueAnimator.REVERSE);

/*        AnimatorSet conjunto = new AnimatorSet();
        conjunto.play(animacion).with(animacion1);*/

        /*conjunto.start();*/
      //  animacion.start();



//Fin de ese intento


        //presenter = new PreStartPresenterImpl(this);
        //Usando getters y setters para ir cargando la imagen de publicidad
        publiimg=new ImageView(this);
        Picasso.with(this).invalidate("http://files2.akitaxi.com/auspuser.jpg");
        Picasso.with(this).load("http://files2.akitaxi.com/auspuser.jpg").memoryPolicy(MemoryPolicy.NO_CACHE) .networkPolicy(NetworkPolicy.NO_CACHE).into(publiimg);
        App.setPublicidad(publiimg);

        startAnimation();
    }

    //Se comienza la animacion
    @Override
    public void startAnimation() {

        //Se crea un contador de 3 segundos
        new CountDownTimer(2000, 2000) {

            @Override
            public void onTick(long millisUntilFinished) {
              //  progress.setProgress(restore(millisUntilFinished));
//Se habia realizado una animacion que ya no será necesaria
                /*if(c==1) {
                    imageview.setVisibility(View.GONE);
                    publicidadimg.setVisibility(View.VISIBLE);
                  *//*  ObjectAnimator animacion1 = ObjectAnimator.ofFloat(publicidadimg, "alpha", 0, 1);

                    animacion1.setDuration(2000);
                    //  animacion1.setStartDelay(2000);
                    animacion1.setInterpolator(new AccelerateInterpolator());
                    animacion1.start();*//*
                }
                c=c+1;*/
            }

            //Metodo que se ejecuta cuando termina
            @Override
            public void onFinish() {
             //   Toast.makeText(PreStartActivity.this, "se llamo al ontick: "+ c, Toast.LENGTH_SHORT).show();
                Intent newfrom = new Intent(PreStartActivity.this, Publicidad.class);
                startActivity(newfrom);
                finish();
            }
        }.start();

    }

    //
  /*  @Override
    public int restore(long millisecondss) {
        return (int) ((milliseconds - millisecondss) / 1000);
    }

    //
    @Override
    public int maximumProgress() {
        return seconds - delay;
    }*/
}
