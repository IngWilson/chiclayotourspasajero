package app.product.ditec.chiclayotours_cliente.Star;

import android.content.Context;

/**
 * Created by usuario on 14/07/2016.
 */
public interface StarView {

    Context getContext();
    void hideProgressE();
    void showProgressE();

    void finished();
    void showDialogError();
    void showDialogSuccessSend();
    void showDialogFailedSend();
    void showDialogInternetError();
}
