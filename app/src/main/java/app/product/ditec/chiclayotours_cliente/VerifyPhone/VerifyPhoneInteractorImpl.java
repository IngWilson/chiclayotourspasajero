package app.product.ditec.chiclayotours_cliente.VerifyPhone;

import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 24/05/15.
 */
public class VerifyPhoneInteractorImpl implements VerifyPhoneInteractor {

    //Metodo que envia el telefono
    @Override
    public void sendPhone(final JSONObject jsonObject, Validation validator, final OnVerifyPhoneFinishedListener listener) {

        // Metodo que verifica que 
        // esta conectado a internet
        if (validator.isOnline()) {

            OkHttpRequest request = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_GETCODE));

            // Valor booleano que indica que hay un error
            boolean error = false;
            try {
                // Se verifica que el telefono no este vacio si es que esta
                if (TextUtils.isEmpty(jsonObject.getString("phone"))) {
                    // Entoces se pone en true el error
                    error = true;
                    // Se ejecuta
                    listener.onEmptyPhoneError();
                }
            } catch (JSONException e) {
                Log.e("JSONException", e.toString());
            }

            // Si es que no hay un error
            if (!error) {
                try {
                    // Se realiza una peticion POST
                    // y se envia el parametros
                    request.post(jsonObject.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {

                            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onError();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            final int code = response.code();

                            Log.i("SP_Code", "" + code);

                            // Se obtiene el hilo principal y se crea una tarea
                            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    // Se verifica el codigo
                                    if (code == 200)
                                        try {

                                            Log.i("SP_phone", jsonObject.getString("phone"));
                                            Log.i("SP_phone", jsonObject.getString("codpais"));
                                            listener.onSuccessSend(jsonObject.getString("phone"));

                                        } catch (JSONException e) {
                                            Log.e("JSONException", e.toString());
                                        }
                                    else if (code == 500)
                                        listener.onPhoneError();
                                    else if (code == 503)
                                        listener.onFailedSend();
                                    else
                                        listener.onFailedSend();
                                }
                            });
                        }
                    });
                } catch (IOException e) {
                    Log.e("SP_IOException", e.toString());
                }
            }
        } else

            listener.onInternetError();
    }
}
