package app.product.ditec.chiclayotours_cliente.Star;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by usuario on 14/07/2016.
 */
public class StarPresenterImpl implements StarPresenter,OnStarFinishedListener {

    StarInteractor starInteractor;
    Validation validator;
    StarView starView;
    Context ctx;
    public StarPresenterImpl(){}
    //Envio de datos
    public StarPresenterImpl(StarView starView){
        this.starView=starView;
        this.starInteractor=new StarInteractorImpl(this);
        ctx=starView.getContext();
        validator=new Validation(starView.getContext());

    }


    @Override
    public void inputrating(String comment, String valor) {

        String email;

        // Metodo que muestra
        // un dialogo de progreso
        starView.showProgressE();
        // Se verifica si es que los datos que se envian
        // desde la actividad no sean nulos
        email = User.loadEmail();

        Log.i("VP_email", email.toString());


        // Se crea un objeto JSON
        JSONObject param = new JSONObject();

        // Se coloca los parametros del telefono y el email
        try {
            param.put("noteservice", comment);
            param.put("star", valor);
            param.put("email", email);
            Log.i("VP_param", param.toString());
        } catch (JSONException e) {
            Log.e("VP_JSONException", e.toString());
        }

        // Se envia los parametros para verificar el telefono
        starInteractor.sendnotestar(param, validator, this);

    }

    @Override
    public void onError() {
        starView.showDialogError();
        starView.hideProgressE();
    }
    @Override
    public void onSuccessSend() {

        starView.showDialogSuccessSend();
        // Se oculta el dialogo de progreso
        starView.hideProgressE();
        starView.finished();
    }

    @Override
    public void onFailedSend() {
        starView.showDialogFailedSend();
        starView.hideProgressE();
    }

    @Override
    public void onInternetError() {
        starView.showDialogInternetError();
    }
}