package app.product.ditec.chiclayotours_cliente.RadarDialog;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 20/04/15.
 */
public interface RadarDialogInteractor {

    void sendCancelRequest(Validation validator, JSONObject jsonObject, OnRadarFinishedListener listener);

    void validateRequest(final JSONObject jsonObject, final OnRadarFinishedListener listener, final Validation validator);

    void desactiveSocketInstance();

    void sendSignin();
}
