package app.product.ditec.chiclayotours_cliente.VerifyPhone;

/**
 * Created by angel on 24/05/15.
 */
public interface OnVerifyPhoneFinishedListener {

    void onError();

    void onInternetError();

    void onSuccessSend(String phone);

    void onFailedSend();

    void onPhoneError();

    void onEmptyPhoneError();
}
