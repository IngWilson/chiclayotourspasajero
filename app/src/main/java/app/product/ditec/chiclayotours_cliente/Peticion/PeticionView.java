package app.product.ditec.chiclayotours_cliente.Peticion;

import android.content.Context;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.Model.Oferta;

public interface PeticionView {
    Context getContext();
    ArrayList<Oferta> getLista();
    void addNewNotification(Oferta oferta);
    void updateNotification(int position, Oferta oferta);
   /* MiAdaptador getLvNotificationAdapter();*/
   void updateList();
    void salir();
    void aceptaroferta(String emaildriver, String emailuser, String ide, Oferta oferta);
    void rechazaroferta(String socketidtaxi);
    void deleteNotification(int position);
    void reproducirnewoferta();
}
