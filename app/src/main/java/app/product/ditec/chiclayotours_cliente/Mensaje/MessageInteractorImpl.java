package app.product.ditec.chiclayotours_cliente.Mensaje;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Validation;

public class MessageInteractorImpl implements MessageInteractor {
    Socket mSocket;
    MessagePresenter presenter;

    public MessageInteractorImpl(MessagePresenter _presenter){
        presenter=_presenter;
        mSocket = App.getSocket();
        //Se verifica que el socket este conectado en caso que no lo este, se conectara
        if(!mSocket.connected())
            mSocket.connect();
    }

    @Override
    public void sendArrive(JSONObject params, final Validation validator) {
        Log.i("mierrormensaje", " " + params);
        Log.i("mierrormensaje", " " + "inicio interactor");
        //Se valida si hay internet
        if(validator.isOnline()){

            //Se emite el mensaje de "arrive" al pasajero
            mSocket.emit("msg_usertotaxi", params, new Ack() {

                @Override
                public void call(Object... args) {
                    String response = (String)args[0];
                    Log.i("mierrormensaje", "response:"+response);
                    //Si es que es "OK" la respuesta entonces se envio correctamente
                    if(response.equals("OK")) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                ((OnMessageFinishedListener) presenter).onSuccessSend();
                            }
                        });


                    }else
                        //Si es que es "error" entonces hubo un error en el server
                        if(response.equals("error")){
                            new Handler(Looper.getMainLooper()).post(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            ((OnMessageFinishedListener)presenter).onFailedSend();
                                        }
                                    }
                            );
                        }
                }
            });

        }
        else
            ((OnMessageFinishedListener)presenter).onErrorConnection();

    }
}
