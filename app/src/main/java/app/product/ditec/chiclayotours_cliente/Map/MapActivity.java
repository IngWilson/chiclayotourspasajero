package app.product.ditec.chiclayotours_cliente.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.Account.AccountActivity;
import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.History.HistoryActivity;
import app.product.ditec.chiclayotours_cliente.Mensaje.MessageActivity;
import app.product.ditec.chiclayotours_cliente.Model.Oferta;
import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;
import app.product.ditec.chiclayotours_cliente.Peticion.PeticionActivity;
import app.product.ditec.chiclayotours_cliente.Promotion.PromotionActivity;
import app.product.ditec.chiclayotours_cliente.ShareLocationDialog.ShareLocationDialogActivity;
import app.product.ditec.chiclayotours_cliente.Star.starActivity;
import app.product.ditec.chiclayotours_cliente.Login.LoginActivity;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.PreRequest.PreRequestActivity;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.RadarDialog.RadarDialogActivity;
import app.product.ditec.chiclayotours_cliente.Sounds;
import app.product.ditec.chiclayotours_cliente.UserDialog.UserDialogActivity;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

//import com.actionbarsherlock.app.ActionBar;
//import com.actionbarsherlock.app.SherlockFragmentActivity;
//import com.actionbarsherlock.view.Menu;
//import com.actionbarsherlock.view.MenuItem;


// Clase actividad que implementa MapView, OnClickListener, OnMarkerDragListener
// OnCameraChangeListener
public class MapActivity extends AppCompatActivity implements MapView,
        DialogInterface.OnClickListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnCameraChangeListener,
            OnMapReadyCallback,NavigationView.OnNavigationItemSelectedListener
{
    //usado para verificar que no es la primera vez que se abre el mapa
    int aux=0;
    // Se declaran los dialogos
    private ProgressDialog progressDialog;
    private AlertDialog.Builder alertDialog;
    private AlertDialog.Builder dialogCancelRequest;

    // Se declaran el presentador
    private static MapPresenter presenter;

    // Se declara el mapa de google
    private GoogleMap googleMap;

    // Se declara el marcador del taxista
    private Marker locationTaxiMarker;

    // Se decalara el marcador del usuario
    private Marker myLocationMarker;
    // Se declara el objeto para los sonidos
    private Sounds sound;

    // variable que verifica que el mapa esta siendo tocado
    // o no
    public static boolean mMapIsTouched = false;

    // Se definen las inyecciones de vista
    // para los elementos de la interfaz
   // @InjectView(R.id.btnCallTaxi)
    static Button btnCallTaxi;

    static Button btnPedirTaxi;

    static Button btnPreofertar;

    static LinearLayout lineartheme;

    static RelativeLayout son_two;

    static SlidingUpPanelLayout slidingPanel;

    static RelativeLayout linearLayout;

    static LinearLayout linearnotemap;

    Button btnCancelTaxi;
    //button gift
    Button btnPresent;
    Button btnPedido;

    Button btnCallTaxiDriver;

    TextView txtprecioacordado;

    TextView txtTitle;
    TextView txtNameTaxiDriver;
    TextView txtEmailTaxiDriver;
    TextView txtAutoPlateTaxiDriver;
    TextView txtPhoneTaxiDriver;
    TextView txtModelAndBrandTaxiDriver;
    TextView txtCompanyTaxiDriver;
    ImageView imgTaxiDriver1;
    ImageView imgTaxiDriver2;
    ImageView imgMarker;

/*    @InjectView(R.id.imgMarker)
    ImageView imgMarker;*/
    private ImageView imagephoto;
    private TextView txtsaludo;

   /* @InjectView(R.id.edtNote)
    TextView edtNote;*/
    static EditText edtNote;
    static EditText edtexactprice;
    String itemtipomovil;
    TextView txtDistance;
    TextView txtTime;
    LinearLayout linearLayoutParameters;
    LinearLayout llConnection;
    LinearLayout botonesmap;

    LinearLayout llbasicoption;
    LinearLayout llofertaroptions;

    Button btnsendrequestpeticion;
    LinearLayout lldestino;
    LinearLayout llprecio;

    TextView txtStateConnection;
   // @InjectView(R.id.txtAddressCity)
    static EditText txtAddressCity;
    static EditText edto;
    static Spinner tipomovil;
   // @InjectView(R.id.edtNumber)
    static EditText edtNumber;
    Button btnOptions;

    int Iprequest=0;
    static MenuItem itemmensaje;
    String destino;


    /*
    @InjectView(R.id.txtAddressCity)
      TextView txtAddressCity;
    */
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    // Variable para la la latitude longitude
    LatLng centerOfMap;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    //Metodo que verifica que  el google playservices  esta actualizado
/*    @Override
    public boolean onCheckGooglePlayServices() {
        Log.i("checkplayservice","checkplayservice");
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getBaseContext());
        Log.i("checkplayservice",String.valueOf(resultCode));
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).
                        show();
            }
            return false;
        }
        return true;
    }*/


    public boolean onCheckGooglePlayServices() {
        Log.i("checkplayservice", "inicio");
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        Log.i("checkplayservice", String.valueOf(resultCode));
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("checkplayservice", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
            @Override
            public void onMapReady(GoogleMap _googleMap) {

                googleMap = _googleMap;

                if (googleMap != null) {
                    if (presenter.checkOnRequest()) {
                        try {
                            Log.i("LocationMarkerCurrent", User.loadLatitude()+ " " + User.loadLongitude());
                            addMyPositionMarker((double) User.loadLatitude(), (double) User.loadLongitude());
                            adjustZoomMap((double) User.loadLatitude(), (double) User.loadLongitude(), 15);
                        } catch (Exception e) {
                        }
                    }
                    //Activa la localizacion del pasajero

                        googleMap.setMyLocationEnabled(true);


                    googleMap.setOnMarkerDragListener(this);
                    //Se coloca la verificación constante que verifica la posición de la camara.
                    googleMap.setOnCameraChangeListener(this);
                    //Ajusta el Zoom del mapa cambiar segun la ciudad la latitud y longitude inicial
                    adjustZoomMap(0.0, 0.0, 15);
                    // Metodo que verifica que la localización del usuario no sea null
                    if (googleMap.getMyLocation() != null)
                        //Ajusta el zoom del mapa con la localizacion actual del usuario
                        adjustZoomMap(googleMap.getMyLocation().getLatitude(), googleMap.getMyLocation().getLongitude(), 15);
                    googleMap.getUiSettings().setMapToolbarEnabled(false);
                    //Paramos la animacion de la camara
                    googleMap.stopAnimation();
                }
                if (presenter.checkOnRequest()) {
                    slidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                else btnCancelTaxi.setVisibility(View.GONE);

            }


            // Metodo onCreate propio del ciclo de vida
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawerlayout);
        //intento de menu lateral

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

// Navigation Drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        /*drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() { @Override public void onClick(View v) {

            onBackPressed(); } });
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


//fin del intento
        btnCallTaxi = (Button) findViewById(R.id.btnCallTaxi);
        lineartheme=(LinearLayout)findViewById(R.id.lineartheme);
        botonesmap=(LinearLayout)findViewById(R.id.BotonesMap);
        son_two=(RelativeLayout)findViewById(R.id.son_two);
        slidingPanel=(SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
        linearLayout=(RelativeLayout)findViewById(R.id.linearLayout);

        linearnotemap=(LinearLayout)findViewById(R.id.linearNoteMap);
        btnPedirTaxi=(Button) findViewById(R.id.btnPedirTaxi);
        btnPreofertar=findViewById(R.id.btnpreofertar);
        btnCancelTaxi = (Button) findViewById(R.id.btnCancelTaxi);
        btnPresent = (Button) findViewById(R.id.btnPresent);

        edtNote = (EditText) findViewById(R.id.edtNote);
        edtexactprice=(EditText) findViewById(R.id.edtExactprice);
        tipomovil =(Spinner) findViewById(R.id.tipomovil1);
        tipomovil
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> arg0,
                                               View arg1, int position, long arg3) {
                        // TODO Auto-generated method stub
                        itemtipomovil= (String) tipomovil.getSelectedItem();
                        Log.i("vermirartipo",itemtipomovil);
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }
                });

        btnCallTaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callTaxi(null, null, null, null, null, null,null,null);
            }
        });
        btnPedirTaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // callOptions();
                callTaxi(null, null, null, null, null, null,null,null);
                restaurarbasicoption();
            }
        });
        txtAddressCity = (EditText) findViewById(R.id.txtAddressCity);
        edto=(EditText)findViewById(R.id.edtTo);

        edtNumber = (EditText) findViewById(R.id.edtNumber);


        // Metodo para colocar el actionbar personalisado en el proyecto akitaxi
        //para intentar que entre el nuevo toolbar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_bar);

        // Se instancia el progressDialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progress_map_send_message));

        // Se instancia el presentador
        presenter = new MapPresenterImpl(this);

        // Se instancian la alertas
        alertDialog = new AlertDialog.Builder(this);
        dialogCancelRequest = new AlertDialog.Builder(this);

        // Se instancia el sonido
        sound = new Sounds();

        // Se inyectan las inyecciones antes definidas
        btnCancelTaxi=findViewById(R.id.btnCancelTaxi);
        btnPedido=findViewById(R.id.btnPedido);
        btnCallTaxiDriver=findViewById(R.id.btnCallTaxiDriver);
        txtprecioacordado=findViewById(R.id.txtPrecioacordado);
        txtTitle=findViewById(R.id.txtTitle);
        txtNameTaxiDriver=findViewById(R.id.txtNameTaxiDriver);
        txtEmailTaxiDriver=findViewById(R.id.txtEmailTaxiDriver);
        txtAutoPlateTaxiDriver=findViewById(R.id.txtAutoPlateTaxiDriver);
        txtPhoneTaxiDriver=findViewById(R.id.txtPhoneTaxiDriver);
        txtModelAndBrandTaxiDriver=findViewById(R.id.txtModelAndBrandTaxiDriver);
        txtCompanyTaxiDriver=findViewById(R.id.txtCompanyTaxiDriver);
        imgTaxiDriver1=findViewById(R.id.imgTaxiDriver1);
        imgTaxiDriver2=findViewById(R.id.imgTaxiDriver2);
        imgMarker=findViewById(R.id.imgMarker);
        txtDistance=findViewById(R.id.txtDistance);
        txtTime=findViewById(R.id.txtTime);
        linearLayoutParameters=findViewById(R.id.linearLayoutParameters);
        llConnection=findViewById(R.id.llConnection);
        llbasicoption=findViewById(R.id.llbasicoption);
        llofertaroptions=findViewById(R.id.llofertaroptions);
        btnsendrequestpeticion=findViewById(R.id.btnSendRequestPeticion);
        lldestino=findViewById(R.id.lldestino);
        llprecio=findViewById(R.id.llprecio);
        txtStateConnection=findViewById(R.id.txtStateConnection);
        btnOptions=findViewById(R.id.btnOptions);

        txtAddressCity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Log.i("destinofinal","LatA: "+ Request.loadLatitudeA() + " LonA: "+ Request.loadLongitudeA());
                    Log.i("destinofinal","LatB: "+ Request.loadLatitudeB() + " LonB: "+ Request.loadLongitudeB());
                    imgMarker.setImageResource(R.mipmap.marker);
                    if(googleMap!=null) {
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(Request.loadLatitudeA(), Request.loadLongitudeA())).zoom(15).build();
                        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }
            }
        });

        edto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Log.i("destinofinal","LatA: "+ Request.loadLatitudeA() + " LonA: "+ Request.loadLongitudeA());
                    Log.i("destinofinal","LatB: "+ Request.loadLatitudeB() + " LonB: "+ Request.loadLongitudeB());
                    float milatitudb=Request.loadLatitudeB();
                    float milongitudb=Request.loadLongitudeB();
                    if(milatitudb==0)
                    {
                        milatitudb=(float)(googleMap.getCameraPosition().target.latitude+0.001);
                        milongitudb=(float)(googleMap.getCameraPosition().target.longitude+0.001);
                    }
                    imgMarker.setImageResource(R.mipmap.markerdestino);
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            //.target(new LatLng(Request.loadLatitudeB(), Request.loadLongitudeB())).zoom(15).build();
                            .target(new LatLng(milatitudb, milongitudb)).zoom(15).build();
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            }
        });
        // Se coloca la tipografia
        setTypeFace();

        // Se obtiene el mapa
        getGoogleMap();

        // Se llama al metodo para validar lo datos
        presenter.validateGetUserData();
        // Se coloca las propiedades a la alerta
        alertDialog.setCancelable(false);
        progressDialog.setCancelable(false);
        btnCancelTaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTaxi();
            }
        });

        btnPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startpresent();
            }
        });

        //Metodo que verifica  que  verifica que esta en pleno pedido
        if (presenter.checkOnRequest()) {
            //Activa  la verificacion constante

            if(presenter.getTravelingState()){
                presenter.stopVerifyRequestState();
                presenter.checkUpdateRequest();
                changeEventCancelRequest();
                changeTextBtnCancel("FINALIZAR");
            }


        }
        //Metodo propio que te permite mantener prendido la pantalla del celular mientras estas dentro de la aplicacion
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        //probando sharedpreferences para ver su contenido
        prefs = this.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
        presenter.validateGetData();


        //poniendo foto
        View headerLayout = navigationView.getHeaderView(0);
        imagephoto = (ImageView) headerLayout.findViewById(R.id.imageViewnav);
        txtsaludo = (TextView) headerLayout.findViewById(R.id.textViewSaludo);
        /*Log.i("verfoto",User.loadphoto());
        imagephoto.setImageDrawable(imgMarker.getDrawable());*/
        //imgMarker.setImageDrawable(imgTaxiDriver2.getDrawable());
        if(!User.loadphoto().equals(""))
//
            ponerFoto(imagephoto, User.loadphoto());
        if(!User.loadName().equals(""))
            txtsaludo.setText("Bienvenido "+User.loadName());


    }
    //añadido para ingresar la foto --> repetido en account activitiy
    protected void ponerFoto(ImageView imageView, String uri) {
        if (uri != null) {
           imageView.setImageBitmap(reduceBitmap(this, uri, 1024, 1024));
           // imageView.setImageURI(Uri.parse(uri));
        } else {
            imageView.setImageBitmap(null);
        }
    }
    public static Bitmap reduceBitmap(Context contexto, String uri,
                                      int maxAncho, int maxAlto) {
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(contexto.getContentResolver()
                    .openInputStream(Uri.parse(uri)), null, options);
            options.inSampleSize = (int) Math.max(
                    Math.ceil(options.outWidth / maxAncho),
                    Math.ceil(options.outHeight / maxAlto));
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(contexto.getContentResolver()
                    .openInputStream(Uri.parse(uri)), null, options);
        } catch (FileNotFoundException e) {
            Toast.makeText(contexto, "Fichero/recurso no encontrado",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        }
    }
    //fin de poner foto

            @SuppressWarnings("StatementWithEmptyBody")
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
/*                if (id == R.id.nav_todos) {
                    // …
                } else if (id == R.id.nav_epico) {
                    // …
                } else if (id == R.id.nav_XIX) {
                }*/

                switch (item.getItemId()) {
                    case R.id.nav_about:
                        // Se muestra un dialogo de alerta
                        // que muestra la version
                        AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
                        alertDialogAbout.setMessage(getString(R.string.vercion) + presenter.getAliasCode() +" "+
                                getString(R.string.marca))
                                .setTitle(getString(R.string.dialog_about_us_title))
                                .setPositiveButton("Aceptar", null)
                                .show();
                        break;
                    case R.id.nav_account:
                        // Llama a la actividad de la cuenta
                        startActivity(new Intent(this, AccountActivity.class));
                        break;
                    case R.id.nav_share_location:
                        // Llama a la actividad de la cuenta
                        startActivity(new Intent(this, ShareLocationDialogActivity.class));
                        break;
                    case R.id.nav_share:
                        if(promotext==null)
                        {
                            presenter.validateGetData();
                        }
                        // Se crear un intent para los mensajes
                        share();
                        break;
                    case R.id.llamado_central:
                        presenter.callcentral();
                        break;
                    case R.id.nav_history:
                        // Se crea un intent para iniciar
                        // el historial
                        Intent historyIntent = new Intent(this, HistoryActivity.class);
                        startActivity(historyIntent);
                        break;
/*                    case R.id.nav_promotion:
                        Intent promotionIntent = new Intent(this, PromotionActivity.class);
                        promotionIntent.putExtra("promo","nopresent");
                        startActivity(promotionIntent);
                        break;*/
                    case R.id.nav_logout:
                        // Se verifica que no este en pleno pedido
                        if (presenter.checkOnRequest())
                            // En caso de que este en pleno pedido
                            // se muestra un mensaje indicando que se tiene que cancelar el pedido antes
                            // de deslogearse
                            Toast.makeText(this, getString(R.string.dialog_logout_fail_message), Toast.LENGTH_LONG).show();
                        else {
                            // Se resetean todos los parametros del preferences
                           // presenter.resetParams();
                            // Se redirige al login
                            //navigateToLogin();
                            // Se finaliza la actual actividad
                            finish();
                        }
                        break;
                    default:
                        break;
                }


                // …
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }

            @Override
            public void onBackPressed() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {

                    if(checkpreofertar==1)
                    {
                        restaurarbasicoption();
                    }
                    else
                    {
                        super.onBackPressed();
                    }
                }
            }


    //Metodo para obtener el mapa
    public void getGoogleMap(){
        //  Es un seleccionador para ver si GooglePlayServices esta actualizado
        if(onCheckGooglePlayServices()) {
            //Obteniendo el mapa
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView)).getMapAsync(this);
            //Verificamos que el mapa no sea nulo

        }
    }
    //Metodo para obtener las fuentes que utilizaremos en la aplicacion
    public void setTypeFace(){

        Typeface typeFaceRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        edtNote.setTypeface(typeFaceRoboto);
        btnCallTaxi.setTypeface(typeFaceRoboto);
        btnCancelTaxi.setTypeface(typeFaceRoboto);
        btnPedirTaxi.setTypeface(typeFaceRoboto);
        btnPedido.setTypeface(typeFaceRoboto);
        btnCallTaxiDriver.setTypeface(typeFaceRoboto);
        txtTitle.setTypeface(typeFaceRoboto);
        txtNameTaxiDriver.setTypeface(typeFaceRoboto);
        txtAutoPlateTaxiDriver.setTypeface(typeFaceRoboto);
        txtPhoneTaxiDriver.setTypeface(typeFaceRoboto);
        txtModelAndBrandTaxiDriver.setTypeface(typeFaceRoboto);
        txtCompanyTaxiDriver.setTypeface(typeFaceRoboto);
        txtprecioacordado.setTypeface(typeFaceRoboto);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Se verifica que esta en pleno pedido
        if (presenter.checkOnRequest())
            // Se remueve la verificacion constante
            // en caso de que este en pleno pedido
            presenter.removeCheckUpdateRequest();
//        presenter.desactiveSocket();

    }

    //Metodo que coloca la direccion en el layout
    @Override
    public void setAddressLocation(String address) {

//        Toast.makeText(this,"TxtAddressLocation : " +address,Toast.LENGTH_SHORT).show();
        Log.i("TxtAddressLocation : ",address);


        if(edto.hasFocus())
        {
            Log.i("destinofinal : ","destino");
                destino = address;
                Request.saveLatitudeB(Request.loadLatitude());
                Request.saveLongitudeB(Request.loadLongitude());

        }
        else {
            Log.i("destinofinal : ","origen");
            txtAddressCity.setText(address);
            Request.saveLatitudeA(Request.loadLatitude());
            Request.saveLongitudeA(Request.loadLongitude());
        }
    }

            @Override
            public void setNumber(String nro) {

                if(edto.hasFocus())
                {
                        edto.setText(destino + ", " + nro);
                }
                else
                    edtNumber.setText(nro);


            }
            public void btnusemap(View view){
                usarmap();
            }

            public void usarmap()
            {
                edto.requestFocus();
                getmidireccion();
            }


    //Metodo  que verifica la posicion de la camara constantemente
    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        /*Log.i("mifoco", "antes de semaforo oncamerachange");
        if (semaforocamera == 0) {
            Log.i("mifoco", "despues de semaforo oncamerachange");
            semaforocamera = 1;
            handlerstopcamera.removeCallbacks(runstopcamera);
            runstopcamera = new Runnable() {
                @Override
                public void run() {*/
                getmidireccion();

             /*   }

            };
            handlerstopcamera.postDelayed(runstopcamera, 500);
            semaforocamera = 0;
        }*/

    //Obtenemos la posicion centrar del mapa

    }

    public void getmidireccion(){
        centerOfMap = googleMap.getCameraPosition().target;
        //Se verifica que no se esta en pleno pedido
        if (!presenter.checkOnRequest()) {
            //Se coloca la posicion alctual del usuario en el Centro del mapa
            presenter.setMyCurrentLocation(centerOfMap.latitude, centerOfMap.longitude);
            //Se verifica que el mapa no esta siendo tocado
            if (!mMapIsTouched)
            // Si no esta siendo tocado entonces se obtiene
            // la direccion
            {
                Log.i("checktouch","se solicito por detención de touch");
                presenter.getAddressDirection();
            }
        }
    }
    //Metodo  que muestra un puto dialogo que dice "Aun no se ha encontrado su direccion"
    @Override
    public void showDialogAddressNotFound() {
        Toast.makeText(this, getString(R.string.map_direction_gps), Toast.LENGTH_SHORT).show();
        //callOptions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //noinspection SimplifiableIfStatement
        // getString(R.string.dialog_logout_fail_message)
        switch (item.getItemId()) {
            /*case R.id.about:
                // Se muestra un dialogo de alerta
                // que muestra la version
                AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
                alertDialogAbout.setMessage(getString(R.string.vercion) + presenter.getAliasCode() +" "+
                        getString(R.string.marca))
                        .setTitle(getString(R.string.dialog_about_us_title))
                        .setPositiveButton("Aceptar", null)
                        .show();
                break;*/
            /*case R.id.account:
                // Llama a la actividad de la cuenta
             //   startActivity(new Intent(this, AccountActivity.class));
                startActivityForResult(new Intent(this, AccountActivity.class), 2);
                break;*/
            case R.id.messagetotaxi:
                Intent messajeintent = new Intent(this, MessageActivity.class);
                startActivity(messajeintent);
                break;
            case R.id.share:
                // Se crear un intent para los mensajes
                if(promotext==null)
                {
                    presenter.validateGetData();
                }
               share();
                break;
            /*case R.id.history:
                // Se crea un intent para iniciar
                // el historial
                Intent historyIntent = new Intent(this, HistoryActivity.class);
                startActivity(historyIntent);
                break;*/
            /*case R.id.spinner:
                // Se crea un intent para iniciar
                // el historial
                //Intent spinner = new Intent(this, SPINNER.class);
                startActivity(spinner);
                break;*/
/*
/*            case R.id.promotion:
                Intent promotionIntent = new Intent(this, PromotionActivity.class);
                promotionIntent.putExtra("promo","nopresent");
                startActivity(promotionIntent);
                break;*/

            /*case R.id.logout:
                // Se verifica que no este en pleno pedido
                if (presenter.checkOnRequest())
                    // En caso de que este en pleno pedido
                    // se muestra un mensaje indicando que se tiene que cancelar el pedido antes
                    // de deslogearse
                    Toast.makeText(this, getString(R.string.dialog_logout_fail_message), Toast.LENGTH_LONG).show();
                else {
                    // Se resetean todos los parametros del preferences
                    presenter.resetParams();
                    // Se redirige al login
                    navigateToLogin();
                    // Se finaliza la actual actividad
                    finish();
                }
                break;*/
            default:
                break;
        }
        return true;
    }

    // ESTE METODO NO SE UTILIZA
    @Override
    public void onMarkerDragEnd(Marker marker) {

        //TODO: Verificar si funciona correctamente si se le quita esto
//        presenter.setMyCurrentLocation(marker.getPosition().latitude, marker.getPosition().longitude);
        Log.i("LocationMarkerDragEnd", marker.getPosition().latitude + " " + marker.getPosition().longitude);
    }

    // ESTE METODO NO SE UTILIZA
    @Override
    public void onMarkerDragStart(Marker marker) {
        Log.i("LocationMarkerDragStart", marker.getPosition().latitude + " " + marker.getPosition().longitude);
    }

    // ESTE METODO NO SE UTILIZA
    @Override
    public void onMarkerDrag(Marker marker) {
        Log.i("LocationMarkerDrag", marker.getPosition().latitude + " " + marker.getPosition().longitude);
    }

    //Metodo que captura la posicion del marcador
    @Override
    public LatLng getLocationMarker() {
        return new LatLng(myLocationMarker.getPosition().latitude, myLocationMarker.getPosition().longitude);
    }
    //Muestra un dialogo de progreso
    @Override
    public void showProgress() {
        progressDialog.show();
    }
    //Oculat un dialogo
    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }
    //Mostrar el dialogo del radar al usuario
    @Override
    public void showUserDialog() {
        User.saveLongitude((float) myLocationMarker.getPosition().longitude);
        User.saveLatitude((float) myLocationMarker.getPosition().latitude);
        startActivity(new Intent(this, RadarDialogActivity.class));
    }

    // Metodo que muestra el error al emviar el request mediante un dialogo
    @Override
    public void showRequestErrorDialog() {
        alertDialog.setTitle(getString(R.string.dialog_map_title_request_error))
                .setMessage(getString(R.string.dialog_map_message_request_error))
                .setPositiveButton(getString(R.string.accept), this)
              //  .setPositiveButton(getString(R.string.accept), this)
                .show();
    }

    //Muestra un dialogo al terminar el pedido.
    @Override
    public void showEndServiceDialog() {

        alertDialog.setTitle(getString(R.string.dialog_map_title_end_service))
                .setMessage(getString(R.string.dialog_map_message_end_service))
                .setPositiveButton(getString(R.string.accept), this)
                .show();
    }

    //Muestra un dialogo
    @Override
    public void showCancelErrorDialog() {
        alertDialog.setTitle(getString(R.string.dialog_map_title_cancel_error))
                .setMessage(getString(R.string.dialog_map_message_cancel_error))
                .setPositiveButton("Aceptar", this)
                .show();
    }
    // Muestra un dialogo donde se encuentra la vercion actual de la aplicacion.
    @Override
    public void showDialogInstallVersion() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false)
                .setTitle(getString(R.string.dialog_GPS_error))
                .setMessage(getString(R.string.dialog_update_message))
                .setPositiveButton(getString(R.string.dialog_update_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                                .setData(Uri.parse("market://details?id=app.product.ditec.chiclayotours_cliente"));
                        startActivity(goToMarket);
                    }
                })
                .show();
    }
    //Metodo que añade el marcador del taxista en el mapa
    @Override
    public void addLocationTaxiMarker(Double latitude, Double longitude, Double bearing) {
        locationTaxiMarker = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title("Mi taxi")
                .rotation(bearing.floatValue())
                .anchor((float) 0.5, (float) 0.5)
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi)));
    }
    //Metodo añade el marcado del pasajero en el mapa
    @Override
    public void addMyPositionMarker(Double latitude, Double longitude) {
        Log.i("LocationMarkerCurrent", latitude+ " " + longitude);
        myLocationMarker = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title("Mi posicion")
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker)));
        //Actualiza la poscicion del pasajero con respecto al marcador
        presenter.setMyCurrentLocation(myLocationMarker.getPosition().latitude, myLocationMarker.getPosition().longitude);


    }
    //Metodo que remueve el marcador del taxista
    @Override
    public void removeTaxiMarker() {
        locationTaxiMarker.remove();
    }
    //Metodo que actualiza los datos el taxista por defecto
    @Override
    public void clean() {

        Log.i("Flow_Data-Clean", "Successfully clean data taxidriver");

        enabledNote();
        //Activa llamar taxi
        enabledCallTaxi();
        enabledoptions();
        //Desactiva el cancel del
        disabledCancelTaxi();

        setTitle(getString(R.string.slide_title_data_taxidriver));
        setNameTaxiDriver(getString(R.string.map_name));
        setModelAndBrandCar(getString(R.string.map_model));
        setAutoPlateTaxiDriver(getString(R.string.map_placa));
        setPhoneTaxiDriver(getString(R.string.map_phone));
        setCompany(getString(R.string.map_empresa));
        setDistance("");
        setTime("");
        getImgTaxiDriver1().setImageResource(R.mipmap.ic_action_user);
        getImgTaxiDriver2().setImageResource(R.mipmap.ic_action_user);

        getTxtDistance().setVisibility(View.INVISIBLE);
        getTxtTime().setVisibility(View.INVISIBLE);
        getLayoutParameters().setVisibility(View.INVISIBLE);
        getImgMarker().setVisibility(View.VISIBLE);

    }
    //Metodo que hace sonar cuando el taxista esta cerca
    @Override
    public void blareSound() {
        Uri sonido = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.son_notif);
        sound.sendNotification(getString(R.string.map_taxi), sonido, this);
    }


    public void startpresent() {
        Intent i = new Intent(this, PromotionActivity.class);
        i.putExtra("promo","present");
        startActivity(i);
    }

    public void callOptions(View view){
                //if(!txtAddressCity.getText().toString().equals("Buscando direccion...")){
                    Intent i = new Intent(this, PreRequestActivity.class);
                    i.putExtra("address",txtAddressCity.getText().toString());
                    i.putExtra("note",edtNote.getText().toString());
                    i.putExtra("number",edtNumber.getText().toString());
                    i.putExtra("latitude",centerOfMap.latitude);
                    i.putExtra("longitude",centerOfMap.longitude);
                    Iprequest=1;
                    startActivity(i);
                /*}else {
                    Toast.makeText(MapActivity.this, "Espere a que se encuentre su ubicación", Toast.LENGTH_SHORT).show();
                }*/
    }

    public void btnofertarprecio(View view){
        if(edtexactprice.getText().toString().isEmpty())
        {
            AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
            alertDialogAbout.setMessage("Debe ingresar un precio de carrera para proponer su Oferta a los taxistas")
                    .setTitle("Ingrese su Oferta")
                    .setPositiveButton("Aceptar", null)
                    .show();
        }
        else if(edto.getText().toString().isEmpty())
        {
            AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
            alertDialogAbout.setMessage("Debe Ingresar un destino para proponer su Oferta a los taxistas")
                    .setTitle("Ingrese su Destino")
                    .setPositiveButton("Aceptar", null)
                    .show();
        }
        else if(Double.parseDouble(edtexactprice.getText().toString())<4)
        {
            AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
            alertDialogAbout.setMessage("El valor de la carrera no puede ser menor a 4 soles")
                    .setTitle("Aumente su oferta")
                    .setPositiveButton("Aceptar", null)
                    .show();
        }
        else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapActivity.this);
            alertDialog.setTitle(getString(R.string.dialog_title))
                    .setCancelable(false)
                    .setMessage("Se enviara su oferta con punto de recojo en "+txtAddressCity.getText().toString()+" "+ edtNumber.getText().toString()
                            +" con destino a "+ edto.getText().toString() + " siendo su oferta "+ edtexactprice.getText().toString() + " soles ¿Es correcto ello?")

                    .setPositiveButton(getString(R.string.dialog_accept), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            enviaroferta();
                            dialog.dismiss();

                        }
                    })
                    .setNegativeButton("No",null)
                    .show();
        }
    }

    public void enviaroferta(){
        itemtipomovil = (String) tipomovil.getSelectedItem();
        // callTaxi(edtNote.getText().toString(), txtAddressCity.getText().toString(), edtNumber.getText().toString(), edto.getText().toString(), edtexactprice.getText().toString(), edtexactprice.getText().toString(), itemtipomovil);
        presenter.validatesendOferta(edtNote.getText().toString(), txtAddressCity.getText().toString(), edtNumber.getText().toString(), edto.getText().toString(), edtexactprice.getText().toString(), edtexactprice.getText().toString(), itemtipomovil);

    }

    @Override
    public void navigateToPeticion(){
        Intent i = new Intent(this, PeticionActivity.class);
        //parametros a enviar
        i.putExtra("price", edtexactprice.getText().toString());
        i.putExtra("note", edtNote.getText().toString());
        i.putExtra("email",User.loadEmail());
        i.putExtra("latitude", presenter.getMyCurrentLocation().getLatitude());
        i.putExtra("longitude", presenter.getMyCurrentLocation().getLongitude());
        i.putExtra("to", edto.getText().toString());
        i.putExtra("exactprice", edtexactprice.getText().toString());
        i.putExtra("address",txtAddressCity.getText().toString()+" "+ edtNumber.getText().toString());
        i.putExtra("tipomovil", itemtipomovil);
        startActivityForResult(i, 1);
        restaurarbasicoption();
    }
    //Metodo que envia el Request
/*    @OnClick(R.id.btnCallTaxi)
    public void callTaxi() {
        disabledCallTaxi();
        presenter.validateSendRequest(edtNote.getText().toString(), txtAddressCity.getText().toString(), edtNumber.getText().toString());
    }*/
   // @OnClick(R.id.btnCallTaxi)
            public static void callTaxi(String note, String address, String number, String to,String price, String exactprice, String tipomovil2, String grupo) {
                disabledCallTaxi();

                if(to!=null&&price!=null&&note!=null&&address!=null&&number!=null)
                    if(exactprice!=null)
                        presenter.validateSendRequest(note, address, number,to,price,exactprice,tipomovil2,tipomovil2);
                    else
                        presenter.validateSendRequest(note, address, number,to,price,null,tipomovil2, tipomovil2);
                else  {
                    String itemtipomovil2 = (String) tipomovil.getSelectedItem();
                    presenter.validateSendRequest(edtNote.getText().toString(), txtAddressCity.getText().toString(), edtNumber.getText().toString(),null,null,null, itemtipomovil2, itemtipomovil2);}
            }

    //Metodo qe cancela el pedido y quita el dialogo del radar
//    @OnClick(R.id.btnCancelTaxi)
    public void cancelTaxi() {
        disabledCancelTaxi();
        showDialogCancelRequest();
    }
    //Metodo  que llama al taxista cuando precionas llamar taxista

    public void callTaxiDriver() {
        presenter.callTaxiDriver();
    }
    public void callcentral(View view){
        presenter.callcentral();
    }
    int checkpreofertar=0;
    public void preofertar(View view){
        if(checkpreofertar==0) {
            checkpreofertar = 1;
            btnPedirTaxi.setVisibility(View.GONE);
           // llbasicoption.setVisibility(View.GONE);
            //llofertaroptions.setVisibility(View.VISIBLE);
            btnsendrequestpeticion.setVisibility(View.VISIBLE);
            botonesmap.setVisibility(View.GONE);
            lldestino.setVisibility(View.VISIBLE);
            llprecio.setVisibility(View.VISIBLE);
           // btnPreofertar.setText("<- VOLVER");
            btnPreofertar.setVisibility(View.GONE);
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams)lineartheme.getLayoutParams();
            layoutParams2.weight = 1.3f;
            lineartheme.setLayoutParams(layoutParams2);
            usarmap();
            imgMarker.setImageResource(R.mipmap.markerdestino);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)imgMarker.getLayoutParams();
            params.setMargins(0,0,0,60);
            imgMarker.setLayoutParams(params);
        }
        else
        {
            restaurarbasicoption();
        }
    }

    public void callTaxista(View view){
        //Se crea un intent y se coloca el telefono ademas se indica el tipo de intent y se ejecuta
        String phone = TaxiDriver.loadPhone();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phone));
        //intent.setData(Uri.parse("tel:" + costumer.getPhone().toString()));
        // intent.setData(Uri.parse("tel:" + request.getCostumer().getPhone()));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try{
            //Si es que el mapa es igual a null
            startActivity(intent);
        }
        catch (Exception e){
            Toast.makeText(this, "No se pudo realizar la llamada", Toast.LENGTH_SHORT).show();
        }
    }

    void restaurarbasicoption(){
        checkpreofertar=0;
        btnPedirTaxi.setVisibility(View.VISIBLE);
        //llbasicoption.setVisibility(View.VISIBLE);
       // llofertaroptions.setVisibility(View.GONE);
        btnsendrequestpeticion.setVisibility(View.GONE);
        botonesmap.setVisibility(View.VISIBLE);
        lldestino.setVisibility(View.GONE);
        llprecio.setVisibility(View.GONE);
      //  btnPreofertar.setText("OFERTE PRECIO");
        btnPreofertar.setVisibility(View.VISIBLE);
        txtAddressCity.requestFocus();
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams)lineartheme.getLayoutParams();
        layoutParams2.weight = 1f;
        lineartheme.setLayoutParams(layoutParams2);
        imgMarker.setImageResource(R.mipmap.marker);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)imgMarker.getLayoutParams();
        params.setMargins(0,0,0,0);
        imgMarker.setLayoutParams(params);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onDisconnect();
//        presenter.desactiveSocket();
    }

    @Override
    protected void onStart() {
        super.onStart();

        //Metodo que verifica la vercion de la aplicacion
        presenter.verifyVersion();

        //Verifica si el google play esta actualizado.
        if(onCheckGooglePlayServices())
            presenter.onConnect();
        //Metodo que verifica  si esta activo el GPS.
        presenter.validateGPS();
        if (!presenter.checkOnRequest()) {
            btnCancelTaxi.setVisibility(View.GONE);
            clean();
            presenter.resetService();
            //Se reseta la posicion del marcador
            presenter.restartMyMarker();
            changeTextBtnCancel("CANCELAR TAXI");
            changeEventCancelRequestForDefault();
            presenter.setTravelingState(false);
            presenter.removeCheckUpdateRequest();
        }
    }

    //Metodo que pone el titulo en el slider.
    @Override
    public void setTitle(String title) {
        txtTitle.setText(title);
    }

    //Metodo que pone el modelo y marca del auto.
    @Override
    public void setModelAndBrandCar(String modelandBrandCar) {
        txtModelAndBrandTaxiDriver.setText(modelandBrandCar);
    }

    //Metodo que pone la compañia del taxi en el slider
    @Override
    public void setCompany(String company) {
        txtCompanyTaxiDriver.setText(company);
    }

    @Override
    public Context getContext() {
        return this;
    }

    //Metodo que  habilita el llamar taxi
    @Override
    public void enabledCallTaxi() {
        Log.i("ultimoresume", "se activo el boton llamar taxi");
        //Metodo que pone habilitado en verdadero
        /*btnCallTaxi.setEnabled(true);
        btnCallTaxi.setVisibility(View.VISIBLE);*/
        //se cambia el boton Llamar taxi por pedir taxi
        btnPedirTaxi.setEnabled(true);
        btnPedirTaxi.setVisibility(View.VISIBLE);
        btnPreofertar.setVisibility(View.VISIBLE);
        lineartheme.setVisibility(View.VISIBLE);
        if(itemlistos==1)
            itemmensaje.setVisible(false);
        //slidingPanel.setPanelHeight(0);
        slidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        /*son_two.setVisibility(View.GONE);
        ViewGroup.LayoutParams params = son_two.getLayoutParams();
        params.height = 1;
        son_two.setLayoutParams(params);*/
        //linearnotemap.setVisibility(View.VISIBLE);
        edtNumber.setEnabled(true);
        //Metodo para colocar el fondo al boton
        //btnCallTaxi.setBackgroundResource(R.drawable.yellow);
        btnPedirTaxi.setBackgroundResource(R.drawable.yellow);
    }

            @Override
            public void enabledoptions() {
                //Metodo que pone habilitado en verdadero
                btnOptions.setEnabled(true);
                //btnOptions.setVisibility(View.VISIBLE);
                //Metodo para colocar el fondo al boton
               // btnCallTaxi.setBackgroundResource(R.drawable.blue);
            }

            @Override
            public void disabledoptions() {
                //Metodo que pone habilitado en falso
                btnOptions.setEnabled(false);
                btnOptions.setVisibility(View.INVISIBLE);
                //Metodo para colocar el fondo al boton
                //btnCallTaxi.setBackgroundResource(R.drawable.locked_btn_call);
            }

            //Metodo que deshabilita el llamar taxi
   // @Override
    public static void disabledCallTaxi() {
        Log.i("ultimoresume", "se desactivo el boton llamar taxi");
        //Metodo que pone habilitado en falso
        /*btnCallTaxi.setEnabled(false);
        btnCallTaxi.setVisibility(View.GONE);*/
        btnPedirTaxi.setEnabled(false);
        btnPedirTaxi.setVisibility(View.GONE);
        btnPreofertar.setVisibility(View.GONE);
        lineartheme.setVisibility(View.GONE);
        if(itemlistos==1)
            itemmensaje.setVisible(true);
       // slidingPanel.setPanelHeight(68);


        /*son_two.setVisibility(View.VISIBLE);
        son_two.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        linearnotemap.setVisibility(View.INVISIBLE);*/
        edtNumber.setEnabled(false);

        //Metodo para colocar el fondo al boton
        //btnCallTaxi.setBackgroundResource(R.drawable.locked_btn_call);
        btnPedirTaxi.setBackgroundResource(R.drawable.locked_btn_call);
    }

    //Metodo que habilita el boton cancelar taxi
    @Override
    public void enabledCancelTaxi() {
        //Metodo que pone habilitado en verdadero
        Log.i("FINALIZO", "se activo el boton cancel");
        btnCancelTaxi.setEnabled(true);
        btnCancelTaxi.setVisibility(View.VISIBLE);
        //Metodo para colocar  el fondo al boton
        btnCancelTaxi.setBackgroundResource(R.drawable.blue);
    }
    //Metodo que deshabilita el cancelar taxi
    @Override
    public void disabledCancelTaxi() {
        //Metodo que pone habilitado en falso
        Log.i("FINALIZO", "se desactivo el boton cancel");
        btnCancelTaxi.setEnabled(false);
        btnCancelTaxi.setVisibility(View.GONE);
        //Metodo que  coloca el fndo al boton
        btnCancelTaxi.setBackgroundResource(R.drawable.locked_btn_call);
    }
    //Metodo que habilita el campo de referencia
    @Override
    public void enabledNote() {
        edtNote.setEnabled(true);
    }
    //Metodo que deshabilita el campo de referencia
    @Override
    public void disabledNote() {
        edtNote.setEnabled(false);
    }
    //Metodo que habilita llamar taxi
    @Override
    public void enabledCallTaxiDriver() {
        //Metodo que pone habilitado en verdadero
        btnCallTaxiDriver.setEnabled(true);
        //Metodo que cambia el fondo del boton
        //btnCallTaxiDriver.setBackgroundResource(R.drawable.green);
        btnCallTaxiDriver.setBackgroundResource(R.drawable.yellow);
    }
    //Metodo que deshabilita el llamar al taxi
    @Override
    public void disabledCallTaxiDriver() {
        //Metodo que  pone el habilitado en falso
        btnCallTaxiDriver.setEnabled(false);
        //Metodo que cambia el fondo del boton
        btnCallTaxiDriver.setBackgroundResource(R.drawable.locked_btn_call);
    }
    //fiorella
    /*@Override
    public void enabledEncargos() {
        //Metodo que pone habilitado en verdadero
        btnPedido.setEnabled(true);
        btnPedido.setBackgroundResource(R.drawable.yellow);
    }
    //Metodo que deshabilita el llamar al taxi
    @Override
    public void disabledEncargos() {
        //Metodo que  pone el habilitado en falso
        btnPedido.setEnabled(false);
        //Metodo que cambia el fondo del boton
        btnPedido.setBackgroundResource(R.drawable.locked_btn_call);
    }*/


    // Metodo que coloca la imagen del taxista en la interfaz
    @Override
    public void setImgTaxiDriver1(Bitmap bitmap) {
        imgTaxiDriver1.setImageBitmap(bitmap);
    }

    // Metodo que coloca la imagen del taxista en la interfaz
    @Override
    public void setImgTaxiDriver2(Bitmap bitmap) {
        imgTaxiDriver2.setImageBitmap(bitmap);
    }

    // Metodo que obtiene la imagen del taxista en la interfaz
    @Override
    public ImageView getImgTaxiDriver1() {
        return imgTaxiDriver1;
    }

    // Metodo que obtiene la imagen del taxista en la interfaz
    @Override
    public ImageView getImgTaxiDriver2() {
        return imgTaxiDriver2;
    }

    @Override
    public ImageView getImgMarker() {
        return imgMarker;
    }

    //Metodo que invoca a la actividad UserDialog
    @Override
    public void navigateToUserDialog(String email, String name, String autoplate, String phone, String image) {
        //Metodo que llama a la actividad, enviando los diferentes parametros siguientes
        Intent i = new Intent(this, UserDialogActivity.class);
        //parametros a enviar
        i.putExtra("email", email);
        i.putExtra("image", image);
        i.putExtra("name", name);
        i.putExtra("autoplate", autoplate);
        i.putExtra("phone", phone);
        startActivity(i);
        Log.i("Flow_Data-NTUD", "Successfully showing user dialog");
    }
    //Metodo que invoca a la actividad RadarDialog
    @Override
    public void navigateToRadarDialog() {
        startActivityForResult(new Intent(this, RadarDialogActivity.class), 1);
    }
    //Metodo que invoca a la actividad Login
    @Override
    public void navigateToLogin() {
        startActivity(new Intent(this, LoginActivity.class));
    }
    //Metodo que se utiliza para verificar el resultado despues de llamar a una actividad
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("problem", "se termino el radar y devolvio respuesta");
        if (requestCode == 1) {
            //Si el resultado despues de cerra la actividad del radar es "RESULT_OK"
            if (resultCode == RESULT_OK) {
                //Entonces no se ha obtenido un pedido
                try {
                    Log.i("Flow_App-OAResult", "Successfully press button cancel radar");
                    Log.i("RESULT", "RESULT OK");
                    //Se activa el llamar el taxi
                    Log.i("prueba3", String.valueOf(prefs.getBoolean(Config.ONCALL, false)));
                    enabledCallTaxi();
                    enabledoptions();
                    //Se limpia el mapa
                    clearMap();

                    Log.i("prueba2", String.valueOf(prefs.getBoolean(Config.ONCALL, false)));
                }catch (Exception e){}
            }
            // Si el resultado despues
            // de cerrar la actividad del radar es "RESULT_FIRST_USER"
            if (resultCode == RESULT_FIRST_USER) {
                // Entonces se obtiene los valores
                // del usuario desde el preferences
                try {
                    Log.i("Flow_App-OAResult", "Successfully get data taxidriver in asynctask");
                    Log.i("RESULT", "RESULT_FIRST_USER");
                    Oferta.savetipopeticion("normal");
                    // Se llama a la actividad de los datos
                    // del taxista y se muestra la imagen
                    presenter.returnUserDialog();
                    slidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                   /* User.saveLongitude((float) myLocationMarker.getPosition().longitude);
                    User.saveLatitude((float) myLocationMarker.getPosition().latitude);*/
                    // Se guardan los valores en el preferences
                    /*

                    // Se actualiza el slide con los datos del taxista
                   // presenter.updateSlideTaxiDriver();
                    presenter.returnDataTaxiDriver();

                    // Se activa la verificacion constante
                    presenter.verifyRequest();
                    // Se indica que los datos han sido obtenidos correctamente
                    presenter.setDataReceived(true);

                    // Se obtiene el texto y se vuelvo visible
                    getTxtTime().setVisibility(View.VISIBLE);
                    // Se obtiene el texto distancia y se vuelvo visible
                    getTxtDistance().setVisibility(View.VISIBLE);
                    // Se obtiene el layout y se vuelvo visible
                    getLayoutParameters().setVisibility(View.VISIBLE);
                    // Se obtiene la imagen y se vuelvo invisible
                    getImgMarker().setVisibility(View.INVISIBLE);

                    // Se habilita el boton cancelar
                    enabledCancelTaxi();*/
                }
                catch (Exception e){}
            }
            if (resultCode == 3) {
                // Entonces se obtiene los valores
                // del usuario desde el preferences
                try {
                    Log.i("Flow_App-OAResult", "Successfully get data taxidriver in asynctask");
                    Log.i("RESULT", "RESULT_FIRST_USER");
                    Oferta.savetipopeticion("oferta");
                    disabledCallTaxi();
                    presenter.prepareaceptaroferta();
                    presenter.returnUserDialog();
                    slidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                }
                catch (Exception e){}
            }
        }
        if (requestCode == 2) {
            if(!User.loadphoto().equals(""))
               ponerFoto(imagephoto, User.loadphoto());
            if(!User.loadName().equals(""))
                txtsaludo.setText("Bienvenido "+User.loadName());
                    }
        if (requestCode == 3) {
            reiniciaractividad();
        }


    }
    //Metodo que muestran un dialogo indicando que el usuario esta logeado en otro celular
    @Override
    public void showDialogError() {
        Toast.makeText(this, getString(R.string.dialog_logout_another_phone), Toast.LENGTH_LONG).show();
    }
    //Metodo que muestra un dialogo indicando un error al enviar una petición (cuando presionas  llamar taxi)
    @Override
    public void showDialogErrorPreCall() {
        Toast.makeText(this, getString(R.string.dialog_error_send_request), Toast.LENGTH_LONG).show();
    }
    //Metodo que muestra un dialogo de erro al cancelarce solo
    @Override
    public void showDialogErrorPreCancel() {
        Toast.makeText(this, getString(R.string.dialog_error_send_request), Toast.LENGTH_LONG).show();
    }

    public void setNameTaxiDriver(String name) {
        txtNameTaxiDriver.setText(name);
    }

    public void setPrecio(String precio) {
        if(Oferta.loadtipopeticion().equals("oferta"))
            txtprecioacordado.setText(precio);
        else
            txtprecioacordado.setText("Consultar al conductor");
    }

    public void setEmailTaxiDriver(String email) {
        txtEmailTaxiDriver.setText(email);
    }

    public void setPhoneTaxiDriver(String phone) {
        txtPhoneTaxiDriver.setText(phone);
    }
    //Metodo que pone la placa del taxista
    public void setAutoPlateTaxiDriver(String autoplate) {
        txtAutoPlateTaxiDriver.setText(autoplate);
    }
    //Metodo que muestra un dialogo de error de GPS
    @Override
    public void showDialogGPSError() {
        //Verifica  si el GPS esta activado
        if (presenter.getGPS()) {
            //Crea un dialogo emergente
            AlertDialog.Builder alertDialogGPS = new AlertDialog.Builder(this);
            //Definimos los datos que iran el dialogo
            alertDialogGPS.setTitle(getString(R.string.dialog_GPS_error))
                    .setMessage(getString(R.string.dialog_GPS_message_error))
                    .setPositiveButton(getString(R.string.settings_services), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNeutralButton("No tengo GPS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            presenter.setGPS(false);

                        }
                    })
                    .setNegativeButton(getString(R.string.cancel_services), null)
                    .show();
        }
    }
    //Metodo que muestra un dialogo cuando la conexion a internet es insierta
    @Override
    public void showDialogConnectionError() {
        alertDialog.setTitle(getString(R.string.dialog_INTERNET_error))
                .setMessage(getString(R.string.dialog_INTERNET_message_error))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
     }
    //Metodo que muestra un dialogo cuando la conexion a internet se a terminado
    @Override
    public void showDialogConnectionInternetError() {
        Toast.makeText(this,getString(R.string.map_conection1) +
                getString(R.string.map_conection2) +
                getString(R.string.map_conection3),Toast.LENGTH_LONG).show();
    }
    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            case -1:
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                break;
            default:
                break;
        }

    }
    //Metodo que ajuta el Zoom en el mapa con las cordenadas actuales del usuario
    @Override
    public void adjustZoomMap(Double latitude, Double longitude, int zoom) {
        if(googleMap!=null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude)).zoom(zoom).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }
    //Metodo que muestra un dialogo cuando ocurre un error en la localizacion del usuario
    @Override
    public void showDialogLocationError() {
        Toast.makeText(this, getString(R.string.dialog_wait_for_location), Toast.LENGTH_LONG).show();
    }
    //Metodo que limpia el mapa
    @Override
    public void clearMap() {
        googleMap.clear();
    }
    //Metodo que muestra el dialogo cuando se termina el servicio de taxi

    @Override
    public void showDialogFinishTravel(){
        Log.i("problem","iniciando staractivity");
        /*try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getApplicationContext());
            alertDialog.setTitle(getString(R.string.dialog_title))
                    .setCancelable(false)
                    .setMessage(getString(R.string.finish_service))
//                .setMessage(getString(R.string.dialog_end_service))
                    .setPositiveButton(getString(R.string.dialog_accept), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    })
                    .show();
        }
        catch (WindowManager.BadTokenException e) {
            //use a log message
            Log.i("problem", e.toString());

        }*/


        startActivityForResult(new Intent(this, starActivity.class),3);
        //reiniciaractividad();

    }
    @Override
    public void reiniciaractividad(){
        this.finish();
        startActivity(new Intent(this, MapActivity.class));
    }

    @Override
    public void changeEventCancelRequestForDefault() {
        btnCancelTaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTaxi();
            }
        });
    }

    @Override
    public void showDialogEndService() {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(getString(R.string.dialog_title))
                    .setCancelable(false)
                    .setMessage("Feliz Viaje")
//                .setMessage(getString(R.string.dialog_end_service))
                    .setPositiveButton(getString(R.string.dialog_accept), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }
        catch (WindowManager.BadTokenException e) {
            //use a log message
        }

    }


    public void changeTextBtnCancel(String text){
        btnCancelTaxi.setText(text);
    }

    public void changeEventCancelRequest(){
        btnCancelTaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapActivity.this);
                alertDialog.setTitle(getString(R.string.dialog_title))
                        .setCancelable(false)
                        .setMessage(getString(R.string.map_carrera))
//                .setMessage(getString(R.string.dialog_end_service))
                        .setPositiveButton(getString(R.string.dialog_accept), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                presenter.validateFinishedRequest();

                            }
                        })
                        .setNegativeButton("No",null)
                        .show();



            }
        });

  //      presenter.setTravelingState(true);
    }

    //Metodo que muestra un dialogo de adbertencia al cancelar el pedido
    @Override
    public void showDialogCancelRequest() {
        dialogCancelRequest.setCancelable(false)
                .setTitle(getString(R.string.dialog_warning))
                .setMessage("¿Desea cancelar la solicitud? \n (Si usted cancela la solicitud " +
                        "corre el riesgo de ser " +
                        "inhabilitado por 3 dias)")
                //Metodo que coloca el boton aceptar en el dialogo
                .setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.validateCancelRequest();
                    }
                })
                //Metodo que coloca el boton cancelar en el dialogo
                .setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        enabledCancelTaxi();
                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //primer plano
        if(prefs.getBoolean("staractivity", false))
        {
            Log.i("problem","resume preferencia de staractivity");
            editor.putBoolean("staractivity", false);
            editor.commit();
            reiniciaractividad();
        }
        miresume();

            }
    public void miresume(){
        if (presenter.checkOnRequest()) {
            disabledCallTaxi();
            // Se actualiza el slide con los datos del taxista

            presenter.returnDataTaxiDriver();

            // Se activa la verificacion constante
            presenter.verifyRequest();
            // Se indica que los datos han sido obtenidos correctamente
            presenter.setDataReceived(true);

            // Se obtiene el texto y se vuelvo visible
            getTxtTime().setVisibility(View.VISIBLE);
            // Se obtiene el texto distancia y se vuelvo visible
            getTxtDistance().setVisibility(View.VISIBLE);
            // Se obtiene el layout y se vuelvo visible
            getLayoutParameters().setVisibility(View.VISIBLE);
            // Se obtiene la imagen y se vuelvo invisible
            getImgMarker().setVisibility(View.INVISIBLE);

            // Se habilita el boton cancelar
            enabledCancelTaxi();

            //Activa  la verificacion constante
            Log.i("ultimoresume", "checkonrequest es true en resume");
            //Retorna los datos del taxi que estan guardados en el preferents

            if (presenter.getTravelingState()) {
                changeEventCancelRequest();
                changeTextBtnCancel("FINALIZAR");
            } else {
                changeTextBtnCancel("CANCELAR TAXI");
                changeEventCancelRequestForDefault();
            }
            slidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }
        else if (aux != 0) {


            //Se limpia el mapa
            clean();

            //  setTravelingState(false);
            //Se resetea la posicion del marcador
            try {
                clearMap();
            } catch (Exception e) {
            }
            //Se activa el boton de llamar taxi
            enabledCallTaxi();
            //Se desactiva el boton cancelar
            disabledCancelTaxi();
            //Se desactiva el boton de llamar al taxista
            disabledCallTaxiDriver();

            //Se resetea los valores en preferences que indican que esta en el servicio o en el pedido
            presenter.resetService();
            //Se reseta la posicion del marcador
            presenter.restartMyMarker();

            //Se obtiene la imagen del taxista y se coloca para que se centre en el imagevieww
            getImgTaxiDriver2().setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {aux = 1; presenter.resetService();enabledCallTaxi();}
    }
    //Metodo que muestra un un dialogo  despues de cancelar satisfactoriamente

    @Override
    protected void onPause() {
        super.onPause();
        //La app esta en segundo plano (background).

    }

    @Override
    public void showSuccessfullyFinish() {
        Toast.makeText(this,getString(R.string.map_final_carrera),Toast.LENGTH_LONG).show();
    }



    @Override
    public void showSuccessfullyCancel() {
        Toast.makeText(this, getString(R.string.dialog_succesfully_cancel), Toast.LENGTH_LONG).show();
    }
    //Metodo que muestra un dialogo cuando el usuario se encuentra baneado
    @Override
    public void showDialogFailCall() {
        AlertDialog.Builder alertDialogTaxi = new AlertDialog.Builder(this);
        alertDialogTaxi.setCancelable(false);
        alertDialogTaxi.setMessage(getString(R.string.MapActivity_showDialogFailCall)+"\n" +
                getString(R.string.MapActivity_showDialogFailCall2)+"\n")
                .setTitle("Aviso")
                .setPositiveButton("Aceptar", null)
                .show();
    }
    //Metodo que muestra un dialogo cuando el taxista hace un lla llege
    @Override
    public void showDialogNearTaxi(String message) {
        if(!isFinishing())
            try {
                AlertDialog.Builder alertDialogTaxi = new AlertDialog.Builder(this);
                alertDialogTaxi.setCancelable(false);
                alertDialogTaxi.setMessage(message)
                        .setTitle(getString(R.string.dialog_title))
                        .setPositiveButton(getString(R.string.dialog_mensajerecibido), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                               presenter.validateRecepcionMensaje("Su mensaje fue leido por el pasajero");
                            }
                        })
                        .show();
            }catch (Exception e){}

    }

    @Override
    public void showDialogUbicacionCompartida(final String origen) {
        Log.i("ubicacioncompartida","dialog");
        Uri sonido = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sound_sharenumber);
        sound.sendNotification("confirmacion de recepcion de ubicación", sonido, this);
            try {
                AlertDialog.Builder ubicacioncompartida = new AlertDialog.Builder(this);
                ubicacioncompartida.setCancelable(false);
                ubicacioncompartida.setMessage("Desea recibir la ubicación de " + origen)
                        .setTitle("Confirmacion de recepcion de ubicación")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                presenter.recibirShareLocation(origen);
                            }
                        })
                        .setNegativeButton("Rechazar",new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }
                                )
                        .show();
            }catch (Exception e){}

    }
    //Metodo que muestra un dialogo al Registrarse y tener un problema con el correo
    @Override
    public void showDialogDisableAccount() {
        Toast.makeText(this, getString(R.string.dialog_disable_account_error), Toast.LENGTH_LONG).show();
    }
    //Metodo que pone la distancia
    @Override
    public void setDistance(String distance) {
        txtDistance.setText(distance);
    }
    //Metodo que pone el tiempo
    @Override
    public void setTime(String time) {
        txtTime.setText(time);
    }
    //Metod para obtener el texto de  la distancia
    @Override
    public TextView getTxtDistance() {
        return txtDistance;
    }
    //Metodo para obtener el texto del tiempo
    @Override
    public TextView getTxtTime() {
        return txtTime;
    }
    //Metodo para obtener los parametros del layout
    @Override
    public LinearLayout getLayoutParameters() {
        return linearLayoutParameters;
    }

    static String REQUESTING_LOCATION_UPDATES_KEY = "requesting_location_updates_key";
    static String LOCATION_KEY = "location_key";
    static String LAST_UPDATE_TIME_STRING_KEY = "last_update_time_string_key";

    // ESTOS METODOS NO SE UTILIZAN
    // ==========================================================================

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble("latitude",presenter.getMyCurrentLocation().getLatitude());
        outState.putDouble("longitude", presenter.getMyCurrentLocation().getLongitude());
    }

            @Override
            protected void onRestoreInstanceState(Bundle savedInstanceState) {
                super.onRestoreInstanceState(savedInstanceState);

                if(googleMap!=null) {
                    Double latitude = savedInstanceState.getDouble("latitude");
                    Double longitude = savedInstanceState.getDouble("longitude");
                    adjustZoomMap(latitude, longitude, 15);
                }
            }
    // ===========================================================================


    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    public void showConnected() {
/*
        llConnection.setBackgroundColor(getResources().getColor(R.color.green));
        llConnection.setLayoutParams
                (new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,50));
        txtStateConnection.setText("Conectado");

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                llConnection.setLayoutParams
                        (new LinearLayout.LayoutParams(0,0));
                txtStateConnection.setVisibility(View.INVISIBLE);
            }
        };
        new Handler(Looper.getMainLooper()).postDelayed(runnable,1000);
*/
    }

    @Override
    public void showDisconnected() {
/*
        llConnection.setLayoutParams
                (new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 25));
        llConnection.setBackgroundColor(getResources().getColor(R.color.red));
        txtStateConnection.setVisibility(View.VISIBLE);
        txtStateConnection.setText("Desconectado");
*/
    }

    @Override
    public void showReconnecting() {
/*
        llConnection.setLayoutParams
                (new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,50));
        llConnection.setBackgroundColor(getResources().getColor(R.color.orange));
        txtStateConnection.setVisibility(View.VISIBLE);
        txtStateConnection.setText("Reconectando...");
*/

    }

String promotext;
    @Override
    public void setId(String promotion) {
        promotext = PromotionActivity.convertir(promotion);
        User.saveIdpromotion(promotext);
    }

    void share() {
        Intent paramView;
        paramView = new Intent("android.intent.action.SEND");
        paramView.setType("text/plain");

        paramView.putExtra("android.intent.extra.TEXT", getString(R.string.nombreapp) + " "+ getString(R.string.mensaje2)  + " " + /*promotext + " " +*/
                getString(R.string.mensaje_descarga) + " \n" + getString(R.string.link));
        startActivity(Intent.createChooser(paramView, "Comparte Nuestro aplicativo"));

    }
    public static int itemlistos=0;
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        itemmensaje=menu.findItem(R.id.messagetotaxi);
        itemlistos=1;
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void showDialogFailedSend() {
        Toast.makeText(this,"No se pudo enviar correctamente, intentelo nuevamente",Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialogConnection(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage("Activa los datos o WI-FI en la configuracion");
        builder.setTitle("Los datos estan desactivados");
        builder.setPositiveButton("Configuración", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                return;
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                return;
            }
        });

        builder.show();
    }

    @Override
    public void sendShareLocation(Double latitude, Double longitude) {
        presenter.sendShareLocation(latitude,longitude);
    }

    @Override
    public void retirarenviarubicacion(final JSONObject jsonparams){
        if(!isFinishing())
        try {
        AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
        alertDialogAbout.setMessage("Se dejo de recibir la ubicación de "+jsonparams.getString("email"))
                .setTitle("Finalizado recepción de ubicación")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        int index = buscarP(jsonparams);
                        if (index != -1) {//ES NUEVO
                            removerPosicion(index);
                        }
                    }
                })
                .show();
        } catch (JSONException e) {
            Log.e("JSONExceptionNewpos", e.toString());
        }
    }

    // ----- marcadores de sharelocation
    ArrayList<cMarker> MARCADORES= new ArrayList<cMarker>();

    @Override
    public void updateShareLocation(JSONObject jsonparams){
        Log.i("updatesharelocation","update");
        Log.i("updatesharelocation",jsonparams.toString());
        int index = buscarP(jsonparams);
        Log.i("updatesharelocation", String.valueOf(index));
        if (index == -1) {//ES NUEVO
            nuevoPosicion(jsonparams);
        } else {//REGISTRADO
            actualizarPosicion(index, jsonparams);
        }
    }


    void nuevoPosicion(JSONObject data) {
            //de socket
            try {
                cMarker marca = new cMarker(googleMap, data.getString("email"), data.getDouble("latitude"), data.getDouble("longitude"));
                //se añade una verificacion del tipo de marca
                    marca.dibujar(R.mipmap.ic_persona_share);
                MARCADORES.add(marca);//agregamos al ARRAY
                Log.i("ubicationshare", "try catch de dibujar");
            } catch (JSONException e) {
                Log.e("JSONExceptionNewpos", e.toString());
            }


    }

    void actualizarPosicion(int index, JSONObject data) {
        try {
            MARCADORES.get(index).remover();
            MARCADORES.get(index).update(data.getDouble("latitude"), data.getDouble("longitude"));
            //  MARCADORES.get(index).dibujar(R.mipmap.taxipanico);
                  MARCADORES.get(index).dibujar(R.mipmap.ic_persona_share);
        } catch (JSONException e) {
            Log.e("JSONExceptionActPos", e.toString());
        }
    }
    void removerPosicion(int index){
        MARCADORES.get(index).remover();
        MARCADORES.remove(index);//eliminamos del ARRAY
    }

    int buscarP(JSONObject data){
        Log.i("valorindex5","Ingresamos a buscarP");
        int index=-1;
        try {

            Log.i("valorindex1",String.valueOf(index));
            int n=MARCADORES.size();
            Log.i("valorindexdeN",String.valueOf(n));
            for(int i=0;i<n;i++){
               Log.i("valorindexn", MARCADORES.get(i).getIde().toString());
                if(MARCADORES.get(i).getIde().equals(data.getString("email"))){

                    index=i;
                    break;
                }
            }

        } catch (JSONException e) {
            Log.e("JSONExceptionRequest", e.toString());
        }
        Log.i("valorindex2",String.valueOf(index));
        return index;
    }
    public class cMarker {

        //Propiedades
        private GoogleMap map;
        private String id;
        private double lat;
        private double lon;
        Marker marker;
        //Constructor
        public cMarker(GoogleMap _map, String _id, double _lat, double _lon) {
            map = _map;
            id= _id;
            lat = _lat;
            lon = _lon;
        }

        //Métodos - getters&setters

        public void dibujar(int ico) {
            Log.i("ubicationalarma3","se dibujo");
            marker = map.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lon))
                    .icon(BitmapDescriptorFactory.fromResource(ico))
                    .title(id)
            );
            marker.showInfoWindow();
        }

        public void remover() {
            marker.remove();
        }

        public String getIde() {
            return id;
        }

        public void update(double Lat, double Lon) {
            lat=Lat;
            lon=Lon;
        }
    }
}
