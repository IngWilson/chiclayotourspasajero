package app.product.ditec.chiclayotours_cliente.Peticion;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.Model.Oferta;
import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.R;

public class MiAdaptador extends
        RecyclerView.Adapter<MiAdaptador.ViewHolder> {
    private LayoutInflater inflador;
    private ArrayList<Oferta> lista;
    private Context context;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    public MiAdaptador(Context context, ArrayList<Oferta> lista) {
        this.context=context;
        this.lista = lista;
        inflador = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflador.inflate(R.layout.peticion_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int i) {
        holder.txttipo.setText(lista.get(i).getType());
        holder.txtprecio.setText(lista.get(i).getPrecio());
        holder.txtnombre.setText(lista.get(i).getName());
        holder.txttiempo.setText(lista.get(i).getTiempo());
        holder.txtdistance.setText(lista.get(i).getDistance());
        holder.btnaceptar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                /// button click event
                ((PeticionView)context).aceptaroferta(lista.get(i).getEmaildriver(), User.loadEmail(), Request.loadId(),lista.get(i));

            }
        });
        holder.btnrechazar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                /// button click event
                ((PeticionView)context).rechazaroferta(lista.get(i).getSocketidtaxi());
                lista.remove(i);
                notifyDataSetChanged();

            }
        });

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.loading)
                .build();

        //Se descarga la imagen en la URL del taxista
        imageLoader.displayImage(lista.get(i).getImageUrl(), holder.imgconductor, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                //Se oculta el imageView y se muestra un icono de progreso
                holder.imgconductor.setVisibility(View.GONE);
                holder.loadingPanel.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                //Se oculta la barra de progreso y se muestra el imageView con la imagen cargada
                holder.loadingPanel.setVisibility(View.GONE);
                holder.imgconductor.setVisibility(View.VISIBLE);
                holder.imgconductor.setImageBitmap(loadedImage);
                holder.imgconductor.setScaleType(ImageView.ScaleType.FIT_XY);

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

    }
    @Override
    public int getItemCount() {
        return lista.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txttipo, txtprecio, txtnombre, txttiempo, txtdistance;
        public Button btnaceptar, btnrechazar;
        public ImageView imgconductor;
        ProgressBar loadingPanel;
        ViewHolder(View itemView) {
            super(itemView);
            txttipo = (TextView)itemView.findViewById(R.id.txttipo);
            txtprecio = (TextView)itemView.findViewById(R.id.txtprecio);
            txtnombre = (TextView)itemView.findViewById(R.id.txtnombre);
            txttiempo = (TextView)itemView.findViewById(R.id.txttiempo);
            txtdistance = (TextView)itemView.findViewById(R.id.txtdistance);
            btnaceptar=(Button)itemView.findViewById(R.id.btnaceptarPeticionitem);
            btnrechazar=(Button)itemView.findViewById(R.id.btnCancelPeticionitem);
            imgconductor=itemView.findViewById(R.id.imgTaxiDriveritem);
            loadingPanel=itemView.findViewById(R.id.loadingPanelitem);
        }
    }
}
