package app.product.ditec.chiclayotours_cliente.History.MonthTabs;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.History.HistoryActivity;
import app.product.ditec.chiclayotours_cliente.History.HistoryAdapter;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNovember extends Fragment {

    View view;
    ListView lvNovember;
    HistoryAdapter historyAdapter;

    public FragmentNovember() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_november, container, false);
        lvNovember = (ListView) view.findViewById(R.id.lvNovember);
        /*historyAdapter = new HistoryAdapter(view.getContext());*/
        historyAdapter = new HistoryAdapter(view.getContext());

        try {
            if (!HistoryActivity.getmRequest(11).equals(null)) {
                historyAdapter.setData(new ArrayList<>(HistoryActivity.getmRequest(11)));
                lvNovember.setAdapter(historyAdapter);
            }
        }catch (NullPointerException e){}
        return view;
    }


}
