package app.product.ditec.chiclayotours_cliente;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import app.product.ditec.chiclayotours_cliente.Map.MapPresenter;
import app.product.ditec.chiclayotours_cliente.Map.MapView;
import app.product.ditec.chiclayotours_cliente.RadarDialog.RadarDialogPresenter;
import app.product.ditec.chiclayotours_cliente.RadarDialog.RadarDialogView;

/**
 * Created by Wilson on 07/05/15.
 */
public class MyBroadcastReceiver extends BroadcastReceiver {

    private MapPresenter mapPresenter;
    private MapView mapView;
    private RadarDialogPresenter radarDialogPresenter;
    private RadarDialogView radarDialogView;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;


    public MyBroadcastReceiver(RadarDialogPresenter radarDialogPresenter) {
        this.radarDialogPresenter = radarDialogPresenter;
        radarDialogView = radarDialogPresenter.getView();
        prefs = radarDialogView.getContext().getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public MyBroadcastReceiver(MapPresenter mapPresenter) {
        this.mapPresenter = mapPresenter;
        mapView = mapPresenter.getView();
        prefs = mapView.getContext().getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();

    }

    @Override
    public void onReceive(Context context, Intent intent) {

/*        try {
            String type = intent.getStringExtra("type");
            Log.i("BroadcastReceiver", "Type : " + type);

            if (type != null)
                switch (type) {
                    case "1u":
                        //TODO: Mostrar el marcador del taxi constantemente, considerar que el marcador del usuario no vuelva a aparecer
*//*                        if (mapView != null) {

                            if (prefs.getBoolean("DataTaxi", false)) {

                                String longitude = intent.getStringExtra("longitude");
                                String latitude = intent.getStringExtra("latitude");

                                Log.i("MBR_Extras_1u", intent.getExtras().toString());

                                if (prefs.getBoolean("Initial", false))
                                    mapView.removeTaxiMarker();

                                mapView.addLocationTaxiMarker(Double.parseDouble(latitude), Double.parseDouble(longitude));
                                mapView.adjustZoomMap(Double.parseDouble(latitude), Double.parseDouble(longitude), 15);

                                Location myCurrentLocation = mapPresenter.getMyCurrentLocation();

                                Double distance = Haversine.calculateDistance(Double.parseDouble(latitude), Double.parseDouble(longitude), myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());

                                mapView.setDistance(String.valueOf(distance).substring(0, 5) + " KM");
                                Double time = (distance / 15) * 60;

                                mapView.setTime(String.valueOf(time).substring(1, 2) == "." ? String.valueOf(time).substring(0, 1) : String.valueOf(time).substring(0, 2) + " minuto(s)");

                                editor.putBoolean("Initial", true);
                                editor.commit();
                            }
                        }*//*
                        break;
                    case "2u":
                        //TODO: Verificar para que se utiliza el GCM_DRIVER
                        //TODO: Se usa para el cancel request
                        //TODO: Cancelar descargas si es que se cancelo el pedido
*//*                        if (mapView != null) {

                            if (!prefs.getBoolean("DataTaxi", false)) {

                                mapView.getTxtTime().setVisibility(View.VISIBLE);
                                mapView.getTxtDistance().setVisibility(View.VISIBLE);
                                mapView.getLayoutParameters().setVisibility(View.VISIBLE);

                                editor.putBoolean("DataTaxi", true);
                                editor.putBoolean("ReceivedData", true);
                                editor.commit();

                                String name         = intent.getStringExtra("name");
                                String phone        = intent.getStringExtra("phone");
                                String autoplate    = intent.getStringExtra("autoplate");
                                String dni          = intent.getStringExtra("dni");
                                String car_brand    = intent.getStringExtra("car_brand");
                                String car_model    = intent.getStringExtra("car_model");
                                String company      = intent.getStringExtra("company");
                                String gcm_driver   = intent.getStringExtra("gcm_driver");
                                String imageURL     = intent.getStringExtra("image");

                                Log.i("MBR_Extras_2u", intent.getExtras().toString());

                                //Se indica que la app esta en el servicio
                                mapPresenter.setOnRequest(true);
                                //Se indica que la app ya no esta llamando
                                mapPresenter.setOnCall(false);

                                mapPresenter.checkUpdateRequest();

                                final TaxiDriver taxiDriver = new TaxiDriver(name, phone, autoplate, mapView.getContext());
                                taxiDriver.saveTaxiDriver();
                                taxiDriver.setGCM(gcm_driver);
                                taxiDriver.setCar_brand(car_brand);
                                taxiDriver.setCar_model(car_model);
                                taxiDriver.setDni(dni);
                                taxiDriver.setCompany(company);
                                taxiDriver.setImageUrl(imageURL);

                                mapView.enabledCancelTaxi();
                                mapView.setTitle("Esperando a : " + name);
                                mapView.setNameTaxiDriver(name);
                                mapView.setPhoneTaxiDriver(phone);
                                mapView.setAutoPlateTaxiDriver(autoplate);
                                mapView.setCompany(company);
                                mapView.setModelAndBrandCar(car_model + " " + car_brand);
                                mapView.enabledCallTaxiDriver();

                                imageLoader = ImageLoader.getInstance();
                                options = new DisplayImageOptions.Builder()
                                        .showImageOnLoading(R.mipmap.loading)
                                        .build();

                                imageLoader.loadImage(imageURL, new ImageLoadingListener() {
                                    @Override
                                    public void onLoadingStarted(String imageUri, View view) {
                                        mapView.getImgTaxiDriver1().setImageResource(R.mipmap.loading);
                                        mapView.getImgTaxiDriver2().setImageResource(R.mipmap.loading);
                                    }

                                    @Override
                                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                                    }

                                    @Override
                                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                        if (loadedImage != null&&mapPresenter.checkOnRequest()) {
                                            taxiDriver.setImage(ImageRefactor.encodeTobase64(loadedImage));
                                            mapView.getImgTaxiDriver2().setScaleType(ImageView.ScaleType.FIT_XY);
                                            mapView.getImgTaxiDriver2().setImageBitmap(loadedImage);
                                            mapView.getImgTaxiDriver1().setImageBitmap(loadedImage);
                                            editor.putBoolean(Config.GETIMAGE, true);
                                            editor.commit();
                                        }
                                    }

                                    @Override
                                    public void onLoadingCancelled(String imageUri, View view) {

                                    }
                                });

                                mapPresenter.returnUserDialog();
                            }
                        }

                        if (radarDialogView != null)
                            ((Activity) radarDialogView.getContext()).finish();*//*
                        break;
                    case "3u":
*//*                        if (mapView != null) {
                            if (prefs.getBoolean("DataTaxi", false)) {
                                Log.i("MBR_Extras_3u", intent.getExtras().toString());
                                mapView.showDialogNearTaxi();
                                mapView.blareSound();
                            }
                        }*//*
                        break;
                    case "4u":
*//*                        if (mapView != null) {

                            if (prefs.getBoolean("DataTaxi", false)) {

                                if (!mapPresenter.checkOnCancel()) {
                                    Log.i("MBR_Extras_4u", intent.getExtras().toString());

                                    editor.putBoolean(Config.ONCANCEL, true);
                                    editor.commit();

                                    mapView.clearMap();
                                    mapView.showDialogEndService();
                                    mapView.clean();
                                    mapView.enabledCallTaxi();
                                    mapView.disabledCancelTaxi();
                                    mapView.disabledCallTaxiDriver();
                                    mapView.getImgTaxiDriver2().setScaleType(ImageView.ScaleType.FIT_CENTER);

                                    mapPresenter.resetService();
                                    mapPresenter.restartMyMarker();
                                    mapPresenter.unRegisterBroadcast();
                                    mapPresenter.removeCheckUpdateRequest();

                                }
                            }
                        }*//*
                        break;
                    default:
                        break;
                }
        } catch (NullPointerException e) {
            Log.e("NullPointerException", e.toString());
        }*/
    }
}
