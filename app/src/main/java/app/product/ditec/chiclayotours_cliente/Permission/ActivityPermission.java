package app.product.ditec.chiclayotours_cliente.Permission;

import android.Manifest;
import android.os.Build;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import app.product.ditec.chiclayotours_cliente.Login.LoginActivity;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * Created by Wilson on 12/02/2019.
 */

public class ActivityPermission extends AppCompatActivity implements DialogInterface.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_versionsixorup);

        if (ContextCompat.checkSelfPermission(ActivityPermission.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            arranque();
        } else {
            Handler handler = new Handler();
            handler.postDelayed(
                    new Runnable() {
                        public void run() {
                            if (ContextCompat.checkSelfPermission(ActivityPermission.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                                    permisoalmacenamiento();
                                }
                                else{
                                    permisonotification();
                                }
                            } else {
                                solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, getString(R.string.permission_map1) + getString(R.string.permission_map), 0);
                            }
                        }
                    }, 2000L);
        }

    }


    void permisoalmacenamiento(){

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            arranque();
        } else {
            solicitarPermiso(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_map1) + getString(R.string.permission_almacen), 2);
        }
    }

    void permisonotification(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
            arranque();
        } else {
            solicitarPermiso(Manifest.permission.POST_NOTIFICATIONS, "Sin el permiso" + " de notificación no podrás ver nuestras alertas.", 3);
        }
    }

    void arranque() {
        Intent i = new Intent(ActivityPermission.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    public void solicitarPermiso(final String permiso, String justificacion, final int codigo) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permiso)) {

            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
            dialogo1.setTitle("Solicitud de permiso");
            dialogo1.setMessage(justificacion);
            dialogo1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    ActivityCompat.requestPermissions(ActivityPermission.this, new String[]{permiso}, codigo);
                }
            });
            dialogo1.show();


        } else {
            ActivityCompat.requestPermissions(this, new String[]{permiso}, codigo);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //  aumentarpermisos();
                if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                    permisoalmacenamiento();
                }
                else{
                    permisonotification();
                }
            } else {
                //Snackbar.make(vista, "Sin el permiso, no puedo realizar la" + "acción", Snackbar.LENGTH_SHORT).show();
                solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, getString(R.string.permission_map1) + getString(R.string.permission_map), 0);
            }
        }
        if (requestCode == 2) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                arranque();
            } else {
                //Snackbar.make(vista, "Sin el permiso, no puedo realizar la" + "acción", Snackbar.LENGTH_SHORT).show();
                solicitarPermiso(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_map1) + getString(R.string.permission_almacen), 2);
            }
        }

        if (requestCode == 3) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                arranque();
            } else {
                //Snackbar.make(vista, "Sin el permiso, no puedo realizar la" + "acción", Snackbar.LENGTH_SHORT).show();
                solicitarPermiso(Manifest.permission.POST_NOTIFICATIONS, getString(R.string.permission_map1) + " de Notificación no podrá ver nuestras alertas.", 3);
            }
        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        return;
    }
}
