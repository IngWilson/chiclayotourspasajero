package app.product.ditec.chiclayotours_cliente.PreRequest;

/**
 * Created by angel on 08/06/15.
 */
public interface PreRequestPresenter {

    void validatePreRequest(final String note, final String origin, final String destination);

    void validatePrice(float price);
}
