package app.product.ditec.chiclayotours_cliente.Register;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 21/04/15.
 */
public interface RegisterInteractor {

    void register(JSONObject jsonParams, OnRegisterFinishedListener onRegisterFinishedListener, Validation validator);
    void verifycodepromo(JSONObject jsonParams, OnRegisterFinishedListener onRegisterFinishedListener, Validation validator);
    void sendcodepromo(JSONObject jsonParams, OnRegisterFinishedListener onRegisterFinishedListener, Validation validator);
}
