package app.product.ditec.chiclayotours_cliente.Map;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by angel on 26/04/15.
 */
// Clase que implementa OnConnectionFailedListener
// , ConnectionCallbacks y LocationListener
// y que sirve para recuperar la localizacion por GPS
public class MapRequestLocation implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        LocationListener {

    // Se declara un nuevo cliente
    GoogleApiClient mGoogleApiClient;
    // Se declara una nueva vista
    MapView mapView;
    // Se declara una nueva localizacion
    Location mLastLocation;
    int countUpdate = 0;

    // boolean flag to toggle periodic location updates
    private LocationRequest mLocationRequest;

    // Se define los parametros de actualizacion
    // y recuperacion de localizacion
    private static int UPDATE_INTERVAL = 5000; // 10 sec
    private static int FATEST_INTERVAL = 5000;  // 5 sec
    private static int DISPLACEMENT = 3;       // 10 meters

    // Metodo constructor donde se le pasa la vista
    public MapRequestLocation(MapView mapView) {
        // Se guarda la vista
        this.mapView = mapView;
        // Se instancia un nuevo GoogleApiClient
        // para manejar las peticiones de recuperacion de localizacion
        mGoogleApiClient = new GoogleApiClient.Builder(mapView.getContext())
            .addApi(LocationServices.API)
            .addScope(Drive.SCOPE_FILE)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build();
    }

    // Metodo que conecta el cliente a la GoogleApiClient
    public void connect() {
        mGoogleApiClient.connect();
    }

    // Metodo que desconecta el cliente a la GoogleApiClient
    public void disconnect() {
        mGoogleApiClient.disconnect();
    }

    // Metodo que crea el tipo de peticion
    // y la forma en la cual se van a recuperar
    // la localizacion desde el GoogleApiClient
    protected void createLocationRequest() {
        // Se instancia un nuevo LocationRequest
        mLocationRequest = new LocationRequest();
        // Se asigna el intervalo de actualizacion
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Se asigna la rapidez con la cual se obtiene las peticiones
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        // Se asigna la prioridad de la peticion indicando
        // que debe ser de alta presicion
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Se asigna la cantidad minima de desplazamiento
        // para que se haga una peticion
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    // Metodo que inicia las peticiones de localizacion
    // cuando se actualiza la actualizacion en el dispositivo
    protected void startLocationUpdates() {

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }

    // Metodo que detiene las peticiones de localizacion
    // cuando se actualiza la actualizacion en el dispositivo
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    // Metodo que se ejecuta cuando se conecta a la GoogleApiClient
    @Override
    public void onConnected(Bundle bundle) {
        // Se obtiene la ultima localizacion
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        // Se crea la peticion de localizacion
        createLocationRequest();
        // Se comienza a ejecutar el escuchador para cuando hay cambios
        // en la posicion
        startLocationUpdates();

        // Se verifica que la localizacion no es nula
        if (mLastLocation != null) {
            // Se ajusta en el mapa el zoom la localizacion
            mapView.adjustZoomMap(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 15);
            Log.i("LocationMapPresenter", "" + mLastLocation.getLongitude());
        } else
            Log.i("LocationMapPresenter", "No se pudo obtener la localizacion");
    }

    // Metodo que se ejecuta cuando la conexion a la GoogleApiClient no se
    // a conectado correctamente
    @Override
    public void onConnectionFailed(ConnectionResult result) {

        Log.i("MapConnection", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    // Metodo que se ejecuta cuando la conexion esta suspendido
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    // Metodo que se ejecuta cuando la localizacion ha cambiado
    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        //Toast.makeText(mapView.getContext(),""+location.getLatitude()+" "+location.getLongitude(),Toast.LENGTH_LONG).show();

        if (countUpdate == 0) {
            mapView.adjustZoomMap(location.getLatitude(), location.getLongitude(), 15);

            //TODO: Verificar si es que funciona sin esto
            //mapView.addMyPositionMarker(location.getLatitude(),location.getLongitude());
            countUpdate++;
        }
        mapView.sendShareLocation(location.getLatitude(),location.getLongitude());
    }

    // Metodo que obtiene la ultima posicion
    public Location getLastLocation() {

        return mLastLocation;
    }

    // Metodo que obtiene la ultima posicion conocida
    public Location getLastKnowLocation() {
        return LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
    }

    // Metodo que verifica que este conectado
    // a la API
    public boolean verifyAAPIClient() {
        return mGoogleApiClient.isConnected();
    }

}
