package app.product.ditec.chiclayotours_cliente.Star;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by usuario on 14/07/2016.
 */
public interface StarInteractor {
    void sendnotestar(JSONObject jsonObject, Validation validator, OnStarFinishedListener listener);
}
