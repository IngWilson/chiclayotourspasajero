package app.product.ditec.chiclayotours_cliente;

public interface Config {

    String url = App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_API);

    //URL para el registro del usuario
    String REGISTERUSER = url + "/signUp-user";

    //URL para enviar un nueva peticion
    String REQUEST = url + "/request/send";

    //URL para iniciar sesion
    String LOGIN = url + "/login";

    //URL para cancelar un pedido en pleno servicio
    String CANCEL = url + "/request/cancel";

    //URL para cancelar un peticion en pleno request
    String PRECANCEL = url + "/request/precancel";

    //URL para verificar que un usuario no esta logeado en otro celular
    String CHECKLOGIN = url + "/login/check";

    //Chequear si el request ha sido eliminado
    String CHECKREQUEST = url + "/requests/check";

    //Obtener datos del usuario
    String GETDATAUSER = url + "/users";

    //Obtener el codigo
    String GETCODE = url + "/users/getCode";

    //Verificar el codigo obtenido
    String VERIFY = url + "/users/verify";

    //Verificar si es que alguien a aceptado el pedido
    String CHECKACCEPT = url + "/request/checkAccept";

    String VERSION = url + "/version/check";

    String checkRequest = url + "/request/check";

    String GETPASSWORD = url + "/forgot";

    String RESETGCM = url + "/users/resetGcm";

    // Google Project Number
    String GOOGLE_PROJECT_ID = "355409770365";

    String TAG = "Register Activity";
    String PREF_TAG = "UserPreferences";
    String APP_VERSION = "appVersion";

    String ONCALL = "onCall";
    String ONSERVICE = "onService";
    String ONREQUEST = "onRequest";
    String ONLOGIN = "onLogin";
    String ONVERIFY = "onVerify";
    String ONREGISTER = "onRegister";
    String ONREGISTERPHONE = "onRegisterPhone";
    String ONRADAR = "onRadar";
    String ONUSERDIALOG = "onUserDialog";
    String ONCANCEL = "onCancel";

    String GETIMAGE = "getimage";

    String EMAIL = "email";
    String GETDATA = "getData";
    String GPS = "gps";

    String TRAVELING = "traveling";

}
