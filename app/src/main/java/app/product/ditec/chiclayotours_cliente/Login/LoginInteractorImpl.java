package app.product.ditec.chiclayotours_cliente.Login;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;


public class LoginInteractorImpl implements LoginInteractor {

    OkHttpRequest okHttpRequest;

    @Override
    public void login(final JSONObject jsonParams, final OnLoginFinishedListener listener, final Validation validator) {

        if (validator.isOnline()) {
            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_LOGIN));

            boolean error = false;
            try {

                if (TextUtils.isEmpty(jsonParams.getString("email"))) {
                    listener.onUsernameError();
                    error = true;
                }

                if (TextUtils.isEmpty(jsonParams.getString("password"))) {
                    listener.onPasswordError();
                    error = true;
                }

                if (TextUtils.isEmpty(jsonParams.getString("gcm"))) {
                    listener.onLoginError();
                    error = true;
                }

            } catch (JSONException e) {
                Log.e("JSONException", e.toString());
            }

            if (!error) {

                try {
                    okHttpRequest.post(jsonParams.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    listener.onLoginError();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {

                            int code = response.code();
                            Log.i("CodeLogin", "" + code);

                            if (code == 200)
                                listener.onSuccess();
                            else if (code == 503) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onUserPassError();
                                    }
                                });

                            } else {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onLoginError();
                                    }
                                });
                            }
                        }
                    });
                } catch (IOException e) {
                    Log.e("IOException", e.toString());
                }
            }

        } else
            listener.onConnectionError();
    }

    @Override
    public void loginPhone(final JSONObject jsonParams, final OnLoginFinishedListener listener, final Validation validator) {

        if (validator.isOnline()) {
            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_LOGINPHONE));

            boolean error = false;
            try {

                if (TextUtils.isEmpty(jsonParams.getString("phone"))) {
                    listener.onPhoneError();
                    error = true;
                }

            } catch (JSONException e) {
                Log.e("JSONException", e.toString());
            }

            if (!error) {

                try {
                    okHttpRequest.post(jsonParams.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    listener.onLoginError();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {

                            int code = response.code();
                            String body = response.body().string();
                            Log.i("CodeLogin", "" + code);

                            if (code == 200) {

                                try {
                                    JSONObject validate = new JSONObject(body);

                                    ;
                                    listener.onSuccessPhone(validate.getString("email"));
                                } catch (JSONException e) {
                                    Log.e("R_JSONException", e.toString());
                                }

                            }
                            else if (code == 503) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onRegisterPhone();
                                    }
                                });

                            } else {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onLoginError();
                                    }
                                });
                            }
                        }
                    });
                } catch (IOException e) {
                    Log.e("IOException", e.toString());
                }
            }

        } else
            listener.onConnectionError();
    }

}
