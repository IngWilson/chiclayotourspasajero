package app.product.ditec.chiclayotours_cliente.Account;

import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 05/05/15.
 */
public class AccountInteractorImpl implements AccountInteractor {

    JSONObject params = new JSONObject();
    OkHttpRequest okHttpRequest;

    //Metodo que recupera los datos del pasajero
    @Override
    public JSONObject getDataUser(final String email, final Validation validator, final OnAccountFinishedListener listener) {

        //Se valida si esta conectado a internet
        if (validator.isOnline()) {

            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_GETDATAUSER)+ "?email=" + email);

            //Se realiza una peticion GET con un callback
            try {
                okHttpRequest.getNotParams(new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {
                        Log.i("AccountFailure", "Failure when get user data");
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {

                        Log.i("AccountAccept", "Get user data");
                        String res = response.body().string();
                        Log.i("Response Body", res);

                        //Se obtiene los datos del taxista en un JSONArray
                        try {
                            JSONArray jsonArray = new JSONArray(res);
                            params = jsonArray.getJSONObject(0);

                            //Se instancia el usuario
                            User user = new User();
                            //Se coloca los datos del usuario en el objeto
                            user.setName(params.getString("first_name"));
                            user.setLastname(params.getString("last_name"));
                            user.setEmail(params.getString("email"));
                            user.setPhone(params.getString("phone"));
                            user.setIsable(params.getBoolean("isable"));
                            user.setVerified(params.getBoolean("verified"));

                            // Metodo que se ejecuta cuando no se obtiene correctamente
                            // el usuario
                            listener.onSuccessGetUserData(user);

                            Log.i("GDU_Account", params.toString());

                        } catch (JSONException e) {
                            Log.e("JSONArrayException", e.toString());
                        }

                    }

                });
            } catch (IOException e) {
                Log.e("IOException", e.toString());
            }
            return params;
        } else
            // Metodo que se ejecuta cuando no hay conexion
            // a internet
            listener.onInternetError();
        return null;
    }
}
