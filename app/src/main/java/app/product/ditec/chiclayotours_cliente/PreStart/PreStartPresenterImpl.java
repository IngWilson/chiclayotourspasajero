package app.product.ditec.chiclayotours_cliente.PreStart;

import android.content.Context;
import android.content.SharedPreferences;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.GCM;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 27/06/15.
 */
public class PreStartPresenterImpl implements PreStartPresenter {

    private GCM gcm;
    private Context ctx;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private PreStartInteractor interactor;
    //private User user;
    private Validation validator;

    public PreStartPresenterImpl() {
    }

    public PreStartPresenterImpl(Context ctx) {
        this.ctx = ctx;
        validator = new Validation(ctx);
        interactor = new PreStartInteractorImpl();
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
        //user = new User(ctx);
        //user.setGCM("");
        validateSendGCM();
    }

    @Override
    public void validateSendGCM() {

/*        if (prefs.getBoolean(Config.ONLOGIN, false)) {
            gcm = new GCM(ctx);
            user = new User(ctx);

            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    return gcm.registerGCM();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    if (!s.isEmpty() || s != null || s == "") {

                        Log.i("NEW GCM", s);

                        try {
                            interactor.sendGCM(new JSONObject().put("gcm", s).put("email", user.getEmail()), validator);
                        } catch (JSONException e) {

                        }
                    }
                }
            }.execute(null, null, null);
        }*/
    }
}

