package app.product.ditec.chiclayotours_cliente.Register;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;


public class RegisterInteractorImpl implements RegisterInteractor {

    public void register(final JSONObject jsonParams, final OnRegisterFinishedListener listener, Validation validator) {

        //final ShareExternalServer appUtil = new ShareExternalServer();
        if (validator.isOnline()) {

            OkHttpRequest okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_REGISTERUSER));

            // Se verifica que hay un error
            boolean error = false;

            // Se verifica que los datos no sean vacios y si 
            // es que hay error entonces se coloca el booleano en true
            try {

                if (TextUtils.isEmpty(jsonParams.getString("email"))) {
                    listener.onEmailError();
                    error = true;
                }
                else
                if (!jsonParams.getString("email").matches("([a-z]|[A-Z]|[0-9])+")) {
                    listener.onSpaceError();
                    error = true;
                }

                if (TextUtils.isEmpty(jsonParams.getString("password"))) {
                    listener.onPasswordError();
                    error = true;
                }

                if (TextUtils.isEmpty(jsonParams.getString("first_name"))) {
                    listener.onNameError();
                    error = true;
                }

                if (TextUtils.isEmpty(jsonParams.getString("last_name"))) {
                    listener.onLastNameError();
                    error = true;
                }

/*                if (TextUtils.isEmpty(jsonParams.getString("phone"))) {
                    listener.onPhoneError();
                    error = true;
                }*/

            } catch (JSONException e) {
                Log.e("JSONException", e.toString());
            }

            // Si es que esta en true entonces no se envia
            if (!error) {
                try {
                    // Se crea la peticion POST y se asignan los parametros
                    okHttpRequest.post(jsonParams.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    listener.onError();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {

                            String body = response.body().string();
                            Log.i("RegisterIntec", body);

                            int codeResponse = response.code();

                            Log.i("R_Code", "" + codeResponse);

                            // Se verifica el codigo 
                            if (codeResponse == 200)
//                                new Handler(Looper.getMainLooper()).post(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        listener.onSuccess();
//                                    }
//                                });

                                // Se crea la tarea y se coloca en el hilo principal
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            // Metodo que se ejecuta cuando se envia con exito
                                            listener.onSuccess(jsonParams.getString("email"), jsonParams.getString("codpais"));
                                        } catch (JSONException e) {
                                            Log.e("Reg_JSONException", e.toString());
                                        }
                                    }
                                });
                            else if (codeResponse == 503) {

                                try {
                                    JSONObject validate = new JSONObject(body);

                                    if (body.isEmpty()) {
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    listener.onSuccess(jsonParams.getString("email"), jsonParams.getString("codpais"));
                                                } catch (JSONException e) {
                                                    Log.e("JSONException", e.toString());
                                                }
                                            }
                                        });

                                    } else {
                                        if (!validate.isNull("email"))
                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    listener.onShowDialogEmailError();
                                                }
                                            });
                                        else if (!validate.isNull("phone")) {
                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    listener.onShowDialogPhoneError();
                                                }
                                            });
                                        }
                                    }

                                } catch (JSONException e) {
                                    Log.e("R_JSONException", e.toString());
                                }

                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onShowDialogEmailError();
                                    }
                                });
                            } else
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onError();
                                    }
                                });

                        }
                    });

                } catch (IOException e) {
                    Log.e("RegisterokHttp", e.toString());
                }

            }

        } else
            listener.onShowDialogInternetError();
    }

    public void verifycodepromo(final JSONObject jsonParams, final OnRegisterFinishedListener listener, Validation validator) {
 /*      if(email.toString().equals("carlos"))
       return true;
       else*/

        if (validator.isOnline()) {

            OkHttpRequest okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_VERIFYCODEPROMO));

            Log.i("prueba", jsonParams.toString());
                try {

                    // Se crea la peticion POST y se asignan los parametros
                    okHttpRequest.post(jsonParams.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    listener.onError();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {

                            String body = response.body().string();
                            Log.i("RegisterIntec", body);

                            int codeResponse = response.code();

                            Log.i("R_Code", "" + codeResponse);

                            // Se verifica el codigo
                            if (codeResponse == 200)
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        // Metodo que se ejecuta cuando se envia con exito
                                            listener.onCorrecCode();

                                    }
                                });
                            else if (codeResponse == 500) {

                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {

                                            // Metodo que se ejecuta cuando se envia con exito
                                            listener.onErrorCode();

                                    }
                                });
                            } else
                                new Handler(Looper.getMainLooper()).post(new Runnable() {

                                    @Override
                                    public void run() {
                                        listener.onError();
                                    }
                                });

                        }
                    });

                } catch (IOException e) {
                    Log.e("RegisterokHttp", e.toString());

                }



        } else
       listener.onShowDialogInternetError();


    }

    public void sendcodepromo(final JSONObject jsonParams, final OnRegisterFinishedListener listener, Validation validator)
    {
        //metodo para enviar el codigo por la ruta SendCodePromo
        if (validator.isOnline()) {

            OkHttpRequest okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_SENDCODEPROMO));

            Log.i("prueba", jsonParams.toString());
            try {

                // Se crea la peticion POST y se asignan los parametros
                okHttpRequest.post(jsonParams.toString(), new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {

                            @Override
                            public void run() {
                                listener.onError();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {

                        String body = response.body().string();
                        Log.i("RegisterIntec", body);

                        int codeResponse = response.code();

                        Log.i("R_Code", "" + codeResponse);

                        // Se verifica el codigo
                        if (codeResponse == 200)
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    // Metodo que se ejecuta cuando se envia con exito
                                    listener.oncodeexit();

                                }
                            });
                        else if (codeResponse == 500) {

                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {

                                    // Metodo que se ejecuta cuando se envia con exito
                                    listener.onErrorCode();

                                }
                            });
                        } else
                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    listener.onError();
                                }
                            });

                    }
                });

            } catch (IOException e) {
                Log.e("RegisterokHttp", e.toString());

            }



        } else
            listener.onShowDialogInternetError();

    }
}
