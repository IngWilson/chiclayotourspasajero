package app.product.ditec.chiclayotours_cliente.VerifyPhone;

/**
 * Created by angel on 24/05/15.
 */
public interface VerifyPhonePresenter {

    void validatePhone(String phone);
}
