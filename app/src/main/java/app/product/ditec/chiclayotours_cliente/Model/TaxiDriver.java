package app.product.ditec.chiclayotours_cliente.Model;

import android.content.Context;
import android.content.SharedPreferences;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import app.product.ditec.chiclayotours_cliente.App;

/**
 * Created by angel on 16/05/15.
 */

// Notacion para indicar el nombre de la tabla
@Table(name = "TaxiDrivers",id = "id")
public class TaxiDriver extends Model {

    public static final String TAG = "TaxiData";
    public static final String NAME = "name";
    public static final String LAST_NAME = "lastname";
    public static final String AUTOPLATE = "autoplate";
    public static final String DNI = "dni";
    public static final String COMPANY = "company";
    public static final String CAR_BRAND = "car_brand";
    public static final String CAR_MODEL = "car_model";
    public static final String IMAGE = "image";
    public static final String IMAGE_URL = "imageUrl";
    public static final String GCM = "gcm_driver";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";

    // Notacion para indicar los nombres de la tabla
    @Column(name = "Name")
    private String name = "";

    @Column(name = "LastName")
    private String lastname = "";

    @Column(name = "Phone")
    private String phone = "";

    @Column(name = "Autoplate")
    private String autoplate = "";

    private String dni = "";

    @Column(name = "Company")
    private String company = "";

    @Column(name = "CarBrand")
    private String car_brand = "";

    @Column(name = "CarModel")
    private String car_model = "";

    private String image = "";
    private String imageUrl = "";
    public String gcm_driver = "";
    private String email = "";

    protected static SharedPreferences prefs = App.getInstance().getSharedPreferences(TAG, Context.MODE_PRIVATE);
    protected static SharedPreferences.Editor localEditor = prefs.edit();

    public TaxiDriver() {
        super();
    }

    // Metodo para guardar en preferences los datos del taxista    
    public static void saveTaxiDriver(TaxiDriver taxiDriver) {
        localEditor.putString(NAME, taxiDriver.name);
        localEditor.putString(LAST_NAME,taxiDriver.lastname);
        localEditor.putString(PHONE, taxiDriver.phone);
        localEditor.putString(AUTOPLATE, taxiDriver.autoplate);
        localEditor.putString(DNI, taxiDriver.dni);
        localEditor.putString(COMPANY, taxiDriver.company);
        localEditor.putString(CAR_BRAND, taxiDriver.car_brand);
        localEditor.putString(CAR_MODEL, taxiDriver.car_model);
        localEditor.putString(GCM, taxiDriver.gcm_driver);
        localEditor.putString(IMAGE,taxiDriver.image);
        localEditor.putString(IMAGE_URL,taxiDriver.imageUrl);
        localEditor.commit();
    }

    // Metodo para limpiar los datos del preferences
    public static void clean() {
        localEditor.putString(NAME, null);
        localEditor.putString(LAST_NAME,null);
        localEditor.putString(PHONE, null);
        localEditor.putString(AUTOPLATE, null);
        localEditor.putString(DNI, null);
        localEditor.putString(COMPANY, null);
        localEditor.putString(CAR_BRAND, null);
        localEditor.putString(CAR_MODEL, null);
        localEditor.putString(GCM, null);
        localEditor.putString(IMAGE,null);
        localEditor.putString(IMAGE_URL,null);
        localEditor.commit();
    }

    // Metodos que comienzan con "save" sirven 
    // para guardar los parametros
    // en el prefereces
    // y los metodos que comienzan con "load" sirven
    // para recuperar los datos del preferences


    public static void saveName(String name){
        localEditor.putString(NAME, name);
        localEditor.commit();
    }

    public static String loadNAME(){
        return prefs.getString(NAME,"");
    }

    public static void saveLastName(String lastname){
        localEditor.putString(LAST_NAME, lastname);
        localEditor.commit();
    }

    public static String loadLastName(){
        return prefs.getString(LAST_NAME,"");
    }

    public static void savePhone(String phone){
        localEditor.putString(PHONE, phone);
        localEditor.commit();
    }

    public static String loadPhone(){
        return prefs.getString(PHONE,"");
    }

    public static void saveAutoplate(String autoplate){
        localEditor.putString(AUTOPLATE, autoplate);
        localEditor.commit();
    }

    public static String loadAutoplate(){
        return prefs.getString(AUTOPLATE,"");
    }

    public static void saveDNI(String dni){
        localEditor.putString(DNI, dni);
        localEditor.commit();
    }

    public static String loadDNI(){
        return prefs.getString(DNI,"");
    }

    public static void saveCompany(String company){
        localEditor.putString(COMPANY, company);
        localEditor.commit();
    }

    public static String loadCompany(){
        return prefs.getString(COMPANY,"");
    }

    public static void saveCar_brand(String car_brand){
        localEditor.putString(CAR_BRAND, car_brand);
        localEditor.commit();
    }

    public static String loadCar_brand(){
        return prefs.getString(CAR_BRAND,"");
    }

    public static void saveCar_model(String car_model){
        localEditor.putString(CAR_MODEL, car_model);
        localEditor.commit();
    }

    public static String loadCar_model(){
        return prefs.getString(CAR_MODEL,"");
    }

    public static void saveGCM(String gcm){
        localEditor.putString(GCM, gcm);
        localEditor.commit();
    }

    public static String loadGCM(){
        return prefs.getString(GCM,"");
    }

    public static void saveImage(String image){
        localEditor.putString(IMAGE, image);
        localEditor.commit();
    }

    public static String loadImage(){
        return prefs.getString(IMAGE,"");
    }

    public static void saveImageUrl(String image_url){
        localEditor.putString(IMAGE_URL, image_url);
        localEditor.commit();
    }

    public static String loadImageUrl(){
        return prefs.getString(IMAGE_URL,"");
    }
    
    public static void saveEmail(String email){
        localEditor.putString(EMAIL, email);
        localEditor.commit();
    }
    
    public static String loadEmail(){
        return prefs.getString(EMAIL,"");
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPhone() {
        return phone;
    }

    public String getAutoplate() {
        return autoplate;
    }

    public String getDni() {
        return dni;
    }

    public String getCompany() {
        return company;
    }

    public String getCar_brand() {
        return car_brand;
    }

    public String getCar_model() {
        return car_model;
    }

    public String getImage() {
        return image;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getGcm_driver() {
        return gcm_driver;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAutoplate(String autoplate) {
        this.autoplate = autoplate;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setCar_brand(String car_brand) {
        this.car_brand = car_brand;
    }

    public void setCar_model(String car_model) {
        this.car_model = car_model;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setGcm_driver(String gcm_driver) {
        this.gcm_driver = gcm_driver;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
