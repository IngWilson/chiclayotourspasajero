package app.product.ditec.chiclayotours_cliente.Promotion;

import android.content.Context;

/**
 * Created by usuario on 29/03/2016.
 */
public interface PromotionView {
    void setId(String idpromotion);

    void setTotalc(String totalc);

    void setSituacion(String situacion);

    void setEstado(String estado);

    Context getContext();

    void showProgress();

    void hideProgress();

    void finishAccount();

    void showDialogInternetError();
//envio de datos
    void sendcode();
    void showDialogSuccessSend();
    void showDialogFailedSend();
    //void showDialogConfirmCode();
    void hideProgressE();
    void showProgressE();
    void showDialogCodeError();
    void showDialogEmptyCode();
    void showDialogRepeatCodeError();
    void showDialogCodeExpireError();
    void showDialogError();

    // fin de envio de datos

}
