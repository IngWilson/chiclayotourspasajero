package app.product.ditec.chiclayotours_cliente.Model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import android.content.Context;
import android.content.SharedPreferences;

import app.product.ditec.chiclayotours_cliente.App;



@Table(name = "Requests",id = "id")
public class Request extends Model {

    private static final String ADDRESS    = "address";
    private static final String DAY        = "day";
    private static final String MONTH      = "month";
    private static final String YEAR       = "year";
    private static final String TAG        = "RequestData";

    @Column(name = "Address")
    private String address;

    @Column(name = "TaxiDriver")
    private TaxiDriver taxiDriver;

    @Column(name = "Day")
    private int day;

    @Column(name = "Month")
    private int month;

    @Column(name = "Year")
    private int year;

    @Column(name = "Hour")
    private int hour;

    @Column(name = "Minute")
    private int minute;

    @Column(name = "Precio")
    private String precio;

    protected static SharedPreferences prefs = App.getInstance().getSharedPreferences(TAG, Context.MODE_PRIVATE);
    protected static SharedPreferences.Editor localEditor = prefs.edit();


    public Request(){
        super();
    }
    public static void saveId(String id ){
        localEditor.putString("IDE",id );
        localEditor.commit();
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public static String loadId(){
        return prefs.getString("IDE","");
    }
    public static float loadLatitudeA(){
        return prefs.getFloat("LATA", 0);
    }

    public static void saveLatitudeA(float latitude){
        localEditor.putFloat("LATA", latitude);
        localEditor.commit();
    }

    public static float loadLongitudeA(){
        return prefs.getFloat("LONGA", 0);
    }

    public static void saveLongitudeA(float longitude){
        localEditor.putFloat("LONGA", longitude);
        localEditor.commit();
    }

    public static float loadLatitudeB(){
        return prefs.getFloat("LATB", 0);
    }

    public static void saveLatitudeB(float latitude){
        localEditor.putFloat("LATB", latitude);
        localEditor.commit();
    }

    public static float loadLongitudeB(){
        return prefs.getFloat("LONGB", 0);
    }

    public static void saveLongitudeB(float longitude){
        localEditor.putFloat("LONGB", longitude);
        localEditor.commit();
    }

    public static float loadLatitude(){
        return prefs.getFloat("LAT", 0);
    }

    public static void saveLatitude(float latitude){
        localEditor.putFloat("LAT", latitude);
        localEditor.commit();
    }

    public static float loadLongitude(){
        return prefs.getFloat("LONG", 0);
    }

    public static void saveLongitude(float longitude){
        localEditor.putFloat("LONG", longitude);
        localEditor.commit();
    }

    public static String loadAddress(){
        return prefs.getString(ADDRESS,"");
    }

    public static void saveAddress(String address){
        localEditor.putString(ADDRESS, address);
        localEditor.commit();
    }

    public static String loadMonth(){
        return prefs.getString(MONTH,"");
    }

    public static void saveMonth(String month ){
        localEditor.putString(MONTH, "");
        localEditor.commit();
    }

    public static String loadDay(){
        return prefs.getString(DAY,"");
    }

    public static void saveDay( ){
        localEditor.putString(DAY, "");
        localEditor.commit();
    }

    public static String loadYear(){
        return prefs.getString(YEAR,"");
    }

    public static void saveYear(String year){
        localEditor.putString(YEAR, year);
        localEditor.commit();
    }

    public String getAddress() {
        return address;
    }

    public TaxiDriver getTaxiDriver() {
        return taxiDriver;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTaxiDriver(TaxiDriver taxiDriver) {
        this.taxiDriver = taxiDriver;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }
}