package app.product.ditec.chiclayotours_cliente.History.MonthTabs;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.History.HistoryActivity;
import app.product.ditec.chiclayotours_cliente.History.HistoryAdapter;
import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFebruary extends Fragment {

    View view;
    HistoryAdapter historyAdapter;
    ListView lvFebruary;

    public FragmentFebruary() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_february, container, false);
        lvFebruary = (ListView) view.findViewById(R.id.lvFebruary);
        /*historyAdapter = new HistoryAdapter(view.getContext());*/
        historyAdapter = new HistoryAdapter(view.getContext());

        try {
            if (!HistoryActivity.getmRequest(2).equals(null)) {
                historyAdapter.setData(new ArrayList<Request>(HistoryActivity.getmRequest(2)));
                lvFebruary.setAdapter(historyAdapter);
            }
        }catch (NullPointerException e){}
        return view;
    }


}
