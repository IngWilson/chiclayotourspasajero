package app.product.ditec.chiclayotours_cliente.History;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentApril;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentAugust;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentDecember;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentFebruary;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentJanuary;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentJuly;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentJune;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentMarch;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentMay;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentNovember;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentOctober;
import app.product.ditec.chiclayotours_cliente.History.MonthTabs.FragmentSeptember;

/**
 * Created by pc on 11/08/2015.
 */
public class HistoryPageAdapter extends FragmentPagerAdapter {

    //declaro una variable de tipo context y el numero todal de paginas
    final int PAGE_COUNT= 12;
    Context context;
    //declaro un array de strings que contienen los meses del año
    private String tabTitles[] = new String[] { "Enero", "Febrero", "Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre" };
    //creamos el contructor de la clase
    public HistoryPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }
    //segun la posicion del fragmento cambiara de pagina(actividad)
    @Override
    public Fragment getItem(int position) {

        Fragment fragment= null;

        if (position==0){
            fragment  = new FragmentJanuary();
        }
        if (position==1) {
            fragment = new FragmentFebruary();
        }
        if (position==2){
            fragment = new FragmentMarch();
        }
        if (position==3){
            fragment = new FragmentApril();
        }
        if (position==4){
            fragment = new FragmentMay();
        }
        if (position==5){
            fragment = new FragmentJune();
        }
        if (position==6){
            fragment = new FragmentJuly();
        }
        if (position==7){
            fragment = new FragmentAugust();
        }
        if (position==8){
            fragment = new FragmentSeptember();
        }
        if (position==9){
            fragment = new FragmentOctober();
        }
        if (position==10){
            fragment = new FragmentNovember();
        }
        if (position==11){
            fragment = new FragmentDecember();
        }

        return fragment;
    }
    //retorna el numero de pagina el que se encuentra
    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
    //retorna el mes del año en que se encuentra segun la posicion
    public CharSequence getPageTitle(int position){
        return tabTitles[position];
    }
}

