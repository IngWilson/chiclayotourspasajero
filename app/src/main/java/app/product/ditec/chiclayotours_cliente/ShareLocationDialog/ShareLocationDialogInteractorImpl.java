package app.product.ditec.chiclayotours_cliente.ShareLocationDialog;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

public class ShareLocationDialogInteractorImpl implements ShareLocationDialogInteractor{
    private Socket socket;
    private ShareLocationDialogFinishedListener listener;
    public ShareLocationDialogInteractorImpl(ShareLocationDialogFinishedListener _listener){
        listener = _listener;
        // Se obtiene el Socket de la App
        socket = App.getSocket();
        // Se activa los escuchadores
        // para el evento de reconectar
        // conectar, reconectando,
        // reconectar, desconectar y cuando existe error
        socket.on(Socket.EVENT_RECONNECT,connect);
        socket.on(Socket.EVENT_CONNECT, connect);
        socket.on(Socket.EVENT_RECONNECTING,reconnecting);
        socket.on(Socket.EVENT_RECONNECT,connect);
        socket.on(Socket.EVENT_DISCONNECT,disconnect);
        socket.on(Socket.EVENT_ERROR,event_error);
        // Se activa el mensaje "arrive","error" y "taxi location"
/*        socket.on("arrive",arrive);
        socket.on("error", error);
        socket.on("taxi location",taxi_location);
        socket.on("ontravel", ontravel);
        socket.on("getonboard", getonboard);*/

        // Se conecta el socket
        socket.connect();
    }
    // Escuchador cuando se "Conecta"
    private Emitter.Listener connect = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            // Metodo que envia el signin
            // para pasar el socket_id
            signin();
        }
    };
    // Metodo que envia el "signIn"
    @Override
    public void signin() {
        Log.i("misharelocation","signin");
        // Se crea una tarea en el hilo principal
        new Handler(Looper.getMainLooper()).post(
                new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(App.getInstance(),"Conectando..",Toast.LENGTH_SHORT).show();
                        new Handler(Looper.getMainLooper()).post(
                                new Runnable() {
                                    @Override
                                    public void run() {

//                                        Toast.makeText(App.getInstance(),"Esta conectado..",Toast.LENGTH_LONG).show();
                                    }
                                }
                        );
                        // Se verifica que el socket esta conectado
                        if(socket.connected()) {

                            // Se emite un mensaje "signin" y se envia
                            // como parametros el email del pasajero

                            socket.emit("signin", User.loadEmail(), new Ack() {
                                @Override
                                public void call(Object... args) {
                                    // Se obtiene la respuesta

                                }
                            });
                        }
                    }
                }
        );

    }
    // Escuchador cuando "Reconectando"
    private Emitter.Listener reconnecting = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            // Se ejecuta el metodo del presentador para indicar que se reconectando
                        }
                    }
            );
        }
    };
    private Emitter.Listener disconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            // Se ejecuta el metodo del presentador para indicar
                            // que esta desconectado

                        }
                    }
            );

        }
    };

    // Escuchador cuando se "Evento error"
    private Emitter.Listener event_error = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            // Se crea un manejador y que obtiene
            // el hilo principal
            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(((MapPresenter) listener).getView().getContext(), "Hubo un error al conectar..", Toast.LENGTH_LONG).show();
                        }
                    }
            );
        }
    };

    @Override
    public void enviarubicacion(JSONObject params, final Validation validator) {
        Log.i("mierrormensaje", " " + params);
        Log.i("mierrormensaje", " " + "inicio interactor");
        //Se valida si hay internet
        if(validator.isOnline()){

            //Se emite el mensaje de "arrive" al pasajero
            socket.emit("enviarubicacion", params, new Ack() {

                @Override
                public void call(Object... args) {
                    String response = (String)args[0];
                    Log.i("mierrormensaje", "response:"+response);
                    //Si es que es "OK" la respuesta entonces se envio correctamente
                    if(response.equals("OK")) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onSuccessSend();
                            }
                        });


                    }else

                    if(response.equals("noexist")){
                        new Handler(Looper.getMainLooper()).post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onFailedNotFound();
                                    }
                                }
                        );
                    }
                        //Si es que es "error" entonces hubo un error en el server
                        if(response.equals("error")){
                            new Handler(Looper.getMainLooper()).post(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            listener.onFailedSend();
                                        }
                                    }
                            );
                        }
                }
            });

        }
        else
            listener.onErrorConnection();

    }

    @Override
    public void finalizarenviarubicacion(JSONObject params, final Validation validator) {
           //Se valida si hay internet
        if(validator.isOnline()){

            //Se emite el mensaje de "arrive" al pasajero
            socket.emit("finalizarenviarubicacion", params, new Ack() {

                @Override
                public void call(Object... args) {
                    String response = (String)args[0];
                    Log.i("mierrormensaje", "response:"+response);
                    //Si es que es "OK" la respuesta entonces se envio correctamente
                    if(response.equals("OK")) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onSuccessFinalizarSend();
                            }
                        });
                    }else
                        //Si es que es "error" entonces hubo un error en el server
                        if(response.equals("error")){
                            new Handler(Looper.getMainLooper()).post(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            listener.onFailedSend();
                                        }
                                    }
                            );
                        }
                }
            });

        }
        else
            listener.onErrorConnection();

    }
}
