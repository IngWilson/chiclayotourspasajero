package app.product.ditec.chiclayotours_cliente.ShareLocationDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import app.product.ditec.chiclayotours_cliente.R;
public class ShareLocationDialogActivity extends Activity implements ShareLocationDialogView{
    ShareLocationDialogPresenter presenter;
    EditText edtsharenumber;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_location_dialog);
        presenter = new ShareLocationDialogPresenterImpl(this);
        edtsharenumber=findViewById(R.id.edtsharenumber);

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Enviando respuesta al pasajero...");
        progressDialog.setCancelable(false);
    }
    @Override
    public Context getContext() {
        return ShareLocationDialogActivity.this;
    }


    public void enviarubicacion(View view) {
        if(edtsharenumber.getText().toString().length()>8)
        presenter.enviarubicacion(edtsharenumber.getText().toString());
                else {
            AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
            alertDialogAbout.setMessage("El número celular ingresado no es valido")
                    .setTitle("Verifique el número")
                    .setPositiveButton("Aceptar", null)
                    .show();
        }

    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void showDialogFailedSend() {
        Toast.makeText(this,"No se pudo enviar correctamente, intentelo nuevamente",Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialogFailedNotFound() {
        Toast.makeText(this,"El numero celular escrito no esta en nuestra BD, recomienda a tu amigo descargar nuestra app",Toast.LENGTH_LONG).show();
        Intent paramView;
        paramView = new Intent("android.intent.action.SEND");
        paramView.setType("text/plain");

        paramView.putExtra("android.intent.extra.TEXT", getString(R.string.nombreapp) + " "+ getString(R.string.mensaje2)  + " " + /*promotext + " " +*/
                getString(R.string.mensaje_descarga) + " \n" + getString(R.string.link));
        startActivity(Intent.createChooser(paramView, "Compartele nuestro aplicativo"));
    }

    @Override
    public void showProgress() {
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    @Override
    public void showDialogConnection(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage("Activa los datos o WI-FI en la configuracion");
        builder.setTitle("Los datos estan desactivados");
        builder.setPositiveButton("Configuración", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                return;
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                return;
            }
        });

        builder.show();
    }

    @Override
    public void envioexitoso(){
        AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
        alertDialogAbout.setMessage("Se le confirmara cuando "+edtsharenumber.getText().toString() +" acepte recibir su ubicación, si no esta en linea indiquele que ingrese a la aplicación e intentelo de nuevo")
                .setTitle("Enviando ubicación")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        finish();
                    }
                })
                .show();
    }

    @Override
    public void SuccessFinalizarSend(){
        AlertDialog.Builder alertDialogAbout = new AlertDialog.Builder(this);
        alertDialogAbout.setMessage("Se dejo de enviar su ubicación")
                .setTitle("Finalizando envio de ubicación")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        finish();
                    }
                })
                .show();
    }

    public void finalizarenviarubicacion(View view) {
        presenter.finalizarenviarubicacion();
    }
}
