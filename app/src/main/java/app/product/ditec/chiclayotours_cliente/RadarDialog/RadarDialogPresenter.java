package app.product.ditec.chiclayotours_cliente.RadarDialog;

/**
 * Created by angel on 20/04/15.
 */
public interface RadarDialogPresenter {

    void validateCancelRequest();

    void onRadar(boolean active);

    RadarDialogView getView();

    void desactiveSocket();
}
