package app.product.ditec.chiclayotours_cliente.Account;

/**
 * Created by angel on 05/05/15.
 */
public interface AccountPresenter {

    void validateGetData();

    boolean checkData();

}
