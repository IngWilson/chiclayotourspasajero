package app.product.ditec.chiclayotours_cliente.Code;

import android.content.Context;

/**
 * Created by lenovo on 13/05/2015.
 */
public interface CodeView {

    void showProgress();

    void hideProgress();

    void finished();

    void showDialogSuccessSend();

    void showDialogErrorSend();

    void showDialogInternetError();

    void showDialogFailSend();

    void showDialogFailreSend();

    void successresend();

    void navigateToLogin();

    void showCodeError();

    Context getContext();

    void sendVerificationCode(String number);
}
