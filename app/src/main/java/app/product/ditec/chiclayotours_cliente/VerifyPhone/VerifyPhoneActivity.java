package app.product.ditec.chiclayotours_cliente.VerifyPhone;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.ActionBar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import app.product.ditec.chiclayotours_cliente.Code.CodeActivity;
import app.product.ditec.chiclayotours_cliente.R;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

//import com.actionbarsherlock.app.ActionBar;
//import com.actionbarsherlock.app.SherlockActivity;

/**
 * Created by angel on 24/05/15.
 */
public class VerifyPhoneActivity extends AppCompatActivity implements VerifyPhoneView, Dialog.OnClickListener {

    EditText edtPhone;
    Button btnSendPhone;

    VerifyPhonePresenter presenter;

    private AlertDialog.Builder dialogConfirmPhone;
    private AlertDialog.Builder alertDialog;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);



        // con esto coloco el actionbar personalisado en el proyecto akitaxi
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_autoregistro);

        presenter = new VerifyPhonePresenterImpl(this);

        edtPhone=findViewById(R.id.edtPhoneNumber);
        btnSendPhone=findViewById(R.id.btnSendPhone);

        dialogConfirmPhone = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        alertDialog = new AlertDialog.Builder(this);
    }

    //para validar que se obtuvo el telefono.
    public void sendPhone() {
        presenter.validatePhone(edtPhone.getText().toString());
    }
    //metodo para obtener el context
    public Context getContext() {
        return VerifyPhoneActivity.this;
    }
    //metodo para afirmacion si el numero se envio correctamente
    @Override
    public void showDialogSuccessSend() {
        Toast.makeText(this, getString(R.string.VerifyPhoneActivity_showDialogSuccessSend), Toast.LENGTH_LONG).show();
    }
    //metodo para enviar error si el telefono no es correcto
    @Override
    public void showDialogFailedSend() {
        Toast.makeText(this, getString(R.string.VeeifyphoneActivity_showDialogFailedSend), Toast.LENGTH_LONG).show();
    }
    //metodo para cambiar de actividad a la de poner el codigo de verificacion que se le enviara al
    //celular
    @Override
    public void navigateToCode(String phone) {
        startActivity((new Intent(this, CodeActivity.class)).putExtra("phone", phone));
    }

    //metodo de advertencia para ver si el numero de telefono ingresado es el correcto
    public void showDialogConfirmPhone(View view) {
        dialogConfirmPhone.setCancelable(false)
                .setTitle("Advertencia")
                .setMessage("¿Su numero es el correcto " + edtPhone.getText().toString() + " ?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.validatePhone(edtPhone.getText().toString());
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    //metodo para ocultar el showprogress
    @Override
    public void hideProgress() {
        progressDialog.hide();
    }


    //showprogress que te dice enviando telefono es como un dialogo de aviso
    @Override
    public void showProgress() {
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage(getString(R.string.VerifyPhoneActivity_showProgress));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    //metodo para conectarte a internet si no estas conectado, te lleva a ajustes
    @Override
    public void showDialogInternetError() {
        alertDialog.setTitle(getString(R.string.dialog_conection_title))
                .setMessage(getString(R.string.dialog_conection_message))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }
    //metodo que te dice si el numero de verificacion que ingresaste ya esta registrado
    @Override
    public void showDialogPhoneError() {
        Toast.makeText(this, getString(R.string.VerifyPhoneActivity_Dialog_Phone_Error), Toast.LENGTH_LONG).show();
    }

    //metodo que te dice si  ingresaste un numero incorrecto para verificar tu cuenta.
    public void showDialogEmptyPhone() {
        Toast.makeText(this, getString(R.string.VerifyPhoneActivity_Dialog_Empty_Phone), Toast.LENGTH_LONG).show();
    }
    //metodo por si nose envio el telefono por errores de coneccion o por el internet lento
    @Override
    public void showDialogError() {
        Toast.makeText(this, getString(R.string.VerifyPhoneActivity_Dialog_Error), Toast.LENGTH_LONG).show();
    }
    //metodo para finalizar
    @Override
    public void finished() {
        finish();
    }

    //metodo que invoca el menu de configuraciones
    @Override
    public void onClick(DialogInterface dialog, int which) {
        startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
    }
}
