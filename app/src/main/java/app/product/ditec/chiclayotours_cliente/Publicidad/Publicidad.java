package app.product.ditec.chiclayotours_cliente.Publicidad;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;
import android.widget.TextView;


import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Login.LoginActivity;
import app.product.ditec.chiclayotours_cliente.Permission.ActivityPermission;

import app.product.ditec.chiclayotours_cliente.R;

/**
 * Created by usuario on 09/12/2016.
 */

public class Publicidad extends Activity implements PublicidadView{

    private static RequestQueue colaPeticiones;
    private static ImageLoader lectorImagenes;

private ImageView publiimg;
    private ImageView publiimg2;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitypublicidad);

/*        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setMax(maximumProgress());*/


/*        colaPeticiones = Volley.newRequestQueue(this);
        lectorImagenes = new ImageLoader(colaPeticiones, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }
        });*/
/*        NetworkImageView publicidadimg;
        publicidadimg=new NetworkImageView(this);
        publicidadimg.setImageUrl("http://files2.akitaxi.com/auspuser.jpg", lectorImagenes);*/

      // publiimg=(ImageView) findViewById((R.id.publicidadimg));
  /*      publiimg.setScaleType(ImageView.ScaleType.FIT_XY);
       publiimg.setImageUrl("http://files2.akitaxi.com/auspuser.jpg", lectorImagenes);*/
       // publiimg.setScaleType(ImageView.ScaleType.FIT_XY);

        //Usando picasso
     /*   publiimg=new ImageView(this);
        Picasso.with(this).invalidate("http://files2.akitaxi.com/auspuser.jpg");
        Picasso.with(this).load("http://files2.akitaxi.com/auspuser.jpg").into(publiimg);*/

       // publiimg2.setScaleType(ImageView.ScaleType.CENTER_CROP);
        //publiimg2.setImageUrl("http://files2.akitaxi.com/auspuser.jpg", lectorImagenes);
publiimg=(ImageView) findViewById((R.id.publicidadimg));
        publiimg.setImageDrawable(App.getPublicidad().getDrawable());

      //  publiimg.setImageDrawable(publicidadimg.getDrawable());
//publiimg.setImageBitmap(publicidadimg.getBitmap());

//        publiwil.setImageDrawable(publicidadimg.getDrawable());
/*
        lectorImagenes.get("http://files2.akitaxi.com/auspuser.jpg",
                ImageLoader.getImageListener(publicidadimg, R.drawable.taxi, R.drawable.radar2));*/

        //presenter = new PreStartPresenterImpl(this);
        startAnimation();
    }

    //Se comienza la animacion
    @Override
    public void startAnimation() {

        //Se crea un contador de 3 segundos
        new CountDownTimer(0, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
               /* progress.setProgress(restore(millisUntilFinished));*/
            }

            //Metodo que se ejecuta cuando termina
            @Override
            public void onFinish() {
                if (Build.VERSION.SDK_INT>= 23) {
                    //Toast.makeText(Publicidad.this, "Eres un Marshmallow o superior", Toast.LENGTH_SHORT).show();
                    // only for gingerbread and newer versions

                Intent newfrom = new Intent(Publicidad.this, ActivityPermission.class);
                startActivity(newfrom);
                finish();
                }
                else
                {
                    //Toast.makeText(Publicidad.this, "Eres inferior a un Marshmallow", Toast.LENGTH_SHORT).show();
                    Intent newfrom = new Intent(Publicidad.this, LoginActivity.class);
                    startActivity(newfrom);
                    finish();
                }
             // Toast.makeText(Publicidad.this, "fin de animacion", Toast.LENGTH_SHORT).show();


            }
        }.start();

    }


}