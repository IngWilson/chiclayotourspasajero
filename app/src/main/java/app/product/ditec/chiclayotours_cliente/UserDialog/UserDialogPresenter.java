package app.product.ditec.chiclayotours_cliente.UserDialog;

/**
 * Created by angel on 15/04/15.
 */
public interface UserDialogPresenter {

    void setUserData();

    void endUserDialog();

}
