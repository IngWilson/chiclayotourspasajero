package app.product.ditec.chiclayotours_cliente.RadarDialog;

import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;

/**
 * Created by angel on 26/04/15.
 */
public interface OnRadarFinishedListener {

    void onSuccessCancel();

    void onError();

    void onConnectionError();

    void onSuccessValidateRequest(TaxiDriver taxiDriver);

    void onFailedValidateRequest();

}
