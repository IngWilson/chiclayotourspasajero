package app.product.ditec.chiclayotours_cliente.Code;

/**
 * Created by angel on 18/05/15.
 */
public interface OnCodeDialogFinishedListener {

    void onSuccessSend();

    void onFailSend();

    void onErrorSend();

    void onInternetError();

    void onCodeError();

    void onSuccessreSend();

    void onFailreSend();

}
