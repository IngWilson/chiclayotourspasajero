package app.product.ditec.chiclayotours_cliente.Login;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.GCM;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;


public class LoginPresenterImpl implements LoginPresenter, OnLoginFinishedListener {

    private LoginView loginView;
    private LoginInteractor loginInteractor;
    private Validation validator;
    private GCM gcm;
    private AsyncTask<Void, Void, String> asyncTask;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Context ctx;


    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        ctx = loginView.getContext();
        this.loginInteractor = new LoginInteractorImpl();
        this.validator = new Validation(loginView.getContext());
        prefs = loginView.getContext().getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
        gcm = new GCM(ctx);
    }

    @Override
    public void validateCredentials(final String email, final String password) {

        loginView.showProgress();

        User.saveEmail(email);
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("email", email);
            jsonParams.put("password", password);
            jsonParams.put("gcm", "adadadad");
            jsonParams.put("type", "user");
            loginInteractor.login(jsonParams, LoginPresenterImpl.this, validator);
        } catch (JSONException e) {
            Log.e("LoginException", e.toString());
        }

    }

    @Override
    public void validatePhone(final String phone) {

        loginView.showProgress();

        User.savePhone("+51" +" "+ phone);
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("phone", phone);
            jsonParams.put("codpais", "+51");
            jsonParams.put("phonev", phone);
            loginInteractor.loginPhone(jsonParams, LoginPresenterImpl.this, validator);
        } catch (JSONException e) {
            Log.e("LoginException", e.toString());
        }

    }
    @Override
    public void onUsernameError() {
        loginView.setEmailError();
        loginView.hideProgress();
    }

    @Override
    public void onPhoneError() {
        loginView.setPhoneError();
        loginView.hideProgress();
    }

    @Override
    public void onPasswordError() {
        loginView.setPasswordError();
        loginView.hideProgress();
    }

    @Override
    public void onConnectionError() {
        loginView.showDialogErrorConnection();
        loginView.hideProgress();
    }

    @Override
    public void onLoginError() {
        loginView.showDialogErrorLogin();
        loginView.hideProgress();
    }

    @Override
    public void onUserPassError() {
        loginView.showDialogErrorUserPass();
        loginView.hideProgress();
    }

    @Override
    public void onSuccess() {
        editor.putBoolean(Config.ONLOGIN, true);
        editor.commit();
        loginView.hideProgress();
        loginView.navigateToMap();
        loginView.finishLogin();
    }

    @Override
    public void onSuccessPhone(String email) {
        User.saveEmail(email);
        editor.putBoolean(Config.ONVERIFY, true);
        editor.commit();
        loginView.hideProgress();
        loginView.arranque();
    }

    @Override
    public void onRegisterPhone() {

        loginView.hideProgress();
        loginView.navigateToRegister();
    }
//onregister >.<


    @Override
    public boolean validateSession() {
        return prefs.getBoolean(Config.ONLOGIN, false);
    }

    //indica que debemos ir a code
    @Override
    public boolean validateVerifyPhone() {
        return prefs.getBoolean(Config.ONVERIFY, false);
    }

    @Override
    public boolean validateRegister() {
        return prefs.getBoolean(Config.ONREGISTER, false);
    }

    @Override
    public boolean validatetoRegister() {
        return prefs.getBoolean(Config.ONREGISTERPHONE, false);
    }

    @Override
    public void validateUserTest() {
        User.savePhone("+51" +" "+ "1234567890999");
        User.saveEmail("1234567890999");
        editor.putBoolean(Config.ONVERIFY, false);
        //añadido para ingreso directo
        editor.putBoolean(Config.ONLOGIN, true);
        //fin del añadido
        editor.commit();
        loginView.hideProgress();
        loginView.arranque();
    }
}
