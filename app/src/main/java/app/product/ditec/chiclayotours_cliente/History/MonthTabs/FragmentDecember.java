package app.product.ditec.chiclayotours_cliente.History.MonthTabs;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.History.HistoryActivity;
import app.product.ditec.chiclayotours_cliente.History.HistoryAdapter;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDecember extends Fragment {


    HistoryAdapter historyAdapter;
    ListView lvDecember;
    View view;

    public FragmentDecember() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_december, container, false);
        lvDecember = (ListView) view.findViewById(R.id.lvDecember);
        /*historyAdapter = new HistoryAdapter(view.getContext());*/
        historyAdapter = new HistoryAdapter(view.getContext());

        try {
            if (!HistoryActivity.getmRequest(12).equals(null)) {
                historyAdapter.setData(new ArrayList<>(HistoryActivity.getmRequest(12)));
                lvDecember.setAdapter(historyAdapter);
            }
        }catch (NullPointerException e){}
        return view;
    }


}
