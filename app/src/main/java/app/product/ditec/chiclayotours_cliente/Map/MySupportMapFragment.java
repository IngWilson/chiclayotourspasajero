package app.product.ditec.chiclayotours_cliente.Map;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by angel on 14/08/15.
 */


// Fragment que se encuentra encima
// del mapa de la actividad
// y que sirve para verificar si esta siendo tocado
public class MySupportMapFragment extends SupportMapFragment {

    // Vista para el Fragment
    public View mOriginalContentView;
    // Wrapper que verifica si esta siendo tocado o no
    public TouchableWrapper mTouchView;

    // Metodo que crea la vista
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, container, savedInstanceState);
        // Se crea una clase para obtener el evento Touch
        mTouchView = new TouchableWrapper(getActivity());
        // Se añade el evento Touch al mapa
        mTouchView.addView(mOriginalContentView);
        return mTouchView;
    }

    @Nullable
    @Override
    public View getView() {
        return super.getView();
    }
}
