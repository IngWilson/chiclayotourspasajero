package app.product.ditec.chiclayotours_cliente.RecoverPassword;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by lenovo on 19/06/2015.
 */
public class RecoverPasswordPresenterImpl implements RecoverPasswordPresenter, OnRecoverPasswordFinishedListener {

    RecoverPasswordInteractor interactor;

    Validation validator;
    RecoverPasswordView recoverPasswordView;
    Context ctx;
    Bundle extras;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    public RecoverPasswordPresenterImpl(RecoverPasswordView recoverPasswordView) {
        this.recoverPasswordView = recoverPasswordView;
        interactor = new RecoverPasswordInteractorImpl();
        ctx = recoverPasswordView.getContext();
        validator = new Validation(ctx);
        extras = ((Activity) ctx).getIntent().getExtras();
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    //Metodo que valida el envio de email
    @Override
    public void validateEmail(String email) {

        //Metodo que muestra un dialogo de progreso
        recoverPasswordView.showProgress();
        
        //Se crea un objeto JSON coloca el email como parametro
        JSONObject param = new JSONObject();
        try {
            param.put("email", email);
        } catch (JSONException e) {
            Log.e("VP_JSONException", e.toString());
        }

        //Metodo que envia el email para recuperar contraseña
        interactor.sendEmail(param, validator, this);
    }

    //Metodo que se activa cuando se envia correctamente el email
    @Override
    public void onSuccessSend() {
        //Metodo que muestra un dialogo indicando que se envio correctamente el email
        recoverPasswordView.showDialogSuccessSend();
        //Metodo que oculta el dialogo de progreso
        recoverPasswordView.hideProgress();
        //Metodo que finaliza la actividad de recuperar contraseña
        recoverPasswordView.finished();
    }

    //Metodo que se ejecuta cuando no se envia correctamente
    @Override
    public void onFailedSend() {
        recoverPasswordView.showDialogFailedSend();
        recoverPasswordView.hideProgress();
    }

    //Metodo que se ejecuta cuando no hay conexion a internet
    @Override
    public void onInternetError() {
        //Metodo que muestra un dialogo indicando que hay un error de conexion
        recoverPasswordView.showDialogInternetError();
        //Metodo que oculta el dialogo de progreso
        recoverPasswordView.hideProgress();
    }

    //Metodo que se ejecuta cuando hay un error en el email
    @Override
    public void onEmailError() {

        //Metodo que muestra un dialogo indicando que hay un error
        recoverPasswordView.showDialogEmailError();
        //Metodo que oculta el dialogo de progreso
        recoverPasswordView.hideProgress();


    }

    //Metodo que se ejecuta cuando hay un error al enviar
    @Override
    public void onError() {
        //Metodo que muestra un dialogo de progreso
        recoverPasswordView.showDialogError();
        //Se oculta la barra de progreso
        recoverPasswordView.hideProgress();

    }

    //Metodo que se ejecuta cuando el email esta vacio
    @Override
    public void onEmptyEmailError() {
        //Metodo que muestra un dialogo indicando que el email esta vacio
        recoverPasswordView.showDialogEmptyemail();
        //Se oculta la barra de progreso
        recoverPasswordView.hideProgress();

    }

    //Metodo que se ejecuta cuando no se ha enviado correctamente
    @Override
    public void onErrorSend() {
        //Se oculta la barra de progreso
        recoverPasswordView.hideProgress();
        //Metodo que muestra un dialogo con un erro de conexion
        recoverPasswordView.showDialogErrorSend();
    }
}
