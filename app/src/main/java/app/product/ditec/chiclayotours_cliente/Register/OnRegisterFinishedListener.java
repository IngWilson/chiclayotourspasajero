package app.product.ditec.chiclayotours_cliente.Register;

/**
 * Created by angel on 25/04/15.
 */
public interface OnRegisterFinishedListener {

    void onEmailError();

    void onSpaceError();

    void onPasswordError();

    void onNameError();

    void onLastNameError();

    void onPhoneError();

    void onSuccess(String email, String codpais);

    void onShowDialogEmailError();

    void onShowDialogPhoneError();

    void onError();
    void onErrorCode();
    void onCorrecCode();
    void oncodeexit();
    void onShowDialogInternetError();
}
