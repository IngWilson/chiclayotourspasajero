package app.product.ditec.chiclayotours_cliente;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Wilson on 28/04/15.
 */


public class OkHttpRequest {

    private static final MediaType JSON = MediaType.parse("application/json;charset=utf-8");
    private String url;
    private static int timeout = 10;
    private OkHttpClient client = new OkHttpClient();

    // Constructor
    public OkHttpRequest(String url) {
        // Se coloca el tiempo de espera
        setTimeout();
        // Se asigna la URL en la instancia
        this.url = url;
    }

    // Metodo para enviar una peticion POST
    public Call post(String json, Callback callback) throws IOException {
        // Se crea el cuerpo del request indicando el tipo JSON y los parametros
        RequestBody body = RequestBody.create(JSON, json);
        // Se crea un request y se indica el cuerpo y la url del
        // pedido
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        // Se ejecuta el pedido
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    // Metodo para enviar una peticion POST sin parametro
    public Call postNotParams(Callback callback) throws IOException {
        // Se crea un request y la url del
        // pedido
        Request request = new Request.Builder()
                .url(url)
                .build();
        // Se ejecuta el pedido
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    // Metodo para enviar una peticion GET sin parametro
    public Call getNotParams(Callback callback) throws IOException {
        // Se crea un request y la url del
        // pedido
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        // Se ejecuta el pedido
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    // Metodo para enviar una peticion GET 
    public Call get(String json, Callback callback) throws IOException {
        // Se crea un request y la url del
        // pedido
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        // Se ejecuta el pedido
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    private void setTimeout() {
        //Se coloca los parametros que indican el tiempo de 
        // espera de la peticion
        client.setConnectTimeout(timeout, TimeUnit.SECONDS); // connect timeout
        client.setRetryOnConnectionFailure(true);
        client.setWriteTimeout(timeout, TimeUnit.SECONDS);
        client.setReadTimeout(timeout, TimeUnit.SECONDS);    // socket timeout
    }
}
