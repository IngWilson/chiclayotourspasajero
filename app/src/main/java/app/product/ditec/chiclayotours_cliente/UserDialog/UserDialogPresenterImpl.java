package app.product.ditec.chiclayotours_cliente.UserDialog;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * Created by angel on 08/05/15.
 */
public class UserDialogPresenterImpl implements UserDialogPresenter {

    private UserDialogView userDialogView;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private TaxiDriver taxiDriver;
    private Context ctx;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageSize targetSize;

    public UserDialogPresenterImpl(UserDialogView userDialogView) {
        this.userDialogView = userDialogView;
        ctx = userDialogView.getContext();
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();

     }

    // Metodo para colocar los datos del taxista en la actividad
    public void setUserData() {

        final Bundle extras = ((Activity) ctx).getIntent().getExtras();
        // Se colocan los datos del taxista en la actividad
        userDialogView.setUserNameTaxiDriver(extras.getString("name"));
        userDialogView.setAutoPlateTaxiDriver(extras.getString("autoplate"));
        userDialogView.setPhoneTaxiDriver(extras.getString("phone"));
        userDialogView.setEmailTaxiDriver(extras.getString("email"));

        Log.i("image", extras.getString("image"));

        editor.putBoolean(Config.ONUSERDIALOG, true);
        editor.commit();

        // Se crea un nueva instancia y se coloca una imagen mientras carga
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.loading)
                .build();

        //Se descarga la imagen en la URL del taxista
        imageLoader.displayImage(TaxiDriver.loadImageUrl(), userDialogView.getImageTaxiDriver(), options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                //Se oculta el imageView y se muestra un icono de progreso
                userDialogView.getImageTaxiDriver().setVisibility(View.GONE);
                userDialogView.getProgressBarImage().setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                editor.putBoolean(Config.GETIMAGE, true);
                editor.commit();

                //Se oculta la barra de progreso y se muestra el imageView con la imagen cargada
                userDialogView.getProgressBarImage().setVisibility(View.GONE);
                userDialogView.getImageTaxiDriver().setVisibility(View.VISIBLE);
                userDialogView.getImageTaxiDriver().setImageBitmap(loadedImage);
                userDialogView.getImageTaxiDriver().setScaleType(ImageView.ScaleType.FIT_XY);

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }

    public void endUserDialog() {
        editor.putBoolean(Config.ONUSERDIALOG, false);
        editor.commit();
    }
}
