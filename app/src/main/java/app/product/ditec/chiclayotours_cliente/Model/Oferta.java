package app.product.ditec.chiclayotours_cliente.Model;

import android.content.Context;
import android.content.SharedPreferences;

import app.product.ditec.chiclayotours_cliente.App;

public class Oferta {
    private static final String TAG  = "OfertaData";
    private String type = "";
    private String name = "";
    private String precio = "";
    private String tiempo = "";
    private String distance = "";
    private String emaildriver = "";
    private String socketidtaxi = "";
    private String phone = "";
    private String autoplate = "";
    private String dni = "";
    private String car_brand = "";
    private String imageurl = "";

    protected static SharedPreferences prefs = App.getInstance().getSharedPreferences(TAG, Context.MODE_PRIVATE);
    protected static SharedPreferences.Editor localEditor = prefs.edit();
    public Oferta(){}
    public Oferta(String _type, String _name,String _precio, String _tiempo, String _distance, String _emaildriver,
                  String _socketidtaxi,String _phone, String _autoplate, String _dni, String _car_brand,
                  String _imageurl){
        phone = _phone;
        autoplate = _autoplate;
        dni = _dni;
        car_brand = _car_brand;
        imageurl=_imageurl;
        type = _type;
        name = _name;
        precio = _precio;
        tiempo = _tiempo;
        distance = _distance;
        emaildriver = _emaildriver;
        socketidtaxi=_socketidtaxi;

    }

    public static String loadPrecio(){
        return prefs.getString("Precio", "---");
    }

    public static void savePrecio(String precio){
        localEditor.putString("Precio", precio);
        localEditor.commit();
    }

    public static String loadtipopeticion(){
        return prefs.getString("tipopeticion", "---");
    }

    public static void savetipopeticion(String tipopeticion){
        localEditor.putString("tipopeticion", tipopeticion);
        localEditor.commit();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

        //
    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }
//
    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getEmaildriver() {
        return emaildriver;
    }

    public void setEmaildriver(String emaildriver) {
        this.emaildriver = emaildriver;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAutoplate() {
        return autoplate;
    }

    public void setAutoplate(String autoplate) {
        this.autoplate = autoplate;
    }

    public String getDNI() {
        return dni;
    }

    public void setDNI(String dni) {
        this.dni = dni;
    }

    public String getCar_brand() {
        return car_brand;
    }

    public void setCar_brand(String car_brand) {
        this.car_brand = car_brand;
    }

    public String getImageUrl() {
        return imageurl;
    }

    public void setImageUrl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getSocketidtaxi() {
        return socketidtaxi;
    }

    public void setSocketidtaxi(String socketidtaxi) {
        this.socketidtaxi = socketidtaxi;
    }

}
