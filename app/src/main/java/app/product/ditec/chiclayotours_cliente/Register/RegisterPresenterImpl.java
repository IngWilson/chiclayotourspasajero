package app.product.ditec.chiclayotours_cliente.Register;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;


public class RegisterPresenterImpl implements RegisterPresenter, OnRegisterFinishedListener {

    RegisterView registerView;
    Validation validator;
    RegisterInteractor registerInteractor;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    Context ctx;

    public RegisterPresenterImpl(RegisterView registerView) {
        this.registerView = registerView;
        ctx = registerView.getContext();
        this.validator = new Validation(ctx);
        this.registerInteractor = new RegisterInteractorImpl();
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    //Metodo que valida los datos del registro que se envia
    public void validateParamsUser(String email, String password, String name, String lastname, String city, String pais, String codpais, String phone) {
//    public void validateParamsUser(String email,String password,String name, String lastname,String phonenumber){

        // Se muestra un dialogo de progreso
        registerView.showProgress();

        //Se crea un nuevo objeto JSON
        JSONObject jsonParams = new JSONObject();

        //Se colocan los parametros del registro
        try {
            jsonParams.put("email", email);
            jsonParams.put("password", password);
            jsonParams.put("first_name", name);
            jsonParams.put("last_name", lastname);
            jsonParams.put("city", city);
            jsonParams.put("pais", pais);
            jsonParams.put("codpais", codpais);
            jsonParams.put("phone", phone);
        } catch (Exception e) {
            Log.e("JSONException", e.toString());
        }


        //Se ejecuta el metodo para enviar los parametros
        Log.i("VPU_UserParams", jsonParams.toString());
        registerInteractor.register(jsonParams, this, validator);
    }

    public void validatecodepromo(String codpromo) {
//    public void validateParamsUser(String email,String password,String name, String lastname,String phonenumber){

        // Se muestra un dialogo de progreso
        registerView.showProgress();

        //Se crea un nuevo objeto JSON
        JSONObject jsonParams = new JSONObject();

        //Se colocan los parametros del registro
        try {
            jsonParams.put("codigo", codpromo);

//            jsonParams.put("phone",phonenumber);
        } catch (Exception e) {
            Log.e("JSONException", e.toString());
        }


        //Se ejecuta el metodo para enviar los parametros
        Log.i("VPU_UserParams", jsonParams.toString());
        registerInteractor.verifycodepromo(jsonParams, this, validator);
            }

    public void SendCodePromo(String email, String code) {
//    public void validateParamsUser(String email,String password,String name, String lastname,String phonenumber){

        // Se muestra un dialogo de progreso
        registerView.showProgress();

        //Se crea un nuevo objeto JSON
        JSONObject jsonParams = new JSONObject();

        //Se colocan los parametros del registro
        try {
            jsonParams.put("email", email);
            jsonParams.put("codigo", code);

//            jsonParams.put("phone",phonenumber);
        } catch (Exception e) {
            Log.e("JSONException", e.toString());
        }


        //Se ejecuta el metodo para enviar los parametros
        Log.i("VPU_UserParams", jsonParams.toString());
        registerInteractor.sendcodepromo(jsonParams, this, validator);
    }

    public void oncodeexit()
    {
        registerView.hideProgress();
        registerView.codeexit();
    }
    public void onError() {
        registerView.hideProgress();
        registerView.showDialogError();
    }

    public void onErrorCode() {
        registerView.hideProgress();
        registerView.codepromoerror();
    }

    public void onCorrecCode() {
        registerView.hideProgress();
        registerView.codepromovalido();
    }

    //Metodo que se ejecuta cuando hay un error en el edittext email
    public void onEmailError() {
        registerView.setEmailError();
        registerView.hideProgress();
    }


    public void onSpaceError() {
        registerView.setSpaceError();
        registerView.hideProgress();
    }

    //Metodo que se ejecuta cuando hay un error en el edittext email
    public void onPasswordError() {
        registerView.setPasswordError();
        registerView.hideProgress();
    }

    //Metodo que se ejecuta cuando hay un error en el edittext nombre
    public void onNameError() {
        registerView.setNameError();
        registerView.hideProgress();
    }

    //Metodo que se ejecuta cuando hay un error en el edittext apellido
    public void onLastNameError() {
        registerView.setLastNameError();
        registerView.hideProgress();
    }

    //Metodo que se ejecuta cuando hay un error en el edittext telefono
    public void onPhoneError() {
        registerView.setPhoneNumberError();
        registerView.hideProgress();
    }

    //Metodo que se ejecuta cuando se registra correctamente
    public void onSuccess(String email, String codpais) {

        // Metodo que indica en preferences que se registro
        setOnRegister();
        // Metodo que guarda el email en preferences
        User.saveEmail(email);
        User.savecodpais(codpais);
        // codigo de pais listo para ser introducido en el preferences para recuperarse en otra actividad
        Log.i("codpais", codpais);
        // Metodo que dirige a la actividad del verify phone
        registerView.navigateToVerifyPhone(email);
        // Metodo que oculta el dialogo de progreso
        registerView.hideProgress();

        // Metodo que muestra un dialogo indicando 
        // que se registro correctamente
        registerView.showDialogRegister();

        // Se finaliza la actividad
        registerView.finishRegister();

    }

    // Metodo que se utiliza para guardar en preferences
    public void setOnRegister() {
        // Se coloca un booleano en preferences
        // para indicar que se registro
        editor.putBoolean(Config.ONREGISTERPHONE, false);
        editor.putBoolean(Config.ONVERIFY, true);
        editor.commit();
    }

    // Metodo que muestra un dialogo
    // que indica hay un error en el email
    public void onShowDialogEmailError() {
        registerView.showDialogEmailError();
        registerView.hideProgress();
    }

    // Metodo que muestra un dialogo
    // que indica hay un error en el phone
    public void onShowDialogPhoneError() {
        registerView.showDialogPhoneError();
        registerView.hideProgress();
    }

    //Metodo que se ejecuta cuando hay un error en el email


    // Metodo que muestra un dialogo
    // indicando que hay un error de conexion
    @Override
    public void onShowDialogInternetError() {
        registerView.showDialogInternetError();
    }
}
