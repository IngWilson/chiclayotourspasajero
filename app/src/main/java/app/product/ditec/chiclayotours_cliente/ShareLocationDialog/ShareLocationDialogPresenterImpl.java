package app.product.ditec.chiclayotours_cliente.ShareLocationDialog;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

public class ShareLocationDialogPresenterImpl implements ShareLocationDialogPresenter,ShareLocationDialogFinishedListener {
    ShareLocationDialogView shareLocationDialogView;
    Context ctx;
    ShareLocationDialogInteractor interactor;
    private Validation validator;
    public ShareLocationDialogPresenterImpl(ShareLocationDialogView _shareLocationDialogView) {
        this.shareLocationDialogView = _shareLocationDialogView;
        ctx = shareLocationDialogView.getContext();
        interactor = new ShareLocationDialogInteractorImpl(this);
        this.validator = new Validation(shareLocationDialogView.getContext());

    }

    @Override
    public void enviarubicacion(String sharenumber) {
        //Se muestra un dialogo de progreso
        shareLocationDialogView.showProgress();

        String email = User.loadEmail();


        //Se coloca el email en un JSONObject
        JSONObject params = new JSONObject();
        try {
            params.put("email", email);
            params.put("message", sharenumber);
        } catch (JSONException e) {
            Log.i("mierrormensaje",e.toString());}

            //Se utiliza este metodo para enviar "ya llegue" al pasajero
            interactor.enviarubicacion(params, validator);

    }
    @Override
    public void onErrorConnection() {
        shareLocationDialogView.showDialogConnection();
        shareLocationDialogView.hideProgress();
    }

    @Override
    public void onSuccessSend() {
        shareLocationDialogView.hideProgress();
        shareLocationDialogView.envioexitoso();
    }


    @Override
    public void onFailedSend() {
        shareLocationDialogView.showDialogFailedSend();
        shareLocationDialogView.hideProgress();
    }

    @Override
    public void onFailedNotFound() {
        shareLocationDialogView.showDialogFailedNotFound();
        shareLocationDialogView.hideProgress();
    }

    @Override
    public void finalizarenviarubicacion(){


        //Se muestra un dialogo de progreso
        shareLocationDialogView.showProgress();

        String email = User.loadEmail();


        //Se coloca el email en un JSONObject
        JSONObject params = new JSONObject();
        try {
            params.put("email", email);
        } catch (JSONException e) {
            Log.i("mierrormensaje",e.toString());}

        //Se utiliza este metodo para enviar "ya llegue" al pasajero
        interactor.finalizarenviarubicacion(params, validator);

    }

    @Override
    public void onSuccessFinalizarSend() {
        shareLocationDialogView.hideProgress();
        shareLocationDialogView.SuccessFinalizarSend();
    }
}
