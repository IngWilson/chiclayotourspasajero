package app.product.ditec.chiclayotours_cliente.Account;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.appcompat.app.ActionBar;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;

import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.R;
//import butterknife.ButterKnife;
//import butterknife.InjectView;

/**
 * Created by angel on 05/05/15.
 */
public class AccountActivity extends AppCompatActivity  implements AccountView, Dialog.OnClickListener {

    ImageView imgstate;
    TextView txtFirstNameAccount;
    TextView txtEmailAccount;
    TextView txtPhoneNumberAccount;
    TextView txtEnabled;
    TextView txtName;
    TextView txtEmail;
    TextView txtPhone;
    TextView txtidpromotion;


    private ImageView imageView;


    AccountPresenter presenter;
    AlertDialog.Builder alertDialog;
    ProgressDialog progressDialog;

    private Uri uriFoto;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        //Coloca un actionbar personalizado con el logo de akitaxi
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layot_logo_account);

        imgstate=findViewById(R.id.imgstate);
        txtFirstNameAccount=findViewById(R.id.txtFirstNameAccount);
        txtEmailAccount=findViewById(R.id.txtEmailAccount);
        txtPhoneNumberAccount=findViewById(R.id.txtPhoneNumberAccount);
        txtEnabled=findViewById(R.id.txtEnabled);
        txtName=findViewById(R.id.txtName);
        txtEmail=findViewById(R.id.txtEmail);
        txtPhone=findViewById(R.id.txtPhone);
        txtidpromotion=findViewById(R.id.txtidprommotional);

        //Coloca el tipo de fuente en el layout para los datos del taxista
        Typeface typeFaceRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        txtFirstNameAccount.setTypeface(typeFaceRoboto);
        txtEmailAccount.setTypeface(typeFaceRoboto);
        txtPhoneNumberAccount.setTypeface(typeFaceRoboto);

        typeFaceRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Black.ttf");
        txtName.setTypeface(typeFaceRoboto);
        txtEmail.setTypeface(typeFaceRoboto);
        txtPhone.setTypeface(typeFaceRoboto);

        presenter = new AccountPresenterImpl(this);

        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Obteniendo usuarios...");

        showProgress();

        presenter.validateGetData();
        imageView = (ImageView) findViewById(R.id.imageView);
        if(!User.loadphoto().equals(""))
            ponerFoto(imageView, User.loadphoto());
        if(!User.loadIdpromotion().equals(""))
            txtidpromotion.setText(User.loadIdpromotion());
    }


    @Override
    public void setFirstName(String firstName) {
        txtFirstNameAccount.setText(firstName);
    }

    @Override
    public void setLastName(String lastName) {

    }

    @Override
    public void setEmail(String email) {
        txtEmailAccount.setText(email);
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        txtPhoneNumberAccount.setText(phoneNumber);
    }

    @Override
    public void setEnabled(String enabled) {
        txtEnabled.setText(enabled);
    }

    @Override
    public Context getContext() {
        return AccountActivity.this;
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    //Metodo que muestra un dialogo
    // cuando no hay conexion a internet
    @Override
    public void showDialogInternetError() {
        alertDialog.setTitle(getString(R.string.dialog_conection_title))
                .setMessage(getString(R.string.dialog_conection_message))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }

    // Metodo para el evento onClick que se asigna en el dialogo
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -2:
                return;
            case -1:
                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                break;
        }
    }

    @Override
    public ImageView getImageIsable() {
        return imgstate;
    }


    //añadido para ingresar la foto

    public void tomarFoto(View view) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        uriFoto = Uri.fromFile(new File( Environment.getExternalStorageDirectory() + File.separator + "img_" + (System.currentTimeMillis() / 1000) + ".jpg"));

        //guardando foto al preference
/*        editor.putString("foto", uriFoto.toString());
        editor.commit();*/
        User.savephoto(uriFoto.toString());

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriFoto);
        startActivityForResult(intent, 1);
    }
    public void galeria(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       if (requestCode == 1 && resultCode == Activity.RESULT_OK
                && !User.loadphoto().equals("") && uriFoto!=null) {
          //  lugar.setFoto(uriFoto.toString());
            ponerFoto(imageView, User.loadphoto());
        }
       else if (requestCode == 2
               && resultCode == Activity.RESULT_OK) {
           User.savephoto(data.getDataString());

           ponerFoto(imageView, User.loadphoto());
       }
    }
    //repetido en mapactivity
    protected void ponerFoto(ImageView imageView, String uri) {
        if (uri != null) {
            imageView.setImageBitmap(reduceBitmap(this, uri, 1024, 1024));
        } else {
            imageView.setImageBitmap(null);
        }
    }

    public static Bitmap reduceBitmap(Context contexto, String uri,
                                      int maxAncho, int maxAlto) {
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(contexto.getContentResolver()
                    .openInputStream(Uri.parse(uri)), null, options);
            options.inSampleSize = (int) Math.max(
                    Math.ceil(options.outWidth / maxAncho),
                    Math.ceil(options.outHeight / maxAlto));
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(contexto.getContentResolver()
                    .openInputStream(Uri.parse(uri)), null, options);
        } catch (FileNotFoundException e) {
            Toast.makeText(contexto, "Fichero/recurso no encontrado",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        }
    }



}
