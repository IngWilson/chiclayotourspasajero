package app.product.ditec.chiclayotours_cliente.Peticion;

import org.json.JSONObject;

public interface PeticionInteractor {
    void signin();
    void cancelar(final JSONObject jsonParams);
    void acceptoferta(final JSONObject jsonParams);
    void aumentar(final JSONObject jsonParams);
    void rechazaroferta(final JSONObject jsonParams);
}
