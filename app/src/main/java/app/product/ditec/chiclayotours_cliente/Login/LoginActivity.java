package app.product.ditec.chiclayotours_cliente.Login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.AppSignatureHelper;
import app.product.ditec.chiclayotours_cliente.Code.CodeActivity;
import app.product.ditec.chiclayotours_cliente.Map.MapActivity;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.RecoverPassword.RecoverPasswordActivity;
import app.product.ditec.chiclayotours_cliente.Register.RegisterActivity;
import app.product.ditec.chiclayotours_cliente.VerifyPhone.VerifyPhoneActivity;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

public class LoginActivity extends Activity implements LoginView, DialogInterface.OnClickListener {

    private ProgressDialog progressDialog;
    private AlertDialog.Builder alertDialog;
    private LoginPresenter presenter;

    private CallbackManager elCallbackManagerDeFacebook;
    // boton oficial de Facebook para login/logout
//
    LoginButton loginButtonOficial;
    EditText edtEmail;
    EditText edtphone;
    EditText edtPassword;
    TextView btnLogin;
    TextView btnLoginphone;
    TextView btnRegister;
    private AlertDialog.Builder dialogConfirmPhone;
    int P;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new LoginPresenterImpl(this);

        Log.i("LA_Session", "" + presenter.validateSession());
        Log.i("LA_Register", "" + presenter.validateRegister());
        Log.i("LA_VerifyPhone", "" + presenter.validateVerifyPhone());
        P=0;
        arranque();

        dialogConfirmPhone = new AlertDialog.Builder(this);
    }

    @Override
    public void arranque(){

        if (presenter.validateSession()) {
            navigateToMap();
            finishLogin();
        }

        if (presenter.validateRegister()) {
            navigateToVerifyPhone();
            finishLogin();
        }



        if (presenter.validateVerifyPhone()) {
            navigateToCode();
            finishLogin();
        }


        if (!presenter.validateSession() && !presenter.validateVerifyPhone() && !presenter.validateRegister()) {
            setContentView(app.product.ditec.chiclayotours_cliente.R.layout.activity_login);
            alertDialog = new AlertDialog.Builder(this);
            alertDialog.setCancelable(false);


            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(false);
            progressDialog.setMessage(getString(R.string.progress_login_title));
            progressDialog.setCancelable(false);

            edtEmail=findViewById(R.id.edtEmailLogin);
            edtphone=findViewById(R.id.edtphonelogin);
            edtPassword=findViewById(R.id.edtPasswordLogin);
            btnLogin=findViewById(R.id.btnLogin);
            btnLoginphone=findViewById(R.id.btnLoginphone);
            btnRegister=findViewById(R.id.btnRegister);

            //Definimos el tipo de letra Roboto-Medium
            Typeface typeFaceRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
            edtEmail.setTypeface(typeFaceRoboto);
            edtphone.setTypeface(typeFaceRoboto);
            edtPassword.setTypeface(typeFaceRoboto);

            //Definimos el tipo de letra Roboto-Black
            typeFaceRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Black.ttf");
            btnLogin.setTypeface(typeFaceRoboto);
            btnLoginphone.setTypeface(typeFaceRoboto);
            btnRegister.setTypeface(typeFaceRoboto);

            // facebook
            loginButtonOficial = (LoginButton) findViewById(R.id.login_button);
            loginButtonOficial.setReadPermissions("email");
            //loginButtonOficial.setReadPermissions(Arrays.asList("public_profile", "email"));

            // crear callback manager de Facebook
//
            this.elCallbackManagerDeFacebook = CallbackManager.Factory.create();
            LoginManager.getInstance().registerCallback(this.elCallbackManagerDeFacebook,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
// App code
                            Toast.makeText(LoginActivity.this, "Login onSuccess()", Toast.LENGTH_LONG).show();
                            facebooklogin();
                        }
                        @Override
                        public void onCancel() {
                            Toast.makeText(LoginActivity.this, "Login onCancel()", Toast.LENGTH_LONG).show();
                            //actualizarVentanita();
                        }
                        @Override
                        public void onError(FacebookException exception) {
// App code
                            Toast.makeText(LoginActivity.this, "Login onError(): " + exception.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            //actualizarVentanita();
                        }
                    });

            AppSignatureHelper signatureHelper = new AppSignatureHelper(this);

            ArrayList<String> appSignatures = signatureHelper.getAppSignatures();

            if (!appSignatures.isEmpty()) {
                Log.i("millave",appSignatures.get(0));
                Log.i("millave",appSignatures.get(0).toString());
            }

        }
    }
    private AccessToken obtenerAccessToken() {
        return AccessToken.getCurrentAccessToken();
    }
    private void facebooklogin()
    {
        AccessToken accessToken = this.obtenerAccessToken();
        if (accessToken == null) {
            Log.d("cuandrav.actualizarVent", "no hay sesion, deshabilito");
//
// sesion con facebook cerrada
//
                      return;
        }
//
// sí hay sesión
//
        Log.d("cuandrav.actualizarVent", "hay sesion habilito");

//
// averiguo los datos básicos del usuario acreditado
//
        Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            // por aca cogemos la imagen
            // this.textoConElMensaje.setText(profile.getProfilePictureUri(300,300).toString());
        }
//
// otra forma de averiguar los datos básicos:
// hago una petición con "graph api" para obtener datos del usuario acreditado
//
        this.obtenerPublicProfileConRequest_async(
// como es asíncrono he de dar un callback
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject datosJSON, GraphResponse response) {
//
// muestro los datos
//
                        // Log.i("datosfacebook",datosJSON.toString());
                        String nombre= "nombre desconocido";
                        String ID="";
                        String email="email";
                        try {
                            nombre = datosJSON.getString("name");
                            ID=datosJSON.getString("id");
                            email=datosJSON.getString("email");;
                       //     nombre=datosJSON.toString();

                        } catch (org.json.JSONException ex) {
                            Log.d("cuandrav.actualizarVent", "callback de obtenerPublicProfileConRequest_async: excepcion: "
                                    + ex.getMessage());
                        } catch (NullPointerException ex) {
                            Log.d("cuandrav.actualizarVent", "callback de obtenerPublicProfileConRequest_async: excepcion: "
                                    + ex.getMessage());
                        }
                        if (email.equals("email")) {
                            email = ID;
                        }
                        Toast.makeText(LoginActivity.this, nombre + " " + ID + " " + email, Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void obtenerPublicProfileConRequest_async (GraphRequest.GraphJSONObjectCallback callback) {
        if (!this.hayRed()) {
            Toast.makeText(this, "¿No hay red?", Toast.LENGTH_LONG).show();
        }
//
// obtengo access token y compruebo que hay sesión
//
        AccessToken accessToken = obtenerAccessToken();
        if (accessToken == null) {
            Toast.makeText(LoginActivity.this, "no hay sesión con Facebook", Toast.LENGTH_LONG).show();
            return;
        }
//
// monto la petición: /me
//
        GraphRequest request = GraphRequest.newMeRequest(accessToken, callback);
        Bundle params = new Bundle ();
        // params.putString("fields", "id,name,email,picture.type(large)");
        params.putString("fields", "id,name,email");
        request.setParameters(params);
//
// la ejecuto (asíncronamente)
//
        request.executeAsync();
    }
    private boolean hayRed() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
// http://stackoverflow.com/questions/15091591/post-on-facebook-wall-without-showing-dialog-on-android
// comprobar que estamos conetactos a internet, antes de hacer el login con
// facebook. Si no: da problemas.
    } // ()

    //FIN FACEBOOK
    //Metodo que muestra el dialogo de progreso
    @Override
    public void showProgress() {
        progressDialog.show();
    }
    //Metodo que esconde el dialogo de progreso
    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }
    //Metodo que pone el error del correo en el edit text
    @Override
    public void setEmailError() {
        /*Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        edtEmail.setBackground(mBackground);*/
        Toast.makeText(this, getString(R.string.login_campo_email),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPhoneError() {
        /*Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        edtEmail.setBackground(mBackground);*/
        Toast.makeText(this, getString(R.string.login_campo_phone),
                Toast.LENGTH_LONG).show();
    }
    //MEtodo que pone el error del poassword en el edit text
    @Override
    public void setPasswordError() {
        /*Drawable mBackground = getResources().getDrawable(R.drawable.background_edit_error);
        edtPassword.setBackground(mBackground);*/
        Toast.makeText(this, getString(R.string.login_campo_password),
                Toast.LENGTH_LONG).show();
    }

    public void onLogin(View view) {
        //Metodo que verifica si el correo ya existe
        presenter.validateCredentials(edtEmail.getText().toString(), edtPassword.getText().toString());
    }

    public void onClickphone(View view) {
        //Metodo que verifica si el correo ya existe
        if(edtphone.getText().toString().length()<9)
            showDialogErrorphone();
            else
        if(edtphone.getText().toString().equals("1234567890999"))
        {
            presenter.validateUserTest();
        }
        else
        dialogConfirmPhone.setCancelable(false)
                .setTitle("VERIFIQUE EL NUMERO")
                .setMessage("¿Su numero es el " + edtphone.getText().toString() + " ?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.validatePhone(edtphone.getText().toString());
                    }
                })
                .setNegativeButton("No", null)
                .show();


    }

    public void showDialogErrorphone() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Nro celular no valido")
                .setMessage("El telefono ingresado tiene menos de 9 digitos")
                .setPositiveButton(getString(R.string.dialog_accept), null)
                .show();
    }

    //Metodo del evento onclick
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

        switch (i) {
            case -2:
                return;
            case -1:
                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
            default:
                return;
        }
    }
    //Metod que inicialisa la actividad MapActivity
    @Override
    public void navigateToMap() {
        startActivity(new Intent(this, MapActivity.class));
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }
    //Metodo que muestra un dialogo de error
    @Override
    public void showDialogErrorConnection() {
        //Metodo que coloca los datos deseados en el  dialogo
        alertDialog.setTitle(getString(R.string.dialog_conection_title))
                .setMessage(getString(R.string.dialog_conection_message))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }
    //Metodo que muestra un dialogo de error al iniciar secion
    @Override
    public void showDialogErrorLogin() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.dialog_login_title)
                .setMessage( getString(R.string.dialog_login_message))
                .setPositiveButton(getString(R.string.dialog_accept), null)
                .show();
    }
    //Metodo que muestra un dialogo error "cuando la contraseña o el correo del usuario es incorescto"
    @Override
    public void showDialogErrorUserPass() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.dialog_login_userpass_error_title))
                .setMessage(getString(R.string.dialog_login_userpass_error_message))
                .setPositiveButton(getString(R.string.dialog_accept), null)
                .show();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
    //boton que te direcciona a la siguiente actividad "RegisterActivity"

    public void navigateToRegister() {
        //Metodo que termina la actividad actual
        finishLogin();
        startActivity(new Intent(this, RegisterActivity.class));
    }
    //Metodo que termina la actividad actual
    @Override
    public void finishLogin() {
        finish();
    }
    //Metodo que verifica  tu telefono
    @Override
    public void navigateToVerifyPhone() {
        //Metodo que direcciona a la siguiente actividad "VerifyPhoneActivity"
        startActivity(new Intent(this, VerifyPhoneActivity.class));
    }
    //Metodo que te direciona a la siguiente actividad "CodeActivity"
    @Override
    public void navigateToCode() {
        startActivity(new Intent(this, CodeActivity.class));
    }
    //boton que recupera tu contraseña

    public void navigateToRecoverPassword() {
        //botonque te direciona ala siguiente actividad "RecoverPasswordActivity"
        startActivity(new Intent(this, RecoverPasswordActivity.class));
    }


    //facebook
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.elCallbackManagerDeFacebook.onActivityResult(requestCode, resultCode, data);
    }

}
