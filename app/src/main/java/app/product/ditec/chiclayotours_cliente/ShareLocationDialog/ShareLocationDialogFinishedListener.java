package app.product.ditec.chiclayotours_cliente.ShareLocationDialog;

public interface ShareLocationDialogFinishedListener {
    void onErrorConnection();

    void onSuccessSend();

    void onFailedSend();

    void onSuccessFinalizarSend();

    void onFailedNotFound();
}
