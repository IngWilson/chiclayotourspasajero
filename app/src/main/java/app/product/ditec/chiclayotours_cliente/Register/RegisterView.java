package app.product.ditec.chiclayotours_cliente.Register;

import android.content.Context;
import android.view.View;

/**
 * Created by angel on 15/04/15.
 */
public interface RegisterView {

    void showProgress();

    void website(View view);

    void hideProgress();

    void setNameError();

    void setLastNameError();

    void setPasswordError();

    void setEmailError();

    void setSpaceError();

    void setPhoneNumberError();

    Context getContext();

    void finishRegister();

    void showDialogRegister();

    void showDialogEmailError();

    void showDialogPhoneError();

    void showDialogError();

    void navigateToVerifyPhone(String email);

    void showDialogInternetError();

    void codepromovalido();

    void codepromoerror();
    void codeexit();
}
