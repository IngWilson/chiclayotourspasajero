package app.product.ditec.chiclayotours_cliente.ShareLocationDialog;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

public interface ShareLocationDialogInteractor {

    void signin();
    void enviarubicacion(JSONObject params, final Validation validator);
    void finalizarenviarubicacion(JSONObject params, final Validation validator);
}

