package app.product.ditec.chiclayotours_cliente.Promotion;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;
/**
 * Created by usuario on 29/03/2016.
 */
public class PromotionInteractorImpl implements PromotionInteractor{
    PromotionPresenter presenter;
    JSONObject params = new JSONObject();
    public PromotionInteractorImpl(PromotionPresenter _presenter){
        presenter = _presenter;
    }

    //Metodo que obtiene los datos del taxista del servidor
    @Override
    public void getDataPromotion(final String email, final Validation validator) {

        //Clase que implementa metodos para peticiones http
        OkHttpRequest okHttpRequest = new OkHttpRequest
                (App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_GETDATAUSER)+"?email="+email);

        //Se valida si es que hay conexion a internet
        if(validator.isOnline())
        {
            try {
                //Se hace un peticion GET sin parametros explicitos
                okHttpRequest.getNotParams(new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {
                        Log.i("AccountFailure", "Failure when get user data");
                    }
                    @Override
                    public void onResponse(Response response) throws IOException {
                        Log.i("AccountAccept","Get user data");
                        String res = response.body().string();
                        Log.i("Response Body", res);

                        Log.i("GTDX_Code",""+response.code());
                       // final User promotion = new user();

                        // Se obtiene los parametros del taxidriver desde
                        // el servidor
                        try {
                            JSONArray jsonArray = new JSONArray(res);
                            params = jsonArray.getJSONObject(0);


                            final String idpromotion         = params.getString("codigo");
                            final String totalcar          = params.getString("services_count");
                            final String situacion              = params.getString("situacion");
                            final String estado             = params.getString("bono");
                            Log.i("Response Body", idpromotion);
                            Log.i("Response Body", totalcar);
                            Log.i("Response Body", situacion);
                            Log.i("Response Body", estado);

                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    ((OnPromotionFinishedListener)presenter).onSuccessGetUserData(idpromotion, totalcar, situacion, estado);
                                }
                            });

                            Log.i("GDU_Account",params.toString());

                        }catch (JSONException e){
                            Log.e("JSONArrayException",e.toString());
                       }
                    }

                });
            }catch (IOException e)
            {
                Log.e("IOException",e.toString());
            }
        }
        else
            ((OnPromotionFinishedListener)presenter).onInternetError();
    }
//Envio
    //Metodo que envia el codigo promocional
    @Override
    public void sendCode(final JSONObject jsonObject, Validation validator, final OnPromotionFinishedListener listener) {

        // Metodo que verifica que
        // esta conectado a internet
        if (validator.isOnline()) {

            OkHttpRequest request = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_SENDCODEPROMO));

            // Valor booleano que indica que hay un error
            boolean error = false;
            try {
                // Se verifica que el telefono no este vacio si es que esta
                if (TextUtils.isEmpty(jsonObject.getString("codigo"))) {
                    // Entoces se pone en true el error
                    error = true;
                    // Se ejecuta
                    listener.onEmptyCodeError();
                }
            } catch (JSONException e) {
                Log.e("JSONException", e.toString());
            }

            // Si es que no hay un error
            if (!error) {
                try {
                    // Se realiza una peticion POST
                    // y se envia el parametros
                    request.post(jsonObject.toString(), new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {

                            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onError();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            final int code = response.code();

                            Log.i("SP_Code", "" + code);

                            // Se obtiene el hilo principal y se crea una tarea
                            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {

                                @Override
                                public void run() {
                                    // Se verifica el codigo
                                    if (code == 200)
                                        try {

                                            Log.i("SP_codigo", jsonObject.getString("codigo"));
                                            listener.onSuccessSend(jsonObject.getString("codigo"));

                                        } catch (JSONException e) {
                                            Log.e("JSONException", e.toString());
                                        }
                                    else if (code == 500)
                                        listener.onCodeError();
                                    else if (code == 503)
                                        listener.onFailedSend();
                                    else if (code == 501)
                                        listener.onFailedRepeatCode();
                                    else if (code == 202)
                                        listener.onFailedCodeExpire();
                                    else
                                        listener.onFailedSend();
                                }
                            });
                        }
                    });
                } catch (IOException e) {
                    Log.e("SP_IOException", e.toString());
                }
            }
        } else

            listener.onInternetError();
    }
    //fin de envio
}
