package app.product.ditec.chiclayotours_cliente.Code;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by lenovo on 16/05/2015.
 */
public interface CodeInteractor {

    void sendCode(JSONObject params, Validation validator, OnCodeDialogFinishedListener listener);

    void login(JSONObject jsonParams, OnCodeDialogFinishedListener listener, Validation validator);

    void resendCode(JSONObject params, Validation validator, OnCodeDialogFinishedListener listener);

    void Verifyfirebase(JSONObject params, Validation validator, OnCodeDialogFinishedListener listener);

}
