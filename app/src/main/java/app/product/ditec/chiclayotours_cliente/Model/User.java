package app.product.ditec.chiclayotours_cliente.Model;

import android.content.Context;
import android.content.SharedPreferences;

import app.product.ditec.chiclayotours_cliente.App;

/**
 * Created by angel on 16/04/15.
 */

public class User {
    public static String TAG = "UserData";

    public static final String NAME = "name";
    public static final String LAST_NAME = "lastname";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String ISABLE = "isable";
    public static final String GCM = "gcm_user";
    public static final String PHONE = "phone";
    public static final String VERIFIED = "verified";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String CODPAIS = "codpais";
    public static final String PHOTO = "photo";

    //
    public static String IDPROMOTION = "";
    public static String TOTALCAR="";
    public static String SITUACION;
    public static String ESTADO="email";
    //

    private String name      = "";
    private String lastname  = "";
    private String email     = "";
    private String phone     = "";
    private boolean isable   = false;
    private boolean verified = false;
    public String gcm_user  = "";
    public double latitude;
    public double longitude;
    private String idpromotion;
/*    //

    private String totalcar;
    private String situacion;
    private String estado;
    //*/
    protected static SharedPreferences prefs = App.getInstance().getSharedPreferences(TAG, Context.MODE_PRIVATE);
    protected static SharedPreferences.Editor localEditor = prefs.edit();

    public User() {
    }

    // Metodo para guardar el nombre de usuario
    public static void saveUser(User user) {
        localEditor.putString(NAME, user.name);
        localEditor.putString(LAST_NAME, user.lastname);
        localEditor.putString(EMAIL, user.email);
        localEditor.putString(PHONE, user.phone);
        localEditor.putBoolean(ISABLE, user.isable);
        localEditor.putBoolean(VERIFIED, user.verified);
        localEditor.putString(GCM,user.gcm_user);
        //
/*        localEditor.putString(IDPROMOTION,paramPromotion.getIdpromotion());
        localEditor.putString(TOTALCAR, paramPromotion.getTotalcar());
        localEditor.putString(SITUACION, paramPromotion.getSituacion());
        localEditor.putString(ESTADO,paramPromotion.getEstado());
        //*/
        localEditor.commit();
    }

    // Los metodos que comienzan con "save"
    // sirven para guardar en preferences
    // y los que comienzan con "load"
    // cargan los datos desde el preferences

    public static void saveName(String name){
        localEditor.putString(NAME, name);
        localEditor.commit();
    }

    public static String loadName(){
        return prefs.getString(NAME,"");
    }

    public static void saveLastname(String lastname){
        localEditor.putString(LAST_NAME,lastname );
        localEditor.commit();
    }

    public static String loadLastname(){
        return prefs.getString(LAST_NAME,"");
    }

    public static void saveEmail(String email){
        localEditor.putString(EMAIL, email);
        localEditor.commit();
    }

    public static String loadEmail(){
        return prefs.getString(EMAIL,"");
    }

    public static void savecodpais(String codpais){
        localEditor.putString(CODPAIS, codpais);
        localEditor.commit();
    }

    public static String loadcodpais(){
        return prefs.getString(CODPAIS,"");
    }

//insertado para añadir la foto en preferences
    public static void savephoto(String photo){
        localEditor.putString(PHOTO, photo);
        localEditor.commit();
    }

    public static String loadphoto(){
        return prefs.getString(PHOTO,"");
    }
//fin
    public static void savePhone(String phone){
        localEditor.putString(PHONE, phone);
        localEditor.commit();
    }

    public static String loadPhone(){
        return prefs.getString(PHONE,"");
    }

    public static void saveIsable(boolean isable){
        localEditor.putBoolean(ISABLE, isable);
        localEditor.commit();
    }

    public static boolean loadIsable(){
        return prefs.getBoolean(ISABLE,false);
    }

    public static void saveGCM(String gcm_user){
        localEditor.putString(GCM, gcm_user);
        localEditor.commit();
    }

    public static String loadGCM(){
        return prefs.getString(GCM,"");
    }

    public static void saveVerified(boolean verified){
        localEditor.putBoolean(VERIFIED,verified );
        localEditor.commit();
    }

    public static boolean loadVerified(){
        return prefs.getBoolean(VERIFIED,false);
    }

    public static void saveLatitude(float latitude){
        localEditor.putFloat(LATITUDE, latitude);
        localEditor.commit();
    }

    public static float loadLatitude(){
        return prefs.getFloat(LATITUDE,0);
    }

    public static void saveLongitude(float longitude){
        localEditor.putFloat(LONGITUDE, longitude);
        localEditor.commit();
    }

    public static float loadLongitude(){
        return prefs.getFloat(LONGITUDE,0);
    }

    public static void clean() {
        localEditor.putString(NAME, "");
        localEditor.putString(LAST_NAME, "");
        localEditor.putString(EMAIL, "");
        localEditor.putString(PHONE, "");
        localEditor.putBoolean(ISABLE, false);
        localEditor.putBoolean(VERIFIED, false);
        localEditor.putString(GCM, "");
        localEditor.putString(IDPROMOTION, null);
        //
/*
        localEditor.putString(TOTALCAR, null);
        localEditor.putString(SITUACION, null);
        localEditor.putString(ESTADO, null);
        //*/
        localEditor.commit();
    }


    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isable() {
        return isable;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setIsable(boolean isable) {
        this.isable = isable;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }


    public static void saveIdpromotion(String idpromotion){
        localEditor.putString(IDPROMOTION,idpromotion);
        localEditor.commit();
    }

    public static String loadIdpromotion(){
        return prefs.getString(IDPROMOTION,"");
    }
    //

/*    public String getIdpromotion() {
        return idpromotion;
    }

    public void setIdPromotion(String idpromotion) {
        this.idpromotion = idpromotion;
    }

    public static void saveIdpromotion(String idpromotion){
        localEditor.putString(IDPROMOTION,idpromotion);
        localEditor.commit();
    }

    public static String loadIdpromotion(){
        return prefs.getString(IDPROMOTION,"");
    }

    public String getTotalcar() {
        return totalcar;
    }

    public void setTotalcar(String totalcar) {
        this.totalcar = totalcar;
    }

    public static void saveTotalcar(String totalcar){
        localEditor.putString(TOTALCAR,totalcar);
        localEditor.commit();
    }

    public static String loadTotalcar(){
        return prefs.getString(TOTALCAR,"");
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public static void saveSituacion(String situacion){
        localEditor.putString(SITUACION,situacion);
        localEditor.commit();
    }

    public static String loadSituacion(){
        return prefs.getString(SITUACION,"");
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public static void saveEstado(String estaddo){
        localEditor.putString(ESTADO,estado);
        localEditor.commit();
    }

    public static String loadEstado(){
        return prefs.getString(ESTADO,"");
    }

    //*/


}
