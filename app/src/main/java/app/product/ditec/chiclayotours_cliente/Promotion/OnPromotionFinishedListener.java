package app.product.ditec.chiclayotours_cliente.Promotion;

/**
 * Created by usuario on 29/03/2016.
 */
public interface OnPromotionFinishedListener {
    void onError();

    void onInternetError();

    void onSuccessGetUserData(String idpromotion, String totalcar, String situacion, String estado);

    // Envio datos

    void onSuccessSend(String code);

    void onFailedSend();

    void onCodeError();

    void onFailedRepeatCode();

    void onFailedCodeExpire();

    void onEmptyCodeError();

    // Fin de Envio
}
