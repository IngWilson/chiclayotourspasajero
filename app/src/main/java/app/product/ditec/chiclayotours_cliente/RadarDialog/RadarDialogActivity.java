package app.product.ditec.chiclayotours_cliente.RadarDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.R;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

/**
 * Created by angel on 20/04/15.
 */
public class RadarDialogActivity extends Activity implements RadarDialogView {

    private ImageView ivAnimacion;
    private AnimationDrawable savingAnimation;
    private RadarDialogPresenter presenter;
    private AlertDialog.Builder alertDialog;
    private Button btnllamarcentral;
    private LinearLayout llradardialog;

    TextView txtmessage;
    Button btnCancelRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dialog_radar);

        ivAnimacion = (ImageView) findViewById(R.id.iv_animacion);
        ivAnimacion.setBackgroundResource(R.drawable.animacion_radar);

        // Crea una nueva animacion
        savingAnimation = (AnimationDrawable) ivAnimacion.getBackground();
        savingAnimation.start();

        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);

        txtmessage=findViewById(R.id.txtmessage);
        btnCancelRequest=findViewById(R.id.btnCancelRequest);

        presenter = new RadarDialogPresenterImpl(this);
        btnllamarcentral=findViewById(R.id.btnllamarcentral);
        llradardialog=findViewById(R.id.llradardialog);
        //Tipo de fuente
        Typeface typeFaceRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        txtmessage.setTypeface(typeFaceRoboto);
        Handler handler = new Handler();
        handler.postDelayed(
                new Runnable() {
                    public void run() {
                        txtmessage.setText("Parece que no hay unidades cercanas le recomendamos llamar a la central");
                        btnllamarcentral.setVisibility(View.VISIBLE);
                        llradardialog.getLayoutParams().height=(int) getResources().getDimension(R.dimen.llradarchange);
                    }
                }, 20000L);
    }

    // Metodo que se presiona cuando Cancel
    public void cancelRequest(View view) {
        // Deshabilita el cancelar pedido
        disabledCancel();
        //Se llama el presentador para validar el cancelar
        presenter.validateCancelRequest();
    }

    @Override
    public Context getContext() {
        return RadarDialogActivity.this;
    }

    // Metodo que muestra un dialogo indicando que hay un error de conexion
    @Override
    public void showDialogConnection() {
        alertDialog.setTitle(getString(R.string.dialog_INTERNET_error))
                .setMessage(getString(R.string.dialog_INTERNET_message_error))
                .setPositiveButton(getString(R.string.settings_services), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_SETTINGS));
                    }
                })
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }

    // Metodo que habilita el boton cancelar
    @Override
    public void enabledCancel() {
        //Activa el boton cancelar pedido
        btnCancelRequest.setEnabled(true);
        //Se coloca de color rojo el boton
        btnCancelRequest.setBackgroundResource(R.drawable.red);
    }

    // Metodo que deshabilita el boton cancelar
    @Override
    public void disabledCancel() {
        //Desactiva el boton cancelar pedido
        btnCancelRequest.setEnabled(false);
        //Se coloca de color plomo el boton
        btnCancelRequest.setBackgroundResource(R.drawable.locked_btn_call);
    }

    // Metodo que finaliza el dialogo del radar cuando no se ha obtenido ningun pedido
    @Override
    public void finishRadarDialog() {
        Intent returnIntent = new Intent();
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    //Metodo que finaliza el dialogo del radar cuando se ha obtenido los datos del taxista
    public void finishRadarDialogData() {
        Intent returnIntent = new Intent();
        setResult(RESULT_FIRST_USER, returnIntent);
        finish();
    }

    //Metodo que muestra un dialogo indicando que no se encontro taxista
    @Override
    public void showDialogTaxiAround() {
        Toast.makeText(this, getString(R.string.show_Dialog_Taxi_Around), Toast.LENGTH_LONG).show();
    }

    //Metodo que muestra un dialogo indicando que hay un error
    @Override
    public void showDialogError() {
        Toast.makeText(this, getString(R.string.show_Dialog_Error), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("LC_OnDestroy", "This activity is stopped");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("LC_OnDestroy", "This activity is restarted");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onRadar(false);
        Log.i("LC_OnDestroy", "This activity is destroyed");
        presenter.desactiveSocket();
        //presenter.unRegisterReceiver();
        // presenter.onCall(false);
    }

    @Override
    public void onBackPressed() {
        //Toast.makeText(context,"Se presiono a atras",Toast.LENGTH_LONG).show();
    }

    //Metodo para evitar que el dialogo se cierre cuando se presione afuera de su contorno
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);

        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    public void llamarcentral(View view){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + App.getInstance().getString(R.string.nrocentral)));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //Se ejecuta el intent
        this.startActivity(intent);
        presenter.validateCancelRequest();
    }
}
