package app.product.ditec.chiclayotours_cliente.Mensaje;

public interface OnMessageFinishedListener {
    void onErrorConnection();

    void onSuccessSend();

    void onFailedSend();
}
