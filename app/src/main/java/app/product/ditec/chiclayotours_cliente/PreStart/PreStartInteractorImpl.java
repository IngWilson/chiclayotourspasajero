package app.product.ditec.chiclayotours_cliente.PreStart;

import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 27/06/15.
 */
public class PreStartInteractorImpl implements PreStartInteractor {

    @Override
    public void sendGCM(JSONObject jsonObject, Validation validator) {

        if (validator.isOnline()) {

            OkHttpRequest okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_RESETGCM));

            try {
                okHttpRequest.post(jsonObject.toString(), new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {

                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        Log.i("SGCM_Code", "" + response.code());
                        Log.i("SGCM_Code", "Sucessfully send new GCM");

                    }
                });
            } catch (IOException e) {
                Log.e("IO_Exception", e.toString());
            }

        }


    }
}
