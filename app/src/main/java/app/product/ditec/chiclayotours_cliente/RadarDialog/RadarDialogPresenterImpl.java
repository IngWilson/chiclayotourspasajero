package app.product.ditec.chiclayotours_cliente.RadarDialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.ImageRefactor;
import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 26/04/15.
 */
public class RadarDialogPresenterImpl implements RadarDialogPresenter, OnRadarFinishedListener {

    private RadarDialogView radarDialogView;
    private Validation validator;
    private RadarDialogInteractor radarDialogInteractor;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Context ctx;
    private int count;

    private ImageLoader imageLoader;
    // Se declara la variable para manejar el tamaño de la imagen
    private ImageSize targetSize;

    private android.os.Handler handler;
    private JSONObject request = new JSONObject();

    public RadarDialogPresenterImpl(RadarDialogView radarDialogView) {
        this.radarDialogView = radarDialogView;
        ctx = radarDialogView.getContext();
        count = 0;

        this.validator = new Validation(ctx);
        this.radarDialogInteractor = new RadarDialogInteractorImpl(this);

        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();

        //Se crea un objeto JSON donde se coloca el email
        String email = User.loadEmail();
        try {
            request.put("email", email);
            request.put("ide", Request.loadId());
            Log.i("VSR_ToSendRequest", request.toString());
        } catch (JSONException e) {
            Log.e("VSR_JSONException", e.toString());
        }

        handler = new android.os.Handler();
        DoThings.run();

        radarDialogInteractor.sendSignin();

        onRadar(true);

    }

    //Se crea un hilo para verificar constantemente si es que se ha pedido
    private Runnable DoThings = new Runnable() {
        @Override
        public void run() {
            //Se verifica que el pedido ha sido aceptado
            radarDialogInteractor.validateRequest(request, RadarDialogPresenterImpl.this, validator);
            Log.i("miradar", "Send verification");
            //Se coloca un contador para verificar la veces que se envia
            count++;
            //Se verifica que la cantidad de veces sea igual a 6
            if (count%3==0) {
                Log.i("miradar "+count,"Se envio el cancel request");
                //Se envia el precancel al servidor
                validateCancelRequest();
            }
            else
            //Se vuelve a ejecutar el mismo pero con un retraso de 10 segundos
                handler.postDelayed(this, 90000);
        }
    };

    public void onRadar(boolean active) {
        Log.i("OnRadar : ", "" + active);
        editor.putBoolean(Config.ONRADAR, active);
        editor.commit();
    }

    public void onFailedValidateRequest() {
    }

    //Metodo que se activa cuando algun taxista acepta el pedido
    @Override
    public void onSuccessValidateRequest(TaxiDriver taxiDriver) {

        //Se remueve los hilos
        handler.removeCallbacks(DoThings);

        //Se guarda los datos del taxista en el preferences
        TaxiDriver.saveName(taxiDriver.getName());
        TaxiDriver.savePhone(taxiDriver.getPhone());
        TaxiDriver.saveAutoplate(taxiDriver.getAutoplate());
        TaxiDriver.saveDNI(taxiDriver.getDni());
        TaxiDriver.saveCar_brand(taxiDriver.getCar_brand());
        TaxiDriver.saveCompany(taxiDriver.getCompany());
        TaxiDriver.saveGCM(taxiDriver.gcm_driver);
        TaxiDriver.saveImageUrl(taxiDriver.getImageUrl());
        TaxiDriver.saveEmail(taxiDriver.getEmail());
        Log.i("FINALIZO", "FINALIZO EL RADAR");
        //Se finaliza la actividad del dialogo


        imageLoader = ImageLoader.getInstance();
        targetSize = new ImageSize(300, 80);
        //Se descarga la imagen en la URL del taxista
        imageLoader.loadImage(TaxiDriver.loadImageUrl(), targetSize, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                //Se oculta el imageView y se muestra un icono de progreso
               /* userDialogView.getImageTaxiDriver().setVisibility(View.GONE);
                userDialogView.getProgressBarImage().setVisibility(View.VISIBLE);*/
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage != null ) {
                    // Se guarda la imagen en el preferences codificandola a base 64
                    TaxiDriver.saveImage(ImageRefactor.encodeTobase64(loadedImage));
                    editor.putBoolean(Config.GETIMAGE, true);
                    editor.commit();
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });


        editor.putBoolean(Config.ONREQUEST, true);
        editor.commit();
        radarDialogView.finishRadarDialogData();
    }

    //Metodo que envia el cancelar pedido al server
    @Override
    public void validateCancelRequest() {
        radarDialogView.disabledCancel();
        Log.i("VCR_DataToSend", request.toString());
        radarDialogInteractor.sendCancelRequest(validator, request, this);
    }

    //Metodo que se ejecuta cuando se envio correctamente el cancelar
    @Override
    public void onSuccessCancel() {
        handler.removeCallbacks(DoThings);
        Log.i("CancelRequest","Se cancelo el pedido de request");
        //Metodo que muestra un dialogo indicando que no se encontro un taxista
        radarDialogView.showDialogTaxiAround();
        //Metodo que finaliza la actividad del dialogo
        radarDialogView.finishRadarDialog();
        //onCall(false);
    }

    //Metodo que se ejecuta cuando no se envio correctamente el cancelar
    @Override
    public void onError() {
        radarDialogView.enabledCancel();
        radarDialogView.showDialogError();
    }

    //Metodo que se ejecuta cuando no hay conexion a internet
    @Override
    public void onConnectionError() {
        // Metodo que muestra un dialogo indicando que hay un error de conexion
        radarDialogView.showDialogConnection();
        // Metodo que habilita el boton cancelar
        radarDialogView.enabledCancel();
    }

    @Override
    public RadarDialogView getView() {
        return radarDialogView;
    }

    @Override
    public void desactiveSocket() {
//        radarDialogInteractor.desactiveSocketInstance();
    }
}
