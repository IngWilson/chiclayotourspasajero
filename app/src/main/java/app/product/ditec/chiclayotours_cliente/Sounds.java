package app.product.ditec.chiclayotours_cliente;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import app.product.ditec.chiclayotours_cliente.Map.MapActivity;

/**
 * Created by Wilson on 20/04/15.
 */
public class Sounds {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    public static final String NOTIFICATION_CHANNEL_ID = "1000";
    public static final String NOTIFICATION_CHANNEL_NAME = "DEMARKTEC";
    MediaPlayer bocina;
    // Se genera una notificacion en el sistema de notificaciones de android
    //        Uri sonido = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.son_notif);
    public void sendNotification(String msg,Uri sonido, Context context) {
        mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent ActivityIntent = new Intent(context, MapActivity.class);
        bocina = MediaPlayer.create(context, sonido);

          /*        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                ActivityIntent, 0);*/
        PendingIntent contentIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            contentIntent = PendingIntent.getActivity(context, 0, ActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_MUTABLE);
        } else {
            contentIntent = PendingIntent.getActivity(context, 0, ActivityIntent, 0);
        }
        // Se crea una nueva notificacion 
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                // Se coloca el icono
                .setSmallIcon(R.mipmap.ic_launcher)
                // Se coloca el titulo
                .setContentTitle("Notificacion Chiclayo Tours")
                // Se coloca el estilo del ti
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                // Se coloca el mensaje
                .setContentText(msg)
                .setAutoCancel(true)
                .setContentIntent(contentIntent);
                // Se coloca el sonido
                //.setSound(sonido);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel =
                    new NotificationChannel(
                            NOTIFICATION_CHANNEL_ID,
                            NOTIFICATION_CHANNEL_NAME,
                            NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(R.color.blue);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
        }
        bocina.start();
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
