package app.product.ditec.chiclayotours_cliente;

import android.content.Context;
import androidx.multidex.MultiDex;
import android.widget.ImageView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;


import java.net.URISyntaxException;

/**
 * Created by Wilson on 14/07/15.
 */
public class App extends Application {

    private static App mInstance;
    private static Socket mSocket;
private static ImageView publicidad;


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        mInstance = this;

/*
        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.reconnection = true;
*/

        // Se crea un nuevo socket 
        try {
            mSocket = IO.socket(getString(R.string.My_URL)+getString(R.string.URLSOCKET1));
        } catch (URISyntaxException e) {
        }

        // Se crea una nueva instancia para 
        // las configuraciones de la libreria
        // para cargar la imagen
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);

        // Se crea un hilo y se inicia el metodo para enviar
        // los errores a Splunk Mint
/*        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i("MintRunning", "Mint is running....");
                Mint.initAndStartSession(mInstance, "03008589");
            }
        }).start();*/

//        ActiveAndroid.clearCache();
        // Se inicializa la libreria para el ORM
        ActiveAndroid.initialize(this);


    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    // Se obtiene la instancia de la misma clase
    public static App getInstance(){
        return mInstance;
    }

    public static Socket getSocket() { return mSocket;}

    public static ImageView getPublicidad() {
        return publicidad;
    }

    public static void setPublicidad(ImageView publicidad) {
        App.publicidad = publicidad;
    }
}
