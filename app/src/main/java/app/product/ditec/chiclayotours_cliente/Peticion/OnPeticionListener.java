package app.product.ditec.chiclayotours_cliente.Peticion;

import app.product.ditec.chiclayotours_cliente.Model.Oferta;

public interface OnPeticionListener {
    void updatelist(Oferta oferta);
    void retirarofertataxista(String email_driver);
}
