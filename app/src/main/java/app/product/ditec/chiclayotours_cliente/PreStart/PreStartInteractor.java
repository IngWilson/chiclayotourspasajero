package app.product.ditec.chiclayotours_cliente.PreStart;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 27/06/15.
 */
public interface PreStartInteractor {
    void sendGCM(JSONObject jsonObject, Validation validator);
}
