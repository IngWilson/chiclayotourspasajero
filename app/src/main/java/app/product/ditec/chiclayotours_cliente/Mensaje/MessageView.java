package app.product.ditec.chiclayotours_cliente.Mensaje;

import android.content.Context;

public interface MessageView {
    Context getContext();
    void showDialogConnection();
    void hideProgress();
    void showDialogFailedSend();
    void showProgress();
}
