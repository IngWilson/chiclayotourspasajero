package app.product.ditec.chiclayotours_cliente.History.MonthTabs;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.History.HistoryActivity;
import app.product.ditec.chiclayotours_cliente.History.HistoryAdapter;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentJuly extends Fragment {

    View view;
    ListView lvJuly;
    HistoryAdapter historyAdapter;

    public FragmentJuly() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_july, container, false);
        lvJuly = (ListView) view.findViewById(R.id.lvJuly);
        /*historyAdapter = new HistoryAdapter(view.getContext());*/
        historyAdapter = new HistoryAdapter(view.getContext());

        try {
            if (!HistoryActivity.getmRequest(7).equals(null)) {
                historyAdapter.setData(new ArrayList<>(HistoryActivity.getmRequest(7)));
                lvJuly.setAdapter(historyAdapter);
            }
        }catch (NullPointerException e){}
        return view;
    }

}
