package app.product.ditec.chiclayotours_cliente.History.MonthTabs;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.History.HistoryActivity;
import app.product.ditec.chiclayotours_cliente.History.HistoryAdapter;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSeptember extends Fragment {

    View view;
    ListView lvSeptember;
    HistoryAdapter historyAdapter;

    public FragmentSeptember() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_september, container, false);
        lvSeptember = (ListView) view.findViewById(R.id.lvSeptember);
        historyAdapter = new HistoryAdapter(view.getContext());

        try {
            if (!HistoryActivity.getmRequest(9).equals(null)) {
                historyAdapter.setData(new ArrayList<>(HistoryActivity.getmRequest(9)));
                lvSeptember.setAdapter(historyAdapter);
            }
        }catch (NullPointerException e){}

        return view;
    }


}
