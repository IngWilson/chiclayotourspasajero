package app.product.ditec.chiclayotours_cliente;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;


/**
 * Created by Wilson on 02/05/15.
 */
public class MyIntentService extends IntentService {

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();

        String title = extras.getString("title");
        String message = extras.getString("message");
        sendNotification(title,message);

        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }


    public void sendNotification(String title,String msg) {
        int NOTIFICATION_ID = 1;
        NotificationManager mNotificationManager;

        mNotificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}
