package app.product.ditec.chiclayotours_cliente.VerifyPhone;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 24/05/15.
 */
public interface VerifyPhoneInteractor {

    void sendPhone(JSONObject jsonObject, Validation validator, OnVerifyPhoneFinishedListener listener);

}
