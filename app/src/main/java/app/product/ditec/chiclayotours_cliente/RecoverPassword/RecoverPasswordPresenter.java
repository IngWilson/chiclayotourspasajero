package app.product.ditec.chiclayotours_cliente.RecoverPassword;

/**
 * Created by lenovo on 19/06/2015.
 */
public interface RecoverPasswordPresenter {

    void validateEmail(String email);
}
