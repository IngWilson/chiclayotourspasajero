package app.product.ditec.chiclayotours_cliente.Promotion;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by usuario on 29/03/2016.
 */
public class PromotionPresenterImpl implements PromotionPresenter,OnPromotionFinishedListener {

    PromotionInteractor promotionInteractor;
    Validation validator;
    PromotionView promotionView;
    Context ctx;
    public PromotionPresenterImpl(){}

    public PromotionPresenterImpl(PromotionView promotionView){
        this.promotionView=promotionView;
        this.promotionInteractor = new PromotionInteractorImpl(this);
        ctx = promotionView.getContext();
        validator = new Validation(promotionView.getContext());
    }

    @Override
    public void validateGetData() {

        String email = User.loadEmail();
        promotionInteractor.getDataPromotion(email,validator);

    }

//    @Override
//    public boolean checkData() {
//        return prefs.getBoolean(Config.GETDATA, false);
//    }

    @Override
    public void onInternetError() {
        promotionView.showDialogInternetError();
    }


//public boolean checkIsable(){
//        return Costumer.loadIsAble();
//    }


    //Guarda los datos del taxista en preferences y los coloca en la vista (layout)
    @Override
    public void onSuccessGetUserData(String idpromotion, String totalcar, String situacion, String estado) {
/*        if(!checkData()) {
            TaxiDriver.saveName(taxiDriver.getName());
            TaxiDriver.saveLastname(taxiDriver.getLastname());
            TaxiDriver.saveEmail(taxiDriver.getEmail());


            accountView.setName(taxiDriver.getName() + " " + taxiDriver.getLastname());
            accountView.setEmail(taxiDriver.getEmail());

            //Se coloca en verdadero cuando se ha obtenido los datos de manera que
            // con este parametro impida descargar de nuevo los datos personales
            State.setGotData(true);
        }*/

        promotionView.setId(idpromotion);
        promotionView.setTotalc(totalcar);
        promotionView.setSituacion(situacion);
        promotionView.setEstado(estado);



    }
    //Envio de datos
    @Override
    public void validateCode(String code) {
        String email;

        // Metodo que muestra
        // un dialogo de progreso
        promotionView.showProgressE();
        // Se verifica si es que los datos que se envian
        // desde la actividad no sean nulos
        email = User.loadEmail();

        Log.i("VP_email", email.toString());
        Log.i("VP_code", code.toString());

        // Se crea un objeto JSON
        JSONObject param = new JSONObject();

        // Se coloca los parametros del telefono y el email
        try {
            param.put("codigo", code);
            param.put("email", email);
            Log.i("VP_param", param.toString());
        } catch (JSONException e) {
            Log.e("VP_JSONException", e.toString());
        }

        // Se envia los parametros para verificar el telefono
        promotionInteractor.sendCode(param, validator, this);
    }

    @Override
    public void onSuccessSend(String code) {

               // Se guarda en preferences
        // el usuario
       // User.savePhone("+51 " + phone);

                // Muestra un dialogo indicando que se envio correctamente
        // el telefono
        promotionView.showDialogSuccessSend();
        // Se oculta el dialogo de progreso
        promotionView.hideProgressE();

        // Se finaliza la actividad de la verificacion
        // del telefono
      //  verifyPhoneView.finished();

    }

    @Override
    public void onFailedSend() {
        promotionView.showDialogFailedSend();
        promotionView.hideProgressE();
    }

    // Metodo que se ejecuta cuando hay un
    // error en el telefono
    @Override
    public void onCodeError() {
        promotionView.showDialogCodeError();
        promotionView.hideProgressE();
    }

    @Override
    public void onFailedRepeatCode() {
        promotionView.showDialogRepeatCodeError();
        promotionView.hideProgressE();
    }

    @Override
    public void onFailedCodeExpire() {
        promotionView.showDialogCodeExpireError();
        promotionView.hideProgressE();
    }

    // Metodo que se ejecuta cuando hay
    // el campo del telefono esta vacio
    public void onEmptyCodeError() {
        promotionView.showDialogEmptyCode();
        promotionView.hideProgressE();
    }

    // Metodo que se ejecuta cuando hay
    // un error
    @Override
    public void onError() {
        promotionView.showDialogError();
        promotionView.hideProgressE();
    }
// Fin de envio
}
