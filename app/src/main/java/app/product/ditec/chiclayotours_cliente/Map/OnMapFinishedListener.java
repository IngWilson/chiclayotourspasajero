package app.product.ditec.chiclayotours_cliente.Map;

import org.json.JSONObject;

/**
 * Created by angel on 25/04/15.
 */

// Interface que define los metodos
// que muestren mensajes en caso de error
// u otros casos
public interface OnMapFinishedListener {

    void onConnectionErrorCall();

    void onGPSErrorCall();

    void onSuccessCall();

    void onSuccessCalloferta();

    void onErrorCall();

    void onSuccessCancel();

    void onErrorCancel();

    void onErrorPreCall();

    void onErrorPreCancel();

    void onErrorConnection();

    void onConnectionErrorCancel();

    void onGPSErrorCancel();

    void onFailGCM();

    void onFailCall();

    void onSuccessGetData();

    void onArrive(String message);

    void onConfirmarUbicacionCompartida(String message);
    void onCorrectVersion();

    void onIncorrectVersion();

    void onSuccessGCM(JSONObject jsonParams);

    void onCancelRequest();

    void onFinishRequest();

    void onSuccessFinish();

    void onSuccessGetPromotionData(String idpromotion, String totalcar, String situacion, String estado);

    void goodtravel();

    void saveId(String ide);

}
