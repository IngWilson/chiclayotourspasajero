package app.product.ditec.chiclayotours_cliente.RecoverPassword;

import android.content.Context;

/**
 * Created by lenovo on 19/06/2015.
 */
public interface RecoverPasswordView {

    Context getContext();

    void showDialogSuccessSend();

    void showDialogFailedSend();

    void navigateToCode(String email);

    void showDialogConfirmEmail();

    void hideProgress();

    void showProgress();

    void showDialogInternetError();

    void showDialogEmailError();

    void showDialogError();

    void finished();

    void showDialogEmptyemail();

    void showDialogErrorSend();

}
