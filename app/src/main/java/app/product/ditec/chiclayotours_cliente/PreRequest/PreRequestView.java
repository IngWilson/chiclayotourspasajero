package app.product.ditec.chiclayotours_cliente.PreRequest;

import android.content.Context;

/**
 * Created by angel on 08/06/15.
 */
public interface PreRequestView {

    void setNoteError();

    void showProgress();

    void hideProgress();

    void setErrorAddressStart();

    void setErrorAddressEnd();

    Context getContext();

    void showMapChooser();

    void setPrice(String price);
}
