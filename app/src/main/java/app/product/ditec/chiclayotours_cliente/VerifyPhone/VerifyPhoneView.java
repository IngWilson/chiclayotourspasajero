package app.product.ditec.chiclayotours_cliente.VerifyPhone;

import android.content.Context;

/**
 * Created by angel on 24/05/15.
 */
public interface VerifyPhoneView {

    Context getContext();

    void showDialogSuccessSend();

    void showDialogFailedSend();

    void navigateToCode(String phone);

    //void showDialogConfirmPhone();

    void hideProgress();

    void showProgress();

    void showDialogInternetError();

    void showDialogPhoneError();

    void showDialogError();

    void finished();

    void showDialogEmptyPhone();

}
