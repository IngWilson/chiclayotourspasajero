package app.product.ditec.chiclayotours_cliente.Code;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.product.ditec.chiclayotours_cliente.Login.LoginActivity;
import app.product.ditec.chiclayotours_cliente.R;

//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;
//import com.actionbarsherlock.app.ActionBar;
//import com.actionbarsherlock.app.SherlockActivity;

/**
 * Created by lenovo on 13/05/2015.
 */
public class CodeActivity extends AppCompatActivity implements CodeView, DialogInterface.OnClickListener {
    private String verificationId;
    private FirebaseAuth mAuth;

    EditText edtCode;
    Button btncode;
    Button btnresend;
    Button btnFirebaseCheck;
    TextView tvmessagefirebase;
    EditText edtFirebasecheck;
    ProgressBar progressBar;

    private CodePresenter presenter;
    private AlertDialog.Builder alertDialog;
    private ProgressDialog progressDialog;
    private SmsRetrieverClient smsRetrieverClient;
    TextView textView;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);
        mAuth = FirebaseAuth.getInstance();

        presenter = new CodePresenterImpl(this);
        alertDialog = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage(getString(R.string.code_envio));
        progressDialog.setCancelable(false);

        edtCode=findViewById(R.id.editCodeUserDialog);
        btncode=findViewById(R.id.btnCodeDialog);
        btnresend=findViewById(R.id.btnreseend);
        btnFirebaseCheck=findViewById(R.id.btnFirebaseCheck);
        tvmessagefirebase=findViewById(R.id.tvmessagefirebase);
        edtFirebasecheck=findViewById(R.id.edtFirebasecheck);
        progressBar=findViewById(R.id.progressbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_autoregistro);
        smsRetrieverClient = SmsRetriever.getClient(this);
        Task<Void> task = smsRetrieverClient.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
            }
        });
        IntentFilter filtro = new IntentFilter(IncomingSms.ACTION_RESP);
        filtro.addCategory(Intent.CATEGORY_DEFAULT);
        if (Build.VERSION.SDK_INT>=34 && getApplicationInfo().targetSdkVersion>=34){
            registerReceiver(new IncomingSms(), filtro,Context.RECEIVER_EXPORTED);
        }
        else {
            registerReceiver(new IncomingSms(), filtro);
        }
        textView = (TextView)findViewById(R.id.Txtconteo);

        CountDownTimer countDownTimer = new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                textView.setText("Si el codigo no  llega presione el siguiente boton en " + String.format(Locale.getDefault(), "%d segundos", millisUntilFinished / 1000L));
            }

            public void onFinish() {
                btnresend.setEnabled(true);
                btnresend.setBackgroundDrawable(getResources().getDrawable(R.drawable.green));
            }
        }.start();
    }

    public void btnReenviarCode(View view) {
        btnresend.setEnabled(false);
        btnresend.setBackgroundDrawable(getResources().getDrawable(R.drawable.green_lineal));
        textView.setText("Codigo reenviado por favor espere... verifique en los sms recibidos");
        //presenter.resendcode();
        presenter.resendcodefirebase();
        btnFirebaseCheck.setVisibility(View.VISIBLE);
        tvmessagefirebase.setVisibility(View.VISIBLE);
        edtFirebasecheck.setVisibility(View.VISIBLE);
        Log.i("btnreenviar","listo para reenviar");
    }

    @Override
    public void successresend() {
        textView.setText("Se Reenvio el codigo satisfactoriamente si no llega en 2 min comuniquese con la central");
    }

    public class IncomingSms extends BroadcastReceiver {
        public static final String ACTION_RESP = "com.google.android.gms.auth.api.phone.SMS_RETRIEVED";
               public void onReceive(Context context, Intent intent) {

                   if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                       Bundle extras = intent.getExtras();
                       Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                       switch(status.getStatusCode()) {
                           case CommonStatusCodes.SUCCESS:
                               // Get SMS message contents
                               String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                               int poscode=message.indexOf(":");
                               edtCode.setText(message.substring(poscode+2,poscode+6));
                               Acceptcode(edtCode);
                               Log.i("mimensaje",message);
                               break;
                           case CommonStatusCodes.TIMEOUT:
                               Log.d("TAG","timed out (5 minutes)");
                               break;
                       }
                   }

        }
    }


    public void Acceptcode(View view) {
        presenter.validateCodeData(edtCode.getText().toString());
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void finished() {
        finish();
    }

    @Override
    public void showDialogSuccessSend() {
        Toast.makeText(this, getString(R.string.dialog_code_send_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDialogErrorSend() {
        Toast.makeText(this, getString(R.string.dialog_code_send_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDialogFailSend() {
        Toast.makeText(this, getString(R.string.dialog_code_send_fail), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDialogFailreSend() {
        Toast.makeText(this, "Hubo un error solicitando reenvio de codigo cierre y abra la app", Toast.LENGTH_SHORT).show();
    }



    //Metodo que muestra un dialogo cuando no se tiene conexion a internet
    @Override
    public void showDialogInternetError() {
        alertDialog.setTitle(getString(R.string.dialog_INTERNET_error))
                .setMessage(getString(R.string.dialog_INTERNET_message_error))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }

    // Metodo del evento onClick que se asigna en el boton del dialogo de conexion
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -1:
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                break;
            default:
                break;
        }
    }

    @Override
    public Context getContext() {
        return CodeActivity.this;
    }

    //Metodo que muestra un dialogo en el edittext cuando hay un error al escribir el codigo
    @Override
    public void showCodeError() {
        Toast.makeText(this, getString(R.string.dialog_code_message), Toast.LENGTH_LONG).show();
    }

    //Metodo que invoca la actividad del login
    @Override
    public void navigateToLogin() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void micheckfirebase(View view) {
        verifyCode(edtFirebasecheck.getText().toString());
    }

    private void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Toast.makeText(CodeActivity.this, "Login exitoso", Toast.LENGTH_LONG).show();
                            presenter.validateFirebase();

                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(CodeActivity.this);
                            builder.setMessage("El codigo ingresado es erroneo, si aún no te ha llegado el codigo es posible que el número haya sido ingresado incorrectamente, intenta desinstalar e instalar la app e ingresar tu número nuevamente")
                                    .setTitle("Codigo erroneo")
                                    .setCancelable(false)
                                    .setNeutralButton("Aceptar",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();                        }
                    }
                });
    }
    @Override
    public void sendVerificationCode(String number) {
        progressBar.setVisibility(View.VISIBLE);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                edtFirebasecheck.setText(code);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(CodeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };
}





