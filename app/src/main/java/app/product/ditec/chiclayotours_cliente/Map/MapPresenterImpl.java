package app.product.ditec.chiclayotours_cliente.Map;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Config;
import app.product.ditec.chiclayotours_cliente.GCM;
import app.product.ditec.chiclayotours_cliente.Haversine;
import app.product.ditec.chiclayotours_cliente.ImageRefactor;
import app.product.ditec.chiclayotours_cliente.Model.Oferta;
import app.product.ditec.chiclayotours_cliente.Model.Request;
import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.MyBroadcastReceiver;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;



// Clase presentador que implementa el MapPresenter
// y el OnMapFinishedListener
public class MapPresenterImpl implements MapPresenter, OnMapFinishedListener {

    // Se declara la variable para la vista
    private MapView mapView;
    // Se declara la variable para el interactor
    private MapInteractor mapInteractor;

    // Se declara el objeto validador
    private Validation validator;
    // Se declara el objeto para las peticiones de la localizacion
    private MapRequestLocation mapRequestLocation;

    // Se declara el broadcastreceiver
    private MyBroadcastReceiver myBroadcastReceiver;

    // Se declara el intentFilter para filtrar los
    // intent que provienen del broadcastreceiver
    private IntentFilter intentFilter;

    // Se declara la localizacion del usuario
    private Location myCurrentLocation;

    // Se declara el preferences y su editor
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private static final String ACTION_MyIntentService = "RESPONSE";

    // Se declara el contexto
    private Context ctx;
    // Se declara el manejador
    private android.os.Handler handler;

    // Se declara el objeto imageLoader para manipular
    // la imagen
    private ImageLoader imageLoader;
    // Se declara las opciones para mostrar la imagen
    private DisplayImageOptions options;
    // Se declara la variable para manejar el tamaño de la imagen
    private ImageSize targetSize;


    // Metodo constructor que se le pasa la vista
    public MapPresenterImpl(MapView mapView) {
        // Se guarda la vista en la variable
        this.mapView = mapView;
        // Se instancia el interactor
        this.mapInteractor = new MapInteractorImpl(this);
        // Se instancia el validator
        this.validator = new Validation(mapView.getContext());

        // Se obtiene el contexto de la vista
        ctx = mapView.getContext();

        // Se instancia el preferences y su editor
        prefs = ctx.getSharedPreferences(Config.PREF_TAG, Context.MODE_PRIVATE);
        editor = prefs.edit();

        // Se llama a la funcion que valida el google play services
        validatePlayServices();

        // Se instancia el broadcastreceiver
        myBroadcastReceiver = new MyBroadcastReceiver(this);

        // Se instancia el intentFilter y se añade de la categoria
        intentFilter = new IntentFilter(ACTION_MyIntentService);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        // Se instancia una nueva localizacion vacia
        myCurrentLocation = new Location("");

        // Se instancia el manejador
        handler = new android.os.Handler();
        Log.i("CheckOnRadar", "" + checkOnRadar());


    }

    public void stopVerifyRequestState(){
        Log.i("FINALIZO", "se detuvo la verificacion del ontravel");
        handler.removeCallbacks(makeCheckState);
    }

    @Override
    public void onFinishRequest() {
        stopVerifyRequestState();
        checkUpdateRequest();

        setTravelingState(true);

        mapView.clearMap();
        mapView.changeTextBtnCancel("Finalizar");
        mapView.changeEventCancelRequest();
       // mapView.showDialogEndService();
        Log.i("FINALIZO", "se cambio los botones a finalizar");
    }
    @Override
    public void goodtravel()
    {
        mapView.showDialogEndService();
    }
    //Actualiza los datos en el slider
    @Override
    public void updateSlideTaxiDriver() {

        //Obtiene los datos del preferences
        String email=TaxiDriver.loadEmail();
        String name = TaxiDriver.loadNAME();
        String autoplate = TaxiDriver.loadAutoplate();
        String carModelAndBrand = TaxiDriver.loadCar_model() + " " + TaxiDriver.loadCar_brand();
        String company = TaxiDriver.loadCompany();
        String phone = TaxiDriver.loadPhone();
        String imageURL = TaxiDriver.loadImageUrl();

        //Crea un nueva instancia de ImageLoader para obtener la imagen a partir de la URL
        imageLoader = ImageLoader.getInstance();

        // Se crea opciones para mostrar la imagen
        // y donde indica que se muestra la imagen "loading" mientras
        // se descarga la imagen
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.loading)
                .build();

        //Se coloca el tamaño de la imagen que se quiere 300x80
        targetSize = new ImageSize(300, 80);

        //Se comienza a obtener la imagen a partir de la url con los parametros
        // (imageURL = URL de la imagen,
        // targetSize = tamano de la imagen que se desea,
        // options = opciones de visualizacion,
        // new SimpleImageLoadingListener clase abstracta que permite saber los estados de la descarga)
        imageLoader.loadImage(imageURL, targetSize, options, new SimpleImageLoadingListener() {

            //Cuando la descarga a comenzado
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            //Cuando la descarga a fallado
            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                if (failReason != null)
                    try {
                        Log.e("IUL_LoadingFailed", failReason.getCause().toString());
                    } catch (Exception e) {
                    }
            }

            //Cuando la descarga se ha completado correctamente
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                //Se verifica si es que la imagen obtenida no es nula y si es que
                // la descarga esta dentro del ambito del servicio, es decir, que se encuentre
                // recibiendo un taxi
                if (loadedImage != null && checkOnRequest()) {
                    // Se guarda la imagen en el preferences codificandola a base 64
                    TaxiDriver.saveImage(ImageRefactor.encodeTobase64(loadedImage));
                    //Se coloca al imageview una escala de FIT_XY que hace que la imagen se ajuste
                    // a el tamano completo del imageView
                    mapView.getImgTaxiDriver2().setScaleType(ImageView.ScaleType.FIT_XY);
                    //Se coloca en el imageView del slide
                    mapView.getImgTaxiDriver2().setImageBitmap(loadedImage);
                    mapView.getImgTaxiDriver1().setImageBitmap(loadedImage);
                    //Se coloca en el preferences un parametro para indicar que se obtuvo la
                    // imagen y que no se vuelva a descargar
                    editor.putBoolean(Config.GETIMAGE, true);
                    editor.commit();
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        }, new ImageLoadingProgressListener() {

            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                Log.i("OnProgressUpdate", "" + current);
            }
        });

        //Se coloca en el slide los demas parametros
        mapView.setTitle("Esperando a : " + name);
        mapView.setEmailTaxiDriver(email);
        mapView.setNameTaxiDriver(name);
        mapView.setAutoPlateTaxiDriver(autoplate);
        mapView.setPhoneTaxiDriver(phone);
        mapView.setCompany(company);
        mapView.setModelAndBrandCar(carModelAndBrand);
        mapView.enabledCallTaxiDriver();

    }

    //Metodo que sirve para obtener el alias del codigo
    @Override
    public String getAliasCode() {
        PackageInfo pInfo = null;
        try {
            pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("PackageManager", e.toString());
        }

        String alias = pInfo.versionName;
        return alias;
    }

    // Metodo que verifica que se han obtenido
    // los datos correctamente
    @Override
    public boolean checkData() {
        return prefs.getBoolean(Config.GETDATA, false);
    }


    // Metodo que verifica que este habilitado
    // el usuario para hacer pedidoss
    @Override
    public boolean checkIsAble() {
        return prefs.getBoolean("isable", false);
    }

    // No se usa
    @Override
    public boolean checkOnVerified() {
        return User.loadVerified();
    }

    //Metodo que activa el hilo que verifica constantemente
    // si es que el pedido en el server existe o no
    @Override
    public void checkUpdateRequest() {
        DoThings.run();
    }

    public void verifyRequest(){
        makeCheckState.run();
    }

    // Metodo que verifica que si se ha cancelado
    // el pedido
    @Override
    public boolean checkOnCancel() {
        return prefs.getBoolean(Config.ONCANCEL, false);
    }
    // Metodo que verifica que se ha llamado
    // al taxista
    @Override
    public boolean checkOnCall() {
        return prefs.getBoolean(Config.ONCALL, false);
    }

    // Metodo que verifica si esta en pleno pedido
    @Override
    public boolean checkOnRequest() {
        return prefs.getBoolean(Config.ONREQUEST, false);
    }

    // Metodo que verifica que esta en la muestra de datos
    // del taxista
    public boolean checkOnUserDialog() {
        return prefs.getBoolean(Config.ONUSERDIALOG, false);
    }

    // Metodo que verifica que esta en pleno radar
    @Override
    public boolean checkOnRadar() {
        return prefs.getBoolean(Config.ONRADAR, false);
    }

    // Metodo que verifica si es que se han obtenido
    // los datos correctamente
    @Override
    public void validateGetUserData() {
        String email = User.loadEmail();
        String gcm = User.loadGCM();
        Log.i("GCM_USER", gcm);

        // Se verifica que se recupero la informac
        // y que este verificado el usuario
        if (!checkData() || !checkOnVerified())
            mapInteractor.getUserData(email, new Validation(mapView.getContext()), this);
        else {
//            Log.i("first_name", user.getName());
//            Log.i("last_name", user.getLastname());
//            Log.i("email", user.getEmail());
//            Log.i("phone", user.getPhone());
//            Log.i("isable", "" + user.getIsable());
        }
    }

    //Se valida que se tiene la ultima actualizacion de play services
    public void validatePlayServices() {
        if (mapView.onCheckGooglePlayServices())
            mapRequestLocation = new MapRequestLocation(mapView);
    }

    @Override
    public void onAddMyLocationMarker() {
//        Location location = mapRequestLocation.getLastLocation();
//      mapView.addMyPositionMarker(location.getLatitude(), location.getLongitude());
    }

    //Remueve la posicion del taxi
    @Override
    public void onDeleteMarker() {
        mapView.removeTaxiMarker();
    }

    @Override
    public void validatesendOferta(String note, String address, String number,String to,String price, String exactprice, String tipomovil) {
        Request.saveAddress(address + " " + number);
        String email = User.loadEmail();
        Location location = getMyCurrentLocation();

        // Se verifaca que la posicion no sea nula y
        // que tampoco sea 0
        if (location != null && location.getLongitude() != 0 && location.getLatitude() != 0) {
            JSONObject request = new JSONObject();
            try {
                request.put("note", note);
                request.put("email", email);
                request.put("latitude", Request.loadLatitudeA());
                request.put("longitude", Request.loadLongitudeA());
                request.put("latitudeB", Request.loadLatitudeB());
                request.put("longitudeB", Request.loadLongitudeB());
                //añadido para el hasta
                if (to != null) {
                    to=to.replaceAll("\\n", " ");
                    request.put("to", to);
                }
                else request.put("to", "");
                if (price != null) request.put("price", price);
                else request.put("price", "");
                if (exactprice != null) request.put("exactprice", exactprice);
                else request.put("exactprice", "");
                if (tipomovil != null) request.put("tipomovil", tipomovil);
                else request.put("tipomovil", "Cualquiera");
                //fin del hasta
//                    request.put("gcm", gcm_user);
                address=address.replaceAll("\\n", " ");
                number=number.replaceAll("\\n", " ");
                request.put("address", address + " " + number);
                Log.i("VSR_ToSendRequest", request.toString());
            } catch (JSONException e) {
                Log.e("VSR_JSONException", e.toString());
            }
            // Se indica que se hizo la peticion
            //setOnCall(true);
            Log.i("prueba", String.valueOf(prefs.getBoolean(Config.ONCALL, false)));
            // Se ejecuta el metodo en el interactor
            // para enviar la peticion al servidor
           mapInteractor.sendRequestOferta(request, validator, this);
        }
    }

        //Se valida el envio del pedido
    @Override
    public void validateSendRequest(String note, String address, String number,String to,String price, String exactprice, String tipomovil, String grupo) {

        // Se valida que el usuario este verificado
        if (!User.loadVerified())
            validateGetUserData();
        // Si el usuario esta verificado
        if (User.loadVerified()) {

            //Si es que la direccion no es igual a "Buscando direccion..."
            if (!address.equals("")) {

                //Se muestra un dialogo de progreso
                mapView.showProgress();
                // Se desactiva el boton del llamar taxi
                MapActivity.disabledCallTaxi();

                mapView.disabledoptions();

                //Se guarda en preferences la direccion junto con el numero
                Request.saveAddress(address + " " + number);

                final GCM gcm = new GCM(mapView.getContext());

                // String gcm_user     = User.loadGCM();
                String email = User.loadEmail();

//            Log.i("VSR_GCM_USER: ", gcm_user);

                //Se obtiene la actual posicion del usuario
                Location location = getMyCurrentLocation();

                // Se verifaca que la posicion no sea nula y
                // que tampoco sea 0
                if (location != null && location.getLongitude() != 0 && location.getLatitude() != 0) {
                    // Se crea un JSONObject y se colocan todos los parametros
                    // de la peticion
                    JSONObject request = new JSONObject();
                    try {
                        request.put("note", note);
                        request.put("email", email);
                        request.put("latitude", location.getLatitude());
                        request.put("longitude", location.getLongitude());
                        request.put("zona", grupo);
                        //añadido para el hasta
                        if(to!=null) {
                            to=to.replaceAll("\\n", " ");
                            request.put("to",to);
                        }
                        else request.put("to","");
                        if(price!=null) request.put("price",price);
                        else request.put("price","");
                        if(exactprice!=null) request.put("exactprice",exactprice);
                        else request.put("exactprice","");
                        if(tipomovil!=null) request.put("tipomovil",tipomovil);
                        else request.put("tipomovil","Cualquiera");
                        //fin del hasta
//                    request.put("gcm", gcm_user);
                        address=address.replaceAll("\\n", " ");
                        number=number.replaceAll("\\n", " ");
                        request.put("address", address + " " + number);
                        Log.i("VSR_ToSendRequest", request.toString());
                    } catch (JSONException e) {
                        Log.e("VSR_JSONException", e.toString());
                    }
                    // Se indica que se hizo la peticion
                    setOnCall(true);
                    Log.i("prueba", String.valueOf(prefs.getBoolean(Config.ONCALL, false)));
                    // Se ejecuta el metodo en el interactor
                    // para enviar la peticion al servidor
                    mapInteractor.sendRequest(request, validator, this, 0, gcm);
                    //setMyCurrentLocation(location.getLatitude(),location.getLongitude());

                } else {
                    // Se oculta el progressDialog
                    mapView.hideProgress();
                    // Se muestra un mensaje indicando que no se tiene una localizacion
                    // correcta
                    mapView.showDialogLocationError();
                    // Se habilita el boton para hacer la peticion
                    mapView.enabledCallTaxi();
                }
            } else {
                // Se habilita el boton para hacer la peticion
                mapView.enabledCallTaxi();
                // Se muestra un mensaje indicando que no se encontro
                // la direccion
                mapView.showDialogAddressNotFound();
            }
        } else {
            // Se habilita el boton para hacer la peticion
            mapView.enabledCallTaxi();
            // Se muestra un dialogo indicando que el la cuenta
            // esta deshabilitada
            mapView.showDialogDisableAccount();
        }
    }


    @Override
    public void validateFinishedRequest(){
        mapView.showProgress();
        String email = User.loadEmail();
        String email_taxi = TaxiDriver.loadEmail();

        Log.i("CR_EMAIL_USER :", email);
        Log.i("CR_EMAIL_DRIVER :", email_taxi);

        //Se crea un objeto JSON y se pone el email del taxista y usuario
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email_user", email);
            jsonObject.put("email_driver", email_taxi);
        } catch (JSONException e) {
            Log.e("JSONExceptionPresenter", e.toString());
        }

        mapInteractor.sendRequest(jsonObject, validator, this, 3, null);

    }

    public void validateRecepcionMensaje(String Message) {

        //Se muestra un dialogo de progreso
        mapView.showProgress();

        //Se obtiene el email del pasajero
        String email = TaxiDriver.loadEmail();


        //Se coloca el email en un JSONObject
        JSONObject params = new JSONObject();
        try {
            params.put("email",email);
            params.put("message", Message);
        }catch (JSONException e){
            Log.i("mierrormensaje",e.toString());}
        Log.i("mierrormensaje","se finalizo el presenter");
        //Se utiliza este metodo para enviar "ya llegue" al pasajero
        mapInteractor.validateRecepcionMensaje(params,validator);
    }

    @Override
    public void onSuccessFinish() {
        if (!checkOnCancel()) {
            Log.i("Flow_App-OSC", "Successfully Cancel");
            //Se coloca en el preferences que se ha cancelado el pedido
            editor.putBoolean(Config.ONCANCEL, true);
            editor.commit();


            mapView.changeTextBtnCancel("CANCELAR TAXI");
            mapView.changeEventCancelRequestForDefault();

            //Se oculta la barra de progreso
            mapView.hideProgress();
            //Metodo que muestra un dialogo indicando que el pedido se cancelo satisfactorio
            mapView.showSuccessfullyFinish();
            //Se limpia el mapa
            mapView.clean();
            setTravelingState(false);
            //Se resetea la posicion del marcador
            mapView.clearMap();
            //Se activa el boton de llamar taxi
            mapView.enabledCallTaxi();
            //Se desactiva el boton cancelar
            mapView.disabledCancelTaxi();
            //Se desactiva el boton de llamar al taxista
            mapView.disabledCallTaxiDriver();

            //Se resetea los valores en preferences que indican que esta en el servicio o en el pedido
            resetService();
            //Se reseta la posicion del marcador
            restartMyMarker();
            //Se desactiva la verificacion constante
            removeCheckUpdateRequest();

            stopVerifyRequestState();
            //Se obtiene la imagen del taxista y se coloca para que se centre en el imagevieww
            mapView.getImgTaxiDriver2().setScaleType(ImageView.ScaleType.FIT_CENTER);
            mapView.reiniciaractividad();
        }
    }

    //Se valida la cancelacion del pedido
    @Override
    public void validateCancelRequest() {
        mapView.showProgress();

        //Se obtiene el email del taxista y usuario
        String email = User.loadEmail();
        String email_taxi = TaxiDriver.loadEmail();

        Log.i("CR_EMAIL_USER :", email);
        Log.i("CR_EMAIL_DRIVER :", email_taxi);


        //Se crea un objeto JSON y se pone el email del taxista y usuario
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email_user", email);
            jsonObject.put("email_driver", email_taxi);
        } catch (JSONException e) {
            Log.e("JSONExceptionPresenter", e.toString());
        }

        //Se coloca en preferences el valor "MyCancel" para indicar que el pedido ha sido cancelado por el usuario y
        // no por el taxista
        editor.putBoolean("MyCancel", true);
        editor.commit();

        // Enviar la cancelacion del pedido y se envia por el mismo metodo pero indicando el tipo (1) que significa cancelacion
        mapInteractor.sendRequest(jsonObject, validator, this, 1, null);

    }

    //Metodo para colocar en preferences un booleano que indica que se esta en pleno servicio
    @Override
    public void setOnRequest(Boolean active) {
        editor.putBoolean(Config.ONREQUEST, active);
        editor.commit();
    }

    //Metodo para colocar en preferences un booleano que indica
    // que se ha obtenido los datos correctamente
    public void setDataReceived(boolean active) {
        editor.putBoolean("DataTaxi", true);
        editor.commit();
    }

    //Se coloca la posicion del usuario en la variable global del myCurrentLocation
    @Override
    public void setMyCurrentLocation(Double latitude, Double longitude) {
        myCurrentLocation.setLatitude(latitude);
        myCurrentLocation.setLongitude(longitude);
        Request.saveLatitude(latitude.floatValue());
        Request.saveLongitude(longitude.floatValue());
    }

    //Metodo para colocar en preferences que se realizo una llamada
    @Override
    public void setOnCall(boolean active) {
        editor.putBoolean(Config.ONCALL, active);
        editor.commit();
    }

    //Metodo que indica que se tiene o no GPS
    @Override
    public void setGPS(boolean active) {
        editor.putBoolean(Config.GPS, active);
        editor.commit();
    }

    @Override
    public void onCorrectVersion() {
    }

    //Metodo que se activa cuando la version en el server es diferente a la del celular
    @Override
    public void onIncorrectVersion() {
        mapView.showDialogInstallVersion();
    }

    //Metodo que se activa cuando se envia correctamente el pedido
    @Override
    public void onSuccessCalloferta() {
        //Se oculta la barra de progreso
        mapView.hideProgress();

       //Se muestra la actividad del radar
        mapView.navigateToPeticion();
    }

    @Override
    public void onSuccessCall() {
        //Se oculta la barra de progreso
        mapView.hideProgress();

        //Se registra el broadcastreceiver
        if (Build.VERSION.SDK_INT>=34 && ctx.getApplicationInfo().targetSdkVersion>=34){
            ctx.registerReceiver(myBroadcastReceiver, intentFilter,Context.RECEIVER_EXPORTED);
        }
        else {
            ctx.registerReceiver(myBroadcastReceiver, intentFilter);
        }

        //Se coloca el marcador del usuario en el mapa
        //TODO: Verificar esto mas adelante
        mapView.addMyPositionMarker(getMyCurrentLocation().getLatitude(), getMyCurrentLocation().getLongitude());

        //Se colca en preferences un booleano para el CANCEL que indica que no se ha cancelado aun el pedido
        editor.putBoolean(Config.ONCANCEL, false);
        editor.commit();

        //Se pone en habilitado el pasajero dado que se pude enviar correctamente un pedido
        User.saveIsable(true);
        //Se coloca en preferences que ya no se encuentra en el estado de pedir un taxi
        setOnCall(false);

        Log.i("Flow_App-OSC", "Successfully Call");

        Log.i("oSC_Broadcast", "Active");
        //Se muestra la actividad del radar
        mapView.navigateToRadarDialog();
    }
   @Override
   public void prepareaceptaroferta(){

       //Se registra el broadcastreceiver
       if (Build.VERSION.SDK_INT>=34 && ctx.getApplicationInfo().targetSdkVersion>=34){
           ctx.registerReceiver(myBroadcastReceiver, intentFilter,Context.RECEIVER_EXPORTED);
       }
       else {
           ctx.registerReceiver(myBroadcastReceiver, intentFilter);
       }

       //Se coloca el marcador del usuario en el mapa
       //TODO: Verificar esto mas adelante
       mapView.addMyPositionMarker(getMyCurrentLocation().getLatitude(), getMyCurrentLocation().getLongitude());

       //Se colca en preferences un booleano para el CANCEL que indica que no se ha cancelado aun el pedido
       editor.putBoolean(Config.ONCANCEL, false);
       editor.commit();

       //Se pone en habilitado el pasajero dado que se pude enviar correctamente un pedido
       User.saveIsable(true);
       //Se coloca en preferences que ya no se encuentra en el estado de pedir un taxi
       setOnCall(false);

   }
    @Override
    public void saveId(String ide) {
        Request.saveId(ide);
        Log.i("valoride",ide);

    }
    //Resetea la posicion del marcador del pasajero
    public void restartMyMarker() {
        try {
            Location location = mapRequestLocation.getLastLocation();
            //mapView.addMyPositionMarker(location.getLatitude(), location.getLongitude());

            if (location != null)
                mapView.adjustZoomMap(location.getLatitude(), location.getLongitude(), 15);
        } catch (Exception e) {
        }
    }

    //Se verifica el valor en preferences del Cancel
    @Override
    public boolean checkMyCancel() {
        return prefs.getBoolean("MyCancel", false);
    }

    //Metodo que se activa cuando el taxista termina el pedido
    @Override
    public void onCancelRequest() {
        Log.i("FINALIZO", "se va a iniciar la finalización de la carrera");
        //Se verifica que el valor "MyCancel" del preferences esta en falso, lo cual indica que el
        // usuario aun no ha cancelado el pedido y
        // utilizando el valor "Cancel" del preferences se verifica que el pedido no se ha terminado por parte del taxista
        if (!checkMyCancel() && !checkOnCancel()) {

            //Se coloca en verdadero que el pedido ha sido cancelado por el taxista
            Log.i("Flow_App-OCR", "Successfully Cancel Request");
            mapView.hideProgress();
            editor.putBoolean(Config.ONCANCEL, true);
            editor.commit();

            //Metodo para guardar en la base de datos el pedido
            saveRequest();

           //            mapView.changeEventCancelRequest();
            MapActivity.disabledCallTaxi();
//                        disabledCancelTaxi();


            //Se limpia el mapa y se elimina el marcador
            mapView.clearMap();

            mapView.disabledCallTaxiDriver();
            Log.i("FINALIZO", "linea 674");
            mapView.changeTextBtnCancel("CANCELAR TAXI");
            mapView.changeEventCancelRequestForDefault();

            //Se coloca los valores del preferences en sus valores originales
            resetService();
            restartMyMarker();
            //Se remueve la verificacion constante
            removeCheckUpdateRequest();
            Log.i("FINALIZO", "linea 683");
            // Se resetea los valores del slider, es decir, los valores del taxista
            mapView.clean();
            setTravelingState(false);

            // Se obtiene la imagen del taxista y se pone una escala que indica que se ponga en la
            // posicion en el imageView
            mapView.getImgTaxiDriver2().setScaleType(ImageView.ScaleType.FIT_CENTER);
            Log.i("FINALIZO", "linea 691");
//            makeCheckState.run();
            //Metodo que muestra un dialogo indicando que el pedido ha sido cancelado
            mapView.showDialogFinishTravel();
        }
    }

    // Metodo que guarda los datos de la peticion
    // en la BD local de Android
    public void saveRequest() {

        //Se obtiene los datos del taxista desde el preferences y se coloca en un nuevo objeto TaxiDriver
        // y tambien se obtiene los datos del pedido desde el preferences y se coloca en un nuevo objeto
        // Request y posteriormente se guarda en la BD
        TaxiDriver taxiDriver = new TaxiDriver();
        taxiDriver.setName(TaxiDriver.loadNAME());
        taxiDriver.setLastname(TaxiDriver.loadLastName());
        taxiDriver.setAutoplate(TaxiDriver.loadAutoplate());
        taxiDriver.setCar_brand(TaxiDriver.loadCar_brand());
        taxiDriver.setCar_model(TaxiDriver.loadCar_model());
        taxiDriver.setCompany(TaxiDriver.loadCompany());
        taxiDriver.setDni(TaxiDriver.loadDNI());
        taxiDriver.setPhone(TaxiDriver.loadPhone());
        //Metodo que guarda los datos en la BD
        taxiDriver.save();

        //Se obtiene la hora actual
        Time date = new Time(Time.getCurrentTimezone());
        date.setToNow();

        Request request = new Request();
        request.setTaxiDriver(taxiDriver);
        request.setAddress(Request.loadAddress());
        request.setDay(date.monthDay);
        int month = date.month +1;
        //Se coloca los parametros actuales de tiempo
        request.setMonth(month);
        request.setYear(date.year);
        request.setHour(date.hour);
        request.setMinute(date.minute);
        if(Oferta.loadtipopeticion().equals("oferta"))
            request.setPrecio(Oferta.loadPrecio());
        else
            request.setPrecio("Acordado con Conductor");
        //Se guarda el pedido en la BD
        request.save();

    }

    //Metodo que se activa cuando el usuario quiere cancelar el pedido
    @Override
    public void onSuccessCancel() {
        //Se verifica que el taxista no ha terminado el pedido
        if (!checkOnCancel()) {
            Log.i("Flow_App-OSC", "Successfully Cancel");
            //Se coloca en el preferences que se ha cancelado el pedido
            editor.putBoolean(Config.ONCANCEL, true);
            editor.commit();

            //Se oculta la barra de progreso
            mapView.hideProgress();
            //Metodo que muestra un dialogo indicando que el pedido se cancelo satisfactorio
            mapView.showSuccessfullyCancel();
            //Se limpia el mapa
            mapView.clean();
            setTravelingState(false);
            //Se resetea la posicion del marcador
            mapView.clearMap();
            //Se activa el boton de llamar taxi
            mapView.enabledCallTaxi();
            //Se desactiva el boton cancelar
            mapView.disabledCancelTaxi();
            //Se desactiva el boton de llamar al taxista
            mapView.disabledCallTaxiDriver();

            //Se resetea los valores en preferences que indican que esta en el servicio o en el pedido
            resetService();
            //Se reseta la posicion del marcador
            restartMyMarker();
            //Se desactiva la verificacion constante
            removeCheckUpdateRequest();

            stopVerifyRequestState();
            //Se obtiene la imagen del taxista y se coloca para que se centre en el imagevieww
            mapView.getImgTaxiDriver2().setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
    }

    //Metodo que se activa cuando se presiona el boton para llamar al taxista
    @Override
    public void callTaxiDriver() {
        //Se obtiene el telefono del taxista desde el preferences
        String phone = TaxiDriver.loadPhone();
        //Se crea un intent indicando el tipo
        Intent intent = new Intent(Intent.ACTION_DIAL);
        //Se coloca la data indicando el telefono
        intent.setData(Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //Se ejecuta el intent
        ctx.startActivity(intent);

        Log.i("Flow_App-CTD", "Successfully Call Taxi Driver");
    }
    //fiorella
    @Override
    public void callcentral(){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + App.getInstance().getString(R.string.nrocentral)));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //Se ejecuta el intent
        ctx.startActivity(intent);
    }


    // Metodo que obtiene los datos del taxista desde el preferences
    @Override
    public void returnDataTaxiDriver() {

        //Se obtiene los datos del taxista desde el preferences
        String email=TaxiDriver.loadEmail();
        String name = TaxiDriver.loadNAME();
        String phone = TaxiDriver.loadPhone();
        String autoplate = TaxiDriver.loadAutoplate();
        String carModelAndBrand = TaxiDriver.loadCar_model() + " " + TaxiDriver.loadCar_brand();
        String company = TaxiDriver.loadCompany();
        Bitmap bmp = ImageRefactor.decodeBase64(TaxiDriver.loadImage());
        String precio = Oferta.loadPrecio();
        //Se pone los datos obtenidos en el layout
        mapView.setTitle("Esperando a : " + name);
        mapView.setEmailTaxiDriver(email);
        mapView.setNameTaxiDriver(name);
        mapView.setModelAndBrandCar(carModelAndBrand);
        mapView.setCompany(company);
        mapView.setPhoneTaxiDriver(phone);
        mapView.setAutoPlateTaxiDriver(autoplate);
        mapView.getImgTaxiDriver1().setImageBitmap(bmp);
        mapView.getImgTaxiDriver2().setImageBitmap(bmp);
        mapView.setPrecio(precio);
        //Se deshabilita el boton de llamar Taxi
        MapActivity.disabledCallTaxi();
        mapView.disabledoptions();

        //Se habilita el boton cancelar Taxi
        mapView.enabledCancelTaxi();
        //Se habilita el boton de llamar al taxista
        mapView.enabledCallTaxiDriver();

    }

    //Metodo que coloca en preferences que se encuentra en radar
    @Override
    public void onRadar(boolean active) {
        editor.putBoolean(Config.ONRADAR, active);
        editor.commit();
    }

    @Override
    public void onSuccessGCM(JSONObject jsonParams) {
        mapView.hideProgress();
        GCM gcm = new GCM(mapView.getContext());
        try {
            jsonParams.put("gcm", User.loadGCM());
        } catch (JSONException e) {
            Log.e("oSGCM", e.toString());
        }
        mapInteractor.sendRequest(jsonParams, validator, this, 0, gcm);
    }

    //
    @Override
    public void onSuccessGetData() {
        editor.putBoolean(Config.GETDATA, true);
        editor.commit();
    }

    // Metodo que resetea los parametros que indican estados en la app
    @Override
    public void resetService() {
        editor.putBoolean(Config.ONCALL, false);
        editor.putBoolean(Config.ONREQUEST, false);
        editor.putBoolean(Config.ONSERVICE, false);
        editor.putBoolean(Config.GETIMAGE, false);
        editor.putBoolean("MyCancel", false);

        editor.putBoolean("Initial", false);
        editor.putBoolean("DataTaxi", false);
        //Se eliminan los datos del taxista
        TaxiDriver.clean();
        editor.commit();
    }

    // Metodo que resetea los parametros que indica los parametros propios de la cuenta
    @Override
    public void resetParams() {
        //Se coloca en falso que se obtuvo los datos
        editor.putBoolean(Config.GETDATA, false);
        //Se coloca en falso que se encuentra logeado
        editor.putBoolean(Config.ONLOGIN, false);
        //Se coloca en falso que se obtuvo la imagen
        editor.putBoolean(Config.GETIMAGE, false);

        //Se borran los datos del pasajero
        User.clean();

        resetService();
    }

    @Override
    public void setTravelingState(boolean state) {
        editor.putBoolean(Config.TRAVELING,state);
    }

    @Override
    public boolean getTravelingState(){
        return prefs.getBoolean(Config.TRAVELING,false);
    }

    //Metodo que se ejecuta cuando no se envia correctamente el pedido
    @Override
    public void onErrorPreCall() {
        //Se oculta el dialogo de progreso
        mapView.hideProgress();
        //Se activa el boton de llamar taxi
        mapView.enabledCallTaxi();
        //Se muestra un dialogo indicando que hay un error al enviar el pedido
        mapView.showDialogErrorPreCall();
    }

    //Metodo que se ejecuta cuando no se envia correctamente la cancelacion del pedido
    @Override
    public void onErrorPreCancel() {
        //Se oculta el dialogo de progreso
        mapView.hideProgress();
        //Se activa el boton de cancelar taxi
        mapView.enabledCancelTaxi();
        //Se muestra un dialogo que indica que hay un error al enviar la cancelacion del pedido
        mapView.showDialogErrorPreCancel();
    }

    @Override
    public void onErrorCall() {
        mapView.hideProgress();
        //user.setGCM("");
        mapView.showDialogError();
        mapView.enabledCallTaxi();
    }

    //Metodo que se ejecuta cuando no se esta conectado al servidor
    @Override
    public void onErrorConnection() {
        //Se oculta el dialogo de progreso
        mapView.hideProgress();
        //Se muestra un dialogo indicando que no se esta conectado al servidor
        mapView.showDialogConnectionInternetError();
        //Se activa el boton de llamar taxi
        mapView.enabledCallTaxi();
    }

    @Override
    public void onErrorCancel() {
        mapView.hideProgress();
        //user.setGCM("");
        mapView.showDialogError();
        mapView.enabledCancelTaxi();
        editor.putBoolean("MyCancel", false);
        editor.commit();

    }

    //Metodo que se ejecuta cuando no se tiene activado GPS
    @Override
    public void onGPSErrorCall() {
        //Se oculta el dialogo de progreso
        mapView.hideProgress();
        //Se muestra un dialogo indicando que no se tiene activado el GPS
        mapView.showDialogGPSError();
        //Se activa el boton de llamar taxi
        mapView.enabledCallTaxi();
    }

    //Metodo que se ejecuta cuando no se tiene activado GPS
    @Override
    public void onGPSErrorCancel() {
        //Se oculta el dialogo de progreso
        mapView.hideProgress();
        //Se muestra un dialogo indicando que no se tiene activado el GPS
        mapView.showDialogGPSError();
        //Se activa el boton de cancelar taxi
        mapView.enabledCancelTaxi();
    }

    //Metodo que se ejecuta cuando no se tiene conexion a internet
    @Override
    public void onConnectionErrorCall() {
        //Se oculta el dialogo de progreso
        mapView.hideProgress();
        //Se muestra un dialogo que indica que no se tiene conexion a internet
        mapView.showDialogConnectionError();
        //Se activa el boton llamar taxi
        mapView.enabledCallTaxi();
    }

    //Metodo que se ejecuta cuando no se tiene conexion a internet
    @Override
    public void onConnectionErrorCancel() {
        mapView.hideProgress();
        mapView.showDialogConnectionError();
        //Se activa el boton cancelar taxi
        mapView.enabledCancelTaxi();
    }

    //Metodo que verifica si es que esta activado el GPS
    @Override
    public void validateGPS() {
        if (!validator.enabledGPS())
            mapView.showDialogGPSError();
    }

    //Metodo que valida que esta conectado a internet
    @Override
    public void validateINTERNET() {
        if (!validator.isOnline())
            mapView.showDialogConnectionError();
    }

    @Override
    public void onConnect() {
        mapRequestLocation.connect();
    }

    @Override
    public void onDisconnect() {
        if (mapView.onCheckGooglePlayServices())
            if (mapRequestLocation != null)
                if (mapRequestLocation.verifyAAPIClient()) {
                    mapRequestLocation.stopLocationUpdates();
                    mapRequestLocation.disconnect();
                }

    }

    //Metodo que remueve la verificacion constante
    @Override
    public void removeCheckUpdateRequest() {
        handler.removeCallbacks(DoThings);
    }

//se verifica si aun estamos esperando al taxi
    Runnable makeCheckState = new Runnable() {

        JSONObject params;
        String email;

        @Override
        public void run() {
            email = User.loadEmail();

            params = new JSONObject();
            try{
                params.put("email",email);
            } catch (JSONException e){

            }

            mapInteractor.checkRequestState(params,validator,MapPresenterImpl.this);
            handler.postDelayed(makeCheckState,150000);

        }
    };

    //Se crea el hilo donde
    // se verifica constantemente si la peticion activa
    // o no
    Runnable DoThings = new Runnable() {

        JSONObject params;
        String email;

        @Override
        public void run() {
            // Se obtiene el email del preferences
            email = User.loadEmail();

            // Se crea un JSONObject
            params = new JSONObject();
            try {
                params.put("email", email);
            } catch (JSONException e) {
                Log.e("CUR_JSONException", e.toString());
            }
            Log.i("Map_DoThings", "DoThings is running");
            // Se ejecuta el metodo en el interactor
            // para verificar si esta activo o no el pedido
            mapInteractor.checkRequest(params, validator, MapPresenterImpl.this);
            handler.postDelayed(DoThings, 150000);
        }
    };


/*    Runnable checkdireccion = new Runnable() {



        @Override
        public void run() {
            if(MapActivity.txtAddressCity.getText().toString().equals("Buscando direccion...")) {
                getAddressDirection();
                handler.postDelayed(checkdireccion, 3000);
            }
            else {
                handler.removeCallbacks(checkdireccion);
            }
        }
    };*/

    @Override
    public void checkaddress() {
       // checkdireccion.run();
    }
    @Override
    public MapView getView() {
        return mapView;
    }


    @Override
    public Location getMyCurrentLocation() {
        return myCurrentLocation;
    }

    @Override
    public void onFailGCM() {
        User.saveGCM("");
    }

    // Metodo que se ejecuta cuando se cierra la App
    // para recuperar los datos del taxista desde el preferences
    @Override
    public void returnUserDialog() {

        String name = TaxiDriver.loadNAME();
        String autoplate = TaxiDriver.loadAutoplate();
        String phone = TaxiDriver.loadPhone();
        String image = TaxiDriver.loadImage();
        String email =TaxiDriver.loadEmail();
        mapView.navigateToUserDialog(email, name, autoplate, phone, image);
    }

    // Metodo que se ejecuta cuando se falla
    // el pedido
    @Override
    public void onFailCall() {
        // Se oculta el progress
        mapView.hideProgress();
        // Se habilita el boton para llamar al taxista
        mapView.enabledCallTaxi();
        // Se guarda el estado del isable en false
        User.saveIsable(true);
        // Se muestra el mensaje que indica que se fallado
        // al hacer la peticion
        mapView.showDialogFailCall();
    }

    // Metodo que verifica si el usuario tiene GPS
    // o no
    public boolean getGPS() {
        return prefs.getBoolean(Config.GPS, true);
    }

    // Metodo que verifica la version
    // y lo envia al servidor para comparar si es lo mismo
    @Override
    public void verifyVersion() {
        PackageInfo pInfo = null;
        try {
            pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("PackageManager", e.toString());
        }
        int code = 0;
        try {
            code = pInfo.versionCode;
        } catch (NullPointerException e) {
        }

        JSONObject params = new JSONObject();
        try {
            params.put("version", "" + code);
            params.put("type", "user");
        } catch (JSONException e) {
            Log.e("VV_JSONException", e.toString());
        }

        mapInteractor.sendVersion(params, validator, this);

    }

    @Override
    public Context getActivityContext() {
        return mapView.getContext();
    }

    // Metodo que se ejecuta cuando el taxista esta cerca
    @Override
    public void onArrive(String message) {
        // Muestra el dialogo indicando que el taxista esta cerca
        mapView.showDialogNearTaxi(message);
        // Realiza el sonido
        mapView.blareSound();
    }

    @Override
    public void onConfirmarUbicacionCompartida(String origen) {
        // Muestra el dialogo indicando que el taxista esta cerca
        mapView.showDialogUbicacionCompartida(origen);
        // Realiza el sonido

    }

    // Metodo que se ejecuta cuando el taxista cambio de posicion
    @Override
    public void updateLocationTaxi(double latitude, double longitude, double bearing) {

        try {
            // Se verifica que halla por lo menos un marcador
            // mediante el preferences
            if (prefs.getBoolean("Initial", false))
                mapView.removeTaxiMarker();
        } catch (NullPointerException e) {
            Log.e("MarkerException", e.toString());
        }
        // Metodo que añade el marcador del taxista
        // en el mapa
        mapView.addLocationTaxiMarker(latitude, longitude,bearing);
        // Ajusta el zoom para enfocarlo al taxista
        mapView.adjustZoomMap(latitude, longitude, 15);

        // Se obtiene la actual posicion del usuario
        Location myCurrentLocation = getMyCurrentLocation();
        // Se calcula la distancia entre la localizacion del taxista y la del usuario
        Double distance = Haversine.calculateDistance(latitude, longitude, myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
        // Se coloca la distancia en la vista
        mapView.setDistance(String.valueOf(distance).substring(0, 5) + " KM");
        // Se calcula el tiempo
        Double time = (distance / 15) * 60;
        // Se coloca el tiempo
        mapView.setTime(String.valueOf(time).substring(1, 2) == "." ? String.valueOf(time).substring(0, 1) : String.valueOf(time).substring(0, 2) + " minuto(s)");
        // Se coloca en el preferences que se puso un marcador
        editor.putBoolean("Initial", true);
        editor.commit();
    }

    // Metodo que se ejecuta para cambiar el estado a conectado
    // en la interfaz
    @Override
    public void showStateConnected() {
        mapView.showConnected();
    }

    // Metodo que se ejecuta para cambiar el estado a desconectado
    // en la interfaz
    @Override
    public void showStateDisconnected() {
        mapView.showDisconnected();
    }

    // Metodo que se ejecuta para cambiar el estado reconectando
    // en la interfaz
    @Override
    public void showStateReconnecting() {
        mapView.showReconnecting();
    }

    // Metodo que obtiene la direccion
    @Override
    public void getAddressDirection() {
        // Se verifica que la posicion no sea nula o 0
        if (getMyCurrentLocation() != null || getMyCurrentLocation().getLongitude() != 0 || getMyCurrentLocation().getLatitude() != 0)
            mapInteractor.getAddress(getMyCurrentLocation(), validator);
    }

    // Metodo que coloca la direccion en la interfaz
    public void setAddressDirection(String address) {
        Log.i("ADDRESS_MAP", address);
        mapView.setAddressLocation(address);
    }
    @Override
    public void setAddressNro(String nro) {
        Log.i("ADDRESS_MAP", nro);
        mapView.setNumber(nro);
    }
    // Metodo que desactiva la instancia del socket
    @Override
    public void desactiveSocket() {
        mapInteractor.desactiveSocketInstance();
    }

    // Metodo que envia el signin
    public void sendSignin()
    {
        mapInteractor.signin();
    }

    @Override
    public void validateGetData() {

        String email = User.loadEmail();
        mapInteractor.getDataPromotion(email,validator);

    }
    @Override
    public void onSuccessGetPromotionData(String idpromotion, String totalcar, String situacion, String estado) {
        mapView.setId(idpromotion);
       /* promotionView.setTotalc(totalcar);
        promotionView.setSituacion(situacion);
        promotionView.setEstado(estado);*/
    }

    @Override
    public void onErrorConnection2() {
        mapView.showDialogConnection();
        mapView.hideProgress();
    }

    @Override
    public void onSuccessSend() {
        mapView.hideProgress();
    }


    @Override
    public void onFailedSend() {
        mapView.showDialogFailedSend();
        mapView.hideProgress();
    }
    @Override
    public void sendShareLocation(Double latitude, Double longitude) {

        //Se obtiene el email del pasajero
        String email = User.loadEmail();

        //Se coloca el email en un JSONObject
        JSONObject params = new JSONObject();
        try {
            params.put("email",email);
            params.put("latitude", latitude);
            params.put("longitude",longitude);
        }catch (JSONException e){
            Log.i("mierrormensaje",e.toString());}
        mapInteractor.sendShareLocation(params,validator);
    }
    @Override
    public void recibirShareLocation(String origen){
        String email = User.loadEmail();

        //Se coloca el email en un JSONObject
        JSONObject params = new JSONObject();
        try {
            params.put("email",email);
            params.put("sharenumber", origen);
        }catch (JSONException e){
            Log.i("mierrormensaje",e.toString());}

         mapInteractor.recibirShareLocation(params,validator);
    }

    @Override
    public void updateShareLocation(JSONObject jsonParams){
        mapView.updateShareLocation(jsonParams);
    }

    @Override
    public void retirarenviarubicacion(JSONObject jsonParams){
        mapView.retirarenviarubicacion(jsonParams);
    }
}
