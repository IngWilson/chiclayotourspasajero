package app.product.ditec.chiclayotours_cliente.UserDialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;


import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Sounds;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

/**
 * Created by angel on 15/04/15.
 */
public class UserDialogActivity extends AppCompatActivity implements UserDialogView {

    TextView txtName;

    TextView txtPhone;

    TextView txtEmaildriver;

    TextView txtAutoPlate;

    ImageView imgTaxiDriver;
    ProgressBar loadingPanel;

    UserDialogPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dialog);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        //Metodo que coloca un layout predefinidos en el actionbar
        actionBar.setCustomView(R.layout.layout_logo_user_dialog);

        txtName=findViewById(R.id.txtNameTaxiDriverDialog);
        txtPhone=findViewById(R.id.txtPhoneTaxiDriverDialog);
        txtEmaildriver=findViewById(R.id.txtEmailTaxiDriverDialog);
        txtAutoPlate=findViewById(R.id.txtAutoPlateTaxiDriverDialog);
        imgTaxiDriver=findViewById(R.id.imgTaxiDriver);
        loadingPanel=findViewById(R.id.loadingPanel);

        presenter = new UserDialogPresenterImpl(this);
        presenter.setUserData();

        //Metodo que hace sonar un silvido cuando se inicia la actividad.
        Uri sonido = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.record);
        Sounds sounds = new Sounds();
        sounds.sendNotification("Taxi encontrado",sonido,this);
    }

    //Metodo que pone el nombre del taxista.
    @Override
    public void setUserNameTaxiDriver(String name) {
        txtName.setText(name);
    }

    //Metodo que pone la placa del taxista.
    @Override
    public void setAutoPlateTaxiDriver(String autoplate) {
        txtAutoPlate.setText(autoplate);
    }

    //Metodo que pone el número telefonico del taxista.
    @Override
    public void setPhoneTaxiDriver(String phone) {
        txtPhone.setText(phone);
    }


    @Override
    public void setEmailTaxiDriver(String email) {
        txtEmaildriver.setText(email);
    }

    //Boton que termina la actividad actual.
    public void onAccept(View view) {
        finish();
    }

     //Metodo que pone la imagen del taxista
    @Override
    public void setImageTaxiDriver(Bitmap imageTaxiDriver) {
        imgTaxiDriver.setImageBitmap(imageTaxiDriver);
    }
    //Metodo  que optiene la imagen del taxista.
    @Override
    public ImageView getImageTaxiDriver() {
        return imgTaxiDriver;
    }

    //Metodo que optiene  el contexto.
    public Context getContext() {
        return UserDialogActivity.this;
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);

        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }
    //Metodo que optiene un dialogo de progreso
    @Override
    public ProgressBar getProgressBarImage() {
        return loadingPanel;
    }
}
