package app.product.ditec.chiclayotours_cliente.PreRequest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 08/06/15.
 */
public class PreRequestPresenterImpl implements PreRequestPresenter,OnPreRequestListener {

    PreRequestView preRequestView;
    Context ctx;
    Bundle bundle;
    PreRequestInteractor interactor;
    Validation validation;

    public PreRequestPresenterImpl(PreRequestView _preRequestView) {
        preRequestView = _preRequestView;
        ctx = preRequestView.getContext();
        interactor = new PreRequestInteractorImpl();
        validation = new Validation(preRequestView.getContext());
    }

    @Override
    public void validatePreRequest(String note, String origin, String destination) {

        bundle = ((Activity) ctx).getIntent().getExtras();
        String latitude = bundle.getString("latitude");
        String longitude = bundle.getString("longitude");
        String gcm = bundle.getString("gcm");

        JSONObject params = new JSONObject();
        try {
            params.put("latitude", latitude);
            params.put("longitude", longitude);
            params.put("gcm", gcm);
            params.put("origin", origin);
            params.put("destination", destination);
        } catch (JSONException e) {
            Log.e("VPR_JSONException", e.toString());
        }

    }

    @Override
    public void validatePrice(float distance) {

        JSONObject params = new JSONObject();
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date dateobj = new Date();

        try {
            //lo volvere dinamico considerando como menos de 1km
            params.put("distance",0.9);
            params.put("distanceR",distance);
            params.put("hours",df.format(dateobj));
            params.put("email", User.loadEmail());

            interactor.calculatePrice(params,validation,this);

        }catch (JSONException e){}

    }

    @Override
    public void setPrice(String price) {
        preRequestView.setPrice(price);
    }
}
