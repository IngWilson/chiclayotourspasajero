package app.product.ditec.chiclayotours_cliente;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
//import butterknife.ButterKnife;
//import butterknife.InjectView;

public class DLinkConductorActivity extends AppCompatActivity {
    Validation validator = new Validation(this);

    TextView txtcodunidad;
    TextView txtnombre;
    TextView txtcelular;
    TextView txtmarca;
    TextView txtplaca;
    TextView txtdesinfeccion;
    ImageView imgTaxiDriver;
    Button btnCallTaxiDriver;
    ProgressBar miprogress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d_link_conductor);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_bar);

        txtcodunidad=findViewById(R.id.txtcodunidad);
        txtnombre=findViewById(R.id.txtnombre);
        txtcelular=findViewById(R.id.txtcelular);
        txtmarca=findViewById(R.id.txtmarca);
        txtplaca=findViewById(R.id.txtplaca);
        txtdesinfeccion=findViewById(R.id.txtdesinfeccion);
        imgTaxiDriver=findViewById(R.id.imgTaxiDriver);
        btnCallTaxiDriver=findViewById(R.id.btnCallTaxiDriver);
        miprogress=findViewById(R.id.miprogress);

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            Log.i("wilveamos", deepLink.toString());
                            getdataconductor(deepLink.toString());
                        }

                        // Handle the deep link. For example, open the linked
                        // content, or apply promotional credit to the user's
                        // account.
                        // ...

                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("tagmierror", "getDynamicLink:onFailure", e);
                    }
                });
    }


    OkHttpRequest okHttpRequest;

    //Metodo que recupera los datos del pasajero

    public void getdataconductor(String url) {

        //Se valida si esta conectado a internet
        if (validator.isOnline()) {

            okHttpRequest = new OkHttpRequest(url);

            //Se realiza una peticion GET con un callback
            try {
                okHttpRequest.getNotParams(new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {
                        Log.i("AccountFailure", "Failure when get user data");
                        Log.i("AccountFailure", e.toString());
                        Log.i("AccountFailure", request.toString());
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {

                        String res = response.body().string();
                        Log.i("wilveamos2", res);
                        try {
                            final JSONObject params = new JSONObject(res);
                            final String email= params.getString("email");
                            final String first_name= params.getString("first_name");
                            final String last_name= params.getString("last_name");
                            final String phone= params.getString("phone");
                            final String car_model= params.getString("car_model");
                            final String autoplate= params.getString("autoplate");
                            //Necesario para taxiplus habilitar//final String controldes= params.getString("controldes");
                            String urlimg = params.getString("photo_profile");
                            //final String urlimg1 = urlimg.replace(" ","%20");
                            final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
                            final String urlimg1 = Uri.encode(urlimg, ALLOWED_URI_CHARS);
                            Log.i("verur", urlimg1);
                            Log.i("vej", urlimg);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    txtcodunidad.setText(email);
                                    txtnombre.setText(first_name + " " + last_name);
                                    txtcelular.setText(phone);
                                    txtmarca.setText(car_model);
                                    txtplaca.setText(autoplate);
                                    //Necesario para taxiplus habilitar//
                                    //txtdesinfeccion.setText("Desinfeccion: " + controldes);
                                    Picasso.with(DLinkConductorActivity.this).load(urlimg1)
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .error(R.mipmap.ic_launcher)
                                            .into(imgTaxiDriver, new com.squareup.picasso.Callback() {
                                                @Override
                                                public void onSuccess() {
                                                    Toast.makeText(DLinkConductorActivity.this, "Carga exitosa", Toast.LENGTH_SHORT).show();
                                                    miprogress.setVisibility(View.GONE);
                                                    imgTaxiDriver.setVisibility(View.VISIBLE);
                                                }

                                                @Override
                                                public void onError() {
                                                    Toast.makeText(DLinkConductorActivity.this, "Ocurrio un error en la descarga", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            });


                        } catch (JSONException e) {
                            Log.e("JSONArrayException", e.toString());
                        }

                    }

                });
            } catch (IOException e) {
                Log.e("IOException", e.toString());
            }

        } else
            Toast.makeText(this, "No tienes conexion a internet", Toast.LENGTH_LONG).show();

    }

    public void llamartaxi(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL,
                Uri.parse("tel:"+txtcelular.getText().toString()));
        startActivity(intent);
    }
}