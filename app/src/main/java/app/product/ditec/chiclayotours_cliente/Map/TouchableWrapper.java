package app.product.ditec.chiclayotours_cliente.Map;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * Created by angel on 14/08/15.
 */
// Wrapper que me permite detectar
// si la pantalla donde esta el mapa en la actividad
// principal esta siendo tocado o no
public class TouchableWrapper extends FrameLayout {

    public TouchableWrapper(Context context){
        super(context);
    }

    //Metodo para el evento Touch
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Se verifica el tipo de accion del evento
        switch (ev.getAction()) {
            // Se verifica el tipo de accion
            // en este caso es cuando esta siendo presionado
            // el mapa
            case MotionEvent.ACTION_DOWN:
                Log.i("TOUCH_EVENT",""+true);
                MapActivity.mMapIsTouched = true;
                break;
            // Se verifica el tipo de accion
            // en este caso es cuando no esta siendo presionado
            // el mapa
            case MotionEvent.ACTION_UP:
                Log.i("TOUCH_EVENT",""+false);
                MapActivity.mMapIsTouched = false;
                break;
        }
        return super.dispatchTouchEvent(ev);
    }
}
