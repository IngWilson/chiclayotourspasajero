package app.product.ditec.chiclayotours_cliente.PreRequest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.ActionBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import app.product.ditec.chiclayotours_cliente.Editspinner.EditSpinner;

import app.product.ditec.chiclayotours_cliente.Haversine;
import app.product.ditec.chiclayotours_cliente.Map.MapActivity;

import app.product.ditec.chiclayotours_cliente.Peticion.PeticionActivity;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * Created by angel on 08/06/15.
 */

public class PreRequestActivity extends AppCompatActivity implements PreRequestView {



    PreRequestPresenter presenter;
    EditText edtNote, edtFrom, edtTo,edtDistance,edtPrice,edtNumber, edtExactPrice;
    Button btnShowMap;
    Button btnSendRequest;
    Button btnSendPeticion;
    int CODE_RESPONSE = 1;

    String note,address,number;
    LatLng locationFrom,locationTo;
    private final static String[] tipomovilidad = { "Cualquiera", "Station Wagon" };
    final String[] stringArray2 = { "", "","", "" ,""};
    Spinner tipomovil;
String itemtipomovil;
    EditSpinner mEditSpinner2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prerequest);
        presenter = new PreRequestPresenterImpl(this);

        double latitude =  (double)getIntent().getExtras().get("latitude");
        double longitude = (double) getIntent().getExtras().get("longitude");


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_direccion);

        locationFrom = new LatLng(latitude,longitude);
        address = getIntent().getExtras().getString("address");
        note = getIntent().getExtras().getString("note");
        number = getIntent().getExtras().getString("number");

        edtNote = (EditText) findViewById(R.id.edtNote);
        edtFrom = (EditText) findViewById(R.id.edtFrom);
        edtDistance = (EditText) findViewById(R.id.edtDistance);
        edtTo   = (EditText) findViewById(R.id.edtTo);
        edtPrice = (EditText) findViewById(R.id.edtPrice);
        edtNumber = (EditText) findViewById(R.id.edtNumber);
        edtExactPrice=(EditText) findViewById(R.id.edtExactprice);
        tipomovil =(Spinner) findViewById(R.id.tipomovil1);

        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, tipomovilidad);
        tipomovil.setAdapter(adapter);


        tipomovil
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> arg0,
                                               View arg1, int position, long arg3) {
                        // TODO Auto-generated method stub
                        itemtipomovil= (String) tipomovil.getSelectedItem();
                        Log.i("vermirartipo",itemtipomovil);
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }
                });

        btnShowMap = (Button) findViewById(R.id.btnShowMap);
        btnShowMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMap();
            }
        });

        btnSendRequest = (Button) findViewById(R.id.btnSendRequest);
        btnSendPeticion=findViewById(R.id.btnSendRequestPeticion);
        btnSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if(!edtExactPrice.getText().toString().equals("") && parseIntWithDefault(edtExactPrice.getText().toString(),0) < parseIntWithDefault(edtPrice.getText().toString(),0))
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(PreRequestActivity.this);
                    alertDialog.setTitle(getString(R.string.dialog_title))
                            .setCancelable(false)
                            .setMessage("El precio propuesto debe ser mayor al aproximado")
//                .setMessage(getString(R.string.dialog_end_service))
                            .setPositiveButton(getString(R.string.dialog_accept), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();

                }
                else
                {*/

                    if(!mEditSpinner2.getText().toString().equals("")) {
                        finish();
                        itemtipomovil = (String) tipomovil.getSelectedItem();
                        MapActivity.callTaxi(edtNote.getText().toString(), mEditSpinner2.getText().toString(), edtNumber.getText().toString(), edtTo.getText().toString(), edtPrice.getText().toString(), edtExactPrice.getText().toString(), itemtipomovil,itemtipomovil);
                        guardarDireccion(mEditSpinner2.getText().toString(),edtNumber.getText().toString());
                    }
                    else
                        Toast.makeText(PreRequestActivity.this, "Por favor ingrese una direccion", Toast.LENGTH_SHORT).show();
                }
           // }
        });

        btnSendPeticion.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  finish();
                                                  startActivity(new Intent(getApplicationContext(),PeticionActivity.class) );
                                              }
                                          });

        edtNote.setText(note);
        initViews();
        if(address.equals("Buscando direccion...")) {
            edtFrom.setText("");
            mEditSpinner2.setText("");
        }
        else {
            edtFrom.setText(address);
            mEditSpinner2.setText(address);
        }
        int check=Buscardireccion(address);
        if(check==-1)
        {
            edtNumber.setText(number);
        }
        else
        {
            edtNumber.setText(stringArray2[check].substring(stringArray2[check].indexOf(",")+1));
        }


    }


    public int Buscardireccion(String s) {
        int ubi=-1;

        for(int i=0;i<=4;i++)
        {
            if(stringArray2[i].length()!=0)
            if(stringArray2[i].substring(0,stringArray2[i].indexOf(",")).equals(s))
            {
                ubi=i;
            }

        }
        return ubi;
    }

    public void guardarDireccion(String direccion, String number)
    {
        SharedPreferences preferencias = getContext().getSharedPreferences("direcciones", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        int check=Buscardireccion(direccion);
        if(check==-1) {


            for (int n = 4; n >= 1; n--) {
                Log.i("mydirec", "direcccion" + n);
                editor.putString("direccion" + n, preferencias.getString("direccion" + (n - 1), ""));
            }

            editor.putString("direccion0", direccion + "," + number);
            editor.commit();
        }
        else
        {
            editor.putString("direccion"+check, direccion + "," + number);
            editor.commit();
        }


    }

    private void initViews() {


        SharedPreferences preferencias=getContext().getSharedPreferences("direcciones",Context.MODE_PRIVATE);
        for(int n=0;n<=4;n++)
        {
            String s=preferencias.getString("direccion"+n,"");

            stringArray2[n]=s;
        }
        //final String[] stringArray2 = getResources().getStringArray(R.array.edits_array_2);
        mEditSpinner2 = (EditSpinner) findViewById(R.id.edit_spinner);
        mEditSpinner2.setDropDownDrawable(getResources().getDrawable(R.drawable.spinner), 25, 25);
        mEditSpinner2.setDropDownDrawableSpacing(50);
        mEditSpinner2.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, stringArray2));

        // it converts the item in the list to a string shown in EditText.
        mEditSpinner2.setItemConverter(new EditSpinner.ItemConverter() {
            @Override
            public String convertItemToString(Object selectedItem) {
                if (selectedItem.toString().equals(stringArray2[stringArray2.length - 1])) {
                    return "";
                } else {
                   // Log.i("direc",selectedItem.toString());
                    return selectedItem.toString();
                }
            }
        });

        // triggered when one item in the list is clicked
        mEditSpinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Log.d(TAG, "onItemClick() position = " + position);
                Log.i("mydirec",stringArray2[position]);
                if(stringArray2[position].length()!=0) {
                    mEditSpinner2.setText(stringArray2[position].substring(0, stringArray2[position].indexOf(",")));
                    edtNumber.setText(stringArray2[position].substring(stringArray2[position].indexOf(",") + 1));
                }
                if (position == stringArray2.length - 1) {
                    showSoftInputPanel(mEditSpinner2);

                }
            }
        });

        mEditSpinner2.setOnShowListener(new EditSpinner.OnShowListener() {
            @Override
            public void onShow() {
                hideSoftInputPanel();
            }
        });

        // select the first item initially
        mEditSpinner2.selectItem(0);
    }


    private void hideSoftInputPanel() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            //imm.hideSoftInputFromWindow(mEditSpinner1.getWindowToken(), 0);
        }
    }

    private void showSoftInputPanel(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, 0);
        }
    }
    public static int parseIntWithDefault(String s, int d) {
        try {
            return Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return d;
        }
    }
    @Override
    public void setNoteError() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setErrorAddressEnd() {

    }

    @Override
    public void setErrorAddressStart() {

    }

//    @OnClick(R.id.btnContinue)
//    public void onContinue() {
//        presenter.validatePreRequest(edtNote.getText().toString(), edtFrom.getText().toString(), edtTo.getText().toString());
//    }

    public void showMap(){
       /* Intent i = new Intent(this,MapChooserActivity.class);
        startActivityForResult(i,CODE_RESPONSE);*/

    }

    @Override
    public Context getContext() {
        return PreRequestActivity.this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==1){
            if(resultCode==RESULT_OK){

                locationTo = new LatLng(data.getExtras().getDouble("latitude"),
                        data.getExtras().getDouble("longitude"));

                edtTo.setText(data.getExtras().getString("address"));
                double distance =  Haversine.calculateDistance(locationTo.latitude,locationTo.longitude,locationFrom.latitude,locationFrom.longitude);
                distance = round(distance,2);
                edtDistance.setText(String.valueOf(distance)+ " KM");
                presenter.validatePrice((float)distance);
                edtExactPrice.setEnabled(true);
            }
        }

    }


    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    @Override
    public void setPrice(String price){
        edtPrice.setText(price);
    }

    @Override
    public void showMapChooser() {
       /* Intent map = new Intent(this, MapChooserActivity.class);
        startActivity(map);*/
    }
}
