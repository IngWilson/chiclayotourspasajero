package app.product.ditec.chiclayotours_cliente.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;


/**
 * Created by angel on 15/04/15.
 */

// Interface que define los metodos
// para manejar la interfaz del mapa
public interface MapView {

    void showProgress();

    void hideProgress();

    void showUserDialog();

    void showRequestErrorDialog();

    void showEndServiceDialog();

    void showCancelErrorDialog();

    void addLocationTaxiMarker(Double latitude, Double longitude, Double bearing);

    void removeTaxiMarker();

    void clean();

    void blareSound();

    Context getContext();

    void enabledCallTaxi();

    void enabledoptions();

    void disabledoptions();

   // void disabledCallTaxi();

    void enabledCancelTaxi();

    void disabledCancelTaxi();

    void enabledCallTaxiDriver();

    void disabledCallTaxiDriver();

    ImageView getImgTaxiDriver1();

    ImageView getImgTaxiDriver2();

    ImageView getImgMarker();

    LinearLayout getLayoutParameters();

    void navigateToRadarDialog();
    void navigateToPeticion();
    void reiniciaractividad();

    void navigateToUserDialog(String email, String name, String phone, String autoplate, String image);

    void setEmailTaxiDriver(String email);

    void setNameTaxiDriver(String name);

    void setTitle(String title);

    void setPhoneTaxiDriver(String phone);

    void setAutoPlateTaxiDriver(String autoplate);

    void setModelAndBrandCar(String modelandBrandCar);

    void setCompany(String company);

    void showDialogError();

    void showDialogConnectionInternetError();

    void showDialogGPSError();

    void showDialogConnectionError();

    void adjustZoomMap(Double latitude, Double longitude, int zoom);

    void showDialogLocationError();

    void clearMap();

    void showDialogEndService();

    void addMyPositionMarker(Double latitude, Double longitude);

    LatLng getLocationMarker();

    void setImgTaxiDriver1(Bitmap bitmap);

    void setImgTaxiDriver2(Bitmap bitmap);

    void showDialogCancelRequest();

    void showSuccessfullyCancel();

    void showDialogFailCall();

    void showDialogNearTaxi(String message);

    void enabledNote();

    void disabledNote();

    void navigateToLogin();

    void showDialogDisableAccount();

    void showDialogInstallVersion();

    void showDialogErrorPreCall();

    void showDialogErrorPreCancel();

    void setDistance(String distance);

    void setTime(String time);

    void setNumber(String nro);

    TextView getTxtTime();

    TextView getTxtDistance();

    boolean onCheckGooglePlayServices();

    void showConnected();

    void showReconnecting();

    void showDisconnected();

    void setAddressLocation(String address);

    void showDialogAddressNotFound();

    void changeEventCancelRequest();

    void changeEventCancelRequestForDefault();

    void changeTextBtnCancel(String text);

    void showDialogFinishTravel();

    void showSuccessfullyFinish();

    void setId(String promotion);

    void showDialogConnection();

    void showDialogFailedSend();

    void showDialogUbicacionCompartida(String message);

    void sendShareLocation(Double latitude, Double longitude);

    void updateShareLocation(JSONObject jsonparams);

    void retirarenviarubicacion(JSONObject jsonparams);

    void setPrecio(String precio);
}
