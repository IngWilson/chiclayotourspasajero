package app.product.ditec.chiclayotours_cliente.Star;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;

import androidx.appcompat.app.ActionBar;

//import android.support.v7.widget.Toolbar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import app.product.ditec.chiclayotours_cliente.R;
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;

public class starActivity extends AppCompatActivity implements StarView, Dialog.OnClickListener {
    private static RatingBar ratingBar;
    private static TextView resultado;
    private static Drawable progress;
    private static String calificacion;

    TextView edtComment;
    public String valor;

    StarPresenter presenter;

    ProgressDialog progressDialog1;

    AlertDialog.Builder alertDialog;
    AlertDialog.Builder dialogConfirmCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.layout_logo_bar);

        edtComment=findViewById(R.id.edtComment);
        cantidada();
        presenter = new StarPresenterImpl(this);
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        dialogConfirmCode = new AlertDialog.Builder(this);
        progressDialog1=new ProgressDialog(this);
        valor="5";
    }



    public void cantidada() {
        ratingBar = (RatingBar) findViewById(R.id.ratingbar);
        resultado = (TextView) findViewById(R.id.calificacion);
        progress = ratingBar.getProgressDrawable();
        //DrawableCompat.setTint(progress, Color.YELLOW);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                valor = Float.toString(v);
                Log.i("valor1 : ", valor);


                if (v == 5) {
                    calificacion = "EXCELENTE";
                    resultado.setText(calificacion);
                    //DrawableCompat.setTint(progress, (getResources().getColor(R.color.celestepasajero)));

                }
                if (v == 4) {
                    calificacion = "BUENO";
                    resultado.setText(calificacion);
                    //DrawableCompat.setTint(progress, Color.YELLOW);
                }
                if (v == 3) {
                    calificacion = "REGULAR";
                    resultado.setText(calificacion);
                    //DrawableCompat.setTint(progress, Color.YELLOW);
                }
                if (v == 2) {
                    calificacion = "MALO";
                    resultado.setText(calificacion);
                    //DrawableCompat.setTint(progress, Color.RED);
                }
                if (v == 1) {
                    calificacion = "PÉSIMO";
                    resultado.setText(calificacion);
                    //DrawableCompat.setTint(progress, Color.RED);
                }
                if (v == 0) {
                    calificacion = "Guardo mi opinión";
                    resultado.setText(calificacion);
                    //DrawableCompat.setTint(progress, Color.YELLOW);

                }
                //resultado.setText(String.valueOf(v));
            }
        });

    }

    //metodo de advertencia para ver si el numero de telefono ingresado es el correcto

    public void showDialogConfirmCode(View view) {

                            presenter.inputrating(edtComment.getText().toString(), valor.toString());

        }

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    @Override
    public void finished() {
        prefs = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        editor = prefs.edit();
        editor.putBoolean("staractivity", true);
        editor.commit();
        Log.i("problem","finalizando star");
        finish();

    }
    @Override
    public void onBackPressed() {
        finished();
    }

    @Override
    public void hideProgressE() {
        progressDialog1.hide();
    }


    @Override
    public void showProgressE() {
        progressDialog1.setIndeterminate(false);
        progressDialog1.setMessage(getString(R.string.StarActivity_showProgress));
        progressDialog1.setCancelable(false);
        progressDialog1.show();
    }

    @Override
    public Context getContext() {
        return starActivity.this;
    }

    @Override
    public void showDialogError() {
        Toast.makeText(this, getString(R.string.StarActivity_Dialog_Error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialogSuccessSend() {
        Toast.makeText(this, getString(R.string.StarActivity_showDialogSuccessSend), Toast.LENGTH_LONG).show();

    }

    @Override
    public void showDialogFailedSend() {
        Toast.makeText(this, getString(R.string.StarActivity_showDialogFailedSend), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialogInternetError() {
        alertDialog.setTitle(getString(R.string.dialog_conection_title))
                .setMessage(getString(R.string.dialog_conection_message))
                .setPositiveButton(getString(R.string.settings_services), this)
                .setNegativeButton(getString(R.string.cancel_services), null)
                .show();
    }
    //Evento onClick para el boton "Configuracion" del dialogo para cuando no se tiene conexion a internet
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -2:
                return;
            case -1:
                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
            default:
                return;
        }
    }
}
