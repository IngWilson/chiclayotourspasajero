package app.product.ditec.chiclayotours_cliente.History.MonthTabs;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.History.HistoryActivity;
import app.product.ditec.chiclayotours_cliente.History.HistoryAdapter;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMay extends Fragment {

    View view;
    HistoryAdapter historyAdapter;
    ListView lvMay;
    public FragmentMay() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_may, container, false);
        lvMay = (ListView) view.findViewById(R.id.lvMay);

        historyAdapter = new HistoryAdapter(view.getContext());

        try {
            if (!HistoryActivity.getmRequest(5).equals(null)) {
                historyAdapter.setData(new ArrayList<>(HistoryActivity.getmRequest(5)));
                lvMay.setAdapter(historyAdapter);
            }
        }catch (NullPointerException e){}
        return view;
    }


}
