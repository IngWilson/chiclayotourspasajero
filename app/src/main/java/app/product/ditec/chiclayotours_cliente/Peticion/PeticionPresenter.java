package app.product.ditec.chiclayotours_cliente.Peticion;

import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Model.Oferta;

public interface PeticionPresenter {
    void cancelar();
    void aceptaroferta(String emaildriver, String emailuser, String ide, Oferta oferta);
    void rechazaroferta(String socketidtaxi);
    void aumentar(JSONObject request);
}
