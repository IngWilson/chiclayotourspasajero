package app.product.ditec.chiclayotours_cliente.Mensaje;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;
import app.product.ditec.chiclayotours_cliente.Validation;

public class MessagePresenterImpl implements MessagePresenter, OnMessageFinishedListener {
    private MessageInteractor interactor;

    private Context ctx;
    private MessageView messageView;
    private Validation validator;
    public MessagePresenterImpl(MessageView _messageView) {
        messageView = _messageView;
        ctx = _messageView.getContext();
        this.validator = new Validation(messageView.getContext());
        interactor=new MessageInteractorImpl(this);

    }
    public void validateSendArrive(String Message) {

        //Se muestra un dialogo de progreso
        messageView.showProgress();

        //Se obtiene el email del pasajero
        String email = TaxiDriver.loadEmail();


        //Se coloca el email en un JSONObject
        JSONObject params = new JSONObject();
        try {
            params.put("email",email);
            params.put("message", Message);
        }catch (JSONException e){
            Log.i("mierrormensaje",e.toString());}
        Log.i("mierrormensaje","se finalizo el presenter");
        //Se utiliza este metodo para enviar "ya llegue" al pasajero
        interactor.sendArrive(params,validator);
    }
    @Override
    public void onErrorConnection() {
        messageView.showDialogConnection();
        messageView.hideProgress();
    }

    @Override
    public void onSuccessSend() {
        messageView.hideProgress();
    }


    @Override
    public void onFailedSend() {
        messageView.showDialogFailedSend();
        messageView.hideProgress();
    }

}