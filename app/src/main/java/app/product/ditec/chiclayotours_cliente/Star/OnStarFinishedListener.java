package app.product.ditec.chiclayotours_cliente.Star;

/**
 * Created by usuario on 14/07/2016.
 */
public interface OnStarFinishedListener {
    void onError();
    void onSuccessSend();
    void onFailedSend();
    void onInternetError();
}
