package app.product.ditec.chiclayotours_cliente.RadarDialog;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.Socket;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import app.product.ditec.chiclayotours_cliente.App;
import app.product.ditec.chiclayotours_cliente.Model.TaxiDriver;
import app.product.ditec.chiclayotours_cliente.Model.User;
import app.product.ditec.chiclayotours_cliente.OkHttpRequest;
import app.product.ditec.chiclayotours_cliente.R;
import app.product.ditec.chiclayotours_cliente.Validation;

/**
 * Created by angel on 26/04/15.
 */
public class RadarDialogInteractorImpl implements RadarDialogInteractor {

    private JSONObject params = null;
    private OkHttpRequest okHttpRequest;
    private Socket socket;

    Runnable runnable;
    boolean flag_signin;
    Handler handler;
    private OnRadarFinishedListener listener2;

    public RadarDialogInteractorImpl(OnRadarFinishedListener _listener2){

        listener2=_listener2;
        handler = new Handler();
        //Se obtiene el socket desde la clase App
        socket = App.getSocket();
//        if(!socket.connected())
        //Se conecta nuevamente el socket

        socket.on("accept",accept);
        socket.connect();
    }
    private Emitter.Listener accept = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            try {

                // Se crea un objeto JSON
                JSONObject paramsrequest = (JSONObject)args[0];
                Log.i("mensaje",paramsrequest.toString());

                final TaxiDriver taxiDriver = new TaxiDriver();

                taxiDriver.setName(paramsrequest.getString("name"));
                taxiDriver.setPhone(paramsrequest.getString("phone"));
                taxiDriver.setAutoplate(paramsrequest.getString("autoplate"));
                taxiDriver.setDni(paramsrequest.getString("dni"));
                taxiDriver.setCar_brand(paramsrequest.getString("car_brand"));
                taxiDriver.setCar_model(paramsrequest.getString("car_model"));
                taxiDriver.setCompany(paramsrequest.getString("company"));
                taxiDriver.setEmail(paramsrequest.getString("email"));
                taxiDriver.setImageUrl(paramsrequest.getString("image"));
                taxiDriver.gcm_driver   = paramsrequest.getString("gcm_driver");

                new Handler(Looper.getMainLooper()).post(
                        new Runnable() {
                            @Override
                            public void run() {

                                listener2.onSuccessValidateRequest(taxiDriver);
                            }
                        }
                );
            }catch (JSONException e){
                Log.e("JSONExpcetionMessage",e.toString());
            }
        }
    };
    //Metodo que cancela el pedido
    @Override
    public void sendCancelRequest(final Validation validator, final JSONObject jsonParams, final OnRadarFinishedListener listener) {

        //Metodo que verifica si esta conectado a internet
        if (validator.isOnline()) {

            //Metodo que verifica que el socket esta conectado
            if (socket.connected()) {
                //Se emite el mensaje "precancel request" por socket enviando 
                // los parametros en el JSON
                socket.emit("precancel request", jsonParams, new Ack() {
                    @Override
                    public void call(Object... args) {
                        String res = (String) args[0];

                        //Se verifica el mensaje
                        if (res.equals("OK")) {
                            //Se obtiene el hilo principal y se ejecuta una tarea
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onSuccessCancel();
                                }
                            });
                            return;
                        }

                        if (res.equals("error")) {

                        }
                    }
                });
            }

/*
            okHttpRequest = new OkHttpRequest(Config.PRECANCEL);

            try {
                okHttpRequest.post(jsonParams.toString(), new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {

                            @Override
                            public void run() {
                                listener.onError();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {

                            @Override
                            public void run() {
                                listener.onSuccessCancel();
                            }
                        });

                    }

                });
            } catch (IOException e) {
                Log.e("IOExceptionRadar", e.toString());
            }*/
        } else
            listener.onConnectionError();

    }

    //Metodo que desactiva un socket
    @Override
    public void desactiveSocketInstance() {
        socket.disconnect();
    }

    //Metodo que que verifica si el request ha sido tomado
    @Override
    public void validateRequest(final JSONObject jsonObject, final OnRadarFinishedListener listener, Validation validator) {

        // Se verifica que esta conectado a internet
        if (validator.isOnline()) {

            okHttpRequest = new OkHttpRequest(App.getInstance().getString(R.string.My_URL)+App.getInstance().getString(R.string.URL_CHECKACCEPT));
            try {
                //Se realiza una peticion POST
                okHttpRequest.post(jsonObject.toString(), new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        Log.i("OnFailure", "Failure in request");
                        listener.onFailedValidateRequest();
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {

                        Log.i("VR_Code", "" + response.code());

                        // Se verifica el codigo de la respuesta
                        if (response.code() == 200) {

                            String res = response.body().string();

                            //Se convierte en bytes el JSON y se coloca un formato
                            byte ptext[] = res.getBytes("ISO-8859-1");
                            //Se asigna nuevamente
                            String value = new String(ptext, "UTF-8");

                            Log.i("VR_Values",value);

                            try {

                                //Se crea un objeto JSON
                                params = new JSONObject(value);

                                // Se crea un objeto TaxiDriver y se asigna
                                // los valores que se obtienen del JSON
                                TaxiDriver taxiDriver = new TaxiDriver();

                                taxiDriver.setName(params.getString("name"));
                                taxiDriver.setPhone(params.getString("phone"));
                                taxiDriver.setAutoplate(params.getString("autoplate"));
                                taxiDriver.setDni(params.getString("dni"));
                                taxiDriver.setCar_brand(params.getString("car_brand"));
                                taxiDriver.setCar_model(params.getString("car_model"));
                                taxiDriver.setCompany(params.getString("company"));
                                taxiDriver.setEmail(params.getString("email"));
                                taxiDriver.setImageUrl(params.getString("image"));
                                taxiDriver.gcm_driver   = params.getString("gcm_driver");

                                //Metodo que se ejecuta despues de obtener los valores
                                listener.onSuccessValidateRequest(taxiDriver);

                            } catch (JSONException e) {
                                Log.i("VR_JSONException", e.toString());
                            }
                        } else
                            listener.onFailedValidateRequest();
                    }
                });
            } catch (IOException e) {
                Log.e("IOException", e.toString());
                listener.onFailedValidateRequest();
            }
        }


    }

    //Metodo que envia el email para que el servidor
    // obtenga el SOCKET_ID y lo asigna a la cuenta del email
    @Override
    public void sendSignin(){

        //Se crea un nuevo hilo
        runnable = new Runnable() {
            @Override
            public void run() {

                //Se verifica que este conectado el socket
                if(socket.connected()) {

                    Log.i("Email_driver", User.loadEmail());
                    //Se emite el mensaje "signin" enviando
                    // como parametros el correo del pasajero
                    socket.emit("signin", User.loadEmail(), new Ack() {
                        @Override
                        public void call(Object... args) {
                            //Se obtiene el parametro devuelto por el servidor
                            final String res = (String) args[0];
                            final String socket_id = (String) args[1];
                            //Se verifica que este sea "OK" indicando que se ejecuto correctamente
                            if (res.equals("OK")) {
                                //Se pone en verdadero indicando que se envio correctamente
                                flag_signin = true;

                                new Handler(Looper.getMainLooper()).post(
                                        new Runnable() {
                                            @Override
                                            public void run() {
//                                                Toast.makeText(App.getInstance(),
//                                                        "SOCKET_ID : " + socket_id, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                );
                            }
                            else
                                flag_signin = false;
                        }
                    });
                }

                //En caso de que no se halla enviado correctamente
                // se reenvia de nuevo con un retraso de 1 segundo
                if(!flag_signin) {
                    Log.i("Signin","Reenviando signin..");
                    handler.postDelayed(this, 1000);
                }
                else {
                    //Si es que si se envio se remueve la tarea
                    Log.i("Signin","Enviado correctamente");
                    flag_signin = false;
                    handler.removeCallbacks(this);
                }
            }
        };
        //Se ejecuta el hilo
        runnable.run();
    }
}
