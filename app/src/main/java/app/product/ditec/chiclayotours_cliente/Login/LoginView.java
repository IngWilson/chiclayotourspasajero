package app.product.ditec.chiclayotours_cliente.Login;

import android.content.Context;


public interface LoginView {

    void arranque();

    void showProgress();

    void hideProgress();

    void showDialogErrorConnection();

    void showDialogErrorLogin();

    void showDialogErrorUserPass();

    void setEmailError();

    void setPasswordError();

    void navigateToMap();

    void navigateToRegister();

    Context getContext();

    void finishLogin();

    void navigateToVerifyPhone();

    void navigateToCode();

    void navigateToRecoverPassword();

    void setPhoneError();

}
