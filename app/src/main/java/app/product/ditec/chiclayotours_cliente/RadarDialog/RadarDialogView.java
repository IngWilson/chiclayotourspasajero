package app.product.ditec.chiclayotours_cliente.RadarDialog;

import android.content.Context;

/**
 * Created by angel on 20/04/15.
 */
public interface RadarDialogView {

    Context getContext();

    void showDialogConnection();

    void enabledCancel();

    void disabledCancel();

    void finishRadarDialog();

    void finishRadarDialogData();

    void showDialogError();

    void showDialogTaxiAround();

}
