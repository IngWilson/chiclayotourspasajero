package app.product.ditec.chiclayotours_cliente.History.MonthTabs;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import app.product.ditec.chiclayotours_cliente.History.HistoryActivity;
import app.product.ditec.chiclayotours_cliente.History.HistoryAdapter;
import app.product.ditec.chiclayotours_cliente.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOctober extends Fragment {

    View view;
    ListView lvOctober;
    HistoryAdapter historyAdapter;

    public FragmentOctober() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_october, container, false);
        lvOctober = (ListView) view.findViewById(R.id.lvOctober);
        historyAdapter = new HistoryAdapter(view.getContext());

        try {
            if (!HistoryActivity.getmRequest(10).equals(null)) {
                historyAdapter.setData(new ArrayList<>(HistoryActivity.getmRequest(10)));
                lvOctober.setAdapter(historyAdapter);
            }
        }catch (NullPointerException e){}

        return view;
    }


}
