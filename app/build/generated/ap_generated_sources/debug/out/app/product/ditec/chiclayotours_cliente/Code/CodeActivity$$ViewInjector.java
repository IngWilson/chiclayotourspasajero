// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.Code;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class CodeActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.Code.CodeActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296389, "field 'edtCode'");
    target.edtCode = finder.castView(view, 2131296389, "field 'edtCode'");
    view = finder.findRequiredView(source, 2131296318, "field 'btncode' and method 'Acceptcode'");
    target.btncode = finder.castView(view, 2131296318, "field 'btncode'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.Acceptcode();
        }
      });
    view = finder.findRequiredView(source, 2131296338, "field 'btnresend'");
    target.btnresend = finder.castView(view, 2131296338, "field 'btnresend'");
    view = finder.findRequiredView(source, 2131296320, "field 'btnFirebaseCheck'");
    target.btnFirebaseCheck = finder.castView(view, 2131296320, "field 'btnFirebaseCheck'");
    view = finder.findRequiredView(source, 2131296646, "field 'tvmessagefirebase'");
    target.tvmessagefirebase = finder.castView(view, 2131296646, "field 'tvmessagefirebase'");
    view = finder.findRequiredView(source, 2131296401, "field 'edtFirebasecheck'");
    target.edtFirebasecheck = finder.castView(view, 2131296401, "field 'edtFirebasecheck'");
    view = finder.findRequiredView(source, 2131296549, "field 'progressBar'");
    target.progressBar = finder.castView(view, 2131296549, "field 'progressBar'");
  }

  @Override public void reset(T target) {
    target.edtCode = null;
    target.btncode = null;
    target.btnresend = null;
    target.btnFirebaseCheck = null;
    target.tvmessagefirebase = null;
    target.edtFirebasecheck = null;
    target.progressBar = null;
  }
}
