// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.Register;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class RegisterActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.Register.RegisterActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296404, "field 'edtName'");
    target.edtName = finder.castView(view, 2131296404, "field 'edtName'");
    view = finder.findRequiredView(source, 2131296403, "field 'edtLastname'");
    target.edtLastname = finder.castView(view, 2131296403, "field 'edtLastname'");
    view = finder.findRequiredView(source, 2131296411, "field 'edtPhone'");
    target.edtPhone = finder.castView(view, 2131296411, "field 'edtPhone'");
    view = finder.findRequiredView(source, 2131296398, "field 'edtEmail'");
    target.edtEmail = finder.castView(view, 2131296398, "field 'edtEmail'");
    view = finder.findRequiredView(source, 2131296415, "field 'edtCode'");
    target.edtCode = finder.castView(view, 2131296415, "field 'edtCode'");
    view = finder.findRequiredView(source, 2131296409, "field 'edtPassword'");
    target.edtPassword = finder.castView(view, 2131296409, "field 'edtPassword'");
    view = finder.findRequiredView(source, 2131296328, "field 'btnRegister' and method 'click'");
    target.btnRegister = finder.castView(view, 2131296328, "field 'btnRegister'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click();
        }
      });
    view = finder.findRequiredView(source, 2131296261, "field 'mySpinner'");
    target.mySpinner = finder.castView(view, 2131296261, "field 'mySpinner'");
    view = finder.findRequiredView(source, 2131296260, "field 'ciudad'");
    target.ciudad = finder.castView(view, 2131296260, "field 'ciudad'");
  }

  @Override public void reset(T target) {
    target.edtName = null;
    target.edtLastname = null;
    target.edtPhone = null;
    target.edtEmail = null;
    target.edtCode = null;
    target.edtPassword = null;
    target.btnRegister = null;
    target.mySpinner = null;
    target.ciudad = null;
  }
}
