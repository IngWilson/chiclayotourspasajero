// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class DLinkConductorActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.DLinkConductorActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296689, "field 'txtcodunidad'");
    target.txtcodunidad = finder.castView(view, 2131296689, "field 'txtcodunidad'");
    view = finder.findRequiredView(source, 2131296695, "field 'txtnombre'");
    target.txtnombre = finder.castView(view, 2131296695, "field 'txtnombre'");
    view = finder.findRequiredView(source, 2131296688, "field 'txtcelular'");
    target.txtcelular = finder.castView(view, 2131296688, "field 'txtcelular'");
    view = finder.findRequiredView(source, 2131296693, "field 'txtmarca'");
    target.txtmarca = finder.castView(view, 2131296693, "field 'txtmarca'");
    view = finder.findRequiredView(source, 2131296696, "field 'txtplaca'");
    target.txtplaca = finder.castView(view, 2131296696, "field 'txtplaca'");
    view = finder.findRequiredView(source, 2131296690, "field 'txtdesinfeccion'");
    target.txtdesinfeccion = finder.castView(view, 2131296690, "field 'txtdesinfeccion'");
    view = finder.findRequiredView(source, 2131296453, "field 'imgTaxiDriver'");
    target.imgTaxiDriver = finder.castView(view, 2131296453, "field 'imgTaxiDriver'");
    view = finder.findRequiredView(source, 2131296313, "field 'btnCallTaxiDriver'");
    target.btnCallTaxiDriver = finder.castView(view, 2131296313, "field 'btnCallTaxiDriver'");
    view = finder.findRequiredView(source, 2131296516, "field 'miprogress'");
    target.miprogress = finder.castView(view, 2131296516, "field 'miprogress'");
  }

  @Override public void reset(T target) {
    target.txtcodunidad = null;
    target.txtnombre = null;
    target.txtcelular = null;
    target.txtmarca = null;
    target.txtplaca = null;
    target.txtdesinfeccion = null;
    target.imgTaxiDriver = null;
    target.btnCallTaxiDriver = null;
    target.miprogress = null;
  }
}
