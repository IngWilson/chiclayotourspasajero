// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.Promotion;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class PromotionActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.Promotion.PromotionActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296664, "field 'txtIdPromotion'");
    target.txtIdPromotion = finder.castView(view, 2131296664, "field 'txtIdPromotion'");
    view = finder.findRequiredView(source, 2131296685, "field 'txtTotalCarreras'");
    target.txtTotalCarreras = finder.castView(view, 2131296685, "field 'txtTotalCarreras'");
    view = finder.findRequiredView(source, 2131296681, "field 'txtSituacion'");
    target.txtSituacion = finder.castView(view, 2131296681, "field 'txtSituacion'");
    view = finder.findRequiredView(source, 2131296662, "field 'txtEstado'");
    target.txtEstado = finder.castView(view, 2131296662, "field 'txtEstado'");
    view = finder.findRequiredView(source, 2131296395, "field 'edtCodigo'");
    target.edtCodigo = finder.castView(view, 2131296395, "field 'edtCodigo'");
    view = finder.findRequiredView(source, 2131296652, "field 'txtCIdPromotion'");
    target.txtCIdPromotion = finder.castView(view, 2131296652, "field 'txtCIdPromotion'");
    view = finder.findRequiredView(source, 2131296654, "field 'txtCTotalCarreras'");
    target.txtCTotalCarreras = finder.castView(view, 2131296654, "field 'txtCTotalCarreras'");
    view = finder.findRequiredView(source, 2131296653, "field 'txtCSituacion'");
    target.txtCSituacion = finder.castView(view, 2131296653, "field 'txtCSituacion'");
    view = finder.findRequiredView(source, 2131296651, "field 'txtCEstado'");
    target.txtCEstado = finder.castView(view, 2131296651, "field 'txtCEstado'");
    view = finder.findRequiredView(source, 2131296394, "field 'edtCCodigo'");
    target.edtCCodigo = finder.castView(view, 2131296394, "field 'edtCCodigo'");
    view = finder.findRequiredView(source, 2131296329, "method 'showDialogConfirmCode'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.showDialogConfirmCode();
        }
      });
  }

  @Override public void reset(T target) {
    target.txtIdPromotion = null;
    target.txtTotalCarreras = null;
    target.txtSituacion = null;
    target.txtEstado = null;
    target.edtCodigo = null;
    target.txtCIdPromotion = null;
    target.txtCTotalCarreras = null;
    target.txtCSituacion = null;
    target.txtCEstado = null;
    target.edtCCodigo = null;
  }
}
