// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.VerifyPhone;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class VerifyPhoneActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.VerifyPhone.VerifyPhoneActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296412, "field 'edtPhone'");
    target.edtPhone = finder.castView(view, 2131296412, "field 'edtPhone'");
    view = finder.findRequiredView(source, 2131296331, "field 'btnSendPhone' and method 'showDialogConfirmPhone'");
    target.btnSendPhone = finder.castView(view, 2131296331, "field 'btnSendPhone'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.showDialogConfirmPhone();
        }
      });
  }

  @Override public void reset(T target) {
    target.edtPhone = null;
    target.btnSendPhone = null;
  }
}
