// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.Star;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class starActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.Star.starActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296396, "field 'edtComment'");
    target.edtComment = finder.castView(view, 2131296396, "field 'edtComment'");
    view = finder.findRequiredView(source, 2131296311, "method 'showDialogConfirmCode'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.showDialogConfirmCode();
        }
      });
  }

  @Override public void reset(T target) {
    target.edtComment = null;
  }
}
