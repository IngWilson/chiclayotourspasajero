// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.Account;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class AccountActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.Account.AccountActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296457, "field 'imgstate'");
    target.imgstate = finder.castView(view, 2131296457, "field 'imgstate'");
    view = finder.findRequiredView(source, 2131296663, "field 'txtFirstNameAccount'");
    target.txtFirstNameAccount = finder.castView(view, 2131296663, "field 'txtFirstNameAccount'");
    view = finder.findRequiredView(source, 2131296658, "field 'txtEmailAccount'");
    target.txtEmailAccount = finder.castView(view, 2131296658, "field 'txtEmailAccount'");
    view = finder.findRequiredView(source, 2131296676, "field 'txtPhoneNumberAccount'");
    target.txtPhoneNumberAccount = finder.castView(view, 2131296676, "field 'txtPhoneNumberAccount'");
    view = finder.findRequiredView(source, 2131296661, "field 'txtEnabled'");
    target.txtEnabled = finder.castView(view, 2131296661, "field 'txtEnabled'");
    view = finder.findRequiredView(source, 2131296672, "field 'txtName'");
    target.txtName = finder.castView(view, 2131296672, "field 'txtName'");
    view = finder.findRequiredView(source, 2131296657, "field 'txtEmail'");
    target.txtEmail = finder.castView(view, 2131296657, "field 'txtEmail'");
    view = finder.findRequiredView(source, 2131296675, "field 'txtPhone'");
    target.txtPhone = finder.castView(view, 2131296675, "field 'txtPhone'");
    view = finder.findRequiredView(source, 2131296692, "field 'txtidpromotion'");
    target.txtidpromotion = finder.castView(view, 2131296692, "field 'txtidpromotion'");
  }

  @Override public void reset(T target) {
    target.imgstate = null;
    target.txtFirstNameAccount = null;
    target.txtEmailAccount = null;
    target.txtPhoneNumberAccount = null;
    target.txtEnabled = null;
    target.txtName = null;
    target.txtEmail = null;
    target.txtPhone = null;
    target.txtidpromotion = null;
  }
}
