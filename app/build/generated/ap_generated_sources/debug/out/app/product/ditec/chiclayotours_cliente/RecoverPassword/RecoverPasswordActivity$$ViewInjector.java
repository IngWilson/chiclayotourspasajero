// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.RecoverPassword;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class RecoverPasswordActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.RecoverPassword.RecoverPasswordActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296405, "field 'edtPassword'");
    target.edtPassword = finder.castView(view, 2131296405, "field 'edtPassword'");
    view = finder.findRequiredView(source, 2131296330, "field 'btnSendPassword' and method 'showDialogConfirmEmail'");
    target.btnSendPassword = finder.castView(view, 2131296330, "field 'btnSendPassword'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.showDialogConfirmEmail();
        }
      });
  }

  @Override public void reset(T target) {
    target.edtPassword = null;
    target.btnSendPassword = null;
  }
}
