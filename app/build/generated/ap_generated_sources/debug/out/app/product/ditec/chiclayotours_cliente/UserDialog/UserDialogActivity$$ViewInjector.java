// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.UserDialog;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class UserDialogActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.UserDialog.UserDialogActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296674, "field 'txtName'");
    target.txtName = finder.castView(view, 2131296674, "field 'txtName'");
    view = finder.findRequiredView(source, 2131296678, "field 'txtPhone'");
    target.txtPhone = finder.castView(view, 2131296678, "field 'txtPhone'");
    view = finder.findRequiredView(source, 2131296660, "field 'txtEmaildriver'");
    target.txtEmaildriver = finder.castView(view, 2131296660, "field 'txtEmaildriver'");
    view = finder.findRequiredView(source, 2131296650, "field 'txtAutoPlate'");
    target.txtAutoPlate = finder.castView(view, 2131296650, "field 'txtAutoPlate'");
    view = finder.findRequiredView(source, 2131296453, "field 'imgTaxiDriver'");
    target.imgTaxiDriver = finder.castView(view, 2131296453, "field 'imgTaxiDriver'");
    view = finder.findRequiredView(source, 2131296493, "field 'loadingPanel'");
    target.loadingPanel = finder.castView(view, 2131296493, "field 'loadingPanel'");
    view = finder.findRequiredView(source, 2131296309, "method 'onAccept'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onAccept();
        }
      });
  }

  @Override public void reset(T target) {
    target.txtName = null;
    target.txtPhone = null;
    target.txtEmaildriver = null;
    target.txtAutoPlate = null;
    target.imgTaxiDriver = null;
    target.loadingPanel = null;
  }
}
