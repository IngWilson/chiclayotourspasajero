// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.Map;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class MapActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.Map.MapActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296317, "field 'btnCancelTaxi'");
    target.btnCancelTaxi = finder.castView(view, 2131296317, "field 'btnCancelTaxi'");
    view = finder.findRequiredView(source, 2131296325, "field 'btnPedido' and method 'callcentral'");
    target.btnPedido = finder.castView(view, 2131296325, "field 'btnPedido'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.callcentral();
        }
      });
    view = finder.findRequiredView(source, 2131296313, "field 'btnCallTaxiDriver' and method 'callTaxiDriver'");
    target.btnCallTaxiDriver = finder.castView(view, 2131296313, "field 'btnCallTaxiDriver'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.callTaxiDriver();
        }
      });
    view = finder.findRequiredView(source, 2131296679, "field 'txtprecioacordado'");
    target.txtprecioacordado = finder.castView(view, 2131296679, "field 'txtprecioacordado'");
    view = finder.findRequiredView(source, 2131296684, "field 'txtTitle'");
    target.txtTitle = finder.castView(view, 2131296684, "field 'txtTitle'");
    view = finder.findRequiredView(source, 2131296673, "field 'txtNameTaxiDriver'");
    target.txtNameTaxiDriver = finder.castView(view, 2131296673, "field 'txtNameTaxiDriver'");
    view = finder.findRequiredView(source, 2131296659, "field 'txtEmailTaxiDriver'");
    target.txtEmailTaxiDriver = finder.castView(view, 2131296659, "field 'txtEmailTaxiDriver'");
    view = finder.findRequiredView(source, 2131296649, "field 'txtAutoPlateTaxiDriver'");
    target.txtAutoPlateTaxiDriver = finder.castView(view, 2131296649, "field 'txtAutoPlateTaxiDriver'");
    view = finder.findRequiredView(source, 2131296677, "field 'txtPhoneTaxiDriver'");
    target.txtPhoneTaxiDriver = finder.castView(view, 2131296677, "field 'txtPhoneTaxiDriver'");
    view = finder.findRequiredView(source, 2131296671, "field 'txtModelAndBrandTaxiDriver'");
    target.txtModelAndBrandTaxiDriver = finder.castView(view, 2131296671, "field 'txtModelAndBrandTaxiDriver'");
    view = finder.findRequiredView(source, 2131296655, "field 'txtCompanyTaxiDriver'");
    target.txtCompanyTaxiDriver = finder.castView(view, 2131296655, "field 'txtCompanyTaxiDriver'");
    view = finder.findRequiredView(source, 2131296454, "field 'imgTaxiDriver1'");
    target.imgTaxiDriver1 = finder.castView(view, 2131296454, "field 'imgTaxiDriver1'");
    view = finder.findRequiredView(source, 2131296455, "field 'imgTaxiDriver2'");
    target.imgTaxiDriver2 = finder.castView(view, 2131296455, "field 'imgTaxiDriver2'");
    view = finder.findRequiredView(source, 2131296452, "field 'imgMarker'");
    target.imgMarker = finder.castView(view, 2131296452, "field 'imgMarker'");
    view = finder.findRequiredView(source, 2131296656, "field 'txtDistance'");
    target.txtDistance = finder.castView(view, 2131296656, "field 'txtDistance'");
    view = finder.findRequiredView(source, 2131296683, "field 'txtTime'");
    target.txtTime = finder.castView(view, 2131296683, "field 'txtTime'");
    view = finder.findRequiredView(source, 2131296476, "field 'linearLayoutParameters'");
    target.linearLayoutParameters = finder.castView(view, 2131296476, "field 'linearLayoutParameters'");
    view = finder.findRequiredView(source, 2131296485, "field 'llConnection'");
    target.llConnection = finder.castView(view, 2131296485, "field 'llConnection'");
    view = finder.findRequiredView(source, 2131296487, "field 'llbasicoption'");
    target.llbasicoption = finder.castView(view, 2131296487, "field 'llbasicoption'");
    view = finder.findRequiredView(source, 2131296489, "field 'llofertaroptions'");
    target.llofertaroptions = finder.castView(view, 2131296489, "field 'llofertaroptions'");
    view = finder.findRequiredView(source, 2131296333, "field 'btnsendrequestpeticion'");
    target.btnsendrequestpeticion = finder.castView(view, 2131296333, "field 'btnsendrequestpeticion'");
    view = finder.findRequiredView(source, 2131296488, "field 'lldestino'");
    target.lldestino = finder.castView(view, 2131296488, "field 'lldestino'");
    view = finder.findRequiredView(source, 2131296490, "field 'llprecio'");
    target.llprecio = finder.castView(view, 2131296490, "field 'llprecio'");
    view = finder.findRequiredView(source, 2131296682, "field 'txtStateConnection'");
    target.txtStateConnection = finder.castView(view, 2131296682, "field 'txtStateConnection'");
    view = finder.findRequiredView(source, 2131296324, "field 'btnOptions' and method 'callOptions'");
    target.btnOptions = finder.castView(view, 2131296324, "field 'btnOptions'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.callOptions();
        }
      });
    view = finder.findRequiredView(source, 2131296337, "method 'preofertar'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.preofertar();
        }
      });
  }

  @Override public void reset(T target) {
    target.btnCancelTaxi = null;
    target.btnPedido = null;
    target.btnCallTaxiDriver = null;
    target.txtprecioacordado = null;
    target.txtTitle = null;
    target.txtNameTaxiDriver = null;
    target.txtEmailTaxiDriver = null;
    target.txtAutoPlateTaxiDriver = null;
    target.txtPhoneTaxiDriver = null;
    target.txtModelAndBrandTaxiDriver = null;
    target.txtCompanyTaxiDriver = null;
    target.imgTaxiDriver1 = null;
    target.imgTaxiDriver2 = null;
    target.imgMarker = null;
    target.txtDistance = null;
    target.txtTime = null;
    target.linearLayoutParameters = null;
    target.llConnection = null;
    target.llbasicoption = null;
    target.llofertaroptions = null;
    target.btnsendrequestpeticion = null;
    target.lldestino = null;
    target.llprecio = null;
    target.txtStateConnection = null;
    target.btnOptions = null;
  }
}
