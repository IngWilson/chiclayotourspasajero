// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.RadarDialog;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class RadarDialogActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.RadarDialog.RadarDialogActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296694, "field 'txtmessage'");
    target.txtmessage = finder.castView(view, 2131296694, "field 'txtmessage'");
    view = finder.findRequiredView(source, 2131296316, "field 'btnCancelRequest' and method 'cancelRequest'");
    target.btnCancelRequest = finder.castView(view, 2131296316, "field 'btnCancelRequest'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.cancelRequest();
        }
      });
  }

  @Override public void reset(T target) {
    target.txtmessage = null;
    target.btnCancelRequest = null;
  }
}
