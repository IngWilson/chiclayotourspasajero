// Generated code from Butter Knife. Do not modify!
package app.product.ditec.chiclayotours_cliente.Login;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class LoginActivity$$ViewInjector<T extends app.product.ditec.chiclayotours_cliente.Login.LoginActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296399, "field 'edtEmail'");
    target.edtEmail = finder.castView(view, 2131296399, "field 'edtEmail'");
    view = finder.findRequiredView(source, 2131296417, "field 'edtphone'");
    target.edtphone = finder.castView(view, 2131296417, "field 'edtphone'");
    view = finder.findRequiredView(source, 2131296410, "field 'edtPassword'");
    target.edtPassword = finder.castView(view, 2131296410, "field 'edtPassword'");
    view = finder.findRequiredView(source, 2131296322, "field 'btnLogin' and method 'onClick'");
    target.btnLogin = finder.castView(view, 2131296322, "field 'btnLogin'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClick();
        }
      });
    view = finder.findRequiredView(source, 2131296323, "field 'btnLoginphone' and method 'onClickphone'");
    target.btnLoginphone = finder.castView(view, 2131296323, "field 'btnLoginphone'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickphone();
        }
      });
    view = finder.findRequiredView(source, 2131296328, "field 'btnRegister' and method 'navigateToRegister'");
    target.btnRegister = finder.castView(view, 2131296328, "field 'btnRegister'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.navigateToRegister();
        }
      });
    view = finder.findRequiredView(source, 2131296680, "method 'navigateToRecoverPassword'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.navigateToRecoverPassword();
        }
      });
  }

  @Override public void reset(T target) {
    target.edtEmail = null;
    target.edtphone = null;
    target.edtPassword = null;
    target.btnLogin = null;
    target.btnLoginphone = null;
    target.btnRegister = null;
  }
}
